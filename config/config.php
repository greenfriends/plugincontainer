<?php

use PluginContainer\Packages\AssetsCompiler\Controller\AssetsCompiler;
use PluginContainer\Packages\BankReportParser\Controller\BankReportParser;
use PluginContainer\Packages\BannerSlider\Controller\BannerSlider;
use PluginContainer\Packages\ChangeCurrencySymbol\Controller\ChangeCurrencySymbol;
use PluginContainer\Packages\Compare\Controller\Compare;
use PluginContainer\Packages\DynamicCopyright\Controller\DynamicCopyright;
use PluginContainer\Packages\GfTranslationManager\Controller\GfTranslationManager;
use PluginContainer\Packages\MegaMenu\Controller\MegaMenu;
use PluginContainer\Packages\ProductSlider\Controller\ProductSlider;
use PluginContainer\Packages\Redirects\Controller\Redirects;
use PluginContainer\Packages\RoleManager\Controller\RoleManager;
use PluginContainer\Packages\ShortenProductUrl\Controller\ShortenProductUrl;
use PluginContainer\Packages\Vendor\Controller\Vendor;
use PluginContainer\Packages\WcCustomOrderStatus\Controller\WcCustomOrderStatus;
use PluginContainer\Packages\WcOrderStatusFlow\Controller\WcOrderStatusFlow;
use PluginContainer\Packages\WcUtilityPack\Controller\WcUtilityPack;
use PluginContainer\Packages\Wishlist\Controller\Wishlist;
use PluginContainer\Packages\FeedImport\Controller\FeedImport;
use PluginContainer\Packages\Indexer\Controller\IndexerController;
use PluginContainer\Packages\FeedPriceMargin\Controller\FeedPriceMargin;

return [
    'featureList' => [
        'Change serbian currency symbol' => ['handlerClass' => ChangeCurrencySymbol::class, 'optionsPage' => false, 'configPage' => false],
        'Vendor' => ['handlerClass' => Vendor::class, 'optionsPage' => true, 'configPage' => false],
        'ProductSlider' =>  ['handlerClass' => ProductSlider::class, 'optionsPage' => true, 'configPage' => false],
        'DynamicCopyright' => ['handlerClass' => DynamicCopyright::class, 'optionsPage' => false, 'configPage' => false],
        'Wishlist' => ['handlerClass' => Wishlist::class, 'optionsPage' => false, 'configPage' => false],
        'Bank report parser' => ['handlerClass' => BankReportParser::class, 'optionsPage' => true, 'configPage' => false],
        'Wc custom order statuses' => ['handlerClass' => WcCustomOrderStatus::class, 'optionsPage' => true, 'configPage' => false],
        'Wc order status flow' => ['handlerClass' => WcOrderStatusFlow::class, 'optionsPage' => true, 'configPage' => false],
        'Redirects' => ['handlerClass' => Redirects::class, 'optionsPage' => true, 'configPage' => false],
        'Feed import' => ['handlerClass' => FeedImport::class, 'optionsPage' => true, 'configPage' => false],
        'Mega Menu' => ['handlerClass' => MegaMenu::class, 'optionsPage' => true, 'configPage' => false],
        'Compare' => ['handlerClass' => Compare::class, 'optionsPage' => false, 'configPage' => false],
        'Shorten product urls' => ['handlerClass' => ShortenProductUrl::class, 'optionsPage' => false, 'configPage' => false],
        'Wc utility pack' => ['handlerClass' => WcUtilityPack::class, 'optionsPage' => false, 'configPage' => false],
        'Elastic Search integration' => ['handlerClass' => IndexerController::class, 'optionsPage' => false, 'configPage' => false],
        'Feed Margin Price' => ['handlerClass' => FeedPriceMargin::class, 'optionsPage' => true, 'configPage' => false],
        'Translation Manager' => ['handlerClass' => GfTranslationManager::class, 'optionsPage' => true, 'configPage' => false],
        'Assets Compiler' => ['handlerClass' => AssetsCompiler::class, 'optionsPage' => false, 'configPage' => false],
        'Role Manager' => ['handlerClass' => RoleManager::class, 'optionsPage' => true, 'configPage' => false],
        'BannerSlider' => ['handlerClass' => BannerSlider::class, 'optionsPage' => true, 'configPage' => false]
    ],
    'translationDomain' => 'plugin-container'
];
