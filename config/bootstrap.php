<?php
/** @noinspection MkdirRaceConditionInspection */

use DI\ContainerBuilder;
use PluginContainer\Core\WpBridge\WpEnqueue;
use Laminas\Config\Config;
use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use PluginContainer\Core\Cache\CacheConnection;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Dashboard\Controller\Feature;
use PluginContainer\Core\Dashboard\MenuPage\MenuPage;
use PluginContainer\Core\Logger\Logger;

$containerBuilder = new ContainerBuilder();
$gfContainer = $containerBuilder->build();
$gfContainer->set(Logger::class, static function () {
    // the default date format is "Y-m-d\TH:i:sP"
    $dateFormat = "Y-m-d H:i:s";
    $output = "%datetime% > %level_name% > %message% %context% %extra%\n";
    $formatter = new LineFormatter($output, $dateFormat);
    $logger = new Logger('plugincontaierlog');
    $logger->setTimezone(new \DateTimeZone('Europe/Belgrade'));
    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logDir = CONTAINER_PLUGIN_DIR . '/data/logs/' . $date->format('Y') . '-' . $date->format('m');
    $debuglogFile = $logDir . '/' . $date->format('d') . '-debug.log';
    $errorLogFile = $logDir . '/' . $date->format('d') . '-error.log';
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!file_exists($debuglogFile)) {
        touch($debuglogFile);
    }
    if (!file_exists($errorLogFile)) {
        touch($errorLogFile);
    }
    $streamError = new StreamHandler($errorLogFile, Logger::ERROR);
    $streamDebug = new StreamHandler($debuglogFile, Logger::DEBUG);
    $streamError->setFormatter($formatter);
    $streamDebug->setFormatter($formatter);
    $logger->pushHandler($streamError);
    $logger->pushHandler($streamDebug);
    if (ENVIRONMENT !== 'production') {
        $logger->pushHandler(new BrowserConsoleHandler());
    }
    ErrorHandler::register($logger);
    return $logger;
});
$gfContainer->set(Config::class, static function () {
    $params = include(CONTAINER_PLUGIN_DIR . "/config/config.php");
    $config = new Config($params);
    if (is_file(CONTAINER_PLUGIN_DIR . "/config/config-local.php")) {
        $params = include(CONTAINER_PLUGIN_DIR . "/config/config-local.php");
        $config->merge(new Config($params));
    }
    return $config;
});
$gfContainer->set(PDO::class, static function () use ($gfContainer) {
    $dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
    $pdo = new PDO($dsn, DB_USER, DB_PASSWORD,[PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    return $pdo;
});
$gfContainer->set(CacheWrapper::class, static function (){
    return new CacheWrapper(new CacheConnection());
});

add_action('wp_enqueue_scripts',function() {
    wp_enqueue_script( 'jquery-ui-slider');
});
WpEnqueue::addFrontendStyle('gfStyle',CONTAINER_PLUGIN_DIR_URI . '/assets/front/css/style.min.css',[],'0.0.1');
WpEnqueue::addFrontendStyle('swiperCss',CONTAINER_PLUGIN_DIR_URI . '/assets/vendor/swiper/css/swiper.min.css',[],'0.0.1');
WpEnqueue::addFrontendScript('swiperJs', CONTAINER_PLUGIN_DIR_URI . '/assets/vendor/swiper/js/swiper.js', [], '0.0.1', true);
WpEnqueue::addFrontendScript('gfJs', CONTAINER_PLUGIN_DIR_URI . 'assets/front/js/main.js', [], '0.0.1', true);
WpEnqueue::addGlobalAdminStyle('jqueri-ui-css','//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
add_action('admin_enqueue_scripts',function(){
    wp_enqueue_script('jquery-ui-widget');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('jquery-ui-accordion');
});
WpEnqueue::addFrontendStyle('jqueri-ui-css','//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');

//Convert script type to module
add_filter('script_loader_tag', function ($tag, $handle, $src) {
    if ('gfJs' === $handle) {
        $tag = sprintf('<script type="module" id="%s" src="%s"></script>',$handle, esc_url($src));
    }
    return $tag;
}, 99, 3);
return $gfContainer;