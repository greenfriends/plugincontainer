<?php /** @noinspection PhpDefineCanBeReplacedWithConstInspection */
/*
Plugin Name: GF Plugins
Plugin URI:
Description: Green friends plugins
Author: Green Friends
Author URI: https://greenfriends.systems/
Version: 2.0.0
text Domain: plugin-container
*/

use PluginContainer\Core\Activator\Activator;
use PluginContainer\Core\AjaxRouter\AjaxRouter;
use PluginContainer\Core\Dashboard\Controller\Feature;
use PluginContainer\Core\Dashboard\MenuPage\MenuPage;

define('CONTAINER_PLUGIN_DIR_URI', plugin_dir_url(__FILE__));
define('CONTAINER_PLUGIN_DIR', __DIR__);
ini_set('display_errors', 1);
ini_set('error_reporting', true);
require __DIR__ . '/../../vendor/autoload.php';
global $gfContainer;
/** @var \DI\Container $gfContainer */
$gfContainer =  require 'config/bootstrap.php';
$activator = $gfContainer->get(Activator::class);
register_activation_hook(__FILE__, [$activator, 'activate']);
if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}
$filePath = explode('/', __FILE__);
$lastKey = array_key_last($filePath);
if (is_plugin_active($filePath[$lastKey-1].'/'.$filePath[$lastKey])) {
    //Activate options page handlers
    $gfContainer->call(Feature::class);
//Activate main menu page
    $menuPage = $gfContainer->get(MenuPage::class);
    $menuPage->setupMainPage();

    $ajaxRouter = $gfContainer->get(AjaxRouter::class);
}