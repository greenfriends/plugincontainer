<?php


namespace PluginContainer\Packages\DynamicCopyright\Controller;


use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;

class DynamicCopyright extends BaseController implements ControllerWithHooksInterface
{

    public function init() :void
    {
        add_shortcode('dynamic_copyright', [$this,'dynamicCopyrightView']);
    }

    /**
     * @return string
     */
    public function dynamicCopyrightView(): string
    {
        return '©' . get_bloginfo('name') . ' ' .  date("Y") . '. Sva prava zadržana.';
    }

}