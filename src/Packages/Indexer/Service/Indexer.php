<?php

namespace PluginContainer\Packages\Indexer\Service;

use \Elastica\Index;


class Indexer
{
    /**
     * @var Index
     */
    private $elasticaIndex;

    public function __construct(Index $elasticaIndex)
    {
        $this->elasticaIndex = $elasticaIndex;
    }

    public function deleteProduct(\WC_Product $item)
    {
        try {
            $response = $this->elasticaIndex->deleteById($item->get_id());
            if (!$response->isOk() || $response->hasError()) {
//                throw new \Exception($response->getError());
            }
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
//            throw $e;
        }
        return true;
    }

    public function indexPost(\WC_Product $item)
    {
        try {
            $response = $this->elasticaIndex->addDocument($this->parseWcProduct($item));
            if (!$response->isOk() || $response->hasError()) {
                var_dump($response->getError());
                die();
            }
            unset($response);
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return true;
    }

//    public function indexImage(\WP_Post $article)
//    {
//        try {
//            $response = $this->elasticaIndex->getType(ES_INDEX_IMAGE)->addDocument($this->parseWpImage($article));
//            if (!$response->isOk() || $response->hasError()) {
//                var_dump($response->getError());
//                die();
//            }
//            unset($response);
//            $this->elasticaIndex->refresh();
//        } catch (\Exception $e) {
//            var_dump($e->getMessage());
//            die();
//        }
//        return true;
//    }

//    public function indexImages()
//    {
//        global $wpdb;
//
//        $perPage = 1000;
//        $totalItems = 0;
//        for ($i = 1; $i < 200; $i++) {
//            $args = [
//                'numberposts' => $perPage,
//                'paged' => $i,
//                'status' => 'publish',
//                'post_type' => 'attachment',
//                'orderby' => 'date',
//                'order' => 'DESC',
//            ];
//            $posts = get_posts($args);
//            $wpdb->flush();
//            if (count($posts) > 0) {
//                $documents = [];
//                foreach ($posts as $post) {
//                    if ($post->post_name === "AUTO-DRAFT") {
//                        continue;
//                    }
////                    $totalItems++;
//                    $documents[] = $this->parseWpImage($post);
//                }
//                try {
//                    $response = $this->elasticaIndex->getType(ES_INDEX_IMAGE)->addDocuments($documents);
//                    if (!$response->isOk() || $response->hasError()) {
//                        var_dump($response->getError());
//                        die();
//                    }
//                    $totalItems += count($documents);
//                    $documents = []; // reset var, clear memory
//                    $this->elasticaIndex->refresh();
//                    echo sprintf('stored %s items.', $totalItems);
//                    unset($response);
//                } catch (\Exception $e) {
//                    var_dump($e->getMessage());
//                }
//            } else {
//                break;
//            }
//        }
//        echo "sync complete. $totalItems items indexed.";
//    }

    /**
     * Batch index
     */
    public function indexProducts()
    {
        global $wpdb;

        $perPage = 1000;
        $totalItems = 0;

        for ($i = 0; $i < 60; $i++) {
            $offset = $i * $perPage;

            $sql = "SELECT ID FROM wp_posts WHERE post_type = 'product' 
            AND post_status = 'publish'
LIMIT {$offset}, {$perPage};";
//            $sql = "SELECT ID FROM wp_posts WHERE post_type = 'product'
// AND ID IN (397944,419590,401317,391140,427106,413564,426681,405142)
// LIMIT {$offset}, {$perPage};";
            $products = $wpdb->get_results($sql);
            $wpdb->flush();
            if (count($products) > 0) {
                $documents = [];
                foreach ($products as $value) {
                    $product = wc_get_product($value->ID);
                    if (!$product) {
                        throw new \Exception('Product not found: ' . $value->ID);
                    }
                    if ($product->get_name() === "AUTO-DRAFT") {
                        continue;
                    }
                    if ($product->get_status() !== 'publish') {
                        //@TODO removing of products....
                        $this->elasticaIndex->deleteDocuments([$this->parseWcProduct($product)]);
                    } else {
                        $documents[] = $this->parseWcProduct($product);
                    }
                }
                try {
                    $response = $this->elasticaIndex->addDocuments($documents);
                    if (!$response->isOk() || $response->hasError()) {
                        var_dump($response->getError());
                        die();
                    }
                    $totalItems += count($documents);
                    $documents = [];
                    echo sprintf('stored %s items.', $response->count());
                    unset($response);
                    $this->elasticaIndex->refresh();
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            } else {
                break;
            }
        }
        echo "sync complete. $totalItems items indexed.";
    }

    public function indexProduct($product)
    {

        if (!$product instanceof \WC_Product) {
            throw new \Exception('Product not found');
        }
        if ($product->get_name() === "AUTO-DRAFT") {
            return;
        }
        $documents = [];
        if ($product->get_status() !== 'publish') {
            $this->elasticaIndex->deleteDocuments([$this->parseWcProduct($product)]);
        } else {
            $documents[] = $this->parseWcProduct($product);
        }
        try {
            if(count($documents) === 0) {
                return;
            }
            $response = $this->elasticaIndex->addDocuments($documents);
            if (!$response->isOk() || $response->hasError()) {
                var_dump($response->getError());
                die();
            }
            unset($response);
            $this->elasticaIndex->refresh();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
    }

    private function parseWcProduct(\WC_Product $product)
    {
        global $wpdb;

        $cats = [];
        $lvl1Cat = null;
        $lvl2Cat = null;
        $lvl3Cat = null;
        $homeUrl = get_home_url();
        foreach ($product->get_category_ids() as $category_id) {
            $cat = get_term_by('id', $category_id, 'product_cat');
            if ($cat->parent === 0){
                $lvl1Cat = $cat->term_id;
                $cat_lvl = 1;
            } else {
                if (get_term($cat->parent, 'product_cat')->parent === 0){
                    $lvl2Cat = $cat->term_id;
                    $cat_lvl = 2;
                } else{
                    $lvl3Cat = $cat->term_id;
                    $cat_lvl = 3;
                }
            }
            $cats[] = [
                'id'    => $cat->term_id,
                'name'  => $cat->name,
                'slug'  => $cat->slug,
                'parent'=> $cat->parent,
                'level' => $cat_lvl,
                'permalink' => get_term_link($cat->term_id, 'product_cat')
            ];
        }
        $attributes = [];
        $salePrice = $product->get_sale_price();
        $price = $regularPrice = $product->get_regular_price();
        if (get_class($product) === \WC_Product_Variable::class) {
            $regularPrice = $product->get_variation_regular_price();
            $salePrice = $product->get_variation_sale_price();
            foreach ($product->get_available_variations() as $variation) {
                foreach ($variation['attributes'] as $attribute => $value) {
                    $attributes[] = [
                        'name' => ltrim($attribute, 'attribute_pa'),
                        'value' => $value
                    ];
                }
            }
        }
        if ($product->get_price() !== 0 && $product->get_price() !== $regularPrice) {
            $salePrice = $price = $product->get_price();
        }

        // @TODO solve better. when no sku detected, use post id.
        if ($product->get_sku() == "") {
//            throw new \Exception('no sku for :' . $product->get_id());
        }

        /* @var \WC_Product_Attribute $attribute */
        foreach ($product->get_attributes() as $name => $attribute) {
            // TODO check multiple ?
            $attributes[] = [
                'name' => str_replace('pa_', '', $attribute->get_name()),
                'value' => $attribute->get_options()[0],
            ];
        }

        $thumbnails[0] = [
            'src' => wc_placeholder_img_src(),
            'name' => 'default'
        ];
        if (has_post_thumbnail($product->get_id())) {
            foreach (get_intermediate_image_sizes() as $thumbName) {
                $thumbnails[] = [
                    'src' => wp_get_attachment_image_src($product->get_image_id(), $thumbName)[0] ?? '',
                    'name' => $thumbName
                ];
            }
        }

        $product_link = get_permalink((int) $product->get_id());
        $rating = $product->get_meta('rating'); // check
//        $ordering = $this->calculateOrderingRating($product);

        $salePriceStart = get_post_meta($product->get_id(), '_sale_price_dates_from', true);
        $salePriceStart = ($salePriceStart === '') ? null : $salePriceStart;
        $salePriceEnd = get_post_meta($product->get_id(), '_sale_price_dates_to', true);
        $salePriceEnd = ($salePriceEnd === '') ? null : $salePriceEnd;

        $data = [
            'postId' => $product->get_id(),
            'category' => $cats,
            'attributes' => $attributes,
//            'lvl1Cat' => $lvl1Cat,
//            'lvl2Cat' => $lvl2Cat,
//            'lvl3Cat' => $lvl3Cat,
//            'attributes' => $attributes,
            'name' => $product->get_name(),
            'manufacturer' => $product->get_meta('pa_proizvodjac'),
            'createdAt' => strtotime($product->get_date_created()),
            'supplierId' => $product->get_meta('supplier'),
            'supplierSku' => $product->get_meta('vendor_code'),
            'description' => $product->get_description(),
            'thumbnails' => $thumbnails,
            'permalink' => $product_link,
            'shortDescription' => $product->get_short_description(),
            'regularPrice' => $regularPrice,
            'salePrice' => $salePrice,
            'salePriceStart' => $salePriceStart,
            'salePriceEnd' => $salePriceEnd,
            'inputPrice' => $product->get_meta('input_price'),
            'stockStatus' => (int) $product->is_in_stock(),
            'status' => (int) ($product->get_status() === 'publish'),
            'viewCount' => 0,
            'rating' => $rating,
            'sku' => $product->get_sku(),
            'type' => $product->get_type(),
            'featured' => (int) $product->is_featured(),
            'full_text' => static::extractFullTextFields($product, $attributes, $cats),
            'sort_price' => $price,
//            'order_data' => [
//                'rating' => $rating,
//                'date' => strtotime($product->get_date_created()),
//                'viewCount' => 0,
//                'stock' => (int) $product->is_in_stock(),
//                'published' => (int) ($product->get_status() === 'publish'),
//                'default' => $ordering['default'],
//            ]
        ];
        return new \Elastica\Document($product->get_id(), $data);
    }

//    private function parseWpImage(\WP_Post $post)
//    {
//        global $wpdb;
//
//        $author = get_user_by('id', $post->post_author);
//        $user = [
//            'id'    => $author->ID,
//            'name'  => $author->user_nicename
//        ];
//        $createdDt = new \DateTime($post->post_date, new \DateTimeZone('Europe/Sarajevo'));
//        $data = [
//            'postId' => $post->ID,
//            'author' => $user,
//            'title' => str_replace('-',' ', $post->post_title),
//            'createdAt' => $createdDt->format(\Datetime::ATOM),
//            'potpis' => wp_get_attachment_caption($post->ID),
//            'legenda' => get_post_meta($post->ID,'legenda',true),
//            'filename' => $post->post_name,
//            'synced' => 1,
//        ];
//
//        return new \Elastica\Document($post->ID, $data);
//    }


    private function extractFullTextBoostedFields(\WC_Product $product, $attributes, $cats)
    {
        $text = $product->get_name();
        $text .= ' ' . $product->get_meta('pa_proizvodjac');
        $text .= ' ' . $product->get_meta('vendor_code');
//        $text .= ' ' . $product->get_sku();
        $attr = '';
        if (count($attributes) > 0) {
            foreach ($attributes as $attribute) {
                $attr .= ' ' . implode(' ', $attribute) . ' ';
            }
        }
        $categories = '';
        if (count($cats) > 0) {
            foreach ($cats as $cat) {
                $categories .= ', '. $cat['name'];
            }
        }
        $text .= ' ' . $attr .' '. $categories;

        return $text;
    }

    /**
     * @param \WC_Product $product
     * @param $attributes
     * @param $cats
     * @return string
     */
    private function extractFullTextFields(\WC_Product $product, $attributes, $cats)
    {
        $text = $product->get_name();
        $text .= ' ' . $product->get_meta('pa_proizvodjac');
        $text .= ' ' . $product->get_meta('vendor_code');
        $text .= ' ' . strip_tags($product->get_description());
        $text .= ' ' . strip_tags($product->get_short_description());
        $text .= ' ' . $product->get_sku();
        $attr = '';
        if (count($attributes) > 0) {
            foreach ($attributes as $attribute) {
                $attr .= implode(' ', $attribute) . ' ';
            }
        }
        $categories = '';
        if (count($cats) > 0) {
            foreach ($cats as $cat) {
                $categories .= ', '. $cat['name'];
            }
        }
        $text .= ' ' . $attr .' '. $categories;

        return $text;
    }
}
