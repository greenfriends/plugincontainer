<?php

namespace PluginContainer\Packages\Indexer\Service;

use Elastica\Client;
use Elastica\Index\Settings;
use Elastica\Mapping;
use Elastica\Response;
use GuzzleHttp\Psr7\Request;
use PluginContainer\Packages\Indexer\Config\ConfigInterface as Config;

class Setup
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Config
     */
    private $config;

    /**
     * Setup constructor.
     * @param Client $elasticaClient
     * @param Config $config
     */
    public function __construct(Client $elasticaClient, Config $config)
    {
        $this->client = $elasticaClient;
        $this->config = $config;
    }

    /**
     * @param bool $recreate
     */
    public function createIndex($recreate = false)
    {
        if ($recreate) {
            try {
                $response = $this->client->getIndex($this->config->getIndex())->delete();
                var_dump($response->getFullError());
                echo 'index ' .$this->config->getIndex(). ' deleted';
            } catch (\Exception $e) {
                if (!strstr($e->getMessage(), 'no such index')) {
                    var_dump($e->getMessage());
                }
            }
        }
//        $this->client->getIndex($this->config->getIndex())->create();
        $this->setupAnalyzers();
        $response = $this->setupMapping();
        if (!$response->isOk()) {
            echo $response->getFullError();
            echo $response->getErrorMessage();
            die('cao');
            throw new \Exception($response->getErrorMessage());
        }
        $msg = $this->config->getIndex() . ' index created.' .$this->config->getIndex() . PHP_EOL;
        if ($recreate) {
            $msg = $this->config->getIndex() . ' index recreated.' .$this->config->getIndex() . PHP_EOL;
        }
        echo $msg;

        return $msg;
    }

    /**
     * @return Response
     */
    private function setupMapping()
    {
        $mapping = new Mapping($this->config->getMapping());

        return $mapping->send($this->client->getIndex($this->config->getIndex()));
    }

    /**
     * @return void
     */
    private function setupAnalyzers()
    {
        $httpClient = new \GuzzleHttp\Client();
        $uri = "http://localhost:9200/{$this->config->getIndex()}?format=json";
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $settings = [
            'settings' => [
                'analysis' => [
                    'analyzer' => [
                        'default' => [
                            'type' => 'custom',
                            'tokenizer' => 'standard',
                            'filter' => [
                                'lowercase',
                                'asciifolding',
                                'stop',
                                'trim',
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $json = json_encode($settings);
//        echo $json;
//        die();
        try {
            $response = $httpClient->send(new Request('PUT', $uri, $headers, $json));
            var_dump(json_decode($response->getBody()->getContents()));
        } catch (\Exception $e) {
            var_dump($e);
            var_dump($e->getMessage());
            die('error');
        }
    }
}