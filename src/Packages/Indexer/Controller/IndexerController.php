<?php


namespace PluginContainer\Packages\Indexer\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;

use PluginContainer\Packages\Indexer\Factory\ElasticClientFactory;
use PluginContainer\Packages\Indexer\Factory\ProductSetupFactory;
use PluginContainer\Packages\Indexer\Service\Indexer;

class IndexerController extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface
{


    private $ignoredVendors = [];

    public function __construct(Feature $featureRepo, Logger $logger)
    {
        parent::__construct($featureRepo, $logger);
//        $this->feedPage = $feedPage;
    }

    public function init(): void
    {
        add_action('syncToElastic', [$this, 'syncElasticIndex']);
        if (defined('WP_CLI') && WP_CLI) {
            ini_set('max_execution_time', 1200);
            ini_set('display_errors', 1);
            \WP_CLI::add_command('createElasticIndex', [$this, 'createElasticIndex']);
            \WP_CLI::add_command('test', [$this, 'test']);
            \WP_CLI::add_command('syncElasticIndex', [$this, 'syncElasticIndex']);
            \WP_CLI::add_command('removeNonExistentProductsFromIndex', [$this, 'removeNonExistentProductsFromIndex']);
        }
        if(ENVIRONMENT === 'production') {
            add_action('save_post_product', [$this,'syncProductToElastic'], 10, 3);
        }
    }

    public function test()
    {
        $id = 31032;
        $product = wc_get_product($id);
        var_dump(get_post_meta($product->get_id(), '_thumbnail_id', true ));
        var_dump(!(int) get_post_meta($product->get_id(), '_thumbnail_id', true ));
        if (!has_post_thumbnail($product->get_id())) {
            $product->set_status('draft');
            $product->save();
            echo 'pending';
        }
        global $wp_object_cache;

        $wp_object_cache->flush( $delay );

    }

    public function createElasticIndex() {
        $elasticaClient = new ElasticClientFactory();
        $productSetupFactory = new ProductSetupFactory($elasticaClient);
//    $termSetupFactory = new \GF\Search\Factory\TermSetupFactory($elasticaClient);
        $recreate = true;
        $productSetupFactory->make()->createIndex($recreate);
//    $termSetupFactory->make()->createIndex(false);
    }

    public function removeNonExistentProductsFromIndex() {
        $cli = new \GF\Cli();
        $cli->removeNonExistentProducts();
    }

    public function syncElasticIndex() {
        ini_set('max_execution_time', 1200);
//        $productConfig = new \GF\Search\Elastica\Config\Product();
        $productConfig = new \PluginContainer\Packages\Indexer\Config\Product();
        $elasticaClient = ElasticClientFactory::make();
        $indexer = new Indexer($elasticaClient->getIndex($productConfig->getIndex()));
        $indexer->indexProducts();
    }

    public function syncProductToElastic($postId, $post, $update)
    {
        $productConfig = new \PluginContainer\Packages\Indexer\Config\Product();
        $elasticaClient = ElasticClientFactory::make();
        $indexer = new Indexer($elasticaClient->getIndex($productConfig->getIndex()));
        $indexer->indexProduct(wc_get_product($postId));
    }

    /**
     * @throws \Exception
     */
    public function setup() :void
    {
//        $this->wishlistRepo->createTable();
//        $this->wishlistRepo->createUserIdIndex();
//        $this->feedPage->createPage();
    }

    /**
     * @throws \Exception
     */
    public function cleanup() :void
    {

    }

    public function getView(): AjaxResponse
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedImport/templates/feedImportPageHeader.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }


}
