<?php

namespace PluginContainer\Packages\Indexer\Config;

class Product implements ConfigInterface
{
    private $index = \PluginContainer\Packages\Indexer\Repository\Product::INDEX_NAME;

    private $setupConfig = [
//        'number_of_shards' => 4,
//        'number_of_replicas' => 1,
//        'analysis' => [
//            'analyzer' => array(
//                'default' => array(
//                    'type' => 'custom',
//                    'tokenizer' => 'standard',
//                    'filter' => array('lowercase', 'stop', 'trim', 'asciifolding') //custom_ascii_folding
//                ),
//                'search' => array(
//                    'type' => 'custom',
//                    'tokenizer' => 'standard',
//                    'filter' => array('standard', 'lowercase', 'trim', 'asciifolding') //@TODO install icu_folding
//                )
//            ),
//        ]
    ];

    private $mapping = [
//        'entity' => [
//            'properties' => [
                'postId' => array('type' => 'integer'),
//                'lvl1Cat' => array('type' => 'integer'),
//                'lvl2Cat' => array('type' => 'integer'),
//                'lvl3Cat' => array('type' => 'integer'),
    // @TODO nested does not work with term filter !?
//                'category' => array(
//                    'type' => 'nested',
//                    'properties' => array(
//                        'id' => array('type' => 'integer'),
//                        'parent' => array('type' => 'integer'),
//                        'level' => array('type' => 'integer'),
//                        'slug' => array('type' => 'text'),
//                        'name' => ['type' => 'text','boost' => 4]
//                    ),
//                ),
                'attributes' => array(
                    'type' => 'nested',
                    'properties' => array(
//                        'name' => array('type' => 'keyword', 'fielddata' => true),
                        'name' => array('type' => 'keyword'),
                        'value' => array('type' => 'keyword')
                    ),
                ),
                'name' => [
                    'type' => 'text', 'boost' => 5
                ],
                'manufacturer' => array('type' => 'text', 'boost' => 5),
                'createdAt' => array('type' => 'date'),
                'salePriceStart' => array('type' => 'date'),
                'salePriceEnd' => array('type' => 'date'),
                'supplierId' => array('type' => 'integer'),
                'supplierSku' => array('type' => 'text'),
                'thumbnails' => array(
                    'type' => 'nested',
                    'properties' => array(
                        'name' => array('type' => 'text'),
                        'value' => array('type' => 'text')
                    ),
                ),
                'permalink' => array('type' => 'text'),
                'description' => array('type' => 'text'),
                'shortDescription' => array('type' => 'text'),
                'regularPrice' => array('type' => 'integer'),
                'salePrice' => array('type' => 'text'),
                'status' => array('type' => 'integer'),
                'stockStatus' => array('type' => 'integer'),
                'sku' => array('type' => 'text', 'boost' => 20),
                'viewCount' => array('type' => 'integer'),
                'rating' => array('type' => 'integer'),
                'product_type' => array('type' => 'text'),
                'inputPrice' => array('type' => 'long'),
                'full_text' => array('type' => 'text'),
                'sort_price' => array('type' => 'long'),
//            ]
//        ],
//        'order_data' => [
//            'properties' => [
//                'price' => array('type' => 'integer'),
//                'rating' => array('type' => 'integer'),
//                'date' => array('type' => 'integer'),
//                'viewCount' => array('type' => 'integer'),
//                'stock' => array('type' => 'integer'),
//                'published' => array('type' => 'integer'),
//                'default' => array('type' => 'integer'),
//            ]
//        ],
//        'search_data' => [
//            'properties' => [
//                'full_text' => array('type' => 'text'),
//                'full_text_boosted' => array('type' => 'text'),
//            ]
//        ],
//        "completion_terms" => [
//            "type" => "text",
//            "analyzer" => "search"
//        ],
//            "suggestion_terms" => [
//                "type" => "text",
//                "index_analyzer" => "search",
//                "search_analyzer" => "search"
//            ]
    ];

    public function getMapping()
    {
        return $this->mapping;
    }

    public function getSetupConfig()
    {
        return $this->setupConfig;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getIndex()
    {
        return $this->index;
    }
}