<?php

namespace PluginContainer\Packages\Indexer\Mapper;

use Elastica\Query\BoolQuery;
use Elastica\Query\Term;
use Elastica\Query;
use Elastica\Query\Range;
use GuzzleHttp\Psr7\Request;

class Product extends ElasticBase
{

    public function getMapping()
    {
        $httpClient = new \GuzzleHttp\Client();
        $uri = 'http://localhost:9200/product/_mapping';
        $headers = [
            'Content-Type' => 'application/json'
        ];
        try {
            $response = $httpClient->send(new Request('GET', $uri, $headers));
            var_dump(json_decode($response->getBody()->getContents()));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        die();
    }

    public static function test()
    {
        $httpClient = new \GuzzleHttp\Client();
        $uri = 'http://localhost:9200/product/_analyze?format=json';
        $headers = [
            'Content-Type' => 'application/json'
        ];
//        $sql = "SELECT * FROM product WHERE name LIKE '%{$query}%'";
        $json = json_encode([
//            'field' => 'name',
            "analyzer" => "default",
            'text' => 'COLOSSUS Seckalica za povrće, voće i a',
//            'track_total_hits' => true,
        ]);
        try {
            $response = $httpClient->send(new Request('POST', $uri, $headers, $json));
            var_dump(json_decode($response->getBody()->getContents()));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        die();
    }

    public function searchByCategory_backup($catId, $limit = 0, $currentPage = 1, $query = null)
    {
        $httpClient = new \GuzzleHttp\Client();
        $uri = 'http://localhost:9200/_sql?format=json';
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $sql = "SELECT * FROM product WHERE full_text LIKE '%{$query}%'";
        $json = json_encode([
            'query' => $sql,
            "analyzer" => "default",
//            'track_total_hits' => true,
        ]);
        try {
            $response = $httpClient->send(new Request('POST', $uri, $headers, $json));
            var_dump(json_decode($response->getBody()->getContents()));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }


        die();
    }

    public function searchByCategory($catId, $limit = 0, $currentPage = 1, $query = null, $filter, $sort)
    {
//        $this->debug = true;
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));
        $boolQuery->addMust(new Term(['category.id' => (int) $catId]));
        if ($query) {
            $searchQuery = new BoolQuery();

            $q = new \Elastica\Query\QueryString();
            $q->setFields(['name']);
            $q->setDefaultOperator('and');
            $q->setQuery('*'. $query . '*');
            $q->setBoost(25);
            $searchQuery->addShould($q);

            $q = new \Elastica\Query\QueryString();
            $q->setFields(['full_text']);
            $q->setDefaultOperator('and');
            $q->setQuery('*'. $query . '*');
            $q->setBoost(1);
            $searchQuery->addShould($q);

            $q = new \Elastica\Query\QueryString();
            $q->setFields(['description']);
            $q->setDefaultOperator('and');
            $q->setQuery('*'. $query . '*');
            $q->setBoost(15);
            $searchQuery->addShould($q);

            $searchQuery->setMinimumShouldMatch(1);
            $boolQuery->addMust($searchQuery);
        }
        $this->setAggregations();
        $boolQuery = $this->applyFilters($filter, $boolQuery);

        $this->query($boolQuery, $limit, $currentPage, $sort);
    }

    public function getProductById(int $id): void
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));
        $boolQuery->addMust(new Term(['postId' =>  $id]));
        $this->query($boolQuery);
    }

    private function applyFilters($filters, BoolQuery $boolQuery)
    {
        foreach ($filters as $filterData) {
            $name = stripslashes($filterData['name']);
            $value = stripslashes($filterData['value']);
            if (in_array($name, ['minPrice', 'maxPrice'])) {
                $operator = 'lte';
                if ($name === 'minPrice') {
                    $operator = 'gte';
                }
                $q = new \Elastica\Query\Range('sort_price', [$operator => $value]);
            } else {
                $value = $name .' '. $value;
                $q = new \Elastica\Query\QueryString();
                $q->setFields(['full_text']);
//                $q->setDefaultOperator('or');
                $q->setQuery($value);
            }

            $boolQuery->addMust($q);
        }

        return $boolQuery;
    }

    private function setAggregations()
    {
        $minPrice = new \Elastica\Aggregation\Min('minPrice');
        $minPrice->setField('sort_price');
        $this->addAggregation($minPrice);

        $maxPrice = new \Elastica\Aggregation\Max('maxPrice');
        $maxPrice->setField('sort_price');
        $this->addAggregation($maxPrice);

        $nameAgg = new \Elastica\Aggregation\Terms('filterNames');
        $nameAgg->setField('attributes.name');
        $nameAgg->setSize(50);

        $valueAgg = new \Elastica\Aggregation\Terms('filterValues');
        $valueAgg->setField('attributes.value');
        $valueAgg->setSize(50);
        $nameAgg->addAggregation($valueAgg);

        $attrAgg = new \Elastica\Aggregation\Nested('attributes', 'attributes');
        $attrAgg->addAggregation($nameAgg);

        $this->addAggregation($attrAgg);
    }

    /**
     * Performs search.
     *
     * @param $keywords
     * @param int $limit
     * @param int $currentPage
     */
    public function search($keywords, $limit = 0, $currentPage = 1, $sort)
    {
        $boolQuery = new BoolQuery();
        $boolQuery->addMust(new Term(['status' => 1]));

        $q = new \Elastica\Query\QueryString();
        $q->setFields(['name']);
        $q->setDefaultOperator('and');
        $q->setQuery('*'. $keywords . '*');
        $q->setBoost(25);
        $boolQuery->addShould($q);

        $q = new \Elastica\Query\QueryString();
        $q->setFields(['full_text']);
        $q->setDefaultOperator('and');
        $q->setQuery('*'. $keywords . '*');
        $q->setBoost(1);
        $boolQuery->addShould($q);

        $q = new \Elastica\Query\QueryString();
        $q->setFields(['description']);
        $q->setDefaultOperator('and');
        $q->setQuery('*'. $keywords . '*');
        $q->setBoost(15);
        $boolQuery->addShould($q);

        $boolQuery->setMinimumShouldMatch(1);

//        $q = new \Elastica\Query\Match();
//        $q->setFieldQuery('body', $keywords);
//        $q->setFieldOperator('body', 'and');
//        $q->setFieldFuzziness('body', 1);
//        $q->setFieldBoost('body', 15);
//        $boolQuery->addMust($q);
//
//        $q = new \Elastica\Query\Match();
//        $q->setFieldQuery('author', $keywords);
//        $q->setFieldOperator('author', 'and');
//        $q->setFieldFuzziness('author', 1);
//        $q->setFieldBoost('author', 10);
//        $boolQuery->addShould($q);
//
//        $q = new \Elastica\Query\Match();
//        $q->setFieldQuery('entity.category.name', $keywords);
//        $q->setFieldFuzziness('entity.category.name', 1);
//        $q->setFieldBoost('entity.category.name', 15);
//        $boolQuery->addShould($q);
//
//        $q = new \Elastica\Query\Match();
//        $q->setFieldQuery('entity.tag.name', $keywords);
//        $q->setFieldFuzziness('entity.tag.name', 1);
//        $q->setFieldBoost('entity.tag.name', 25);
//        $boolQuery->addShould($q);

        $this->query($boolQuery, $limit, $currentPage, $sort);
    }

}