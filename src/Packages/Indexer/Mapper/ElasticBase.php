<?php

namespace PluginContainer\Packages\Indexer\Mapper;

use Elastica\ResultSet;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Search;

class ElasticBase
{
    private $aggregations = [];

    private $search;

    /**
     * @var ResultSet
     */
    private $resultSet;

    public $debug;

    /**
     * ElasticBase constructor.
     * @TODO add logger
     *      *
     * @param Search $search configured Search object
     * @param bool $debug
     */
    public function __construct(Search $search, $debug = false)
    {
        $this->search = $search;
        $this->debug = $debug;
    }

    public function addAggregation($aggregation)
    {
        $this->aggregations[] = $aggregation;
    }

    /**
     * @return array
     */
    public function getIds()
    {
        $ids = [];
        foreach ($this->resultSet->getResults() as $result) {
            $ids[] = $result->getDocument()->getId();

        }

        return $ids;
    }

    /**
     * Executes given query on ES server
     *
     * @param BoolQuery $boolQuery
     * @param $limit
     * @param $page
     * @param $order
     */
    protected function query(BoolQuery $boolQuery, $limit = 20, $page = 1, $sort = null)
    {
        $mainQuery = new Query();
        $mainQuery->setQuery($boolQuery);
        $mainQuery->setTrackScores(true);
        $mainQuery->setParam('track_total_hits', true);
//        $mainQuery->setTrackTotalHits();
        if ($sort !== null) {
            $mainQuery->setSort(['createdAt' => ['order' => 'desc']]);
            if ($sort) {
                $mainQuery->setSort([$sort['sort'] => ['order' => $sort['order']]]);
            }
        }
        $this->search->setQuery($mainQuery);
        if (count($this->aggregations)) {
            foreach ($this->aggregations as $aggregation) {
                $mainQuery->addAggregation($aggregation);
            }
        }

        $this->search->setOption('size', 20);
        if ($limit) {
            $this->search->setOption('size', $limit);
        }
        $this->search->setOption('from', 0);
        if ($page > 1) {
            $this->search->setOption('from', ($page - 1) * $limit);
        }

        $this->resultSet = $this->search->search();
        if ($this->debug) {
            var_dump(json_encode($mainQuery->toArray()));
            $this->printDebug();
            die();
        }
    }

    /**
     * @return \Elastica\ResultSet
     */
    public function getResultSet()
    {
        return $this->resultSet;
    }

    public function printDebug()
    {
        $totalResults = $this->resultSet->getTotalHits();
        var_dump('total results: ' . $totalResults);
        var_dump('paged results: ' . count($this->resultSet->getResults()));

        echo '<table><tr><th></th><th>score</th><th width="250px">name</th><th width="150px">desc</th><th>cat</th>
            </tr>';
        /* @var \Elastica\Result $result */
        $i=0;
        foreach ($this->resultSet->getResults() as $result) {
            $i++;
            echo '<tr>';
            echo '<td>' . $i . '</td>';
            echo '<td>' . $result->getScore() . '</td>';
            echo '<td>' . $result->getDocument()->getData()['title'] . '</td>';
            echo '<td><textarea>' . $result->getDocument()->getData()['description'] . '</textarea></td>';
            echo '<td>';
            foreach ($result->getDocument()->getData()['category'] as $cat) {
                echo $cat['name'] . ', ';
            }
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';

        var_dump($this->resultSet->getResults());
    }
}