<?php

namespace PluginContainer\Packages\Indexer\Factory;


class Mapper
{
    public static function make($indexName, $debug = false)
    {
        $client = ElasticClientFactory::make();
        $search = new \Elastica\Search($client);
        $search->addIndex($indexName);
        $indexName = explode('-', $indexName)[0];
        $className = '\\PluginContainer\\Packages\\Indexer\\Mapper\\' . ucfirst($indexName);

        return new $className($search, $debug);
    }

}