<?php

namespace PluginContainer\Packages\Indexer\Factory;

use PluginContainer\Packages\Indexer\Factory\ElasticClientFactory as ElasticFactory;

/**
 * Class ProductSetupFactory
 * @package Gf\Search\Factory
 */
class ProductSetupFactory
{
    /**
     * @var ElasticClientFactory
     */
    private $elasticClientFactory;

    /**
     * ProductSetupFactory constructor.
     */
    public function __construct(ElasticFactory $elasticClientFactory)
    {
        $this->elasticClientFactory = $elasticClientFactory;
    }

    /**
     * @return \PluginContainer\Packages\Indexer\Service\Setup
     */
    public function make()
    {
        return new \PluginContainer\Packages\Indexer\Service\Setup(
            $this->elasticClientFactory->make(),
            new \PluginContainer\Packages\Indexer\Config\Product()
        );
    }
}