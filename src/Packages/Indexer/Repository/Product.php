<?php

namespace PluginContainer\Packages\Indexer\Repository;


use PluginContainer\Packages\Indexer\Mapper\Product as ProductMapper;
use PluginContainer\Packages\Indexer\Factory\Mapper;

class Product
{
    const INDEX_NAME = ES_INDEX_PRODUCT . '-' . ENVIRONMENT . PROJECT_PREFIX;

    private $results;

    public function getMapping()
    {
        $mapper = static::createMapper();

        return $mapper->getMapping();
    }

    public function test()
    {
        $mapper = static::createMapper();
        $mapper::test();

        return $this->parseResultsFromMapper($mapper);
    }

    public function getProductById(int $id): array
    {
        if (!isset($this->results[$id])) {
            throw new \Exception('Product with id ' . $id . ' not found in result set.');
        }

        return $this->results[$id];
    }

    /**
     * Returns article list from ES
     *
     * @param $type
     * @param $term
     * @param $currentPage
     * @param int $perPage
     * @return array
     */
    public function searchByCategory($catId, $currentPage, $query = null, $perPage = 20, $filter = [], $sort = null)
    {
        $mapper = static::createMapper();
        $mapper->searchByCategory($catId, $perPage, $currentPage, $query, $filter, $sort);

        return $this->parseResultsFromMapper($mapper);
    }

    public function search($input, $page = 1, $perPage = 20, $sort = null)
    {
        $mapper = static::createMapper();
        $mapper->search($input, $perPage, $page, $sort);

        return $this->parseResultsFromMapper($mapper);
    }

    /**
     * @return ProductMapper
     */
    public static function createMapper()
    {
        return Mapper::make(static::INDEX_NAME);
    }

    public function parseResultsFromMapper(ProductMapper $mapper)
    {
        $items = [];
        $filters = [];
        $posts = [];
        foreach ($mapper->getResultSet() as $result) {
            $postObj = new \stdClass();
            $postObj->ID = $result->getId();
            $posts[] = $postObj;
            $items[$result->getId()] = $result->getData();
        }
        if ($mapper->getResultSet()->hasAggregations()) {
            $filters = $mapper->getResultSet()->getAggregation('attributes');
            $minPrice = $mapper->getResultSet()->getAggregation('minPrice');
            $maxPrice = $mapper->getResultSet()->getAggregation('maxPrice');
        }
        $this->results = $items;

        return [
            'totalCount' => $mapper->getResultSet()->getTotalHits(),
            'wpPostIds' => $posts,
//            'items' => $items,
            'filters' => $filters,
            'minPrice' => $minPrice ?? 0,
            'maxPrice' => $maxPrice ?? 1000000,
        ];
    }

    /**
     * Sync post data to elastic on save.
     *
     * @param \WP_Post $post
     */
    public static function syncItemToElastic(\WC_Product $post) {
        if ($post && strtolower($post->post_status) !== 'auto-draft' && strtolower($post->post_name) !== 'auto-draft'
            && $post->post_type === 'product') {
            $indexer = new \PluginContainer\Packages\Indexer\Service\Indexer(
                \PluginContainer\Packages\Indexer\Factory\ElasticClientFactory::make()->getIndex(static::INDEX_NAME)
            );
            $indexer->indexPost($post);
        }
    }

    public static function deleteItemFromElastic(\WC_Product $post) {
        $indexer = new \PluginContainer\Packages\Indexer\Service\Indexer(
            \PluginContainer\Packages\Indexer\Factory\ElasticClientFactory::make()->getIndex(static::INDEX_NAME)
        );
        $indexer->deleteProduct($post);
    }
}