<?php
namespace PluginContainer\Packages\FeedImport\Service;

class ImageHandler
{
    /**
     * Inserts images into media library for given post id.
     *
     * @param $images
     * @param $productId
     * @throws \Exception
     */
    public function handleImage($images, $productId)
    {
        if (!is_int($productId)) {
            var_dump($productId);
            throw new \Exception('invalid product id');
        }
        $explodedImages = explode(',', $images);
        if (trim($images) == '' || count($explodedImages) === 0) {
            throw new \Exception(sprintf('No images found for item %s.', $productId));
        }
        $image_main_url = $explodedImages[0];

        if (false !== strpos($image_main_url, 'ftp')) {
            return $this->handleFtpImage($explodedImages, $productId);
        }
        if (is_object($image_main_url) && get_class($image_main_url) === \WP_Error::class) {
            $msg = 'Failed to fetch image for item: ' . $productId . PHP_EOL;
            $msg .= $image_main_url->get_error_messages();
            $msg .= $image_main_url;
            throw new \Exception($msg);
        }
        $image_main_url = trim($image_main_url);
        if (!strlen($image_main_url)) {
            throw new \Exception('Empty image url received, skipping...');
        }

        //Main image
        $image_main_id = \media_sideload_image($image_main_url, $productId, '', 'id');
        if (is_object($image_main_id) && get_class($image_main_id) === \WP_Error::class) {
            $msg = 'Failed to fetch image for item: ' . $productId . PHP_EOL;
            $msg .= print_r($image_main_id->get_error_messages(), true);
            $msg .= $image_main_url;
            throw new \Exception($msg);
        }
        if (is_object($image_main_id) && get_class($image_main_id) === \WP_Error::class) {
            throw new \Exception(sprintf('Could not save image for item %s. Url: %s. Error %s .',
                $productId, $image_main_url, print_r($image_main_id, true)));
        }
        \set_post_thumbnail($productId, $image_main_id);
        \update_post_meta($productId, '_thumbnail_id', $image_main_id);

        //Gallery images
        $image_gallery_urls = explode(',', $images);
        $image_gallery_ids = [];
        foreach ($image_gallery_urls as $key => $url) {
            if ($key > 0) {
                $url = trim($url);
                // @TODO check image via http ?
                $image = \media_sideload_image($url, $productId, '', 'id');
                //@TODO add try/catch and log error
                if (is_object($image) && get_class($image) === \WP_Error::class) {
                    throw new \Exception(sprintf('Could not save image for item %s. Url: %s. Error %s .',
                        $productId, $url, print_r($image->errors, true)));
                }
                $image_gallery_ids[] = $image;
            }
        }

        \update_post_meta($productId, '_product_image_gallery', implode(',', $image_gallery_ids));

        return true;
    }

    public function handleFtpImage($images, $productId)
    {
        $galleryIds = [];
        foreach ($images as $key => $imageUrl) {
            $image = $this->fetchFtpImage($imageUrl);
            $file_array = array();
            $file_array['name'] = basename($image);
            $file_array['tmp_name'] = $image;
            $id = media_handle_sideload($file_array, $productId);
            if (is_wp_error($id)) {
                unlink($file_array['tmp_name']);
                throw new \Exception('failed to save image.' . $imageUrl);
            }
            if ($key > 0) {
                $galleryIds[] = $id;
            } else {
                \set_post_thumbnail($productId, $id);
                \update_post_meta($productId, '_thumbnail_id', $id);
            }
        }
        \update_post_meta($productId, '_product_image_gallery', implode(',', $galleryIds));
    }

    private function fetchFtpImage($url)
    {
        $fileName = basename($url);
        $localFile = ABSPATH . 'wp-content/uploads/feed/' . $fileName;
        $urlInfo = parse_url($url);
        $ftpConnection = ftp_connect($urlInfo['host']);
        ftp_login($ftpConnection, 'anonymous', 'anonymous@example.org');
        ftp_pasv($ftpConnection, true);
        if (!ftp_get($ftpConnection, $localFile, $urlInfo['path'], FTP_BINARY)) {
            var_dump(error_get_last());
            throw new \Exception(sprintf('Could not fetch image %s from %s. ', $urlInfo['path'], $urlInfo['host']));
        }
        ftp_close($ftpConnection);

        return $localFile;
    }
}