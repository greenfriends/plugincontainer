<?php

namespace PluginContainer\Packages\FeedImport\Service;

use PluginContainer\Packages\FeedImport\Model\Product;

class Updater
{
    private $imageHandler;

    private $delta = [];

    public function __construct()
    {
        $this->imageHandler = new ImageHandler();
    }

    /**
     * Update product data when item is updated.
     *
     * @param \WC_Product $product
     * @param Product $productData
     * @return \WC_Product
     */
    public function updateWcProduct(Product $productData, $priceRules)
    {
        global $wpdb;

        $this->delta = [];

        /* @var \WC_Product $product */
        $product = wc_get_product($productData->getPostId());
        if (!$product) {
            throw new \Exception(sprintf('product with id %s and vendorcode %s not found.', $productData->getPostId(), $productData->getSupplierSku()));
        }

//        if (count($productData->getAttributes()) !== count($product->get_attributes())) {
        if (count($productData->getAttributes()) > 0) {
            $this->updateAttributes($productData, $product);
//            var_dump($product->get_id());
        }

        $this->updateStatus($productData, $product);
        $this->updateOtherAttributes($productData, $product);
        $this->updateSimplePrices($productData, $product, $priceRules);
//        $this->updateImage($productData, $product);


        //update category where required
        // @TODO don't remove special categories
        // @TODO think of a way to draft products if they lose category

            $categories = explode(',', $productData->getCategoryIds());
            if (count($categories) === 0) {
                throw new \Exception('no categories ' . $productData->getCategoryIds());
            }
            if ($product->get_meta('ignoreExternalCategoryChange', true) !== '1' &&
                (count(array_diff($categories,  $product->get_category_ids())) ||
                    count(array_diff($product->get_category_ids(), $categories)))) {
                        $oldCats = implode(',', $product->get_category_ids());
                        $product->set_category_ids($categories);
                        $product->save();
                        $newCats = implode(',', $product->get_category_ids());
                        $this->setDelta($product, $oldCats, $newCats, 'category', 'updating categories');
                    }
        if (count($this->delta) > 0) {
            foreach ($this->delta as $postId => $data) {
                $postId = (int) $postId;
                $sql = sprintf("INSERT INTO gfFeedLog (`productId`, `supplierId`, `type`, `attribute`, `oldValue`, `newValue`, `message`, `timestamp`) VALUES (
        {$postId},'{$data['supplierId']}', '{$data['type']}','{$data['attribute']}', '{$data['old']}', '{$data['new']}', '{$data['message']}', NOW()
)");
                if (!$wpdb->query($sql)) {
                    var_dump($wpdb->last_error);
                }
            }
        }

        $product->update_meta_data('vendor_code', $productData->getSku());
        $product->save();

        return $product;
    }

    private function updateAttributes(Product $productData, \WC_Product $product)
    {
        $attributes = [];
        foreach ($productData->getAttributes() as $name => $value) {

            if (count($product->get_attributes()) > 0) {
//                var_dump($product->get_attributes());
//                var_dump($product->get_id());
//                die('update attributes');
            }

            if (strlen($name) > 28 || strlen(trim($value)) === 0) {
                continue;
            }

            $taxSlug = wc_attribute_taxonomy_slug($name);
            $termSlug = wc_attribute_taxonomy_slug($value);

            $attribute = new \WC_Product_Attribute();
            $attribute->set_name($name);
            $attribute->set_visible(true);
            $attribute->set_variation(false);
            $attribute->set_options([$value]);

//            $id = static::createWcAttribute($name);
//            $taxonomy = static::createAttribute($name, $taxSlug);
//            $term = static::createTerm($value, $termSlug, $taxSlug);

//            $attribute->set_id($term->term_id);

            $attributes[] = $attribute;

        }
        $product->set_attributes($attributes);
        $product->save();
    }

    public static function createTerm(string $termName, string $termSlug, string $taxonomy, int $order = 0): ?\WP_Term
    {
        $taxonomy = wc_attribute_taxonomy_name($taxonomy);

        if (is_wp_error($taxonomy)) {
            var_dump('cannot get tax name');
            var_dump($taxonomy);
            die();
        }

        if (! $term = get_term_by('slug', $termSlug, $taxonomy)) {
            if (is_wp_error($term)) {
                var_dump('cannot get tag');
                var_dump($term);
                var_dump($termName);
                var_dump($taxonomy);
                die();
            }

            $term = wp_insert_term($termName, $taxonomy, array(
                'slug' => $termSlug,
            ));
            if (is_wp_error($term)) {
                var_dump('cannot insert tag');
                var_dump($term);
                var_dump($termName);
                var_dump($taxonomy);
                die();
            }
            $term = get_term_by('id', $term['term_id'], $taxonomy);
            if ($term) {
                update_term_meta($term->term_id, 'order', $order);
            }
        }

        return $term;
    }

    public static function createAttribute(string $attributeName, string $attributeSlug): ?\stdClass
    {
        delete_transient('wc_attribute_taxonomies');
        \WC_Cache_Helper::invalidate_cache_group('woocommerce-attributes');

        $attributeLabels = wp_list_pluck(wc_get_attribute_taxonomies(), 'attribute_label', 'attribute_name');
        $attributeWCName = array_search($attributeSlug, $attributeLabels, TRUE);

        if (!$attributeWCName) {
            $attributeWCName = wc_sanitize_taxonomy_name($attributeSlug);
        }

        $attributeId = wc_attribute_taxonomy_id_by_name($attributeWCName);
        if (!$attributeId) {
            $taxonomyName = wc_attribute_taxonomy_name($attributeWCName);
            unregister_taxonomy($taxonomyName);
            $attributeId = wc_create_attribute(array(
                'name' => $attributeName,
                'slug' => $attributeSlug,
                'type' => 'select',
                'order_by' => 'menu_order',
                'has_archives' => 0,
            ));

            register_taxonomy($taxonomyName, apply_filters('woocommerce_taxonomy_objects_' . $taxonomyName, array(
                'product'
            )), apply_filters('woocommerce_taxonomy_args_' . $taxonomyName, array(
                'labels' => array(
                    'name' => $attributeSlug,
                ),
                'hierarchical' => FALSE,
                'show_ui' => FALSE,
                'query_var' => TRUE,
                'rewrite' => FALSE,
            )));
        }

        return wc_get_attribute($attributeId);
    }

    public static function createWcAttribute($name)
    {
        global $wpdb;

        $slug = sanitize_title($name);

        if ( strlen( $slug ) >= 28 ) {
            return new \WP_Error( 'invalid_product_attribute_slug_too_long', sprintf( __( 'Name "%s" is too long (28 characters max). Shorten it, please.', 'woocommerce' ), $slug ), array( 'status' => 400 ) );
        }
        if ( wc_check_if_attribute_name_is_reserved($slug) ) {
            return new \WP_Error( 'invalid_product_attribute_slug_reserved_name', sprintf( __( 'Name "%s" is not allowed because it is a reserved term. Change it, please.', 'woocommerce' ), $slug ), array( 'status' => 400 ) );
        }
        if ( taxonomy_exists( wc_attribute_taxonomy_name($name) ) ) {
            return new \WP_Error( 'invalid_product_attribute_slug_already_exists', sprintf( __( 'Name "%s" is already in use. Change it, please.', 'woocommerce' ), $label_name ), array( 'status' => 400 ) );
        }

        $data = array(
            'attribute_label'   => $name,
            'attribute_name'    => $slug,
            'attribute_type'    => 'select',
            'attribute_orderby' => 'menu_order',
            'attribute_public'  => 0, // Enable archives ==> true (or 1)
        );
        $results = $wpdb->insert( "{$wpdb->prefix}woocommerce_attribute_taxonomies", $data );

        if (is_wp_error($results)) {
            return new \WP_Error( 'cannot_create_attribute', $results->get_error_message(), array( 'status' => 400 ) );
        }
        $id = $wpdb->insert_id;

        do_action('woocommerce_attribute_added', $id, $data);
        wp_schedule_single_event( time(), 'woocommerce_flush_rewrite_rules' );
        delete_transient('wc_attribute_taxonomies');

        return $id;
    }

    private function updateImage(Product $productData, \WC_Product $product)
    {
        $sourceImages = explode(',', $productData->getImages());
        // if main image name is changed, or there are more images for gallery
        if (basename(wp_get_original_image_path($product->get_image_id())) !== basename($sourceImages[0]) ||
            (count($sourceImages)-1) > count($product->get_gallery_image_ids())) {
            $this->imageHandler->handleImage($productData->getImages(), $product->get_id());
            $this->setDelta($product, '!old-value!', 'images updated', 'image', 'updating images');
        }
    }

    private function updateOtherAttributes(Product $productData, \WC_Product $product)
    {
//        $updateFor = [Prometz::SUPPLIER_ID];
//        $updateFor = [];
//        if (!in_array($productData->getSupplierId(), $updateFor)) {
            $save = false;
            if ($productData->getName() != $product->get_name()) {
                $old = $product->get_name();
                $product->set_name($productData->getName());
                $this->setDelta($product, $old, $productData->getName(), 'name', 'updating name');
                $save = true;
            }
            if ($product->get_meta('ignoreExternalDescriptionChange', true) !== '1'
                && $productData->getDescription() !== '' && $productData->getDescription() != $product->get_description()) {
                $old = $product->get_description();
                $product->set_description($productData->getDescription());
                $this->setDelta($product, $old, $productData->getDescription(), 'description', 'updating description');
                $save = true;
            }
            if ($productData->getWeight() != $product->get_weight()) {
                $old = $product->get_weight();
                $product->set_weight($productData->getWeight());
                $this->setDelta($product, $old, $productData->getWeight(), 'weight', 'updating global weight');
                $save = true;
            }
            if ($save) {
                $product->save();
            }
//        }
    }

    private function parseMarginRules(Product $productData, \WC_Product $product, $priceRules)
    {
        $marginToApply = 0;
        $cats = $this->sortCategoriesByHierarchy($product->get_category_ids());
        /* @var \PluginContainer\Packages\FeedPriceMargin\Model\FeedPriceMargin $rule */
        foreach ($cats as $catId) {
            if ($catId === 15) { // uncategorised
                return;
            }
            if (isset($priceRules[$catId])) {
                $rule = $priceRules[$catId];
                // set default margin for supplier
                if ($marginToApply === 0) {
                    $marginToApply = $rule->getDefaultMargin();
                }
                $margins = unserialize($rule->getMargin());
                foreach ($margins as $key => $margin) {
                    if ($margin) {
                        // check other rules
                        $rules = unserialize($rule->getRules());
                        $limiter = unserialize($rule->getLimiter());

                        // get specific rule and overwrite margin
                        if ($limiter[$key]) {
                            if ($rules[$key] === 'lt' && $productData->getRegularPrice() <= $limiter[$key]) {
                                $marginToApply = $margin;
                            }
                            if ($rules[$key] === 'gt' && $productData->getRegularPrice() >= $limiter[$key]) {
                                $marginToApply = $margin;
                            }
                        } else {
                            $marginToApply = $margin;
                        }
                    }
                }
            }
        }
        $price = $productData->getRegularPrice();
        $product->update_meta_data('inputPrice', $price);
        $product->save();
        $price += $price * $marginToApply / 100; // apply margin
        $price = ceil($price + $price * 20 / 100); // apply pdv
        $salePrice = $productData->getSalePrice();
        if ($salePrice) {
            $salePrice += $salePrice * $marginToApply / 100; // apply margin
            $salePrice = ceil($salePrice + $salePrice * 20 / 100); // apply pdv
        }
        return ['price' => $price, 'salePrice' => $salePrice];
    }
    private function sortCategoriesByHierarchy($catIds)
    {
        $sorted = [];
        foreach ($catIds as $key => $catId) {
            $cat = get_term($catId);
            if ($cat->parent === 0) {
                $sorted[] = $catId;
                unset($catIds[$key]);
            }
        }
        foreach ($catIds as $catId) {
            $cat = get_term($catId);
            if ($cat->parent === $sorted[0]) {
                $sorted[1] = $catId;
            } else {
                $sorted[2] = $catId;
            }
        }
        ksort($sorted);
        return $sorted;
    }
    private function updateSimplePrices(Product $productData, \WC_Product $product, $priceRules)
    {
        if ($product->get_meta('ignoreExternalPriceChange', true) === '1') {
            return;
        }
        $prices = $this->parseMarginRules($productData, $product, $priceRules);
        $price = $prices['price'];
        $salePrice = $prices['salePrice'];

        if (get_class($product) == \WC_Product_Variable::class) {
            // seems it cannot be done this way
        } else {
            if ($price != $product->get_regular_price()
              ||  $salePrice != $product->get_sale_price()) {
                if ($price == '0' || (int) $price === 0) {
                    $product->set_status('draft');
                    $product->set_stock_status('outofstock');
//                    var_dump($productData);
//                    throw new \Exception('there was a problem with the price');
                }
                $old = 'price: ' . $product->get_regular_price() .' | sale price: ' . $product->get_sale_price();
                $new = 'price: ' . $price .' | sale price: ' . $salePrice;
                $this->setDelta($product, $old, $new, 'price', 'simple product price');

                $product->set_regular_price($price);
                $product->set_sale_price($salePrice);
                $product->save();
            }
//            else {
//                $product->set_sale_price('');
//                $product->save();
//            }
        }
    }

    private function setDelta($product, $old, $new, $attribute, $message = '')
    {
        $type = 'simple';
        if (get_class($product) === \WC_Product_Variable::class) {
            $type = 'variable';
        }
        $this->delta[$product->get_id()]['old'] = $old;
        $this->delta[$product->get_id()]['new'] = $new;
        $this->delta[$product->get_id()]['attribute'] = $attribute;
        $this->delta[$product->get_id()]['type'] = $type;
        $this->delta[$product->get_id()]['message'] = $message;
        $this->delta[$product->get_id()]['supplierId'] = get_post_meta($product->get_id(),'supplier',true);
    }

    /**
     * Will update price as well for variable products
     *
     * @param Product $productData
     * @param \WC_Product $product
     * @return mixed
     * @throws \Exception
     */
    private function updateStatus(Product $productData, \WC_Product $product)
    {
        if (get_class($product) == \WC_Product_Variable::class) {
            if ($product->get_status() === 'draft' && $productData->getStatus() === 'publish') {
                $product->set_status('publish');
                $product->save();
                $this->setDelta($product, 'draft', 'publish', 'status', '!!! published drafted product !!!');
            }

            // @TODO find a way to create new variations here

            foreach ($product->get_available_variations() as $available_variation) {
                $variation = wc_get_product($available_variation['variation_id']);
                if (!is_array($productData->getVelicina())) {
                    var_dump($productData);
                    die('no parsed options found in variable product');
                }

                $boja = null;
                $velicina = null;
                $price = null;
                foreach ($productData->getOptions() as $option) {
                    if (isset($available_variation['attributes']['attribute_pa_boja'])) {
                        if (!isset($option['boja']['value'])) {
                            continue;
                        }
                        $boja = wc_sanitize_taxonomy_name($option['boja']['value']);
                        if ($boja !== $available_variation['attributes']['attribute_pa_boja']) {
                            continue;
                        }
                        $stockStatus = $option['stockStatus'];
                        $price = $option['regularPrice'];
                        $salePrice = $option['salePrice'];
                    }
                    if (isset($available_variation['attributes']['attribute_pa_velicina'])) {
                        if (!isset($option['velicina']['value'])) {
                            continue;
                        }
                        $velicina = wc_sanitize_taxonomy_name($option['velicina']['value']);
                        if ($velicina !== $available_variation['attributes']['attribute_pa_velicina']) {
                            continue;
                        }
                        $stockStatus = $option['stockStatus'];
                        $price = $option['regularPrice'];
                        $salePrice = $option['salePrice'];
                    }
                }

                if (!$velicina && !$boja) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('outofstock');
                    $save = $variation->save();
                    if (is_object($save)) {
                        var_dump('variation stock status to outofstock, item removed from feed');
                        die();
                    }

                    $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' item removed from feed');
//                    $changed[$product->get_id()][$variation->get_id()][] = 'variation stock status to outofstock, item removed from feed';
                    continue;
                }

//                if ($product->get_id() == 473329) {
//                    var_dump($variation->get_stock_status());
//                    var_dump($price);
//                    var_dump($velicina);
//                    var_dump($boja);
//                    die();
//                }

                //variation not found in feed
                if (!isset($price)) {
                    if ($variation->get_stock_status() !== 'outofstock') {
                        $old = $variation->get_stock_status();
                        $variation->set_stock_status('outofstock');
                        $variation->save();
                        $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' variation removed from feed');
                    }
                    continue;
                }

                if ($variation->get_stock_status() === 'outofstock' && $stockStatus) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('instock');
                    $variation->save();
                    $this->setDelta($product, $old, 'instock', 'status', $variation->get_id() . ' variation status');
                } elseif ($variation->get_stock_status() === 'instock' && !$stockStatus) {
                    $old = $variation->get_stock_status();
                    $variation->set_stock_status('outofstock');
                    $variation->save();
                    $this->setDelta($product, $old, 'outofstock', 'status', $variation->get_id() . ' variation status');
                }
                if ($variation->get_regular_price() != $price || $variation->get_sale_price() != $salePrice) {
//                    if ($variation->get_sale_price() > 0) {
//                        $variation->set_sale_price('');
//                    }
                    $old = 'regular price:' . $variation->get_regular_price() .' sale price: ' . $variation->get_sale_price();
                    $new = 'regular price:' . $price .' sale price: ' . $salePrice;
                    $variation->set_sale_price($salePrice);
                    $variation->set_regular_price($price);
                    $variation->save();
                    $this->setDelta($product, $old, $new, 'price', $variation->get_id() . ' variation price');
                }
            }
        } else {

//            if ($product->get_id() == 30960) {
//                var_dump($productData);
//                var_dump($product->get_id());
//                var_dump($product->get_status());
//                var_dump($productData->getStatus());
//                die();
//            }

            if ($productData->getStatus() === 'publish' && $product->get_status() !== 'publish') {
                $this->setDelta($product, $product->get_status(), 'publish', 'status', 'simple status published');
                $product->set_status('publish');
                $product->save();
//                echo $product->get_id();
//                echo 'published';
            }
            if ($productData->getStatus() === 'pending' && $product->get_status() !== 'publish') {
                $this->setDelta($product, $product->get_status(), 'pending', 'status', 'product needs manual edit');
                $product->set_status('pending');
                $product->save();
//                echo 'pendinged';
            }
            if ($productData->getStatus() === 'draft' && $product->get_status() === 'publish') {
                $this->setDelta($product, $product->get_status(), 'draft', 'status', 'simple status to draft');
                $product->set_status('draft');
                $product->save();
//                echo 'drafted';
            }
            if ($product->get_stock_status() != $productData->getStockStatus()) {
                $this->setDelta($product, $product->get_stock_status(), $productData->getStockStatus(), 'stockStatus',  'simple stock status');
                $product->set_stock_status($productData->getStockStatus());
                $product->save();
//                echo 'set stock status: ' . $productData->getStockStatus();
            }
            // disable item if no image detected
            if (!(int) get_post_meta($product->get_id(), '_thumbnail_id', true )) {
                $product->set_status('pending');
                $product->save();
                $this->setDelta($product, $product->get_status(), 'pending', 'status',  'no image detected, item disabled.');
            }
        }
    }
}