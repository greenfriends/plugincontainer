<?php

namespace PluginContainer\Packages\FeedImport\Service;

use PluginContainer\Packages\FeedPriceMargin\Repository\FeedPriceMargin;
use PluginContainer\Packages\FeedImport\Model\Product;

class Importer
{
    private $redis;

    /**
     * @var \wpdb $wpdb
     */
    private $db;

    private $httpClient;

    private $baseKey;

    private $creator;

    private $updater;

    private $debug;

    private $priceMarginRepo;

    /**
     * Importer constructor.
     * @param \Redis $redis
     * @param \wpdb $wpdb
     * @param \GuzzleHttp\Client $httpClient
     * @param $key
     * @param $debug
     */
    public function __construct(\Redis $redis, \wpdb $wpdb, \GuzzleHttp\Client $httpClient, $key, $debug = true, FeedPriceMargin $priceMarginRepo)
    {
        $this->redis = $redis;
        $this->db = $wpdb;
        $this->httpClient = $httpClient;
        $this->baseKey = $key;
        $this->debug = $debug;
        $this->priceMarginRepo = $priceMarginRepo;
        $this->creator = new Creator();
        $this->updater = new Updater();
    }

    public function getCount()
    {
        $keys = $this->redis->sMembers($this->baseKey . 'index');

        return count($keys);
    }

    public function resetQueue()
    {
        $keys = $this->redis->sMembers($this->baseKey . 'index');
//        var_dump($keys);
        foreach ($keys as $key) {
            $this->redis->sRem($this->baseKey . 'index', $key);
        }
        echo sprintf($this->baseKey. 'queue clean. %s items removed.', count($keys)) . PHP_EOL;
    }

    private function sortPriceRules($supplierId)
    {
        $rules = [];
        /* @var \PluginContainer\Packages\FeedPriceMargin\Model\FeedPriceMargin $rule */
        foreach ($this->priceMarginRepo->getBy('supplierId', $supplierId) as $rule) {
            $rules[$rule->getCategoryId()] = $rule;
        }

        return $rules;
    }

    public function importItems($offset = 0, $limit = 2000, $supplierId)
    {
        $priceRules = $this->sortPriceRules($supplierId);
        $supplierName = SUPPLIERS[$supplierId]['name'];
        $keys = array_slice($this->redis->sMembers($this->baseKey . 'index'), $offset, $limit, true);
        $total = count($keys);
        // reserve items by removing them from queue, to prevent duplicate items
        foreach ($keys as $key) {
            $this->redis->sRem($this->baseKey . 'index', $key);
        }
        $logTpl = $action = 'nothing to do';
        $importCount = 0;
        foreach ($keys as $key) {
            try {
                /* @var Product $product */
                $product = unserialize($this->redis->get($this->baseKey . $key));
                if (strpos($this->baseKey, 'Update')) {
                    $logTpl = "Updated %d items.";
                    $action = 'updateStat';
                    if (!$this->updater->updateWcProduct($product, $priceRules)) {
                        var_dump($product);
                        die('import items');
                    }
                    $importCount++;
                } else {
                    $logTpl = "Created %d items.";
                    $action = 'createStat';
                    $this->creator->createItem(new \WC_Product(), $product, $priceRules);
                    $importCount++;
                }
            } catch (\Exception $e) {
                $logTpl = "Error %s .";
//                $key = 'error';
                if (get_class($e) === \WC_Data_Exception::class) {
                    $data = sprintf('%s: %s', date('Y:m:d H:i:s'), $e->getMessage() . ' - ' . $key);

//                    $filePath = LOG_PATH . 'debug.log';
//                    file_put_contents($filePath, $data . PHP_EOL, FILE_APPEND);
                    var_dump($data);
                    die('import items exception');
//                    \NSS_Log::log($e->getMessage() . ' - ' . $key);
                    continue;
                }
                echo $e->getMessage() . PHP_EOL;
//                throw $e;
            }
        }

        $msg = sprintf($logTpl, $importCount, $total);

        \WP_CLI::success($msg);
        $this->sendMailLog($supplierName, $msg, $action, $importCount);

        return $msg;
    }

    public function sendMailLog($supplier, $message, $action, $importCount)
    {
        $from = 'mailer@diteh.rs';
        $headers = [
            'Content-Type: text/html; charset=UTF-8',
            "From: Diteh importer <'{$from}'>",
        ];
        $to[] = 'djavolak@mail.ru';
        $subject = 'Diteh update feed cron report: ' . $supplier;

        $key = sprintf('importFeed:%s:#%s', $supplier, $action);
        $this->redis->set($key, $importCount);
        $key = sprintf('importFeed:%s:#updateTime', $supplier);
        $this->redis->set($key, date('d/m/Y H:i'));

        wp_mail($to, $subject, $message, $headers);
    }
}