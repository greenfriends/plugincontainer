<?php

namespace PluginContainer\Packages\FeedImport\Service;

use \GuzzleHttp\Client;
use PluginContainer\Packages\FeedPriceMargin\Repository\FeedPriceMargin;

class Feed
{
    private $redis;

    private $httpClient;

    private $wpdb;

    private $parser;

    private $priceMarginRepo;

    public function __construct(Client $httpClient, \Redis $redis, \wpdb $wpdb, FeedPriceMargin $priceMarginRepo)
    {
        $this->redis = $redis;
        $this->httpClient = $httpClient;
        $this->wpdb = $wpdb;
        $this->priceMarginRepo = $priceMarginRepo;
    }

    public function importNew($args)
    {
        $key = ENVIRONMENT.'importFeedQueueCreate:' . SUPPLIERS[$args[0]]['name'] .':';

        return $this->startImport($args, $key);
    }

    public function importExisting($args)
    {
        $key = ENVIRONMENT.'importFeedQueueUpdate:' . SUPPLIERS[$args[0]]['name'] .':';

        return $this->startImport($args, $key);
    }

    public function startImport($args, $key)
    {
        $offset = 0;
        $limit = 2000;
        if (isset($args[1])) {
            $offset = $args[1];
        }
        if (isset($args[2])) {
            $limit = $args[2];
        }
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key, 0, $this->priceMarginRepo);

        return $importer->importItems($offset, $limit, $args[0]);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function parseFeed($args)
    {
        $this->parser = ParserFactory::make(SUPPLIERS[$args[0]], $this->httpClient, $this->redis);

        return $this->parser->processItems();
    }

    public function resetQueue($args)
    {
        $key = ENVIRONMENT . 'importFeedQueueUpdate:' . SUPPLIERS[$args[0]]['name'] .':';
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key);
        $importer->resetQueue();

        $key = ENVIRONMENT. 'importFeedQueueCreate:' . SUPPLIERS[$args[0]]['name'] .':';
        $importer = new Importer($this->redis, $this->wpdb, $this->httpClient, $key);
        $importer->resetQueue();
    }

    public function getErrors()
    {
        return $this->parser->parseErrors();
    }
}