<?php

namespace PluginContainer\Packages\FeedImport\Service;


class ParserFactory
{

    /**
     * @TODO pass supplierId to parser in stead of using const
     *
     * @param array $supplierConfig
     * @param \GuzzleHttp\Client $httpClient
     * @param \Redis $redis
     * @return Parser\ParserInterface
     */
    static public function make(array $supplierConfig, \GuzzleHttp\Client $httpClient, \Redis $redis)
    {
        $parserName = 'PluginContainer\\Packages\\FeedImport\\Model\\Parser\\' . ucfirst($supplierConfig['name']);

        return new $parserName($httpClient, $redis);
    }
}