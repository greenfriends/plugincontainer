<?php

namespace PluginContainer\Packages\FeedImport\Service;

use PluginContainer\Packages\FeedPriceMargin\Repository\FeedPriceMargin;

class FeedFactory
{
    public function __invoke(FeedPriceMargin $priceMarginRepo)
    {
        global $wpdb;

        $httpClient = new \GuzzleHttp\Client(['defaults' => [
            'verify' => false
        ]]);
        $redis = new \Redis();
        $redis->connect(CACHE_HOST);

        return new Feed($httpClient, $redis, $wpdb, $priceMarginRepo);
    }
}