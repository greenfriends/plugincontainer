<?php

namespace PluginContainer\Packages\FeedImport\Service;

use League\Csv\Reader;
use League\Csv\Writer;
use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class CategoryMapper extends BaseMapper
{
    private $supplierId;

    //deprecated but should be here until next commit
    private $storage;

    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfFeedMappings');
    }

    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        $this->storage = sprintf(
            '%s/uploads/feed/mapping/%s-new.csv', WP_CONTENT_DIR, \SUPPLIERS[$supplierId]['name']
        );
    }

    public static function getCategories($exclude = [])
    {
        $args = array(
            'orderby' => 'name',
            'order' => 'asc',
            'hide_empty' => false,
            'exclude' => $exclude,
        );
        return get_terms('product_cat', $args);
    }

    /**
     * @param $mappedCategories
     * @param $sourceCategories
     * @return void
     * @throws \Exception
     */
    public function updateMappingFromFeed($mappedCategories, $sourceCategories): void
    {
        $data = [];
        //Format source categories to match the format needed to save in database
        foreach ($sourceCategories as $sourceCategory) {
            $parts = explode('###', $sourceCategory);
            $part0 = $parts[0];
            unset($parts[0]);
            $part1 = implode('|', $parts);
            $data[] = [$part0, $part1, '', ''];
        }
        if ($mappedCategories !== []) {
            foreach ($mappedCategories as $item) {
                //Equalise the mapped categories with the source categories
                $itemId = $item['id'];
                $item['localId1'] = '';
                $item['localId2'] = '';
                unset($item['id'], $item['createdAt'], $item['updatedAt'], $item['supplierId']);
                $item = array_values($item);
                // category removed from source delete it from mapping
                if (!in_array($item, $data, false)) {
                    $this->deleteMap($itemId);
                }
            }
        }
        $this->createMapping($data);
    }

    private function checkSupplierIdIsSet()
    {
        if (!$this->supplierId) {
            throw new \Exception('setSupplierId has to be called first.');
        }
    }

    private function deleteMap($itemId)
    {
        $this->checkSupplierIdIsSet();
        $sql = "DELETE FROM `{$this->tableName}` WHERE `id` = $itemId";
        $this->driver->exec($sql);
    }

    /**
     * @param array $data
     * @return void
     * @throws \Exception
     */
    public function saveMappingFromPost(array $data): void
    {
        $this->checkSupplierIdIsSet();
        $sql = "UPDATE $this->tableName SET supplierId =:supplierId, source1 = :source1, source2 = :source2, 
        localId1 = :localId1, localId2 = :localId2 WHERE source1 = :source11 AND source2 = :source22";
        $stmt = $this->driver->prepare($sql);
        foreach ($data as $item) {
            $stmt->bindParam(':supplierId', $this->supplierId);
            $stmt->bindParam(':source1', $item[0]);
            $stmt->bindParam(':source2', $item[1]);
            $stmt->bindParam(':localId1', $item[2]);
            $stmt->bindParam(':localId2', $item[3]);
            $stmt->bindParam(':source11', $item[0]);
            $stmt->bindParam(':source22', $item[1]);
            $stmt->execute();
        }
    }

    /**
     * @param array $data
     * @return void
     * @throws \Exception
     */
    public function createMapping(array $data): void
    {
        $this->checkSupplierIdIsSet();
        $sql = "INSERT INTO $this->tableName (supplierId, source1, source2, localId1, localId2) VALUES (:supplierId, :source1, :source2, :localId1, :localId2)";
        $stmt = $this->driver->prepare($sql);
        foreach ($data as $item) {
            $stmt->bindParam(':supplierId', $this->supplierId);
            $stmt->bindParam(':source1', $item[0]);
            $stmt->bindParam(':source2', $item[1]);
            $stmt->bindParam(':localId1', $item[2]);
            $stmt->bindParam(':localId2', $item[3]);
            try {
                $stmt->execute();
            } catch (\Exception $e) {
                if (str_contains($e->getMessage(), 'Duplicate entry')) {
                    continue;
                }
                throw $e;
            }
        }
    }

    /**
     * @param $data
     * @return void
     * @throws \Exception
     * @deprecated but should be here until next commit
     */
    public function saveMappingFromCsv($data): void
    {
        $this->checkSupplierIdIsSet();
        $sql = "INSERT INTO $this->tableName (supplierId, source1, source2, localId1, localId2) VALUES (:supplierId, :source1, :source2, :localId1, :localId2)";
        $stmt = $this->driver->prepare($sql);
        foreach ($data as $item) {
            $stmt->bindParam(':supplierId', $this->supplierId);
            $stmt->bindParam(':source1', $item[0]);
            $stmt->bindParam(':source2', $item[1]);
            $stmt->bindParam(':localId1', $item[2]);
            $stmt->bindParam(':localId2', $item[3]);
            $stmt->execute();
        }
    }
    /**
     * @param $supplierId
     * @return array
     * @throws \Exception
     * @throws \League\Csv\Exception
     */
    public function getMapping(): array
    {
        $this->checkSupplierIdIsSet();
        $sql = "SELECT * FROM $this->tableName WHERE supplierId = $this->supplierId";
        return $this->driver->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return false|int
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \Exception
     * @deprecated but should be here until next commit
     */
    public function getMappingFromCsv()
    {
        $this->checkSupplierIdIsSet();
        $catList = Reader::createFromPath($this->storage, 'r');
        $catList->setHeaderOffset(0);
        return $catList;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function createMappingTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
          `supplierId` int(5) UNSIGNED NOT NULL,
		  `source1` varchar(128),
		  `source2` varchar(128),
		  `localId1` int(3),
		  `localId2` int(3),
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey),
          UNIQUE KEY ssp_index (supplierId , source1, source2)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }
}