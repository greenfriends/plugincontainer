<?php


namespace PluginContainer\Packages\FeedImport\Service;


class FeedImportPage
{
    public $pageSlug;

    private $categoryMapper;

    private $ignoredVendors = [];

    public function __construct()
    {
        $this->pageSlug = 'feedImport';
        global $gfContainer;
        $this->categoryMapper = $gfContainer->get(CategoryMapper::class);
    }

    public function initShortcode()
    {
        add_shortcode('feedImport_page', [$this, 'pageView']);
    }

    public function createPage(): void
    {
        $isPageCreated = get_page_by_title('FeedImport', 'OBJECT', 'page');
        // Check if the page already exists
        if (empty($isPageCreated)) {
            $pageId = wp_insert_post(
                [
                    'comment_status' => 'close',
                    'ping_status' => 'close',
                    'post_author' => 1,
                    'post_title' => 'FeedImport',
                    'post_name' => $this->pageSlug,
                    'post_status' => 'publish',
                    'post_content' => '[feedImport_page]',
                    'post_type' => 'page'
                ]
            );
            add_filter('display_post_states', static function ($post_states, $post) use ($pageId) {
                if ($post->ID === $pageId) {
                    $post_states[] = 'Do not delete this page';
                }
                return $post_states;
            }, 10, 2);
        }
    }

    /**
     * Callback for the [wishlist_page] shortcode.
     * @return false|string
     */
    public function pageView() :?string
    {
        $categories = $this->categoryMapper->getCategories();
        $catList = false;
        $msg = false;
        if (isset($_GET['supplierId'])) {
            $this->categoryMapper->setSupplierId($_GET['supplierId']);
            if (!in_array($_GET['supplierId'], $this->ignoredVendors)) {
                try {
                    if (isset($_POST['saveMapping'])) {
                        $this->saveCsvFromPost();
                        $msg = 'saved data';
                    }
                    $catList = $this->categoryMapper->getMapping();
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                }
            } else {
                $msg = sprintf('Dobavljač <em>%s</em> ne koristi mapiranje.', SUPPLIERS[$_GET['supplierId']]['name']);
            }
        }

        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedImport/templates/feedImportPage.php';
        return ob_get_clean();
    }

    private function saveCsvFromPost()
    {
        $limit = count($_POST['source1']);
        $data = [];
        for ($i=0; $i<$limit; $i++) {
            $data[] = [
                $_POST['source1'][$i], $_POST['source2'][$i], $_POST['localId1'][$i], $_POST['localId2'][$i]
            ];
        }
        $this->categoryMapper->saveMappingToCsv($data);
    }

    /**
     * @return string
     */
    public function getPageUrl() :string
    {
        return get_home_url() . '/' . $this->pageSlug . '/';
    }


    public function deletePage(): void
    {
        /**
         * @var \WP_Post $page
         */
        $page = get_page_by_title('FeedImport', 'OBJECT', 'page');
        if($page) {
            wp_delete_post($page->ID,true);
        }
    }
}