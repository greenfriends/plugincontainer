<?php


namespace PluginContainer\Packages\FeedImport\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\FeedImport\Service\CategoryMapper;
use PluginContainer\Packages\FeedImport\Service\FeedFactory;
use PluginContainer\Packages\FeedImport\Service\FeedImportPage;
use PluginContainer\Packages\FeedPriceMargin\Repository\FeedPriceMargin;

class FeedImport extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface
{

    /**
     * @var FeedImportPage
     */
    private $feedPage;

    private $categoryMapper;

    private $priceMarginRepo;

    private $ignoredVendors = [];

    public function __construct(Feature $featureRepo, Logger $logger, FeedImportPage $feedPage, FeedPriceMargin $priceMarginRepo, \PDO $pdo)
    {
        parent::__construct($featureRepo, $logger);
        $this->feedPage = $feedPage;
        $this->priceMarginRepo = $priceMarginRepo;
        $this->categoryMapper = new CategoryMapper($pdo, $logger);
    }

    public function init(): void
    {
        $this->feedPage->initShortcode();
//        $this->addWishlistHtmlInProductInfo();
        if (defined('WP_CLI') && WP_CLI) {
            ini_set('max_execution_time', 1200);
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
            $factory = new FeedFactory();
            $feed = $factory($this->priceMarginRepo);
            \WP_CLI::add_command('feed', $feed);
            \WP_CLI::add_command('parseCsvMappingToDatabase', [$this, 'parseCsvMappingToDatabase']);
            //Used for manual fixing of items or bulk delete or smth idk
            \WP_CLI::add_command('productForeach', [$this, 'productForeach']);
        }
        $this->addIgnoreCustomFieldsToAdminProductPage();
        $this->ignoreCustomFieldsHandler();

    }

    /**
     * @throws \Exception
     */
    public function setup() :void
    {
//        $this->wishlistRepo->createTable();
//        $this->wishlistRepo->createUserIdIndex();
        $this->feedPage->createPage();
        $this->categoryMapper->createMappingTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup() :void
    {
//        $this->wishlistRepo->deleteTable();
        $this->feedPage->deletePage();
        $this->categoryMapper->deleteTable();
    }

    public function parseCsvMappingToDatabase($args): void
    {
        $this->categoryMapper->setSupplierId($args[0]);
        $data = $this->categoryMapper->getMappingFromCsv();
        $formattedData = [];
        foreach ($data->getIterator() as $item) {
            $formattedData[] = array_values($item);
        }
        $this->categoryMapper->saveMappingFromCsv($formattedData);

    }

    public function getView(): AjaxResponse
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedImport/templates/feedImportPageHeader.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    public function getMappingPage()
    {
        $categories = $this->categoryMapper::getCategories();
        $catList = false;
        $msg = false;
//        var_dump($_GET['supplierId']);
//        var_dump($_POST);
        if (isset($_GET['supplierId']) && $_GET['supplierId']) {
            $this->categoryMapper->setSupplierId($_GET['supplierId']);
            if (!in_array($_GET['supplierId'], $this->ignoredVendors)) {
                try {
                    if (count($_POST)) {
                        $this->saveMappingFromPost();
                        $msg = 'saved data';
                    }
                    $catList = $this->categoryMapper->getMapping();
                } catch (\Exception $e) {
                    $msg = $e->getMessage();
                }
            } else {
                $msg = sprintf('Dobavljač <em>%s</em> ne koristi mapiranje.', SUPPLIERS[$_GET['supplierId']]['name']);
            }
        }

        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedImport/templates/feedImportPage.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());

        return $response;
    }

    /**
     * @return void
     * @throws \Exception
     */
    private function saveMappingFromPost(): void
    {
        $limit = count($_POST['source1']);
        $data = [];
        for ($i=0; $i<$limit; $i++) {
            $data[] = [
                $_POST['source1'][$i], $_POST['source2'][$i], $_POST['localId1'][$i], $_POST['localId2'][$i]
            ];
        }
        $this->categoryMapper->saveMappingFromPost($data);
    }

    private function addIgnoreCustomFieldsToAdminProductPage(): void
    {
        add_action('woocommerce_product_options_general_product_data', static function ()
        {

            $postId = $_GET['post'] ?? '';

            $ignorePriceText = __('Ignoriši promene cene','plugin-container');
            $ignoreCatText = __('Ignoriši promene kategorije','plugin-container');
            $ignoreDescriptionText = __('Ignoriši promene opisa', 'plugin-container');
            $ignorePrice = get_post_meta($postId, 'ignoreExternalPriceChange', true)  ? 'checked' : '';
            $ignoreCategory = get_post_meta($postId, 'ignoreExternalCategoryChange', true) ? 'checked' : '';
            $ignoreDescription = get_post_meta($postId, 'ignoreExternalDescriptionChange', true) ? 'checked' : '';

            echo <<<html
                <p id="ignoreExternalPriceChange" class="form-field">     
                    <label for="ignoreExternalPriceChange">$ignorePriceText</label>
                    <input $ignorePrice name="ignoreExternalPriceChange" id="ignorePrice" type="checkbox">
                </p>
                <p id="ignoreExternalCategoryChange" class="form-field">     
                    <label for="ignoreExternalPriceChange">$ignoreCatText</label>
                    <input $ignoreCategory name="ignoreExternalCategoryChange" id="ignoreCategory" type="checkbox">
                </p>
                <p id="ignoreExternalDescriptionChange" class="form-field">
                    <label for="ignoreExternalDescriptionChange">$ignoreDescriptionText</label>
                    <input $ignoreDescription name="ignoreExternalDescriptionChange" id="ignoreDescription" type="checkbox">
                </p>
            html;
        });
    }

    private function ignoreCustomFieldsHandler(): void
    {
        add_action('woocommerce_process_product_meta', static function ($postId)
        {
            $ignorePrice = $_POST['ignoreExternalPriceChange'];
            $ignoreCategory = $_POST['ignoreExternalCategoryChange'];
            $ignoreDescription = $_POST['ignoreExternalDescriptionChange'];

            if($ignorePrice) {
                update_post_meta($postId, 'ignoreExternalPriceChange', true);
            } else {
                delete_post_meta($postId, 'ignoreExternalPriceChange');
            }
            if($ignoreCategory) {
                update_post_meta($postId, 'ignoreExternalCategoryChange', true);
            } else {
                delete_post_meta($postId, 'ignoreExternalCategoryChange');
            }
            if($ignoreDescription) {
                update_post_meta($postId, 'ignoreExternalDescriptionChange', true);
            } else {
                delete_post_meta($postId, 'ignoreExternalDescriptionChange');
            }
        });
    }
    public function productForeach()
    {
        $products = wc_get_products([
            'limit' => -1,
//            'status' => 'published',
            'meta_key' => 'supplier',
            'meta_value' => 5
        ]);
        $count = 0;
        /* @var \WC_Product $product */
        foreach ($products as $key => $product) {
            if ($count === 10) {
                die();
            }
            $sku = $product->get_sku();
            var_dump($product->get_id());
            var_dump($sku);
            var_dump($product->get_title());
            var_dump($product->get_name());
            var_dump($product->get_meta('vendor_code'));
            $count++;
//            $product->update_meta_data('vendor_code', $sku);
        }
    }
}
