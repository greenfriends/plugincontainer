import Tab from "../../../../../assets/admin/js/Tab.js";

export default class FeedImportPage extends Tab{
    constructor() {
        super();
        this.supplierFilter = document.getElementById('supplierFilter');
        this.saveMapping = document.getElementById('saveMapping');
    }

    init() {
        this.addListeners()
    }

    addListeners(){
        this.supplierFilter.addEventListener('change', async (e) => {
            let urlParams = new URLSearchParams({
                action: 'gfAjaxRouter',
                handlerClass: 'PluginContainerPackagesFeedImportControllerFeedImport',
                method: 'getMappingPage',
                supplierId: e.target.value
            });
            this.isLoading = true;
            let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`);
            let response = JSON.parse(await data.text());
            if (!response.success) {
                alert('error');
            }
            document.getElementById('feedFormWrapper').innerHTML = response.body.data;
        });

        this.saveMapping.addEventListener('click', async (e) => {
            let feedForm = document.getElementById('feedForm');
            if (feedForm) {
                let url = new URL(window.location);
                let supplierId = this.supplierFilter.value;
                let formData = new FormData(feedForm);
                let urlParams = new URLSearchParams({
                    action: 'gfAjaxRouter',
                    handlerClass: 'PluginContainerPackagesFeedImportControllerFeedImport',
                    method: 'getMappingPage',
                    supplierId: supplierId
                });
                let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`, {body:formData, method:'post'});
                let response = JSON.parse(await data.text());
                document.getElementById('feedFormWrapper').innerHTML = response.body.data;
            }
        });

    };
}