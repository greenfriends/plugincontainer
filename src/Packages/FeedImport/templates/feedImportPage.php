<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\Wishlist\Controller\Wishlist;

global $gfContainer;
?>
    <form method="post" id="feedForm">
        <?php if ($msg): ?>
            <p><strong><?=$msg?></strong></p>
        <?php endif; ?>
        <table id="logtable" class="stripe cell-border">
            <thead>
            <tr>
                <th colspan="2" class="no-sort">Izvorna kategorija</th>
                <!--        <th class="no-sort">Izvorna kategorija 2</th>-->
                <th class="no-sort">Lokalna kategorija</th>
                <th class="no-sort">Lokalna kategorija</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /** @var array $catList */
            if ($catList):
                foreach ($catList as $record):?>
                    <tr>
                        <td colspan="2">
                            <input readonly name="source1[]" size="60" type="text" value="<?=$record['source1']?>" /> <br />
                            <input readonly name="source2[]" size="60" type="text" value="<?=$record['source2']?>" /> <br />&nbsp;
                        </td>
                        <td><select name="localId1[]" style="width: 350px"><option value="0">-- Izaberi kategoriju --</option>
                                <option value="false" <?=('false' == $record['localId1']) ? 'selected':''?>>-- Ignoriši --</option>
                                <?php foreach ($categories as $category):
                                    $catName = $category->name;
                                    if ($category->parent) {
                                        $parent = get_term_by('id', $category->parent, 'product_cat');
                                        $catName .= ' / ' . $parent->name;
                                        if ($parent->parent) {
                                            $grandParent = get_term_by('id', $parent->parent, 'product_cat');
                                            $catName .= ' / ' . $grandParent->name;
                                        }
                                    }
                                    ?>
                                    <option value="<?=$category->term_id?>" <?=($category->term_id == $record['localId1']) ? 'selected':''?>><?=$catName?></option>
                                <?php endforeach; ?></select></td>
                        <td><select name="localId2[]" style="width: 350px"><option value="0">-- Izaberi kategoriju --</option>
                                <option value="false" <?=('false' == $record['localId2']) ? 'selected':''?>>-- Ignoriši --</option>
                                <?php foreach ($categories as $category):
                                    $catName = $category->name;
                                    if ($category->parent) {
                                        $parent = get_term_by('id', $category->parent, 'product_cat');
                                        $catName .= ' / ' . $parent->name;
                                    }
                                    ?>
                                    <option value="<?=$category->term_id?>" <?=($category->term_id == $record['localId2']) ? 'selected':''?>><?=$catName?></option>
                                <?php endforeach; ?></select></td>
                    </tr>
                <?php endforeach; endif;  ?>
            </tbody>
        </table>

    </form>
