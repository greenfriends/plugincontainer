<?php

namespace PluginContainer\Packages\FeedImport\Model\Parser;


use GuzzleHttp\Psr7\Request;
use http\Exception\RuntimeException;
use PluginContainer\Packages\FeedImport\Model\Parser;
use PluginContainer\Packages\FeedImport\Model\Product;

class Comtrade extends Parser
{
    const CACHE_KEY_CREATE = ENVIRONMENT . 'importFeedQueueCreate:comtrade:';
    const CACHE_KEY_UPDATE = ENVIRONMENT . 'importFeedQueueUpdate:comtrade:';
    const SUPPLIER_ID = 5;

    private $catLog = [];
    protected $useMapping = true;

    protected $categories = [];
    protected $products = [];

    protected $user = 'diteh';
    protected $pass = '14D3i8T8e9h32';
    protected $source = 'https://www.ct4partners.com/ws/ctproductsinstock.asmx';

    /**
     * @todo note: Items are already parsed b'cause of CT. so just appending cat data and postId here.
     *
     * @param $productElement
     * @param $postId
     * @return Product
     * @throws \Exception
     */
    public function parseSource($productElement, $postId = false)
    {
//        var_dump($productElement);
//        die();
        if ($productElement['supplierSku'] === '') {
            throw new \Exception('No remote id provided for ' . $productElement['name']);
        }
        $productElement['categoryIds'] = $this->parseCategories($productElement['categoryIds']);
        $productElement['postId'] = ($postId) ?: '';
        $p = new Product($productElement);

        return $p;
    }

    /**
     * @return \SimpleXMLElement
     */
    protected function fetchItems()
    {
        $simpleXml = new \SimpleXMLElement($this->getCategories());
        libxml_use_internal_errors(true);
        $categoriesData = $simpleXml->xpath('//soap:Body')[0]->GetCTProductGroups_WithAttributesResponse
            ->GetCTProductGroups_WithAttributesResult->ProductGroup;
        $breakAfter = 0;
        $productsData = [];
        $categories = [];
        foreach ($categoriesData as $categoryElement) {
            $xml = $this->getAllProducts((string) $categoryElement->Code)->getContents();
            $simpleXml = new \SimpleXMLElement($xml);
            $products = $simpleXml->xpath('//soap:Body')[0]->GetCTProducts_WithAttributesResponse
                ->GetCTProducts_WithAttributesResult->CTPRODUCT;
            $categoryCode = (string) $categoryElement->Code;
            if (!array_key_exists($categoryCode, $categories)) {
                $categories[$categoryCode] = $categoryElement;
            }
            $catAttributes = [];
            foreach ($categoryElement->Attributes->ProductAttribute as $catAttr) {
                $catAttributes[(string) $catAttr->AttributeCode] = (string) $catAttr->AttributeName;
            }
            foreach ($products as $productElement) {
                $qty = (int) str_replace(['>', '<'], '', (string) $productElement->QTTYINSTOCK);
                // @TODO change status to out of stock
                if ($qty === 0) {
                    continue;
                }

//                if (strstr((string) $productElement->NAME, 'MICROSOFT Surface Headphone')) {
//                    var_dump($productElement->QTTYINSTOCK);
//                    var_dump($qty);
//                    die();
//                }

//                if ($breakAfter > 200) {
//                    break(2);
//                }
//                $breakAfter++;

                $productTitle = (string) $productElement->NAME;
                // not all items have barcode... !@#$!@
                $vendorCode = (string) $productElement->BARCODE;
                $eurExchangeRate = (string) $productElement->EUR_ExchangeRate;
                $exchangeRateFormat = str_replace(',', '.', $eurExchangeRate);
                $formatedExchangeRate = round($exchangeRateFormat, 2);

                $price = ceil($productElement->PRICE * $formatedExchangeRate);
//                $specialPrice = ceil($productElement->RETAILPRICE * $formatedExchangeRate);
                $specialPrice = '';
                $category = (string) $categoryElement->GroupDescription;
                $warranty = (string) $productElement->WARRANTY;
                $description = (string) $productElement->SHORT_DESCRIPTION;
                if ($description === '') {
                    $description = sprintf('%s %s %s', $productTitle, $vendorCode, $warranty);
                } else {
                    $description = $description . sprintf(' %s %s', $vendorCode, $warranty);
                }
                $images = [];
                foreach ($productElement->IMAGE_URLS->URL as $url) {
                    $images[] = (string) $url;
                }

                $attributes = [];
                if ($productElement->ATTRIBUTES->ATTRIBUTE) {
                    foreach ($productElement->ATTRIBUTES->ATTRIBUTE as $itemAttr) {
                        $code = trim((string) $itemAttr->AttributeCode);
                        $value = trim((string) $itemAttr->AttributeValue);
                        if (in_array($code, ['ADDSPECIFICATIONS', 'Dodatno'])) {
                            continue;
                        }
                        if (strlen($value) > 70) {
                            continue;
                        }
                        if($value === '') {
                            continue;
                        }
                        if(strpos($value, 'http') !== false || strpos($value, 'ftp') !== false) {
                            continue;
                        }
                        $attributes[$catAttributes[$code]] = (string) $itemAttr->AttributeValue;
                    }
                }
                if (count($attributes) > 0) {
                    $specs = '<br /><ul>';
                    foreach ($attributes as $key => $value) {
                        $specs .= '<li>' . sprintf('%s: %s', $key, $value) . '</li>';
                    }
                    $specs .= '</ul><br />';
                    $description .= $specs;
                }
                if (!in_array($category, $this->sourceCategories)) {
                    $this->sourceCategories[] = $category;
                }

                $data = [
                    'sku' => (string) $productElement->CODE,
                    'postId' => '',
                    'supplierSku' => (string) $productElement->CODE,
                    'supplierId' => self::SUPPLIER_ID,
//                    'categoryIds' => $this->findCategory($category),
                    'categoryIds' => $category,
                    'name' => (string) $productElement->NAME,
                    'status' => 'publish',
                    'shortDescription' => '',
                    'description' => $description,
                    'images' => implode(',', $images),
                    'inputPrice' => $price,
                    'regularPrice' => $price,
                    'salePrice' => ($specialPrice == 0) ? '' : $specialPrice,
                    'stockStatus' => 'instock',
                    'pdv' => 20,
                    'postPaid' => 1,
                    'manufacturer' => (string) $productElement->MANUFACTURER,
                    'type' => 'simple',
                    'attributes' => $attributes,
                    'weight' => '',
                ];
                $productsData[] = $data;
            }
        }
        $this->categories = $categories;
        $this->products = $productsData;
    }

    private function parseCategories($category)
    {
        $catId = null;
        foreach ($this->mappedCategories as  $item) {
            if ($category === $item['source1']) {
                $catId = $item['localId1'];
            }
        }
        if ($catId === 'false') {
            throw new \Exception(sprintf('Category ignored: %s.', $category));
        }
        if ((int) $catId === 0) {
            throw new \Exception(sprintf('No category mapped for %s.', $category));
        }

        return $this->getCatTree($catId);
    }

    private function getCategories()
    {
        $headers = [
            'Content-Type' => 'application/soap+xml',
        ];
        $xml = $this->compileGetCategoriesWithAttributesXml();
        $request = new Request('POST', $this->source, $headers, $xml);
        $response = $this->getHttpClient()->send($request);

        return $response->getBody()->getContents();
    }

    private function getAllProducts(string $categoryCode)
    {
        $headers = [
            'Content-Type' => 'application/soap+xml',
        ];
        $xml = $this->compileGetProductsWithAttributesXml($categoryCode);
        $request = new Request('POST', $this->source, $headers, $xml);
        $response = $this->getHttpClient()->send($request);

        return $response->getBody();
    }

    private function compileGetProductsWithAttributesXml($categoryCode)
    {
        return sprintf('<?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
             <soap12:Body>
               <GetCTProducts_WithAttributes xmlns="http://www.ct4partners.com/B2B">
                 <username>%s</username>
                 <password>%s</password>
                 <productGroupCode>%s</productGroupCode>
               </GetCTProducts_WithAttributes>
             </soap12:Body>
            </soap12:Envelope>', $this->user, $this->pass, $categoryCode);
    }

    private function compileGetCategoriesWithAttributesXml()
    {
        return sprintf('<?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
             <soap12:Body>
               <GetCTProductGroups_WithAttributes xmlns="http://www.ct4partners.com/B2B">
                 <username>%s</username>
                 <password>%s</password>
               </GetCTProductGroups_WithAttributes>
             </soap12:Body>
            </soap12:Envelope>', $this->user, $this->pass);
    }
}