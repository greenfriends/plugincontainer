<?php

namespace PluginContainer\Packages\FeedImport\Model\Parser;

interface ParserInterface
{
    function processItems();

    function parseSource($data, $postId = null);
}