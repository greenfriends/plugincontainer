<?php

namespace PluginContainer\Packages\FeedImport\Model\Parser;

use PluginContainer\Packages\FeedImport\Model\Product;
use GuzzleHttp\Psr7\Request;
use PluginContainer\Packages\FeedImport\Model\Parser;

class Ctc extends Parser
{
    const CACHE_KEY_CREATE = ENVIRONMENT . 'importFeedQueueCreate:ctc:';
    const CACHE_KEY_UPDATE = ENVIRONMENT . 'importFeedQueueUpdate:ctc:';
    const SUPPLIER_ID = 18;

    protected $useMapping = true;
    protected $source = 'https://b2b.ctc-unit.com/GetSalesItems.aspx?apiKey=650-655424960';

    protected function parseSource($product, $postId = null)
    {
        $status = 'publish';
        $stock_status = 'instock';
        $type = 'simple';
        if((int)$product->Stock <= 0) {
            $stock_status = 'outofstock';
        }
        $name = (string) $product->Name;
        $vendorId = (string) trim($product->Identifier);
        if (!$this->checkIgnoredItems($name, $postId)) {
            $status = 'draft';
        }
        $categories = explode('/', $product->Category);
        $imageUrl = 'https://' . $product->ImageUlr; // mind the typo !
        $imageUrl = str_replace('\\','/', $imageUrl);
        $description = $product->Description;
        $manufacturer = '';
        foreach ($product->ItemAttributes as $itemAttribute){
            if ($itemAttribute->Name === 'Barcode') {
                continue;
            }
            if ($itemAttribute->Name === 'BREND') {
                $manufacturer = $itemAttribute->Value;
            }
            $description .= PHP_EOL . $itemAttribute->Name . ': ' .  $itemAttribute->Value;
        }
        $catString = implode('###', $categories);
        if (!in_array($catString, $this->sourceCategories)) {
            $this->sourceCategories[] = $catString;
        }
        $categories = $this->parseCategories($categories);

        //category mapping template
//        if (!in_array($categories[0] . $categories[1], $this->catLog)) {
//            $this->catLog[] = $categories[0] . $categories[1];
//            echo $categories[0] .','. $categories[1] . PHP_EOL;
//        }
//        echo trim($product->Identifier) .','. $product->Name . PHP_EOL;

        $dto = [
            'sku' => '',
            'postId' => $postId,
            'supplierSku' => $vendorId,
            'supplierId' => self::SUPPLIER_ID,
            'categoryIds' => $categories,
            'name' => $name,
            'status' => $status,
            'shortDescription' => '',
            'description' => $description,
            'images' => $imageUrl,
//            'regularPrice' => ceil($product->Price + $product->Price * 0.30),
            'regularPrice' => $product->Price,
            'salePrice' => '',
            'inputPrice' => $product->Price,
            'stockStatus' => $stock_status,
            'pdv' => 20,
            'postPaid' => '',
            'manufacturer' => $manufacturer,
            'boja' => '',
            'type' => $type,
            'velicina' => '',
            'options' => '',
            'weight' => 0.1,
            'quantity' => 0,
            'attributes' => []
        ];
        return new Product($dto);
    }

    private function checkIgnoredItems($name, $postId)
    {
        $ignoredItems = [
            'Čaša', 'Čaše', 'čaša', 'čaša', 'čaše', 'Casa', 'CASA', 'CASE', 'casa', 'case', 'tegla', 'Dekanter', 'dekanter',
            'ČŠA', 'stakleni', 'staklena', 'stakla', 'Čša'
        ];
        $ignoredIds = [
            592470, 592204, 593378, 593276, 592173, 593207, 593079, 593045, 592991, 592951, 592823, 592732, 592718,
            593023, 593079, 592993, 592776
        ];
        foreach ($ignoredItems as $item) {
            if (mb_strstr($name, $item)) {
                return false;
            }
        }
//        if (in_array($postId, $ignoredIds)) {
//            return false;
//        }

        return true;
    }

    private function parseCategories($categories)
    {
        $catId = null;
        $cats = '';
        foreach ($this->mappedCategories as $item) {
            if ($item['localId1'] == 0) {
                continue;
            }
            if ($item['source1'] === $categories[0] && $item['source2'] === $categories[1]) {
                if ((int) $item['localId2']) {
                    $cats .= $this->getCatTree($item['localId2']) . ',';
                }
//                var_dump($cats);
                $cats .= $this->getCatTree($item['localId1']);
            }
        }
        if (trim($cats) === '') {
            throw new \Exception('No category mapped for this item.');
        }

        return $cats;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function fetchItems()
    {
        $response = $this->getHttpClient()->send(new Request('get', $this->source));
        $this->products = json_decode($response->getBody()->getContents());
    }


}