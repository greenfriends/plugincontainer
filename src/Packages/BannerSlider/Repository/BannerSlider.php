<?php


namespace PluginContainer\Packages\BannerSlider\Repository;


use PluginContainer\Core\Repository\BaseRepository;

class BannerSlider extends BaseRepository
{
    protected $mapper;

    public function __construct(\PluginContainer\Packages\BannerSlider\Mapper\BannerSlider $mapper)
    {
        parent::__construct(\PluginContainer\Packages\BannerSlider\Model\BannerSlider::class);
        $this->mapper = $mapper;
    }


    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }
}