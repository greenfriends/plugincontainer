<?php


namespace PluginContainer\Packages\BannerSlider\Model;


class BannerSlider
{
    private $id;
    private $shortcode;
    private $images;
    private $options;

    public function __construct(string $shortcode, string $images, string $options, ?int $id = null)
    {
        $this->shortcode = $shortcode;
        $this->images = $images;
        $this->options = $options;
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getShortcode(): string
    {
        return $this->shortcode;
    }

    /**
     * @return string
     */
    public function getImages(): string
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getOptions(): string
    {
        return $this->options;
    }

}