<?php


namespace PluginContainer\Packages\BannerSlider\Mapper;


use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class BannerSlider extends BaseMapper
{
    public function __construct(\PDO $pdo, Logger $logger)
    {
        parent::__construct($pdo, $logger, 'gfBannerSlider');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
     		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
     		  `shortcode` TEXT,	
     		  `images` TEXT,
              `options` TEXT,
     		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     		  PRIMARY KEY  ($this->primaryKey)
     		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

}