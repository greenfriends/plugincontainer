export default class BannerSlider {
    initProps(container) {
        this.bannerContainer = container;
        this.bannerImageContainers = [];
        if(this.bannerContainer) {
            this.bannerImageContainers = container.querySelectorAll('.bannerSliderImageContainer');
            this.previousButton = container.querySelector('.previousBannerSlider');
            this.nextButton = container.querySelector('.nextBannerSlider');
            this.numberOfSlides = this.bannerImageContainers.length;
            this.bannerFlexContainer = container.querySelector('.bannerFlexContainer');
            this.activeSlide = 1;
            this.initialX = null;
            this.initialY = null;
            this.swipeThreshold = 20;
            this.interval = null;
            this.activeDotClass = 'active';
            this.dotClass = 'sliderDot';
            this.dots = container.querySelectorAll(`.${this.dotClass}`);
        }
        return this.bannerImageContainers.length > 0;
    }

    init() {
        if(this.numberOfSlides > 1) {
            this.startSlide();
            this.setPauseListeners();
            this.addPreviousAndNextListeners();
            this.addDottedPaginationListeners();
        }
    }

    setPauseListeners() {
        this.bannerContainer.addEventListener('mouseenter', () => {
           clearInterval(this.interval);
        });

        this.bannerContainer.addEventListener('mouseleave', () => {
            this.startSlide();
        });
    }

    startSlide() {
        clearInterval(this.interval);
        this.interval = setInterval(() => {
           this.nextSlide();
        },5000);
    }

    addPreviousAndNextListeners() {
        this.previousButton.addEventListener('click',() => {
            this.previousSlide();
        });
        this.nextButton.addEventListener('click',() => {
            this.nextSlide();
        });

        this.bannerFlexContainer.addEventListener('touchstart', this.setInitialSwipeCoordinates.bind(this));
        this.bannerFlexContainer.addEventListener('touchend', this.handleGesture.bind(this));
    }

    setInitialSwipeCoordinates(e) {
        clearInterval(this.interval);
        this.initialX = e.changedTouches[0].screenX;
        this.initialY = e.changedTouches[0].screenY;
    }

    handleGesture(e) {
        this.startSlide();
        let finalX = e.changedTouches[0].screenX;
        let finalY = e.changedTouches[0].screenY;
        if (finalX + this.swipeThreshold < this.initialX) {
            this.nextSlide();
        }
        if (finalX  > this.initialX + this.swipeThreshold) {
            this.previousSlide();
        }
        // if (finalY < this.initialY) {
        //     return;
        // }
        // if (finalY > this.initialY) {
        //     return;
        // }
    }

    previousSlide() {
        this.removeDottedPaginationActiveById(this.activeSlide);
        if(this.activeSlide === 1) {
            this.activeSlide = this.numberOfSlides + 1;
        }
        this.activeSlide--;
        this.addDottedPaginationActiveById(this.activeSlide);
        this.bannerFlexContainer.style.transform = `translate(-${this.getSlideWidth() * (this.activeSlide - 1)}px, 0)`;
    }

    nextSlide() {
        this.removeDottedPaginationActiveById(this.activeSlide);
        if(this.activeSlide === this.numberOfSlides) {
            this.activeSlide = 0;
        }
        this.activeSlide++;
        this.addDottedPaginationActiveById(this.activeSlide);
        this.bannerFlexContainer.style.transform = `translate(-${this.getSlideWidth() * (this.activeSlide - 1)}px, 0)`;
    }

    setSlide(index) {
        clearInterval(this.interval);
        this.removeDottedPaginationActiveById(this.activeSlide);
        this.activeSlide = parseInt(index);
        this.addDottedPaginationActiveById(this.activeSlide);
        this.bannerFlexContainer.style.transform = `translate(-${this.getSlideWidth() * (this.activeSlide - 1)}px, 0)`;
        this.startSlide();
    }

    getSlideWidth() {
        return this.bannerImageContainers[0].getBoundingClientRect().width;
    }

    removeDottedPaginationActiveById(id) {
        let target = this.dotClass + id;
        this.bannerContainer.querySelector(`.${target}`).classList.remove(this.activeDotClass);
    }

    addDottedPaginationActiveById(id) {
        let target = this.dotClass + id;
        this.bannerContainer.querySelector(`.${target}`).classList.add(this.activeDotClass);
    }

    addDottedPaginationListeners() {
        this.dots.forEach((dot) => {
           dot.addEventListener('click', () => {
               this.setSlide(dot.getAttribute('data-index'));
           });
        });
    }
}