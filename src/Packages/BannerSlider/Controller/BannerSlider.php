<?php


namespace PluginContainer\Packages\BannerSlider\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Components\CrudTable\Column;
use PluginContainer\Core\Components\CrudTable\CrudTable;
use PluginContainer\Core\Components\CrudTable\FormField;
use PluginContainer\Core\Components\CrudTable\Traits\CrudTable as CrudTableTrait;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithCrudTableInterface;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;

class BannerSlider extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface, ControllerWithCrudTableInterface
{
    use CrudTableTrait;

    /**
     * @var \PluginContainer\Packages\BannerSlider\Repository\BannerSlider
     */
    private $repo;

    /**
     * @var CacheWrapper
     */
    private $cache;

    public function __construct(Feature $featureRepo, Logger $logger,\PluginContainer\Packages\BannerSlider\Repository\BannerSlider $repo,
    CacheWrapper $cache)
    {
       parent::__construct($featureRepo, $logger);
       $this->repo = $repo;
       $this->cache = $cache;
    }

    public function create(): AjaxResponse
    {
       $data = $_GET;
       $this->repo->create($data);
       return new AjaxResponse(true, 'Successfully created Banner Slider');
    }

    /**
     * @throws \Exception
     */
    public function update(): AjaxResponse
    {
        $data = $_GET;
        $this->repo->update($data);
        $this->resetCacheForBannerSlider($data['shortcode']);
        return new AjaxResponse(true, 'Successfully updated Banner Slider');

    }

    /**
     * @throws \Exception
     */
    public function delete(): AjaxResponse
    {
        $this->repo->delete($_GET['id']);
        return new AjaxResponse(true, 'Successfully deleted Banner Slider');
    }

    /**
     * @throws \Exception
     */
    public function getFormFields(): AjaxResponse
    {
        $method = 'create';
        $shortcode = null;
        $formHeader = 'Create Banner Slider';
        $images = new FormField('images', 'hidden', '');
        $options = new FormField('options', 'hidden', '');
        $imageUpload = new FormField('image', 'wpImageUploadMultiple', __('Choose images', 'plugin-container'), null, '');

        if (isset($_GET['id'])) {
            $bannerSlider = $this->repo->getById((int)$_GET['id']);
            if($bannerSlider) {
                $method = 'update';
                $formHeader = 'Update Banner Slider';
                $shortcode = $bannerSlider->getShortcode();
                $images = new FormField('images', 'hidden', '', $bannerSlider->getImages());
            } else {
                throw new \Exception('Banner Slider you are trying to update does not exist');
            }
        }
        $shortcodeField = new FormField('shortcode', 'hidden', __('Shortcode', 'plugin-container'),
                                        $shortcode ?? uniqid('', false), 'Auto generated shortcode', true);
        $response = new AjaxResponse(true);
        $response
            ->addParamToBody('data',
                             [$images, $options, $imageUpload, $shortcodeField])
            ->addParamToBody('formHeader', $formHeader)
            ->addParamToBody('method', $method)
            ->addParamToBody('submitLabel', __('Submit', 'plugin-container'));
        return $response;


    }

    /**
     * @throws \Exception
     */
    public function getView(): AjaxResponse
    {
        $id = new Column('id', 'Id');
        $shortcode = new Column('shortcode','Shortcode');
        $response = new AjaxResponse(true);
        $response->addParamToBody('data',[(new CrudTable([$id,$shortcode], $this->repo))->getList()]);
        return $response;
    }

    public function init(): void
    {
        add_action('admin_enqueue_scripts', function ()
        {
            wp_enqueue_media();
        });

        add_shortcode('gf_bannerslider', [$this, 'shortCodeView']);
    }

    public function shortCodeView($args) {
        if (isset($args['id']) && $args['id'] !== '') {
            $html = $this->cache->get('banner_slider_' . $args['id']);
            if (!$html) {
                $bannerSliderData = $this->repo->getBy('shortcode', $args['id'])[0] ?? null;
                if ($bannerSliderData) {
                    $imagesData = $bannerSliderData->getImages();
                    $imagesData = explode(';', $imagesData);
                    $html = '<div class="bannerSliderContainer"><div class="bannerFlexContainer">';
                    $numberOfImages = 0;
                    foreach ($imagesData as $key => $imageData) {
                        $imageInfo = explode(',', $imageData);
                        if(isset($imageInfo[0], $imageInfo[1])) {
                            $hidden = '';
                            $numberOfImages++;
                            if($key !== 0) {
                                $hidden = ' hiddenImage';
                            }
                            $html .= '<div class="bannerSliderImageContainer' . $hidden .'">
                                        <a target="_blank" href="' . $imageInfo[1] . '" title="' . $imageInfo[1] .'">
                                             <img src="' . $imageInfo[0] . '" alt="banner image">
                                        </a>
                                      </div>';
                        }
                    }
                    $dots = '<div id="sliderDotsContainer">';
                    for($i = 1; $i <= $numberOfImages; $i++) {
                        $active = '';
                        if($i === 1) {
                            $active = ' active';
                        }
                        $dots .= '<span class="sliderDot' . $active . ' sliderDot' . $i .'"  data-index="' . $i .'"></span>';
                    }
                    $dots .= '</div>';
                    $html .= '</div>';
                    if($numberOfImages > 1) {
                        $html .= '<div class="previousBannerSlider"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" />
                    </svg></div>';
                        $html .= '<div class="nextBannerSlider"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" />
                </svg></div>';
                        $html .= $dots;
                    }
                    $html .= '</div>';
                    $this->cache->set('banner_slider_' . $args['id'], $html, 600);
                    }
            }
            return $html;
        }
        return null;
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->repo->createTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->repo->deleteTable();
    }

    /**
     * @param string $bannerSliderId
     * @return void
     */
    public function resetCacheForBannerSlider(string $bannerSliderId): void
    {
        $this->cache->delete('banner_slider_' . $bannerSliderId);
    }

}