import Tab from "../../../../../assets/admin/js/Tab.js";

export default class RoleManager extends Tab {
    init() {
        this.form = document.getElementById('editUserInfo')
        this.addEventListeners();
    }

    addEventListeners() {
        this.form.addEventListener('submit', (e) => {
            this.startLoader();
            e.preventDefault();
            let formData = new FormData(e.target);
            let params = [...formData.entries()];
            this.fetchResponsePost(params,e.target.getAttribute('data-method'), this.container).then((response) => {
                this.activeTab.click();
                this.printNotice(response);
            })
            this.removeLoader();
        })
    }
}