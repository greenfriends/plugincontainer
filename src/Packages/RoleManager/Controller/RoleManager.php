<?php


namespace PluginContainer\Packages\RoleManager\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;

class RoleManager extends BaseController implements ControllerWithSettingPageInterface
{

    public function getView()
    {
        $response = new AjaxResponse(true,'');
        $html = '<form data-method="maybeChangeUserToDeveloper" id="editUserInfo"><label> Unesi email usera za koga zelis da dodas role developer
        <input type="text" name="userEmail"></label><input type="submit" value="Submit"></form>';
        $response->addParamToBody('data', $html);
        return $response;
    }

    /**
     * @return AjaxResponse
     * @route Ajax
     */
    public function maybeChangeUserToDeveloper(): AjaxResponse
    {
        $userEmail = $_POST['userEmail'] ?? '';
        if ($userEmail) {
            $user = get_user_by('email', $userEmail);
            if ($user){
                $user->add_role('developer');
                return new AjaxResponse(true, 'Uspesno dodat role developer, WOOOOOOO!!!!');
            }
        }

        return new AjaxResponse(false, 'Ne postoji user sa ovim emailom.');
    }
}