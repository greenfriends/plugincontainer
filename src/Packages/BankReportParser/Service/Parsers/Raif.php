<?php

namespace PluginContainer\Packages\BankReportParser\Service\Parsers;

use Exception;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\WcOrderStatusFlow\Service\StatusFlowService;

class Raif extends BaseReportParser implements ParserInterface
{

    private $filePath;

    public function __construct(string $filePath, Logger $logger, StatusFlowService $statusFlowService)
    {
        parent::__construct($logger, $statusFlowService);
        $this->filePath = $filePath;
    }

    /**
     * @return AjaxResponse
     * @throws Exception
     */
    public function parseBankReport(): AjaxResponse
    {
        $xml = simplexml_load_string(file_get_contents($this->filePath));
        if ($xml) {
            $statementNumber = $xml->Zaglavlje['BrojIzvoda'];
            foreach ($xml->Stavke as $column) {
                //We are only looking for received payments
                if ((int)$column['Potrazuje'] !== 0) {
                    $referenceNumber = (int)$column['PozivNaBrojKorisnika'];
                    $order = wc_get_order($referenceNumber);
                    $receivedTotal = number_format((float)$column['Potrazuje'], 2, '.', '');
                    if ($order) {
                        $orderStatus = $order->get_status();
                        if ($orderStatus === 'pending' || $orderStatus === 'delivered') {
                            $orderTotal = number_format($order->get_total(), 2, '.', '');
                            if ($orderTotal === $receivedTotal) {
                                $nextStatus = $this->statusFlowService->getNextStatusInFlow($order, $this);
                                $order->set_status($nextStatus,__(sprintf('STATEMENT number %s', $statementNumber), 'plugin-container'));
                                $order->save();
                                $resolvedPayment = new ResolvedPayment(
                                    $column['NalogKorisnik'], $column['SifraPlacanja'], $referenceNumber,
                                    $receivedTotal, $order->get_id(), $order->get_edit_order_url()
                                );
                                $this->addResolvedPaymentToArray($resolvedPayment);
                            } else {
                                $unresolvedPayment = new UnresolvedPayment(
                                    $column['NalogKorisnik'], $column['SifraPlacanja'], $referenceNumber,
                                    $receivedTotal);
                                $this->addUnresolvedPaymentToArray($unresolvedPayment);
                            }
                        }
                    } else {
                        $unresolvedPayment = new UnresolvedPayment(
                            $column['NalogKorisnik'], $column['SifraPlacanja'], $referenceNumber, $receivedTotal);
                        $this->addUnresolvedPaymentToArray($unresolvedPayment);
                    }
                }
            }
            $response = new AjaxResponse(true, 'File successfully parsed');
            $ordersWaitingPayment = wc_get_orders([
                'status' => ['wc-pending', 'wc-delivered'],
                'payment_method' => ['BACS', 'COD'],
            ]);

            ob_start();
            if (count($this->getResolvedPayments()) > 0) {
                include __DIR__ . '/../../templates/resolvedPayments.php';
            }
            if (count($this->getUnresolvedPayments()) > 0) {
                include __DIR__ . '/../../templates/unresolvedPayments.php';
            }
            if (count($ordersWaitingPayment) > 0) {
                include __DIR__ . '/../../templates/allUnpaidOrders.php';
            }
            $response->addParamToBody('data', ob_get_clean());
            $this->logger->debug('Parsing of statement complete', ['statementNumber' => $statementNumber]);
            return $response;
        }
        throw new Exception('Error in loading xml');
    }
}