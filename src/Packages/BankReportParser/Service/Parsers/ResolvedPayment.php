<?php

namespace PluginContainer\Packages\BankReportParser\Service\Parsers;

class ResolvedPayment
{
    /**
     * @var string
     */
    public $senderName;
    /**
     * @var string
     */
    public $paymentCode;
    /**
     * @var string
     */
    public $referenceNumber;
    public $amount;
    /**
     * @var int
     */
    public $orderId;
    /**
     * @var string
     */
    public $orderEditUrl;

    /**
     * @param string $senderName
     * @param string $paymentCode
     * @param string $referenceNumber
     * @param int $amount
     * @param int $orderId
     * @param string $orderEditUrl
     */
    public function __construct(string $senderName, string $paymentCode, string $referenceNumber, int $amount, int $orderId, string $orderEditUrl)
    {
        $this->senderName = $senderName;
        $this->paymentCode = $paymentCode;
        $this->referenceNumber = $referenceNumber;
        $this->amount = $amount;
        $this->orderId = $orderId;
        $this->orderEditUrl = $orderEditUrl;
    }
}