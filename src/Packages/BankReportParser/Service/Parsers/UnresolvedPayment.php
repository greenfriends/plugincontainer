<?php

namespace PluginContainer\Packages\BankReportParser\Service\Parsers;

class UnresolvedPayment
{
    /**
     * @var string
     */
    public $senderName;
    /**
     * @var string
     */
    public $paymentCode;
    /**
     * @var string
     */
    public $referenceNumber;
    public $amount;

    /**
     * @param string $senderName
     * @param string $paymentCode
     * @param string $referenceNumber
     * @param $amount
     */
    public function __construct(string $senderName, string $paymentCode, string $referenceNumber, $amount)
    {
        $this->senderName = $senderName;
        $this->paymentCode = $paymentCode;
        $this->referenceNumber = $referenceNumber;
        $this->amount = $amount;
    }
}