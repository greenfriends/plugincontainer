<?php

namespace PluginContainer\Packages\BankReportParser\Service\Parsers;

interface ParserInterface
{
    function parseBankReport();
}