<?php

namespace PluginContainer\Packages\BankReportParser\Service\Parsers;

use PluginContainer\Core\Logger\Logger;
use PluginContainer\Packages\WcOrderStatusFlow\Service\StatusFlowService;

class BaseReportParser
{

    /**
     * @var UnresolvedPayment[]
     */
    private $unresolvedPayments = [];

    /**
     * @var ResolvedPayment[]
     */
    private $resolvedPayments = [];

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var StatusFlowService
     */
    protected $statusFlowService;

    public function __construct(Logger $logger, StatusFlowService $statusFlowService)
    {
        $this->logger = $logger;
        $this->statusFlowService = $statusFlowService;
    }

    /**
     * @param UnresolvedPayment $payment
     */
    protected function addUnresolvedPaymentToArray(UnresolvedPayment $payment): void
    {
        $this->unresolvedPayments[] = $payment;
    }

    /**
     * @return UnresolvedPayment[]
     */
    public function getUnresolvedPayments(): array
    {
        return $this->unresolvedPayments;
    }

    /**
     * @param ResolvedPayment $payment
     */
    protected function addResolvedPaymentToArray(ResolvedPayment $payment): void
    {
        $this->resolvedPayments[] = $payment;
    }

    /**
     * @return ResolvedPayment[]
     */
    public function getResolvedPayments(): array
    {
        return $this->resolvedPayments;
    }
}