<?php

namespace PluginContainer\Packages\BankReportParser\Controller;

use Exception;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Controller\{BaseController, ControllerWithConfigInterface, ControllerWithSettingPageInterface};
use PluginContainer\Core\Cache\CacheConnection;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\BankReportParser\Service\Parsers\Raif;
use PluginContainer\Packages\WcOrderStatusFlow\Service\StatusFlowService;

class BankReportParser extends BaseController implements ControllerWithSettingPageInterface
{
    /**
     * @var StatusFlowService
     */
    private $statusFlowService;

    public function __construct(Feature $featureRepo, Logger $logger, StatusFlowService $statusFlowService)
    {
        parent::__construct($featureRepo, $logger);
        $this->statusFlowService = $statusFlowService;
    }

    /**
     * @return AjaxResponse
     */
    public function getView(): AjaxResponse
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/BankReportParser/templates/reportUpload.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    /**
     * @return AjaxResponse
     * @throws Exception
     */
    public function parseBankReport(): AjaxResponse
    {
        $this->logger->debug('Starting to parse bank report');
        $fileData = $_FILES['bankReport'];
        if ($fileData) {
            $filePath = $fileData['tmp_name'];
            $this->logger->debug('Parsing file',['filePath'=> $filePath]);
            //@todo get parser from some config
            $parser = new Raif($filePath, $this->logger, $this->statusFlowService);
            $this->logger->debug('Using parser',['parserClassName'=> Raif::class]);
            return $parser->parseBankReport();
        }
        throw new Exception('You must upload file');
    }

    /**
     * Used in BankReportPage.js addMarkOrderAsPaidListeners() function
     *
     * @throws Exception
     */
    public function markOrderAsPaid(): ?AjaxResponse
    {
        $orderId = $_GET['orderId'] ?? null;
        if ($orderId) {
            $order = wc_get_order($orderId);
            if ($order){
                $order->set_status('completed',
                    __(sprintf('Manual resolve STATEMENT number %s', $_GET['statementNumber'] ?? 'unknown')), 'plugin-container');
                $order->save();
                return new AjaxResponse(true, 'Successfully changed order status to completed');
            }
            throw new Exception('Order not found');
        }
        throw new Exception('Order id not found in request');
    }
}