<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\BankReportParser\Service\Parsers\Raif;

/** @var Raif $this */

/** @var array $allUnpaidOrders */

/** @see Raif::parseBankReport() */

?>
<h2><?=__('Unresolved payments', 'plugin-container')?></h2>
<table>
    <tr>
        <th><?=__('Sender name', 'plugin-container')?></th>
        <th><?=__('Payment code', 'plugin-container')?></th>
        <th><?=__('Reference number', 'plugin-container')?></th>
        <th><?=__('Amount', 'plugin-container')?></th>
    </tr>
    <?php
    foreach ($this->getUnresolvedPayments() as $unresolvedPayment): ?>
        <tr>
            <td><?=$unresolvedPayment->senderName?></td>
            <td><?=$unresolvedPayment->paymentCode?></td>
            <td><?=$unresolvedPayment->referenceNumber?></td>
            <td><?=$unresolvedPayment->amount.' '.get_woocommerce_currency_symbol()?></td>
        </tr>
    <?php
    endforeach; ?>
</table>
