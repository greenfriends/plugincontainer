<?php
/**
 * @see BankReportParser::getView()
 */
use PluginContainer\Core\WpBridge\Translations as T;
?>
<ol>
    <li><p><?=__('Upload bank statement', 'plugin-container')?></p></li>
    <li><p><?=__('Wait for program to finish', 'plugin-container')?></p></li>
    <li><p><?=__('If there was some unresolved payments you will see list of those payments 
    and also all unpaid orders so you can manually resolve them if needed.', 'plugin-container')?></p></li>
</ol>
<form id="bankReportForm" data-method="parseBankReport" enctype="multipart/form-data">
    <label>
        <?=__('Upload bank report', 'plugin-container')?>
        <input id="bankReportUpload" type="file" name="bankReport">
    </label>
    <button type="submit"><?=__('Upload', 'plugin-container')?></button>
</form>
<div id="dataContainer"></div>