<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\BankReportParser\Service\Parsers\Raif;

/** @var Raif $this */

/** @see Raif::parseBankReport() */

?>
<h2><?=__('Resolved payments', 'plugin-container')?></h2>
<table>
    <tr>
        <th><?=__('Sender name', 'plugin-container')?></th>
        <th><?=__('Payment code', 'plugin-container')?></th>
        <th><?=__('Reference number', 'plugin-container')?></th>
        <th><?=__('Amount', 'plugin-container')?></th>
        <th><?=__('OrderId', 'plugin-container')?></th>
    </tr>
    <?php
    foreach ($this->getResolvedPayments() as $resolvedPayment): ?>
        <tr>
            <td><?=$resolvedPayment->senderName?></td>
            <td><?=$resolvedPayment->paymentCode?></td>
            <td><?=$resolvedPayment->referenceNumber?></td>
            <td><?=$resolvedPayment->amount.' '.get_woocommerce_currency_symbol()?></td>
            <td><a href="<?=$resolvedPayment->orderEditUrl?>">#<?=$resolvedPayment->orderId?></a></td>
        </tr>
    <?php
    endforeach; ?>
</table>
