<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\BankReportParser\Service\Parsers\Raif;

/** @var WC_Order[] $ordersWaitingPayment */
/** @var string $statementNumber */
/** @see Raif::parseBankReport() */
?>
<h2><?=__('All unpaid orders', 'plugin-container')?></h2>
<table>
    <tr>
        <th><?=__('Order id', 'plugin-container')?></th>
        <th><?=__('Total', 'plugin-container')?></th>
        <th><?=__('Mark as resolved', 'plugin-container')?></th>
    </tr>
    <?php
    foreach ($ordersWaitingPayment as $order): ?>
        <tr>
            <td><a target="_blank" href="<?=$order->get_edit_order_url()?>">#<?=$order->get_id()?></a></td>
            <td><?=$order->get_total() .' '. get_woocommerce_currency_symbol()?></td>
            <td><input data-statementNumber="<?=$statementNumber?>" data-method="markOrderAsPaid" data-orderId="<?=$order->get_id()?>"
                       class="orderResolvedCheckbox" aria-label="checkbox" type="checkbox"></td>
        </tr>
    <?php
    endforeach; ?>
</table>
