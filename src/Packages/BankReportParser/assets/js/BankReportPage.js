import BaseFetcher from "../../../../../assets/front/js/BaseFetcher.js";

export default class BankReportPage extends BaseFetcher
{
    handlerClass = 'PluginContainerPackagesBankReportParserControllerBankReportParser';
    form = document.getElementById('bankReportForm');
    dataContainer = document.getElementById('dataContainer');
    fileInput = document.getElementById('bankReportUpload');
    action = 'gfAjaxRouter'

    init(){
        this.form.addEventListener('submit', async (e) => {
            this.startLoader(this.dataContainer)
            e.preventDefault();
            let form = e.target;
            let data = new FormData(form)
            await this.submitForm({method: e.target.getAttribute('data-method')}, data).then((response) => {
                this.printNotice(response);
                this.dataContainer.innerHTML = response.body.data;
                this.addMarkOrderAsPaidListeners();
            })
        })
    }

    async submitForm(url, data){
        let urlParams = new URLSearchParams(url)
        const req = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}&action=${this.action}&handlerClass=${this.handlerClass}`,
            {method:'POST', body:data});
        let reqText = await req.text();
        return JSON.parse(reqText);
    }
    addMarkOrderAsPaidListeners(){
        let checkboxes = document.querySelectorAll('.orderResolvedCheckbox');
        checkboxes.forEach((checkbox) => {
            checkbox.addEventListener('click', async(e) =>{
                await this.fetchResponse({
                    method: e.target.getAttribute('data-method'),
                    orderId: e.target.getAttribute('data-orderId'),
                    statementNumber: e.target.getAttribute('data-statementNumber')
                }).then(response =>{
                    if (response.success === true) {
                        e.target.parentElement.remove()
                    }
                    this.printNotice(response);
                });
            })
        })
    }
}