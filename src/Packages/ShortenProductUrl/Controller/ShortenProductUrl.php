<?php

namespace PluginContainer\Packages\ShortenProductUrl\Controller;

use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;

class ShortenProductUrl extends BaseController implements ControllerWithHooksInterface
{

    public function init(): void
    {
        $this->removeProductFromProductSlug();
    }

    private function removeProductFromProductSlug()
    {
        add_filter('post_type_link', static function ($postLink, $post)
        {
            if ('product' != $post->post_type || 'publish' != $post->post_status) {
                return $postLink;
            }
            return str_replace(['/product/', '/proizvod/'], '/', $postLink);
        }, 10, 2);

        add_action('pre_get_posts', static function ($query)
        {
            if (!$query->is_main_query() || 2 !== count($query->query) || !isset($query->query['page'])) {
                return;
            }
            if (!empty($query->query['name'])) {
                $query->set('post_type', ['post', 'product', 'page']);
            } elseif (!empty($query->query['pagename']) && false === strpos($query->query['pagename'], '/')) {
                $query->set('post_type', ['post', 'product', 'page']);
                // We also need to set the name query var since redirect_guess_404_permalink() relies on it.
                $query->set('name', $query->query['pagename']);
            }
        }, 99);
    }
}

