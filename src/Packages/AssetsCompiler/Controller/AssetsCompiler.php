<?php


namespace PluginContainer\Packages\AssetsCompiler\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\AjaxRouter\AjaxRouter;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;

class AssetsCompiler extends BaseController implements ControllerWithHooksInterface
{
    public function init(): void
    {
        add_action('wp_head', [$this, 'compileHeadScripts'], 1);
        add_action('wp_footer', [$this, 'compileFooterScripts'], 1);
        add_action('wp_head', [$this, 'compileStyles'], 1);
        add_action('admin_bar_menu', [$this, 'addClearCssMenuToAdminBar'], 100);

//        add_action('wp_head', [$this, 'compileHeadScripts'], 666666);
//        add_action('wp_footer', [$this, 'compileFooterScripts'], 666666);
//        add_action('after_setup_theme', [$this, 'compileScripts'], 1);
    }

    private function processHandle(\_WP_Dependency $wpDep, $handle)
    {
        $httpClient = new \GuzzleHttp\Client();
        $src = strtok($wpDep->src, '?');

        $string = PHP_EOL . '/** ' . $handle . ' */' . PHP_EOL;
        if (strpos($src, 'http') !== false || strpos($src, '//') !== false) {
            $site_url = site_url();
            if (strpos($src, $site_url) !== false) {
                $src = str_replace($site_url, '', $src);
                if (file_exists(ABSPATH . $src)) {
                    $string .= PHP_EOL . file_get_contents(ABSPATH . '..' . $src) . PHP_EOL;
                } else {
                    throw new \Exception('file not found. ' . $src);
                }
            } else {
                $response = $httpClient->send(new \GuzzleHttp\Psr7\Request('GET', $src));
                $string .= PHP_EOL . $response->getBody()->getContents() . PHP_EOL;
            }
        } else {
            if (file_exists(ABSPATH . $src)) {
                $string .= PHP_EOL . file_get_contents(ABSPATH . $src) . PHP_EOL;
            } else {
                throw new \Exception('file not found. ' . $src);
            }
        }

        return $string;
    }

    public function compileFooterScripts()
    {
        if (is_admin() || current_user_can('administrator')) {
            return;
        }

        /* @vara \WP_Script $wp_scripts */
        global $wp_scripts;

//        $wp_scripts->all_deps(['shoptimizer-main', 'wc-single-product', 'cookie-notice-front']);
        $targets = [
            'shoptimizer-main',
            'wc-single-product',
            'gfJs',
            'addtoany-jquery',
            'wc-add-to-cart',
            'js-cookie',
            'wc-cart-fragments',
            'mousewheel_js',
            'elementor-webpack-runtime',
            'elementor-frontend-modules',
            'elementor-waypoints'
        ];
        $compiledJs = '/uploads/compiledFooter.js';
        $compiledJsHead = '/uploads/compiledHead.js';
        $compiledJsPath = ABSPATH . '/wp-content' . $compiledJs;
        $mergedScript = '';

        $version = time();
//        if (file_exists($compiledCssPath) && !$compileOverrideActive) {
        /* @var \_WP_Dependency $wpDep */
        foreach ($wp_scripts->to_do as $handle) {
            $mergedScript .= $wp_scripts->get_data($handle, 'data');

//            if (!isset($wp_scripts->registered[$handle])) {
            if (!in_array($handle, $targets)) {
                continue;
            }
            $this->logger->debug(sprintf('Added script %s to compiled footer file.', $handle));
            $wpDep = $wp_scripts->registered[$handle];
            $before = $wp_scripts->get_data($handle, 'before');
            if ($before) {
                $mergedScript .= $before[1];
            }
            $mergedScript .= $this->processHandle($wpDep, $handle);
            $after = $wp_scripts->get_data($handle, 'after');
            if ($after) {
                $mergedScript .= $after[1];
            }
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        }

//            file_put_contents($compiledCssPath, str_replace('  ', ' ', $mergedStyleSheet));
        file_put_contents($compiledJsPath, $mergedScript);

//        add_action('wp_footer', function() use($compiledJs) {
//        wp_enqueue_script('merged-script',  get_stylesheet_directory_uri() . '/../../' . $compiledJsHead, [], $version);
//        wp_enqueue_script('merged-script',  get_stylesheet_directory_uri() . '/../../' . $compiledJs, [], $version);
        echo '<script id="merged-script" src="' . get_stylesheet_directory_uri() . '/../../' . $compiledJsHead . '"></script>';
        echo '<script id="merged-script" src="' . get_stylesheet_directory_uri() . '/../../' . $compiledJs . '"></script>';
//        });
    }

    public function compileHeadScripts()
    {
        if (is_admin() || current_user_can('administrator')) {
            return;
        }
        /* @vara \WP_Script $wp_scripts */
        global $wp_scripts;

        $wp_scripts->all_deps($wp_scripts->queue, true);
//        $wp_scripts->all_deps($wp_scripts->args, true);

        $compiledJs = '/uploads/compiledHead.js';
        $compiledJsPath = ABSPATH . '/wp-content' . $compiledJs;
        $mergedScript = '';
        $mergedScriptAfter = '';
        $ignoredScripts = [
            'gfJs',
            'wc-add-to-cart',
            'wc-cart-fragments',
            'js-cookie',
            'shoptimizer-main',
            'wc-single-product'
        ];

        $version = time();
//        if (file_exists($compiledCssPath) && !$compileOverrideActive) {
        /* @var \_WP_Dependency $wpDep */
        foreach ($wp_scripts->to_do as $handle) {
            if (strpos($handle, 'jquery') !== false) {
                continue;
            }
            if (in_array($handle, $ignoredScripts)) {
                continue;
            }
            $mergedScript .= $wp_scripts->get_data($handle, 'data');
            if (!isset($wp_scripts->registered[$handle])) {
                continue;
            }
            $wpDep = $wp_scripts->registered[$handle];
            $before = $wp_scripts->get_data($handle, 'before');
            if ($before) {
                $mergedScript .= $before[1];
            }
            $mergedScript .= $this->processHandle($wpDep, $handle);
            $this->logger->debug(sprintf('Added script %s to compiled header file.', $handle));
            $after = $wp_scripts->get_data($handle, 'after');
            if ($after) {
                $mergedScript .= $after[1];
            }
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        }

//            file_put_contents($compiledCssPath, str_replace('  ', ' ', $mergedStyleSheet));
        file_put_contents($compiledJsPath, $mergedScript . ' ' . PHP_EOL . ' ' . $mergedScriptAfter);
//        wp_enqueue_script('merged-script',  get_stylesheet_directory_uri() . '/../../' . $compiledJs, [], $version);

//        add_action('wp_footer', function() use($compiledJs) {
//            echo '<script id="merged-script" src="' . get_stylesheet_directory_uri() . '/../../' . $compiledJs .'"></script>';
//        }, 1);
    }

    public function compileStyles()
    {
        if (is_admin() || current_user_can('administrator')) {
            return;
        }
        global $wp_styles;

        $compileOverrideActive = false;
        /**
         * #1. Reorder the handles based on its dependency,
         * The result will be saved in the to_do property ($wp_scripts->to_do)
         */
        $wp_styles->all_deps($wp_styles->queue);
        $version = time();
//        $version = time();
        $ignoredStyles = [];
        $compiledCss = '/uploads/compiled.css';
        if (wp_is_mobile()) {
            $compiledCss = "/uploads/compiled-mobile.css";
        } else {
            wp_dequeue_style('woocommerce-smallscreen');
            wp_deregister_style('woocommerce-smallscreen');
        }
        $compiledCssPath = ABSPATH . '/wp-content' . $compiledCss;
        $mergedStyleSheet = '';

//        if (file_exists($compiledCssPath) && !$compileOverrideActive) {
        /* @var \_WP_Dependency $wpDep */
        foreach ($wp_styles->to_do as $handle) {
            if (in_array($handle, $ignoredStyles) || str_contains($handle,'elementor')) {
                continue;
            }
            $this->logger->debug(sprintf('Added stylesheet %s to compiled file.', $handle));
            $wpDep = $wp_styles->registered[$handle];
            try {
                $mergedStyleSheet .= $this->processHandle($wpDep, $handle);
            } catch(\Exception $e) {
                $this->logger->debug('Process Handle failed: ' . $e->getMessage());
            }
            wp_dequeue_style($handle);
            wp_deregister_style($handle);
        }
        if (!file_exists($compiledCssPath)) {
            file_put_contents($compiledCssPath, $mergedStyleSheet);
        }
        wp_enqueue_style('merged-styles', get_stylesheet_directory_uri() . '/../../' . $compiledCss, [], $version);
//            return;
//        }

//        die();

//        $merged_script	= '';
//        $httpClient = new \GuzzleHttp\Client();
//        foreach($wp_styles->to_do as $handle) {
//            if (in_array($handle, $ignoredStyles)) {
//                continue;
//            }
//            // Clean up url
//            $src = strtok($wp_styles->registered[$handle]->src, '?');
//            $js_file_path = $src;
//            $merged_script .= PHP_EOL .'/** '. $handle .' */'. PHP_EOL;
//            $this->logger->debug(sprintf('Added script %s to compiled file %s.', $src, $handle));
//            if (strpos($src, 'http') !== false || strpos($src, '//') !== false) {
//                $site_url = site_url();
//                if (strpos($src, $site_url) !== false) {
//                    $js_file_path = str_replace($site_url, '', $src);
//                    if (file_exists(ABSPATH . $js_file_path)) {
//                        $merged_script .= PHP_EOL . file_get_contents(ABSPATH .'..'. $js_file_path) . PHP_EOL;
//                    } else {
//                        throw new \Exception('file not found. ' . $js_file_path);
//                    }
//                } else {
//                    $response = $httpClient->send(new \GuzzleHttp\Psr7\Request('GET', $js_file_path));
//                    $merged_script .= PHP_EOL . $response->getBody()->getContents() . PHP_EOL;
//                }
//            } else {
//                if (file_exists(ABSPATH . $js_file_path)) {
//                    $merged_script .= PHP_EOL . file_get_contents(ABSPATH . $js_file_path) . PHP_EOL;
//                } else {
//                    throw new \Exception('file not found. ' . $js_file_path);
//                }
//            }
//            wp_dequeue_style($handle);
//            wp_deregister_style($handle);
//        }
//
//        file_put_contents($compiledJsPath, str_replace('  ', ' ', $merged_script));
//        wp_enqueue_style('merged-styles',  get_stylesheet_directory_uri() . '/../../' . $compiledJs, [], $version);
    }

    /**
     * Id is assets compiler because in the future it should be dropdown to clear js compiled files
     * @param $adminBar
     * @return void
     */
    public function addClearCssMenuToAdminBar($adminBar): void
    {
        $adminBar->add_menu(['id' => 'assetsCompiler', 'title' => 'Purge css', 'href' => '#']);
    }

    /**
     * @return void
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @see AjaxRouter::handleRouteFront()
     */
    public function clearCss(): AjaxResponse
    {
        return self::clearCompiledCss();
    }

    public static function clearCompiledCss()
    {
        $basePath = ABSPATH . 'wp-content/';
        $compiledCss = 'uploads/compiled.css';
        $compiledCssMobile = 'uploads/compiled-mobile.css';
        $compiledCssPath = "$basePath$compiledCss";
        $compiledCssPathMobile = "$basePath$compiledCssMobile";
        $msg = '';
        if (file_exists($compiledCssPathMobile)) {
            if (unlink($compiledCssPathMobile)){
                $msg .= "Uspesno obrisan mobile css: $compiledCssPathMobile \n";
            } else {
                $msg .= "Neuspesno obrisan mobile css: $compiledCssPathMobile \n";
            }
        }
        if (file_exists($compiledCssPath)) {
            if (unlink($compiledCssPath)){
                $msg .= "Uspesno obrisan css: $compiledCssPath \n";
            }else {
                $msg .= "Neuspešno obrisan  css: $compiledCssPath \n";
            }
        }
        return new AjaxResponse(false, $msg);
    }
}