import BaseFetcher from "../../../../assets/front/js/BaseFetcher.js";
export default class PurgeCssAndScripts extends BaseFetcher {
    adminBar = document.getElementById('wpadminbar');
    button = document.getElementById('wp-admin-bar-assetsCompiler');
    handlerClass = 'PluginContainerPackagesAssetsCompilerControllerAssetsCompiler';
    init() {
        this.purgeCss();
    }

    checkIfElementsAreVisible() {
        return this.adminBar && this.button;
    }

    purgeCss() {
        this.button.addEventListener('click', async () => {
            let params = {
                method: 'clearCss',
            };
            let response = await this.fetchResponse(params);
            alert(response.message);
        });
    }
}