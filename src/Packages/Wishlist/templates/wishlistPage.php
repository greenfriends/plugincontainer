<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\Wishlist\Controller\Wishlist;

if(!is_user_logged_in()):?>
<h2><?=__(
        sprintf('Morate biti ulogovani da biste koristili listu želja, to možete učiniti <a style="text-decoration: underline" href="%s">ovde</a>',
                get_permalink( get_option('woocommerce_myaccount_page_id'))), 'plugin-container')?></h2>
<?php else:
global $gfContainer;
$wishlistController = $gfContainer->get(Wishlist::class);
$wishlistProducts = $wishlistController->getWishlistProducts();
if(count($wishlistProducts) === 0):?>
    <h2><?=__('There are no products in your wishlist.', 'plugin-container')?></h2>
<?php endif; ?>
<div id="wishlistProductsContainer">
<?php
foreach($wishlistProducts as $id) {

    $product = wc_get_product($id);
    if ($product) {
        echo '<div class="wishlistProduct">';
        $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()),'woocommerce_thumbnail');
        $title = $product->get_title();
        $price = $product->get_price_html();
        $url = $product->get_permalink();
        $rating = wc_get_rating_html($product->get_average_rating());
        $addToCartUrl = "?add-to-cart={$product->get_id()}";
        include CONTAINER_PLUGIN_DIR . '/src/Packages/Wishlist/templates/wishlistProductItem.php';
        echo '</div>';
    }
}
?>
</div>
<?php endif;?>

