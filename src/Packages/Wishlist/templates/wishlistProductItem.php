<?php
use PluginContainer\Core\WpBridge\Translations as T;
?>
<a target="_blank" href="<?=$url?>" title="<?=$title?>">
    <img class="wishlistThumbnail" src="<?=$thumbnail[0]?>" alt="<?=$title?>" width="<?=$thumbnail[1]?>" height="<?=$thumbnail[2]?>">
</a>
<a target="_blank" href="<?=$url?>"
   title="<?=$title?>">
    <h3 class="wishlistTitle"><?=$title?></h3>
</a>
<span class="wishlistRating">
    <?=$rating?>
</span>
<span class="price"><?=$price?></span>
<a class="button product_type_simple add_to_cart_button ajax_add_to_cart" href="<?=$addToCartUrl?>"
   title="<?=__('add to cart', 'plugin-container')?>">
    <?=__('Add to cart', 'plugin-container')?>
</a>
<div class="deleteFromWishlist" data-id="<?=$product->get_id()?>" data-method="deleteFromWishlist"
     title="<?=__('Remove from wishlist', 'plugin-container')?>">
    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
    </svg>
</div>
