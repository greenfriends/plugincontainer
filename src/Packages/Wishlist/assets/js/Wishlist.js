import BaseFetcher from "../../../../../assets/front/js/BaseFetcher.js";

export default class Wishlist extends BaseFetcher
{
    handlerClass = 'PluginContainerPackagesWishlistControllerWishlist';
    addToWishlistLoopButtons = document.querySelectorAll('.addToWishlist');
    deleteFromWishlistButtons = document.querySelectorAll('.deleteFromWishlist');
    checkIfElementsAreVisible() {
        return this.addToWishlistLoopButtons.length > 0 || this.deleteFromWishlistButtons.length > 0;
    }

    init() {
        let self = this;
        this.addToWishlistLoopButtons.forEach((elem) => {
           elem.addEventListener('click',async function handler() {
               self.startLoader(elem);
               await self.fetchResponse(
                   {
                       method: elem.getAttribute('data-method'),
                       productId: elem.getAttribute('data-id')
                   }).then((response) => {
                       // console.log(response);
               })
               self.removeLoader(elem);
               elem.removeEventListener('click',handler);
           });
        });

        this.deleteFromWishlistButtons.forEach((elem) => {
           elem.addEventListener('click',() => {
               this.fetchResponse({
                   method: elem.getAttribute('data-method'),
                   productId: elem.getAttribute('data-id')
               });
              elem.parentElement.remove();
           });
        });
    }

    startLoader(elem)
    {
        elem.innerHTML = '<div class="gfLoader green"></div>';
    }

    removeLoader(elem)
    {
        elem.innerHTML = `<svg style="color:green;" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4" />
                         </svg>`;
    }
}