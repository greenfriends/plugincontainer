<?php


namespace PluginContainer\Packages\Wishlist\Repository;


use PluginContainer\Core\Repository\BaseRepository;

class Wishlist extends BaseRepository
{
    public function __construct(\PluginContainer\Packages\Wishlist\Mapper\Wishlist $mapper)
    {
        parent::__construct(\PluginContainer\Packages\Wishlist\Model\Wishlist::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $this->mapper->deleteTable();
    }

    /**
     * @throws \Exception
     */
    public function createUserIdIndex(): void
    {
        $this->mapper->createUserIdIndex();
    }
}