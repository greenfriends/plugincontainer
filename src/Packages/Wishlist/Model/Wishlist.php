<?php


namespace PluginContainer\Packages\Wishlist\Model;


class Wishlist
{
    /**
     * @var int
     */
    private $userId;
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string
     */
    private $products;
    /**
     * @var string|null
     */
    private $createdAt;
    /**
     * @var string|null
     */
    private $updatedAt;

    /**
     * Wishlist constructor.
     * @param int $userId
     * @param int|null $id
     * @param string $products
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(int $userId, int $id = null, string $products = '', string $createdAt = null, string $updatedAt = null)
    {
        $this->userId = $userId;
        $this->id = $id;
        $this->products = $products;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProducts(): string
    {
        return $this->products;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
}