<?php


namespace PluginContainer\Packages\Wishlist\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\Wishlist\Repository\Wishlist as Repo;
use PluginContainer\Packages\Wishlist\Service\WishlistPage;

class Wishlist extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface
{
    /**
     * @var Repo
     */
    private $wishlistRepo;
    /**
     * @var WishlistPage
     */
    private $wishlistPage;

    public function __construct(Feature $featureRepo, Logger $logger, Repo $wishlistRepo, WishlistPage $wishlistPage)
    {
        parent::__construct($featureRepo, $logger);
        $this->wishlistRepo = $wishlistRepo;
        $this->wishlistPage = $wishlistPage;
    }

    public function init(): void
    {
        $this->wishlistPage->initShortcode();
        $this->addWishlistHtmlInProductInfo();
    }

    /**
     * @throws \Exception
     */
    public function setup() :void
    {
        $this->wishlistRepo->createTable();
        $this->wishlistRepo->createUserIdIndex();
        $this->wishlistPage->createPage();
    }

    /**
     * @throws \Exception
     */
    public function cleanup() :void
    {
        $this->wishlistRepo->deleteTable();
        $this->wishlistPage->deletePage();
    }


    private function addWishlistHtmlInProductInfo(): void
    {
        add_action( 'woocommerce_after_shop_loop_item_title',function() {
            if(!is_user_logged_in()) {
                return null;
            }
            global $product;
            echo '<div title="' . __('Add to wishlist', 'plugin-container') .'" data-method="addToWishlist" data-id="' . $product->get_id() .'" class="addToWishlistLoop addToWishlist"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                  </svg></div>';
        },11);
        add_action( 'woocommerce_before_add_to_cart_form',function() {
            if(!is_user_logged_in()) {
                return null;
            }
            global $product;
            echo '<div title="' . __('Add to wishlist', 'plugin-container') .'" data-method="addToWishlist" data-id="' . $product->get_id() .'" class="addToWishlistSingleProduct addToWishlist"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                  </svg><span>' . __('Add to wishlist', 'plugin-container') .'</span></div>';
        },11);
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function addToWishlist(): AjaxResponse
    {
        if (isset($_GET['productId'])) {
            $productId = $_GET['productId'];
            $userId = get_current_user_id();
            $wishList = $this->wishlistRepo->getAllWithFilters(['userId' => ['value' => $userId]],false)[0];
            if($wishList !== null) {
                $products = unserialize($wishList['products'], ['allowed_classes' => false]);
                if(!in_array($productId, $products, false)) {
                    $products[] = $productId;
                    $this->wishlistRepo->update(['id' => $wishList['id'],'products' => serialize($products),'userId' => $userId]);
                } else {
                    throw new \Exception('Product ID already exists in the wishlist');
                }
            } else {
                $this->wishlistRepo->create(['products' => serialize([$productId]),'userId' => $userId]);
            }
            return new AjaxResponse(true, 'Product successfully added to wishlist');
        }
        throw new \Exception('Product ID not found in the GET request');
    }


    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getWishlistProducts() :array
    {
        $wishlist = $this->wishlistRepo->getAllWithFilters(['userId' => ['value' => get_current_user_id()]],true)[0] ?? null;
        $products = [];
        if($wishlist !== null) {
            $products = unserialize($wishlist->getProducts(), ['allowed_classes' => false]);
        }
       return $products;
    }


    /**
     * @return AjaxResponse
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function deleteFromWishlist(): AjaxResponse
    {
        $wishlist = $this->wishlistRepo->getAllWithFilters(['userId' => ['value' => get_current_user_id()]],true)[0];
        $products = [];
        if($wishlist !== null) {
            $products = unserialize($wishlist->getProducts(), ['allowed_classes' => false]);
        }
        unset($products[array_search($_GET['productId'],$products,false)]);
        $this->wishlistRepo->update(['id' => $wishlist->getId(),'products' => serialize($products),'userId' => $wishlist->getUserId()]);

        return new AjaxResponse(true,'Successfully deleted the product from the wishlist');
    }
}