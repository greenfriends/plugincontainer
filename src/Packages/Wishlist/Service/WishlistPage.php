<?php


namespace PluginContainer\Packages\Wishlist\Service;


class WishlistPage
{
    public $pageSlug;

    public function __construct()
    {
        $this->pageSlug = 'wishlist';
    }

    public function initShortcode()
    {
        add_shortcode('wishlist_page', [$this, 'pageView']);

    }


    public function createPage(): void
    {
        $isPageCreated = get_page_by_title('Wishlist', 'OBJECT', 'page');
        // Check if the page already exists
        if (empty($isPageCreated)) {
            $pageId = wp_insert_post(
                [
                    'comment_status' => 'close',
                    'ping_status' => 'close',
                    'post_author' => 1,
                    'post_title' => 'Wishlist',
                    'post_name' => $this->pageSlug,
                    'post_status' => 'publish',
                    'post_content' => '[wishlist_page]',
                    'post_type' => 'page'
                ]
            );
            add_filter('display_post_states', static function ($post_states, $post) use ($pageId) {
                if ($post->ID === $pageId) {
                    $post_states[] = 'Do not delete this page';
                }
                return $post_states;
            }, 10, 2);
        }
    }

    /**
     * Callback for the [wishlist_page] shortcode.
     * @return false|string
     */
    public function pageView() :?string
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/Wishlist/templates/wishlistPage.php';
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getPageUrl() :string
    {
        return get_home_url() . '/' . $this->pageSlug . '/';
    }


    public function deletePage(): void
    {
        /**
         * @var \WP_Post $page
         */
        $page = get_page_by_title('Wishlist', 'OBJECT', 'page');
        if($page) {
            wp_delete_post($page->ID,true);
        }
    }
}