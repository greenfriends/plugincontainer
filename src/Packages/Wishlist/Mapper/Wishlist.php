<?php


namespace PluginContainer\Packages\Wishlist\Mapper;


use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class Wishlist extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfWishlist');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
     		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
     		  `userId` int(5) UNSIGNED NOT NULL,
     		  `products` TEXT,	
     		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     		  PRIMARY KEY  ($this->primaryKey)
     		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $sql = "DROP TABLE $this->tableName";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function createUserIdIndex(): void
    {
        $sql = "CREATE UNIQUE INDEX `userIdIndex` ON {$this->tableName} (userId)";
        $this->handleStatement($sql);
    }
}