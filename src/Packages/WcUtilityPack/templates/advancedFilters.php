<?php

global $wp_query;
$data = [];
if ($wp_query->is_archive || $wp_query->is_search) {
    $filters = $wp_query->get('esFilters');
    if($filters && count($filters) > 0) {
        foreach ($filters as $filterData) {
            foreach ($filterData['filterValues']['buckets'] as $filters) {
                if (strpos($filterData['key'], 'pa_') !== false) {
                    continue;
                }
                $data[$filterData['key']][] = [
                        $filters['key'],
                        $filters['doc_count']
                ];
            }
        }
    }
}
?>

<?php
$minPrice = $wp_query->get('minPrice');
$maxPrice = $wp_query->get('maxPrice');
if (count($data) > 0):?>
<form id="filterForm" method="get">
    <div class="price-range-block">
        <div class="sliderText"><?=__('Filtriraj po ceni','plugin-container')?></div>
        <div id="slider-range" class="price-filter-range" name="rangeInput"></div>
        <div style="margin:30px auto">
            <input aria-label="min" name="minPrice" type="number" min="<?=$minPrice?>" max="<?=$maxPrice?>" oninput="validity.valid||(value='<?=$minPrice?>');" id="min_price" class="price-range-field" value="<?=$_GET['minPrice'] ?? $minPrice?>" />
            <span class="currency"><?=get_woocommerce_currency_symbol()?></span>
            <span> - </span>
            <input aria-label="max" name="maxPrice"  type="number" min="<?=$minPrice?>" max="<?=$maxPrice?>" oninput="validity.valid||(value='<?=$maxPrice?>');" id="max_price" class="price-range-field" value="<?=$_GET['maxPrice'] ?? $maxPrice?>"/>
            <span class="currency"><?=get_woocommerce_currency_symbol()?></span>
        </div>
    </div>
    <button type="submit" class="advancedFiltersSubmit"><?=__('Filtriraj', 'plugin-container')?></button>
    <?php if (is_search()):?>
    <input type="hidden" name="s" value="<?=$_GET['s']?>">
    <input type="hidden" name="post_type" value="<?=$_GET['post_type']?>">
    <input type="hidden" name="specificSearch" value="<?=$_GET['specificSearch']?>">
    <?php endif;?>
    <?php foreach ($data as $key => $value): ?>
    <div class="attributeGroup">
        <h4><?=ucfirst($key)?></h4>
        <ul>
            <?php
            foreach ($value as $attribute):
                if (isset($_GET['filters'][$key]) && in_array(addslashes($attribute[0]), $_GET['filters'][$key])) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                ?>
                <li>
                    <div>
                        <input aria-label="<?=htmlentities($attribute[0])?>" <?=$checked?> type="checkbox" name="filters[<?=$key?>][]"
                                             value="<?=htmlentities($attribute[0])?>">
                        <label><?=$attribute[0]?></label>
                    </div>
                    <?php
                    // $attribute[1] is the number of products for the given attribute
                    ?>
                </li>
            <?php
            endforeach; ?>
        </ul>
    </div>
<?php
endforeach;?>
    <button type="submit" class="advancedFiltersSubmit"><?=__('Filtriraj', 'plugin-container')?></button>
<?php endif; ?>
</form>