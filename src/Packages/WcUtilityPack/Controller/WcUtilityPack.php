<?php

namespace PluginContainer\Packages\WcUtilityPack\Controller;

use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Packages\Indexer\Repository\Product as EsRepo;
use PluginContainer\Packages\WcUtilityPack\Model\Product;

class WcUtilityPack extends BaseController implements ControllerWithHooksInterface
{
    public $esRepo;

    public function init(): void
    {
        $this->sendCancelOrderToUsers();
        $this->advancedFiltersShortcode();
        if (defined('USE_ELASTIC') && USE_ELASTIC) {
            $this->esRepo = new EsRepo();
            $this->cutMainQueryOnCategoryPages();
            $this->archivePageQueryResultSet();
            $this->searchResultSet();
            $this->setupGlobalProductFromElastic();
        }
    }

    private function sendCancelOrderToUsers(): void
    {
        add_filter('woocommerce_email_recipient_cancelled_order', static function ($recipient, $order)
        {
            return $recipient . ',' . $order->billing_email;
        }, 10, 2);
    }
    /**
     * Return an array of top level categories with their IDs, names and urls
     *
     * @return array
     */
    public static function getTopLevelCategories(): array
    {
        $topLevelCategories = self::getCategoriesByParentId(0);
        $categoryList = [];
        foreach ($topLevelCategories as $topLevelCategory) {
            if ($topLevelCategory->name === 'Uncategorized' || $topLevelCategory->name === 'Nekategorisano' ||
                $topLevelCategory->name === 'Nekategorizovano') {
                continue;
            }
            $categoryList[$topLevelCategory->term_id] = [
                'topLevelCategory' => [
                    'id' => $topLevelCategory->term_id,
                    'name' => $topLevelCategory->name,
                    'url' => get_term_link($topLevelCategory->term_id, 'product_cat'),
                    'productCount' => (string)get_term_by('id', $topLevelCategory->term_id, 'product_cat')->count,
                ]
            ];
        }
        return $categoryList;
    }
    /**
     * @return array
     */
    public static function getCategoriesIncludingSecondLevel(): array
    {
        $categoryList = self::getTopLevelCategories();
        foreach ($categoryList as $key => $value) {
            $secondLevelCategories = self::getCategoriesByParentId($value['topLevelCategory']['id']);
            foreach ($secondLevelCategories as $secondLevelCategory) {
                $categoryList[$key]['secondLevelCategories'][$secondLevelCategory->term_id] = [
                    'id' => $secondLevelCategory->term_id,
                    'name' => $secondLevelCategory->name,
                    'url' => get_term_link($secondLevelCategory->term_id, 'product_cat'),
                    'productCount' => (string)get_term_by('id', $secondLevelCategory->term_id, 'product_cat')->count,
                ];
            }
        }
        return $categoryList;
    }
    /**
     * @return array
     */
    public static function getCategoryListFromWp(): array
    {
        $categoryList = self::getCategoriesIncludingSecondLevel();
        foreach ($categoryList as $key => $value) {
            if (isset($value['secondLevelCategories'])) {
                foreach ($value['secondLevelCategories'] as $index => $secondLevelCategory) {
                    $thirdLevelCategories = self::getCategoriesByParentId($secondLevelCategory['id']);
                    foreach ($thirdLevelCategories as $thirdLevelCategory) {
                        $categoryList[$key]['secondLevelCategories'][$index]['thirdLevelCategories'][$thirdLevelCategory->term_id] = [
                            'id' => $thirdLevelCategory->term_id,
                            'name' => $thirdLevelCategory->name,
                            'url' => get_term_link($thirdLevelCategory->term_id, 'product_cat'),
                            'productCount' => (string)get_term_by('id', $thirdLevelCategory->term_id,
                                'product_cat')->count,
                        ];
                    }
                }
            }
        }
        return $categoryList;
    }
    /**
     * @param $parentCatId
     * @return array
     */
    public static function getCategoriesByParentId($parentCatId): array
    {
        $args = [
            'show_option_none' => '',
            'hide_empty' => 0,
            'parent' => $parentCatId,
            'taxonomy' => 'product_cat'
        ];
        return get_categories($args);
    }
    //Query stuff
    private function cutMainQueryOnCategoryPages(): void
    {
        add_filter('posts_request', function ($sql, $query)
        {
            if (isset($query->query_vars['wc_query']) && $query->query_vars['wc_query'] === 'product_query' && $query->is_main_query()) {
                return '';
            }
            return $sql;
        }, 10, 2);
    }

    private function archivePageQueryResultSet(): void
    {
        add_filter('posts_pre_query', function ($posts, $query)
        {
            if (
                isset($query->query_vars['wc_query']) &&
                $query->query_vars['wc_query'] === 'product_query' &&
                $query->is_main_query() &&
                ($query->is_archive() && !is_admin())
            ) {
                $posts = [];
                $sortParam = $query->query['orderby'] ?? '';
                $maybeCat = get_queried_object();
                if ($maybeCat && $maybeCat instanceof \WP_Term && $maybeCat->taxonomy === 'product_cat' && $maybeCat->parent !== 0) {
                    $cols = get_option('woocommerce_catalog_columns', 4);
                    $rows = get_option('woocommerce_catalog_rows', 4);
                    $perPage = $cols * $rows;
                    $filters = [];
                    if (isset($_GET['filters']) && count($_GET['filters']) > 0) {
                        foreach ($_GET['filters'] as $filter => $value) {
                            foreach ($value as $filterValue) {
                                $filters[] = [
                                    'name' => $filter,
                                    'value' => addslashes($filterValue),
                                ];
                            }
                        }
                    }
                    if(isset($_GET['minPrice'])) {
                        $filters[] = [
                            'name' => 'minPrice',
                            'value' => (int)$_GET['minPrice'],
                        ];
                    }
                    if(isset($_GET['maxPrice'])) {
                        $filters[] = [
                            'name' => 'maxPrice',
                            'value' => (int)$_GET['maxPrice'],
                        ];
                    }
                    //fetch for global filters
                    $data = $this->esRepo->searchByCategory($maybeCat->term_id, 1, null,
                        $perPage, [], $this->parseWcSortOptions($sortParam)) ?? [];


                    $allFilters = $data['filters']['filterNames']['buckets'] ?? [];
                    $minPrice = $data['minPrice']['value'] ?? null;
                    $maxPrice = $data['maxPrice']['value'] ?? null;

                    $results = $this->esRepo->searchByCategory($maybeCat->term_id, $query->query['paged'] ?? 1, null,
                    $perPage, $filters, $this->parseWcSortOptions($sortParam));


                    $numberOfPosts = $results['totalCount'];
                    $maxPages = ceil($numberOfPosts / $perPage);
                    $query->found_posts = $numberOfPosts; // number of all posts
                    $query->post_count = $perPage; // posts per page
                    $query->max_num_pages = $maxPages;
                    $posts = $results['wpPostIds'];
                    $query->posts = $posts;
                    $query->set('esFilters', $allFilters);
                    $query->set('minPrice', $minPrice);
                    $query->set('maxPrice',$maxPrice);
                }
            }
            return $posts;
        }, 10, 2);
    }

    private function searchResultSet(): void
    {
        add_filter('posts_pre_query', function ($posts, $query)
        {
            if (
                isset($query->query_vars['wc_query']) &&
                $query->query_vars['wc_query'] === 'product_query' &&
                $query->is_main_query() &&
                ($query->is_search() && !is_admin())
            ) {
                $search = preg_replace('/[^A-Za-z0-9 ]/', '',$query->query['s']) ?? '';
                if ($search === '') {
                    return [];
                }
                $sortParam = $query->query['orderby'] ?? '';
                $cols = get_option('woocommerce_catalog_columns', 4);
                $rows = get_option('woocommerce_catalog_rows', 4);
                $perPage = $cols * $rows;
                $filters = [];
                if (isset($_GET['filters']) && count($_GET['filters']) > 0) {
                    foreach ($_GET['filters'] as $filter => $value) {
                        foreach ($value as $filterValue) {
                            $filters[] = [
                                'name' => $filter,
                                'value' => addslashes($filterValue),
                            ];
                        }
                    }
                }
                if(isset($_GET['minPrice'])) {
                    $filters[] = [
                        'name' => 'minPrice',
                        'value' => (int)$_GET['minPrice'],
                    ];
                }
                if(isset($_GET['maxPrice'])) {
                    $filters[] = [
                        'name' => 'maxPrice',
                        'value' => (int)$_GET['maxPrice'],
                    ];
                }
                if (isset($_GET['specificSearch']) && $_GET['specificSearch'] !== '0') {
                    $catId = $_GET['specificSearch'];
                    $results = $this->esRepo->searchByCategory($catId, $query->query['paged'] ?? '', $search, $perPage, $filters,
                        $this->parseWcSortOptions($sortParam));
                } else {
                    $results = $this->esRepo->search($search, $query->query['paged'] ?? 1, $perPage, $this->parseWcSortOptions($sortParam));
                }
                $numberOfPosts = $results['totalCount'];
                $maxPages = ceil($numberOfPosts / $perPage);
                $query->found_posts = $numberOfPosts; // number of all posts
                $query->post_count = $perPage; // posts per page
                $query->max_num_pages = $maxPages;
                $posts = $results['wpPostIds'];
                $query->posts = $posts;
                $query->set('esFilters', $results['filters']['filterNames']['buckets'] ?? []);
                $query->set('minPrice',$results['minPrice']['value']);
                $query->set('maxPrice',$results['maxPrice']['value']);
            }
            return $posts;
        }, 10, 2);
    }

    public function parseWcSortOptions($sortParam): array
    {
        //@todo implement relevance sort
        //@todo sort by price but including sale price
        //Skip date sort its default one
        switch ($sortParam) {
            case 'popularity':
                return ['sort' => 'viewCount', 'order' => 'desc'];
            case 'rating':
                return ['sort' => 'rating', 'order' => 'desc'];
            case 'price':
                return ['sort' => 'regularPrice', 'order' => 'asc'];
            case 'price-desc':
                return ['sort' => 'regularPrice', 'order' => 'desc'];
            default:
                return ['sort' => 'createdAt', 'order' => 'desc'];
        }
    }

    private function setupGlobalProductFromElastic(): void
    {
        add_action('init', function ()
        {
            //remove default wc setup of global product
            remove_action('the_post', 'wc_setup_product_data');
            //Set our function as the global product setup
            add_action('the_post', function ($obj)
            {
                if ((is_archive() && !is_admin())) {
                    unset($GLOBALS['product']);
                    $result = $this->esRepo->getProductById($obj->ID);
                    if (!$result) {
                        $product = wc_get_product($obj->ID);
                        $GLOBALS['product'] = $product;
                        return $GLOBALS['product'];
                    }
                    $product = new Product($result);
                } else {
                    $product = wc_get_product($obj->ID);
                }
                $GLOBALS['product'] = $product;
                return $GLOBALS['product'];
            });
        });
    }

    public static function getProductClassList($product): void
    {
        if ($product instanceof \WC_Product) {
            wc_product_class('', $product);
            return;
        }
        $class = '';
        $postClasses = [];
        // Run through the post_class hook so 3rd parties using this previously can still append classes.
        // Note, to change classes you will need to use the newer woocommerce_post_class filter.
        // @internal This removes the wc_product_post_class filter so classes are not duplicated.
        $filtered = has_filter('post_class', 'wc_product_post_class');

        if ($filtered) {
            remove_filter('post_class', 'wc_product_post_class', 20);
        }

        $postClasses = apply_filters('post_class', $postClasses, $class, $product->get_id());

        if ($filtered) {
            add_filter('post_class', 'wc_product_post_class', 20, 3);
        }
        $classes = array_merge(
            $postClasses,
            [
                'product',
                'type-product',
                'post-' . $product->get_id(),
                'status-' . $product->get_status(),
                wc_get_loop_class(),
                $product->get_stock_status(),
            ],
//            wc_get_product_taxonomy_class( $product->get_tag_ids(), 'product_tag' ) // @todo check if this is ever used?
        );
        foreach ($product->get_categories() as $category) {
            $classes[] = 'product_cat-' . $category['name'];
        }

        if (count($product->get_images()) > 0) {
            $classes[] = 'has-post-thumbnail';
        }
        if ($product->is_on_sale()) {
            $classes[] = 'sale';
        }
        if ($product->is_featured()) {
            $classes[] = 'featured';
        }
        if ($product->is_taxable()) {
            $classes[] = 'taxable';
        }
        if ($product->is_shipping_taxable()) {
            $classes[] = 'shipping-taxable';
        }
        if ($product->is_purchasable()) {
            $classes[] = 'purchasable';
        }
        if ($product->get_type()) {
            $classes[] = 'product-type-' . $product->get_type();
        }
        echo 'class="' . implode(' ', $classes) . '"';
    }

    private function advancedFiltersShortcode(): void
    {
        add_shortcode('product_filters', [$this, 'advancedFiltersShortcodeView']);
    }

    public function advancedFiltersShortcodeView()
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/WcUtilityPack/templates/advancedFilters.php';
        return ob_get_clean();
    }

}