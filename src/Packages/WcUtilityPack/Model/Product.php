<?php

namespace PluginContainer\Packages\WcUtilityPack\Model;

class Product
{
    private $status;
    /**
     * @var int
     */
    private $salePrice;
    /**
     * @var int
     */
    private $regularPrice;
    /**
     * @var string|null
     */
    private $salePriceStart;
    /**
     * @var string|null
     */
    private $salePriceEnd;
    /**
     * @var string
     */
    private $type;
    /**
     * @var array
     */
    private $images;
    /**
     * @var float
     */
    private $rating;
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $stockStatus;
    /**
     * @var int
     */
    private $sku;
    /**
     * @var string
     */
    private $permalink;
    /**
     * @var array
     */
    private $categories;
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $featured;

    /**
     * @param array $esData
     */
    public function __construct(array $esData)
    {
        $this->status = $esData['status'];
        $this->salePrice = (int)$esData['salePrice'];
        $this->regularPrice = (int)$esData['regularPrice'];
        $this->salePriceStart = $esData['salePriceStart'];
        $this->salePriceEnd = $esData['salePriceEnd'];
        $this->type = $esData['type'];
        $this->images = $esData['thumbnails'];
        $this->rating = (float)$esData['rating'];
        $this->id = $esData['postId'];
        $this->stockStatus = $esData['stockStatus'];
        $this->sku = $esData['sku'];
        $this->permalink = $esData['permalink'];
        $this->categories = $esData['category'];
        $this->title = $esData['name'];
        $this->featured = $esData['featured'];
    }

    public function is_featured(): bool
    {
        return $this->featured === 1;
    }

    public function get_status()
    {
        if ($this->status === 1) {
            return 'publish';
        }
        return 'draft';
    }

    public function is_visible()
    {
        return $this->status;
    }

    public function get_sale_price()
    {
        return $this->salePrice;
    }

    public function get_regular_price()
    {
        return $this->regularPrice;
    }

    public function get_sale_price_start()
    {
        return $this->salePriceStart;
    }

    public function get_sale_price_end()
    {
        return $this->salePriceEnd;
    }

    public function get_type()
    {
        return $this->type;
    }

    public function is_type($type)
    {
        return $this->type === $type;
    }

    public function get_price(): int
    {
        if ($this->is_on_sale()) {
            return $this->get_sale_price();
        }
        return $this->get_regular_price();
    }

    public function get_image_by_size($size)
    {
        foreach ($this->images as $image) {
            if (isset($image['name']) && $image['name'] === $size) {
                return $image['src'];
            }
        }
        return $this->images[0]['src'];
    }

    public function get_gallery_image_ids()
    {
        return [];
    }

    public function get_average_rating(): float
    {
        return $this->rating;
    }

    /**
     * Returns the price in html format.
     *
     * @param string $deprecated Deprecated param.
     *
     * @return string
     */
    public function get_price_html($deprecated = '')
    {
        if ('' === $this->get_price()) {
            $price = apply_filters('woocommerce_empty_price_html', '', $this);
        } elseif ($this->is_on_sale()) {
            $price = wc_format_sale_price(wc_get_price_to_display($this, ['price' => $this->get_regular_price()]),
                    wc_get_price_to_display($this)) . $this->get_price_suffix();
        } else {
            $price = wc_price(wc_get_price_to_display($this)) . $this->get_price_suffix();
        }

        return apply_filters('woocommerce_get_price_html', $price, $this);
    }

    public function is_taxable(): bool
    {
        return false;
    }

    public function is_shipping_taxable(): bool
    {
        return false;
    }

    public function get_price_suffix(): string
    {
        return '';
    }

    public function get_id(): int
    {
        return $this->id;
    }

    public function is_purchasable(): bool
    {
        return $this->is_visible() && $this->get_price() !== 0;
    }

    public function get_stock_status()
    {
        if ($this->stockStatus === 1) {
            return 'instock';
        }
        return 'outofstock';
    }

    public function is_in_stock(): bool
    {
        return $this->stockStatus === 1;
    }

    /**
     * @return bool
     * @todo sync supports array to elastic and have it in constructor and then copy this function from wc product
     */
    public function supports(): bool
    {
        return true;
    }

    public function get_sku(): string
    {
        return $this->sku;
    }

    public function add_to_cart_description(): string
    {
        return '';
    }

    public function get_permalink(): string
    {
        return $this->permalink;
    }

    /**
     * Get the add to url used mainly in loops.
     *
     * @return string
     */
    public function add_to_cart_url()
    {
        return '?add-to-cart=' . $this->get_id();
    }

    /**
     * Get the add to cart button text.
     *
     * @return string
     */
    public function add_to_cart_text()
    {
        return apply_filters('woocommerce_product_add_to_cart_text', __('Add to cart', 'woocommerce'), $this);
    }

    public function get_categories(): array
    {
        return $this->categories;
    }

    /**
     * Returns the main product image.
     *
     * @param string $size (default: 'woocommerce_thumbnail').
     * @param array $attr Image attributes.
     * @param bool $placeholder True to return $placeholder if no image is found, or false to return an empty string.
     * @return string
     * @todo srediti ovooooooo :O
     */
    public function get_image($size = 'woocommerce_thumbnail', $attr = [], $placeholder = true)
    {
        $imageUrl = $this->get_image_by_size($size);
        $mobileImageUrl = $this->get_image_by_size('thumbnail');
        $html = '<a href="' . $this->get_permalink() . '" 
        aria-label="' . htmlentities($this->get_title()) . '"  
        class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
        <img width="300" height="300" src="' . $imageUrl . '"
        class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" 
        srcset="' . $imageUrl . ' 300w, 
        ' . $mobileImageUrl . ' 150w" 
        sizes="(max-width: 360px) 147px, (max-width: 300px) 100vw, 300px">			
	   ';
        return $html;
    }


    public function get_images()
    {
        return $this->images;
    }

    public function get_title(): string
    {
        return $this->title;
    }

    /**
     * Returns whether the product is on sale.
     *
     * @return bool
     */
    public function is_on_sale(): bool
    {
        if ($this->get_sale_price() !== 0 && $this->get_regular_price() > $this->get_sale_price()) {
            $on_sale = true;

            if ($this->get_sale_price_start() && $this->get_sale_price_start() > time()) {
                $on_sale = false;
            }

            if ($this->get_sale_price_end() && $this->get_sale_price_end() < time()) {
                $on_sale = false;
            }
        } else {
            $on_sale = false;
        }
        return $on_sale;
    }

    public function get_variation_prices(): array
    {
        return [
            'price' => [$this->get_id() => $this->get_price()],
            'regular_price' => [$this->get_id() => $this->get_regular_price()],
            'sale_price' => [$this->get_id() => $this->get_sale_price()]

        ];
    }
}