<?php

namespace PluginContainer\Packages\Redirects\Mapper;

use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class Redirects extends BaseMapper
{

    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfRedirects');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `fromUrl` varchar(150) NOT NULL,
		  `toUrl` varchar(150) NOT NULL,
		  `statusCode` int(3) UNSIGNED NOT NULL,
		  `active` int(1) DEFAULT 0,
		  `userId` int(10) UNSIGNED DEFAULT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  (id),
		  CONSTRAINT fromUrl UNIQUE (`fromUrl`)
		) DEFAULT CHARSET=utf8mb4;";
        $this->handleStatement($sql);
    }
}