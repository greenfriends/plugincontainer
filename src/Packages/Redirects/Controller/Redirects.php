<?php

namespace PluginContainer\Packages\Redirects\Controller;

use Exception;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Components\CrudTable\Column;
use PluginContainer\Core\Components\CrudTable\CrudTable;
use PluginContainer\Core\Components\CrudTable\FormField;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithCrudTableInterface;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Packages\Redirects\Model\Redirects as RedirectsModel;
use PluginContainer\Packages\Redirects\Repository\Redirects as RedirectsRepo;
use PluginContainer\Packages\Redirects\Service\AutoRedirects;
use PluginContainer\Packages\Redirects\Service\PageRedirects;
use PluginContainer\Core\WpBridge\Translations as T;

class Redirects extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface,
                                                  ControllerWithCrudTableInterface
{
    use \PluginContainer\Core\Components\CrudTable\Traits\CrudTable;
    /**
     * @var PageRedirects
     */
    private $pageRedirects;
    /**
     * @var AutoRedirects
     */
    private $autoRedirects;
    /**
     * @var RedirectsRepo
     */
    private $repo;

    public function __construct(
        Feature $featureRepo, Logger $logger,
        RedirectsRepo $repo, PageRedirects $pageRedirects, AutoRedirects $autoRedirects
    ) {
        parent::__construct($featureRepo, $logger);
        $this->repo = $repo;
        $this->pageRedirects = $pageRedirects;
        $this->autoRedirects = $autoRedirects;
    }

    /**
     * @throws Exception
     */
    public function create(): AjaxResponse
    {
        $this->repo->create($_GET);
        return new AjaxResponse(true, 'Successfully created redirect');
    }

    /**
     * @return AjaxResponse
     * @throws Exception
     */
    public function update(): AjaxResponse
    {
        $this->repo->update($_GET);
        return new AjaxResponse(true, 'Successfully updated redirect');
    }

    /**
     * @return AjaxResponse
     * @throws Exception
     */
    public function delete(): AjaxResponse
    {
        $this->repo->delete($_GET['id']);
        return new AjaxResponse(true, 'Successfully deleted redirect');
    }

    /**
     * @throws \Exception
     */
    public function getFormFields(): AjaxResponse
    {
        $method = 'create';
        $formHeader = 'Create Redirect';
        if (isset($_GET['id'])){
            /** @var RedirectsModel $redirect */
            $redirect = $this->repo->getById((int)$_GET['id']);
            if ($redirect) {
                $method = 'update';
                $formHeader = 'Update Status';
                $from = $redirect->getFromUrl();
                $to = $redirect->getToUrl();
                $statusCode = $redirect->getStatusCode();
                $active = $redirect->getActive();
                $userId = $redirect->getUserId();
            } else {
                throw new Exception('Redirect you are trying to update does not exist');
            }
        }
        $fromField = new FormField('fromUrl', 'text', __('From', 'plugin-container'), $from ?? null);
        $toField = new FormField('toUrl', 'text', __('To', 'plugin-container'), $to ?? null);
        $statusCodeField = new FormField('statusCode', 'text', __('Code', 'plugin-container'), $statusCode ?? null);
        $activeField = new FormField('active', 'text', __('Active', 'plugin-container'), $active ?? null);
        $userIdField = new FormField('userId', 'text', __('User id', 'plugin-container'), $userId ?? null);
        $response = new AjaxResponse(true);
        $response
            ->addParamToBody('data', [$fromField, $toField, $statusCodeField, $activeField, $userIdField])
            ->addParamToBody('formHeader', $formHeader)
            ->addParamToBody('method', $method)
            ->addParamToBody('submitLabel', __('Submit', 'plugin-container'));
        return $response;
    }

    public function init(): void
    {
        $this->pageRedirects->addActions();
        $this->autoRedirects->addFilters();
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->repo->createTable();
        $this->repo->createIndex('active_index', 'active');
        $this->repo->createIndex('userId_index', 'userID');
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->repo->deleteTable();
    }


    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function getView(): AjaxResponse
    {
        $id = new Column('id', 'Id');
        $id->addViewParam('filterable', false);
        $from = new Column('fromUrl', 'From');
        $from->addViewParam('filterable', false);
        $to = new Column('toUrl', 'To');
        $to->addViewParam('filterable', false);
        $statusCode = new Column('statusCode', 'Code');
        $active = new Column('active', 'Active');
        $userId = new Column('userId', 'User Id');
        $createdAt = new Column('createdAt', 'Created at');
        $updatedAt = new Column('updatedAt', 'Updated at');
        $crudTable = new CrudTable([$id, $from, $to, $statusCode, $active, $userId, $createdAt, $updatedAt],
            $this->repo);
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [$crudTable->getList()]);
        return $response;
    }
}