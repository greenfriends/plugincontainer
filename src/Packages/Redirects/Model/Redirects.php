<?php

namespace PluginContainer\Packages\Redirects\Model;

class Redirects
{
    private $id;
    private $fromUrl;
    private $toUrl;
    private $statusCode;
    private $active;
    private $userId;
    private $createdAt;
    private $updatedAt;

    /**
     * Redirect constructor.
     *
     * @param string $fromUrl
     * @param string $toUrl
     * @param string $statusCode
     * @param int|null $id
     * @param int $active
     * @param int|null $userId
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(
        string $toUrl,
        string $fromUrl,
        string $statusCode,
        int $id = null,
        int $active = 0,
        int $userId = null,
        string $createdAt = null,
        string $updatedAt = null
    ) {
        $this->id = $id;
        $this->fromUrl = $fromUrl;
        $this->toUrl = $toUrl;
        $this->statusCode = $statusCode;
        $this->active = $active;
        $this->userId = $userId;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFromUrl(): string
    {
        return $this->fromUrl;
    }

    /**
     * @return string
     */
    public function getToUrl(): string
    {
        return $this->toUrl;
    }

    /**
     * @return string
     */
    public function getStatusCode(): string
    {
        return $this->statusCode;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
}