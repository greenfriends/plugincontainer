<?php

namespace PluginContainer\Packages\Redirects\Service;

use PluginContainer\Packages\Redirects\Repository\Redirects;

class PageRedirects
{
    private $repo;

    public function __construct(Redirects $repo)
    {
        $this->repo = $repo;
    }

    public function addActions(): void
    {
        add_action('template_redirect', [$this, 'redirectPage']);
    }

    /**
     * Check for redirect in wp_gf_redirects table and if found redirect user to url specified in from column
     *
     * @throws \ReflectionException
     */
    public function redirectPage(): void
    {
        $currentUrl = home_url($_SERVER['REQUEST_URI']);
        $redirect = $this->repo->getBy('fromUrl', $currentUrl)[0] ?? null;
        if ($redirect){
            exit(wp_redirect($redirect->getToUrl(), $redirect->getStatusCode(),'gfRedirects'));
        }
    }
}