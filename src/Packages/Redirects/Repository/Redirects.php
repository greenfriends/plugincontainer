<?php

namespace PluginContainer\Packages\Redirects\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\Redirects\Mapper\Redirects as RedirectsMapper;
use PluginContainer\Packages\Redirects\Model\Redirects as RedirectsModel;

class Redirects extends BaseRepository
{

    public function __construct(RedirectsMapper $mapper)
    {
        parent::__construct(RedirectsModel::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }
}