<?php

namespace PluginContainer\Packages\ChangeCurrencySymbol\Controller;

use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithoutPage;

class ChangeCurrencySymbol extends BaseController implements ControllerWithHooksInterface
{
    public function init(): void
    {
        //Maybe has problems with multisite setup
        add_action('plugins_loaded', function () {
            if (class_exists('WooCommerce')) {
                add_filter('woocommerce_currency_symbol', [$this, 'changeCurrencySymbol'], 10, 2);
            }
        });
    }

    /**
     * @param $currencySymbol
     * @param $currency
     * @return mixed|string
     */
    public function changeCurrencySymbol($currencySymbol, $currency)
    {
        if ($currency === 'RSD') {
            $currencySymbol = 'RSD';
        }
        return $currencySymbol;
    }
}