import Tab from "../../../../../assets/admin/js/Tab.js";

export default class SortableCategories extends Tab {
    init() {
        jQuery('.topLevel').sortable({
            handle: "h2",
            axis: 'y',
            cursor: 'move',
            items: '.accordionFirstLevel',
        });
        jQuery('.secondLevel').sortable({
            handle: "h4",
            axis: 'y',
            cursor: 'move',
            items: '.accordionSecondLevel',
        });
        jQuery('.thirdLevel').sortable({
            handle: "h5",
            axis: 'y',
            cursor: 'move',
            items: '.thirdLevelItem',
        });
        jQuery('.accordionFirstLevel').accordion({
            collapsible: true,
            header: ">h2",
            heightStyle: "content",
            active:false,
            icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
        });
        jQuery('.accordionSecondLevel').accordion({
            collapsible: true,
            header: ">h4",
            heightStyle: "content",
            active:false,
            icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
        });
        this.addFormSubmitListener();
        this.addResetEventListener();
        this.addMediaLibraryHandler();
        this.addImageSelectedEventListener();
        this.addImagePreviewAndRemoveHandler().then(() => {
            this.addPreviewImageListeners();
            this.addRemoveImageListeners();
        });
    }
    addFormSubmitListener() {
        let form = document.getElementById('sortableCategoriesForm');
        form.addEventListener('submit', (e) => {
            e.preventDefault();
            let formData = new FormData(e.target);
            let params = [...formData.entries()];
            this.fetchResponsePost(params,e.target.getAttribute('data-method'), this.container).then((response) => {
                this.activeTab.click();
                this.printNotice(response);
            })
        })
    }

    addResetEventListener() {
        let resetButton = document.getElementById('reset');
        resetButton.addEventListener('click', () => {
            this.fetchResponse({method:resetButton.getAttribute('data-method')}, this.container).then((response) => {
                this.activeTab.click();
                this.printNotice(response);
            }).then(() => {
                //@todo not working check why at some point
                let form = document.getElementById('sortableCategoriesForm');
                // form.querySelector('input[type=submit]').click();
            })
        })
    }

    addMediaLibraryHandler() {
        jQuery(document).ready(function ($) {
            $('.uploadImageButton').click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var $button = $(this);
                // Create the media frame.
                var file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select or upload image',
                    library: { // remove these to show all
                        type: 'image' // specific mime
                    },
                    button: {
                        text: 'Select'
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                });
                // When an image is selected, run a callback.
                file_frame.on('select', function () {
                    // We set multiple to false so only get one image from the uploader
                    var attachment = file_frame.state().get('selection').first().toJSON();
                    $button.siblings('input').val(attachment.sizes.thumbnail.url).change();
                    let data = {
                        src: attachment.sizes.thumbnail.url,
                        elem: $button[0]
                    };
                    document.dispatchEvent(new CustomEvent('imageSelected', {'detail': data}));
                });

                // Finally, open the modal
                file_frame.open();
            });
        });
    }

    /**
     * Go through all image inputs and check if the value is set.
     * If the value is set, print the preview button
     */
    addImagePreviewAndRemoveHandler()
    {
        return new Promise((resolve) => {
            let imageInputs = document.querySelectorAll('.imageInput');
            imageInputs.forEach((input) => {
                if(input.value.length > 0) {
                    let target = input.parentElement;
                    target.appendChild(this.getPreviewImageElement(input.value));
                    target.appendChild(this.getRemoveImageElement(input.value));
                }
            });
            resolve();
        })
    }


    getRemoveImageElement(inputValue) {
        let removeImageElement = document.createElement('div');
        removeImageElement.classList.add('removeImage');
        removeImageElement.title = 'Remove Image';
        removeImageElement.setAttribute('data-src',inputValue);
        removeImageElement.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>`;
        return removeImageElement;
    }
    getPreviewImageElement(inputValue) {
        let previewImageElement = document.createElement('div');
        previewImageElement.classList.add('previewImage');
        previewImageElement.title = 'Preview Image';
        previewImageElement.setAttribute('data-src',inputValue);
        previewImageElement.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                        </svg>`;
        return previewImageElement;
    }

    addPreviewImageListeners() {
        let previewImageButtons = document.querySelectorAll('.previewImage');
        previewImageButtons.forEach((button) => {
            this.addPreviewImageListener(button);
        });
    }

    addPreviewImageListener(elem) {
        elem.addEventListener('click', (e) => {
            let img = document.createElement('img');
            img.setAttribute('src', elem.getAttribute('data-src'));
            e.preventDefault();
            e.stopPropagation();
            this.printContentModal('150','150',img,true, 'px', 'previewImageModal').then(() => {});
        })
    }

    /**
     * Listeners for the imageSelected listener fired when selecting an image
     * If the image was already chosen, updates the data-src.
     * If there was no image, adds the preview and remove buttons with their listeners
     */
    addImageSelectedEventListener() {
        document.addEventListener('imageSelected', (e) => {
            this.elementPrintCheckmark(e.detail.elem.querySelector('svg'));
            let target = e.detail.elem.parentElement.querySelector('.previewImage');
            if(target) {
                target.setAttribute('data-src',e.detail.src);
            } else {
                let previewElement = this.getPreviewImageElement(e.detail.src);
                let removeElement = this.getRemoveImageElement(e.detail.src);
                e.detail.elem.parentElement.appendChild(previewElement);
                e.detail.elem.parentElement.appendChild(removeElement);
                this.addRemoveImageListener(removeElement);
                this.addPreviewImageListener(previewElement);
            }
        })
    }

    addRemoveImageListeners() {
        let removeImageButtons = document.querySelectorAll('.removeImage');
        removeImageButtons.forEach((button) => {
           this.addRemoveImageListener(button);
        });
    }

    addRemoveImageListener(elem) {
        elem.addEventListener('click', (e) => {
            elem.parentElement.querySelector('.previewImage').remove();
            elem.parentElement.querySelector('.imageInput').value = '';
            elem.remove();
            e.preventDefault();
            e.stopPropagation();
        })
    }
}

