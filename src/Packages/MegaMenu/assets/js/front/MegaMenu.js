export default class MegaMenu {
    megaMenu;
    mainArrow;
    firstLevel;
    secondLevels;

    init() {
        this.megaMenu = document.getElementById('megaMenu');
        this.mainArrow = document.getElementById('firstLevelArrow');
        this.firstLevel = this.megaMenu.querySelector('.firstLevel');
        this.firstLevelItemArrows = this.firstLevel.querySelectorAll('.firstLevelItem > a .arrow');
        this.secondLevelArrows = this.firstLevel.querySelectorAll('.secondLevel  a .arrowMobile');
        this.addEventListeners();
    }

    addEventListeners() {
        this.addMainArrowEventListener();
        this.addFirstLevelEventListeners();
        this.addSecondLevelEventListeners();
    }

    addMainArrowEventListener() {
        this.mainArrow.addEventListener('click', () => {
            this.toggleClass(this.mainArrow, 'expand');
            this.toggleClass(this.firstLevel, 'expand');
        });
    }

    addFirstLevelEventListeners() {
        this.firstLevelItemArrows.forEach((arrow) => {
            arrow.addEventListener('click', (e) => {
                e.stopPropagation();
                e.preventDefault();
                this.toggleClass(arrow, 'expand');
                this.toggleClass(this.getParentFromArrow(arrow, 'second'), 'expand');
            });
        });
    }

    addSecondLevelEventListeners() {
        this.secondLevelArrows.forEach((arrow) => {
            arrow.addEventListener('click', (e) => {
                e.stopPropagation();
                e.preventDefault();
                this.toggleClass(arrow, 'expand');
                this.toggleClass(this.getParentFromArrow(arrow, 'third'), 'expand');
            })
        });
    }

    getParentFromArrow(arrowClicked, level) {
        return arrowClicked.parentElement.parentElement.querySelector(`.${level}Level`);
    }

    toggleClass(elem, className) {
        if (elem.classList.contains(className)) {
            elem.classList.remove(className);
        } else {
            elem.classList.add(className);
        }
    }

    checkIfElementsAreVisible() {
        let firstLevel, firstLevelItemArrows, secondLevelArrows;
        let megaMenu = document.getElementById('megaMenu');
        let mainArrow = document.getElementById('firstLevelArrow');
        if (megaMenu) {
           firstLevel = megaMenu.querySelector('.firstLevel');
        }
        if (firstLevel) {
             firstLevelItemArrows = firstLevel.querySelectorAll('.firstLevelItem > a .arrow');
             secondLevelArrows = firstLevel.querySelectorAll('.secondLevel  a .arrowMobile');
        }
        return megaMenu && mainArrow && firstLevel && firstLevelItemArrows && secondLevelArrows;
    }
}