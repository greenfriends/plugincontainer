<?php


namespace PluginContainer\Packages\MegaMenu\Mapper;


use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class MegaMenu extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfMegaMenuCategoryList');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `categoryOrder` LONGTEXT NOT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function dropTable(): void
    {
        $sql = "DROP TABLE $this->tableName;";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function clearCategoryList(): void
    {
        $sql = "TRUNCATE TABLE $this->tableName;";
        $this->handleStatement($sql);
    }
}