<?php


namespace PluginContainer\Packages\MegaMenu\Repository;


use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\MegaMenu\Mapper\MegaMenu as MenuMenuMapper;
use PluginContainer\Packages\MegaMenu\Model\MegaMenu as MegaMenuModel;


class MegaMenu extends BaseRepository
{
    public function __construct(MenuMenuMapper $mapper)
    {
        parent::__construct(MegaMenuModel::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createMegaMenuTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteMegaMenuTable(): void
    {
        $this->mapper->dropTable();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function categoryListExists(): bool
    {
        return $this->mapper->getCount()[0] >= 1;
    }

    /**
     * @throws \Exception
     */
    public function clearCategoryList(): void
    {
        $this->mapper->clearCategoryList();
    }
}