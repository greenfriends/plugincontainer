<?php


namespace PluginContainer\Packages\MegaMenu\Model;


class MegaMenu
{

    /**
     * @var string
     */
    private $categoryOrder;
    /**
     * @var int|null
     */
    private $id;

    public function __construct(string $categoryOrder,int $id = null)
    {
        $this->categoryOrder = $categoryOrder;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategoryOrder(): string
    {
        return $this->categoryOrder;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


}