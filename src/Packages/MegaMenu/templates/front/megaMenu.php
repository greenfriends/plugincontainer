<?php
/**
 * @var $categoryList
 * @see \PluginContainer\Packages\MegaMenu\Controller\MegaMenu::shortCodeView()
 */
?>

<div id="megaMenu">
    <div id="megaMenuHeading">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
        </svg>
        <span><?=__('PREGLED KATEGORIJA', 'plugin-container')?></span>
        <div class="arrow" id="firstLevelArrow">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
        </svg>
        </div>
    </div>
    <ul class="firstLevel">
        <?php foreach($categoryList as $category): ?>
            <?php if($category['topLevelCategory']['productCount'] === '0') {continue;} ?>
            <li class="firstLevelItem">
                <a href="<?=$category['topLevelCategory']['url']?>">
                    <span class="firstLevelItemTitle"><?=$category['topLevelCategory']['name']?></span>
                    <?php if(isset($category['secondLevelCategories'])): ?>
                    <div class="arrow">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                        </svg>
                    </div>
                    <?php endif;?>
                </a>
                <?php if(isset($category['secondLevelCategories'])): ?>
                    <ul class="secondLevel">
                        <?php foreach($category['secondLevelCategories'] as $secondLevelCategory):?>
                            <?php if($secondLevelCategory['productCount'] === '0') {continue;}?>
                            <li class="secondLevelItem">
                                <a title="<?=$secondLevelCategory['name']?>" href="<?=$secondLevelCategory['url']?>">
                                    <?php if(isset($secondLevelCategory['categoryImage']) && $secondLevelCategory['categoryImage'] !== ''):?>
                                        <img src="<?=$secondLevelCategory['categoryImage']?>" alt="<?=$secondLevelCategory['name']?>">
                                    <?php endif; ?>
                                    <?=$secondLevelCategory['name']?>
                                    <?php if(isset($secondLevelCategory['thirdLevelCategories'])): ?>
                                    <div class="arrow arrowMobile">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                                        </svg>
                                    </div>
                                    <?php endif;?>
                                </a>
                                    <?php if(isset($secondLevelCategory['thirdLevelCategories'])): ?>
                                        <ul class="thirdLevel">
                                        <?php foreach($secondLevelCategory['thirdLevelCategories'] as $thirdLevelCategory):?>
                                            <?php if($thirdLevelCategory['productCount'] === '0') {continue;}?>
                                            <li class="thirdLevelItem">
                                                <a title="<?=$thirdLevelCategory['name']?>" href="<?=$thirdLevelCategory['url']?>">
                                                    <?=$thirdLevelCategory['name']?>
                                                </a>
                                            </li>
                                        <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                            </li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>