<?php

/**
 * @var $categoryList
 * @see \PluginContainer\Packages\MegaMenu\Controller\MegaMenu::getView()
 */
use PluginContainer\Core\WpBridge\Translations as T;
?>
<h2><?=__('Category sorting', 'plugin-container')?></h2>
<form data-method="saveForm" id="sortableCategoriesForm">
    <div id="sortableCategories">
        <ul class="topLevel">
            <?php foreach($categoryList as $category): ?>
                <?php
                    if($category['topLevelCategory']['name'] === 'Nekategorisano' || $category['topLevelCategory']['name'] === 'Uncategorized') {
                        continue;
                    }
                    $catObject = get_term_by('id',(int)$category['topLevelCategory']['id'],'product_cat');
                    $catName = $catObject->name;
                    $catUrl = get_term_link($catObject->term_id,'product_cat');
                    $productCount = $catObject->count;
                    if(is_wp_error($catUrl)) {
                        continue;
                    }
                ?>
                <li class="accordionFirstLevel">
                    <h2><?=$catName?>
                        <input class="imageInput" type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][topLevelCategory][categoryImage]" value="<?=get_term_meta($catObject->term_id, 'megaMenuImage', true)?>">
                        <div class="uploadImageButton" title="<?=__('Upload Image', 'plugin-container')?>">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                            </svg>
                        </div>
                        <div class="productCount <?= $productCount > 0 ? 'full' : 'empty'?>">
                            <?=$productCount?>
                        </div>
                    </h2>
                    <div>
                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][topLevelCategory][id]" value="<?=$category['topLevelCategory']['id']?>">
                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][topLevelCategory][name]" value="<?=$catName?>">
                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][topLevelCategory][url]" value="<?=$catUrl?>">
                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][topLevelCategory][productCount]" value="<?=$productCount?>">
                        <ul class="secondLevel">
                            <?php foreach($category['secondLevelCategories'] as $secondLevelCategory):?>
                            <?php
                                $catObject = get_term_by('id',(int)$secondLevelCategory['id'],'product_cat');
                                $catName = $catObject->name;
                                $catUrl = get_term_link($catObject->term_id,'product_cat');
                                $productCount = $catObject->count;
                                if(is_wp_error($catUrl)) {
                                    continue;
                                }
                                ?>
                                <li class="accordionSecondLevel">
                                    <h4><?=$catName?>
                                        <input class="imageInput" type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][categoryImage]" value="<?=get_term_meta($catObject->term_id, 'megaMenuImage', true)?>">
                                        <div class="uploadImageButton" title="<?=__('Upload Image', 'plugin-container')?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                            </svg>
                                        </div>
                                        <div class="productCount <?= $productCount > 0 ? 'full' : 'empty'?>">
                                            <?=$productCount?>
                                        </div>
                                    </h4>
                                    <div>
                                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][id]" value="<?=$secondLevelCategory['id']?>">
                                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][name]" value="<?=$catName?>">
                                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][url]" value="<?=$catUrl?>">
                                        <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][productCount]" value="<?=$productCount?>">
                                        <ul class="thirdLevel">
                                            <?php foreach($secondLevelCategory['thirdLevelCategories'] as $thirdLevelCategory):?>
                                                <?php
                                                $catObject = get_term_by('id',(int)$thirdLevelCategory['id'],'product_cat');
                                                $catName = $catObject->name;
                                                $catUrl = get_term_link($catObject->term_id,'product_cat');
                                                if(is_wp_error($catUrl)) {
                                                   continue;
                                                }
                                                $productCount = $catObject->count;
                                                ?>
                                                <li class="thirdLevelItem">
                                                    <h5><?=$catName?>
                                                        <input class="imageInput" type="hidden"  name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][thirdLevelCategories][<?=$thirdLevelCategory['id']?>][categoryImage]" value="<?=get_term_meta($catObject->term_id, 'megaMenuImage', true)?>">
                                                        <div class="uploadImageButton" title="<?=__('Upload Image', 'plugin-container')?>">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                                            </svg>
                                                        </div>
                                                        <div class="productCount <?= $productCount > 0 ? 'full' : 'empty'?>">
                                                            <?=$productCount?>
                                                        </div>
                                                    </h5>
                                                    <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][thirdLevelCategories][<?=$thirdLevelCategory['id']?>][id]" value="<?=$thirdLevelCategory['id']?>">
                                                    <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][thirdLevelCategories][<?=$thirdLevelCategory['id']?>][name]" value="<?=$catName?>">
                                                    <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][thirdLevelCategories][<?=$thirdLevelCategory['id']?>][url]" value="<?=$catUrl?>">
                                                    <input type="hidden" name="categories[<?=$category['topLevelCategory']['id']?>][secondLevelCategories][<?=$secondLevelCategory['id']?>][thirdLevelCategories][<?=$thirdLevelCategory['id']?>][productCount]" value="<?=$productCount?>">
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    </div>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
    <input type="submit" value="<?=__('Save', 'plugin-container')?>" class="greenButton">
</form>
<button data-method="resetCategories" id="reset" class="greenButton"><?=__('Reset order and delete all data', 'plugin-container')?></button>
