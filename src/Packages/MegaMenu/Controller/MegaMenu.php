<?php


namespace PluginContainer\Packages\MegaMenu\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Packages\MegaMenu\Repository\MegaMenu as MegaMenuRepo;
use PluginContainer\Packages\WcUtilityPack\Controller\WcUtilityPack;

class MegaMenu extends BaseController implements ControllerWithSettingPageInterface, ControllerWithSetupInterface,
                                                 ControllerWithHooksInterface
{

    /**
     * @var MegaMenuRepo
     */
    private $megaMenuRepo;
    /**
     * @var CacheWrapper
     */
    private $cache;

    private $cacheKey = 'gf_megaMenu';


    public function __construct(Feature $featureRepo, Logger $logger, MegaMenuRepo $megaMenuRepo, CacheWrapper $cache)
    {
        parent::__construct($featureRepo, $logger);
        $this->megaMenuRepo = $megaMenuRepo;
        $this->cache = $cache;
    }

    public function init(): void
    {
        add_shortcode('gfMegaMenu', [$this, 'shortCodeView']);
        // Enqueue wp media to use the native wp media library for images
        add_action('admin_enqueue_scripts', function ()
        {
            wp_enqueue_media();
        });
        // Displays the shortcode in the given menu's description (used to display the mega menu shortcode in the nav)
        add_filter('walker_nav_menu_start_el', function ($itemOutput, $item)
        {
            if (!is_object($item) || !isset($item->object)) {
                return $itemOutput;
            }
            if ($item->post_title === 'PREGLED KATEGORIJA') {
                $itemOutput = do_shortcode($item->description);
            }
            return $itemOutput;
        }, 20, 2);
    }

    /**
     * @return AjaxResponse
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getView(): AjaxResponse
    {
        ob_start();
        $categoryList = $this->getCategoryList();
        include __DIR__ . '/../templates/megaMenuList.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->megaMenuRepo->createMegaMenuTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->megaMenuRepo->deleteMegaMenuTable();
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function saveForm(): AjaxResponse
    {
        $results = $this->megaMenuRepo->getAll(null, null, false);
        if (isset($results[0])) {
            $results[0]['categoryOrder'] = serialize($_POST['categories']);
            $this->megaMenuRepo->update($results[0]);
            $this->saveCatImages($_POST['categories']);
            $this->clearCacheAndGenerateNewOne();
            return new AjaxResponse(true, 'Successfully updated categories');
        }
        $this->megaMenuRepo->create(['categoryOrder' => serialize($_POST['categories'])]);
        $this->saveCatImages($_POST['categories']);
        $this->clearCacheAndGenerateNewOne();
        return new AjaxResponse(true, 'Successfully saved categories');
    }

    /**
     * @param $categoryArray
     * @return void
     */
    private function saveCatImages($categoryArray): void
    {
        foreach ($categoryArray as $categoryData) {
            update_term_meta($categoryData['topLevelCategory']['id'], 'megaMenuImage',
                $categoryData['topLevelCategory']['categoryImage']);
            foreach ($categoryData['secondLevelCategories'] as $secondLevelCategory) {
                update_term_meta($secondLevelCategory['id'], 'megaMenuImage', $secondLevelCategory['categoryImage']);
                foreach ($secondLevelCategory['thirdLevelCategories'] as $thirdLevelCategory) {
                    update_term_meta($thirdLevelCategory['id'], 'megaMenuImage', $thirdLevelCategory['categoryImage']);
                }
            }
        }
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function resetCategories(): AjaxResponse
    {
        $this->megaMenuRepo->clearCategoryList();
        $this->clearCacheAndGenerateNewOne();
        return new AjaxResponse(true, 'Category reset was successful');
    }

    /**
     * @throws \ReflectionException
     */
    private function clearCacheAndGenerateNewOne(): void
    {
        //Clear cache for megaMenu
        $this->cache->delete($this->cacheKey);
        //We call generate shortcode here, so we generate html for the first time after clearing cache and save it to cache
        $this->shortCodeView();
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function shortCodeView(): string
    {
        $html = $this->cache->get($this->cacheKey);
        if (!$html) {
            $categoryList = $this->getCategoryList();
            ob_start();
            include __DIR__ . '/../templates/front/megaMenu.php';
            $html = ob_get_clean();
            $this->cache->set($this->cacheKey, $html, 300);
        }
        return $html;
    }

    /**
     * @return int
     */
    private function getCategoriesCount(): int
    {
        $args = [
            'show_option_none' => '',
            'hide_empty' => 0,
            'taxonomy' => 'product_cat'
        ];
        return count(get_categories($args));
    }

    /**
     * @return array|mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    private function getCategoryList()
    {
        $categoryListWp = WcUtilityPack::getCategoryListFromWp();
        if ($this->megaMenuRepo->categoryListExists()) {
            $categoryList = unserialize($this->megaMenuRepo->getAll()[0]->getCategoryOrder(), ['allowed_classes' => false]);
            return $this->mergeWithNewCatsIfNeeded($categoryList, $categoryListWp);
        }
        return $categoryListWp;
    }

    private function mergeWithNewCatsIfNeeded($categoryList, $categoryListWp): array
    {
       $catArray = $this->deleteDeletedCategories($categoryList, $categoryListWp);
       return  $this->addNewCategories($catArray, $categoryListWp);
    }

    private function deleteDeletedCategories($categoryList, $categoryListWp)
    {
        foreach ($categoryList as $firstLevelId => $category) {
            if (!array_key_exists($firstLevelId, $categoryListWp)) {
                unset($categoryList[$firstLevelId]);
                continue;
            }
            foreach ($category['secondLevelCategories'] as  $secondLevelId => $categoryOrderData) {
                if (!array_key_exists($secondLevelId, $categoryListWp[$firstLevelId]['secondLevelCategories'])) {
                    unset($categoryList[$firstLevelId]['secondLevelCategories'][$secondLevelId]);
                    continue;
                }
                foreach ($categoryOrderData['thirdLevelCategories'] as $thirdLevelId => $categoryThirdLevelOrderData) {
                    if (!array_key_exists($thirdLevelId, $categoryListWp[$firstLevelId]['secondLevelCategories'][$secondLevelId]['thirdLevelCategories'])) {
                        unset($categoryList[$firstLevelId]['secondLevelCategories'][$secondLevelId]['thirdLevelCategories'][$thirdLevelId]);
                    }
                }
            }
        }
        return $categoryList;
    }

    private function addNewCategories($categoryList, $categoryListWp)
    {
        foreach ($categoryListWp as $firstLevelId => $category) {
            if (!array_key_exists($firstLevelId, $categoryList)) {
                $categoryList[$firstLevelId] = $category;
                continue;
            }
            foreach ($category['secondLevelCategories'] as  $secondLevelId => $categoryOrderData) {
                if (!array_key_exists($secondLevelId, $categoryList[$firstLevelId]['secondLevelCategories'])) {
                    $categoryList[$firstLevelId]['secondLevelCategories'][$secondLevelId] = $categoryOrderData;
                    continue;
                }
                foreach ($categoryOrderData['thirdLevelCategories'] as $thirdLevelId => $categoryThirdLevelOrderData) {
                    if (!array_key_exists($thirdLevelId, $categoryList[$firstLevelId]['secondLevelCategories'][$secondLevelId]['thirdLevelCategories'])) {
                        $categoryList[$firstLevelId]['secondLevelCategories'][$secondLevelId]['thirdLevelCategories'][$thirdLevelId] = $categoryThirdLevelOrderData;
                    }
                }
            }
        }
        return $categoryList;
    }
}
