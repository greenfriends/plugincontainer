<?php

namespace PluginContainer\Packages\WcOrderStatusFlow\Model;

class WcOrderStatusFlow
{

    /**
     * @var string
     */
    private $paymentMethodName;
    /**
     * @var array
     */
    private $flowData;
    /**
     * @var int|null
     */
    private $id;

    public function __construct(string $paymentMethodName, string $flowData, int $id = null)
    {
        $this->paymentMethodName = $paymentMethodName;
        $this->flowData = $flowData;
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPaymentMethodName(): string
    {
        return $this->paymentMethodName;
    }

    /**
     * @return string
     */
    public function getFlowData(): string
    {
        return $this->flowData;
    }
}