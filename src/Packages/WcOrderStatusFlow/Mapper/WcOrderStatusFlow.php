<?php

namespace PluginContainer\Packages\WcOrderStatusFlow\Mapper;

use Exception;
use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class WcOrderStatusFlow extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfOrderStatusFlow');
    }

    /**
     * @throws Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `paymentMethodName` varchar(100) UNIQUE NOT NULL,
		  `flowData` LONGTEXT NOT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }
}