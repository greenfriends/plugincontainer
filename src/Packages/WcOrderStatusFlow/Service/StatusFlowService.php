<?php

namespace PluginContainer\Packages\WcOrderStatusFlow\Service;

use PluginContainer\Core\Logger\Logger;
use PluginContainer\Packages\BankReportParser\Service\Parsers\BaseReportParser;
use PluginContainer\Packages\WcOrderStatusFlow\Repository\WcOrderStatusFlow;
use WC_Gateway_Nestpay;

class StatusFlowService
{

    /**
     * @var WcOrderStatusFlow
     */
    private $repo;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(WcOrderStatusFlow $repo, Logger $logger)
    {
        $this->repo = $repo;
        $this->logger = $logger;
    }

    /**
     * @param \WC_Order $order
     * @param $handler
     * @return mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getNextStatusInFlow(\WC_Order $order, $handler)
    {
        $this->statusHandlerPermissionCheck($handler, $order);
        /** @var \PluginContainer\Packages\WcOrderStatusFlow\Model\WcOrderStatusFlow $flow */
        $flow = $this->repo->getBy('paymentMethodName', $order->get_payment_method())[0];
        $flowData = unserialize($flow->getFlowData());
        $currentKey = array_search('wc-'.$order->get_status(), $flowData, true);
        if (isset($flowData[$currentKey + 1])) {
            return $flowData[$currentKey + 1];
        }
        $exception =  new \Exception('No next status in flow');
        $this->logger->debug('Something tried to change order status, but status was already on final one',
            ['paymentMethod' => $order->get_payment_method(), 'orderId' => $order->get_id(),
                'status' => $order->get_status(), 'flowData' => $flowData, 'trace' => $exception->getTraceAsString()]);
        throw $exception;
    }

    /**
     * @param $handler
     * @param \WC_Order $order
     * @return void
     * @throws \Exception
     */
    private function statusHandlerPermissionCheck($handler, \WC_Order $order): void
    {
        $orderStatus = $order->get_status();
        $orderPaymentMethod = $order->get_payment_method();
        switch ($handler){
            case $handler instanceof BaseReportParser:
                if ($orderStatus === 'pending' && $orderPaymentMethod === 'bacs') {
                    break;
                }
                if ($orderStatus === 'delivered' && $orderPaymentMethod === 'cod') {
                    break;
                }
                $msg = 'Handler does not have permission to change order status from this status';
                $this->logger->error($msg,
                    ['paymentMethod' => $orderPaymentMethod, 'orderId' => $order->get_id(), 'status' => $orderStatus, 'handler' => $handler]);
                throw new \Exception($msg);
                break;
            case $handler instanceof WC_Gateway_Nestpay:
                if ($orderStatus === 'pending' && $orderPaymentMethod === 'nestpay') {
                    break;
                }
                $msg = 'Handler does not have permission to change order status from this status';
                $this->logger->error($msg,
                                     ['paymentMethod' => $orderPaymentMethod, 'orderId' => $order->get_id(), 'status' => $orderStatus, 'handler' => $handler]);
                throw new \Exception($msg);
                break;
            default:
                $msg = 'Handler does not exist in status permission check function';
                $this->logger->error($msg,
                    ['paymentMethod' => $orderPaymentMethod, 'orderId' => $order->get_id(), 'status' => $orderStatus, 'handler' => $handler]);
                throw new \Exception($msg);
        }
    }
}