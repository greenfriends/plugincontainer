<?php

namespace PluginContainer\Packages\WcOrderStatusFlow\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\WcOrderStatusFlow\Mapper\WcOrderStatusFlow as WcOrderStatusFlowMapper;
use PluginContainer\Packages\WcOrderStatusFlow\Model\WcOrderStatusFlow as WcOrderStatusFlowModel;

class WcOrderStatusFlow extends BaseRepository
{

    public function __construct(WcOrderStatusFlowMapper $mapper)
    {
        parent::__construct(WcOrderStatusFlowModel::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $this->mapper->deleteTable();
    }
}