import Tab from "../../../../../assets/admin/js/Tab.js";

export default class WcOrderStatusFlowPage extends Tab{
    constructor() {
        super();
        this.addStatusButtons = document.querySelectorAll('.addNextStatusButton');
        this.removeLastStatusButtons = document.querySelectorAll('.removeLastStatusButton');
        this.submitFormButton = document.getElementById('submitFlowForm');
    }

    init() {
        this.addAddStatusButtonsListeners();
        this.addSubmitButtonEventListener();
    }


    addAddStatusButtonsListeners(){
        this.addStatusButtons.forEach((elem) => {
            elem.addEventListener('click', (e) =>{
                e.preventDefault();
                let select = e.target.previousElementSibling;
                e.target.parentElement.insertBefore(select.cloneNode(true), e.target);
            })
        })
        this.removeLastStatusButtons.forEach((elem) => {
            elem.addEventListener('click', (e) =>{
                e.preventDefault();
                let selects = e.target.parentElement.getElementsByClassName('orderStatusSelect');
                if (selects.length > 1) {
                    selects[selects.length-1].remove();
                }
            })
        })
    };
    addSubmitButtonEventListener(){
        this.submitFormButton.addEventListener('click', (e)=>{
            e.preventDefault();
            let form = e.target.parentElement;
            let data = new FormData(form);
            data.append('method', e.target.getAttribute('data-method'));
            this.fetchResponse(data, document.getElementById('loaderContainer') ).then(response => {
                console.log(response);
            });
        })
    }
}