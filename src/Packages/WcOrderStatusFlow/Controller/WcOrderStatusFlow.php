<?php
namespace PluginContainer\Packages\WcOrderStatusFlow\Controller;

use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\WcOrderStatusFlow\Repository\WcOrderStatusFlow as WcOrderStatusFlowRepo;

class WcOrderStatusFlow extends BaseController implements ControllerWithSetupInterface,
                                                          ControllerWithSettingPageInterface,
                                                          ControllerWithHooksInterface
{
    /**
     * @var WcOrderStatusFlowRepo
     */
    private $repo;

    public function __construct(Feature $featureRepo, Logger $logger, WcOrderStatusFlowRepo $repo)
    {
        parent::__construct($featureRepo, $logger);
        $this->repo = $repo;
    }

    public function init():void
    {
        $this->removeNonEssentialWcStatuses();
        $this->setDefaultOrderStatus();
        $this->sendNewOrderEmailOnPendingStatus();
    }

    private function removeNonEssentialWcStatuses(): void
    {
        add_filter('wc_order_statuses', static function ($statuses) {
            if (isset($statuses['wc-on-hold'])){
                unset($statuses['wc-on-hold']);
            }
            if (isset($statuses['wc-failed'])){
                unset($statuses['wc-failed']);
            }
            if (isset($statuses['wc-processing'])){
                unset($statuses['wc-processing']);
            }
            return $statuses;
        });
    }

    /**
     * @return AjaxResponse
     */
    public function getView(): AjaxResponse
    {
        ob_start();
        include __DIR__ . '/../templates/wcOrderStatusFlowPage.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function saveFlowForm(): AjaxResponse
    {
        foreach ($_GET['paymentsStatusFlow'] as $key => $value){
            /** @var \PluginContainer\Packages\WcOrderStatusFlow\Model\WcOrderStatusFlow $flowData */
            $flowData = $this->repo->getBy('paymentMethodName', $key)[0];
            if ($flowData) {
                $this->repo->update(['paymentMethodName' => $key, 'flowData' => serialize($value), 'id' => $flowData->getId()]);
            } else {
                $this->repo->create(['paymentMethodName' => $key, 'flowData' => serialize($value)]);
            }
        }
        return new AjaxResponse(true, 'Successfully saved status flow');
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->repo->createTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->repo->deleteTable();
    }

    public function setDefaultOrderStatus(): void
    {
        /** @var \PluginContainer\Packages\WcOrderStatusFlow\Model\WcOrderStatusFlow[] $statusFlows */
        $statusFlows = $this->repo->getAll();
        if (count($statusFlows) === 0) {
            return;
        }
        foreach($statusFlows as $statusFlow)
        {
            $flowData = unserialize($statusFlow->getFlowData());
            add_filter("woocommerce_{$statusFlow->getPaymentMethodName()}_process_payment_order_status", static function () use ($flowData) {
                return $flowData[0];
            });
        }

    }

    private function sendNewOrderEmailOnPendingStatus(){
        add_action( 'woocommerce_checkout_order_processed', function ($orderId){
            $order = wc_get_order($orderId);
            if(!$order->has_status( 'pending')) {
                return;
            }
            WC()->mailer()->get_emails()['WC_Email_Customer_On_Hold_Order']->trigger($orderId);
            WC()->mailer()->get_emails()['WC_Email_New_Order']->trigger($orderId);
        }, 20, 1 );
    }
}