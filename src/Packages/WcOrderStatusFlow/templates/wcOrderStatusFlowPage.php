<?php

use PluginContainer\Packages\WcOrderStatusFlow\Model\WcOrderStatusFlow;

$gateways = WC()->payment_gateways()->get_available_payment_gateways();
$availableStatuses = wc_get_order_statuses();
?>
<form id="wcOrderStatusFlowForm">
   <?php
   /** @var WC_Payment_Gateway[] $gateways */
   foreach ($gateways as $gateway):?>
   <?php
       /** @var WcOrderStatusFlow $savedFlow */
       $savedFlow = $this->repo->getBy('paymentMethodName', $gateway->id)[0];
   ?>
   <label>
       <h2><?=$gateway->id?></h2>
       <?php if ($savedFlow): $savedFlowData = unserialize($savedFlow->getFlowData());?>
       <?php foreach ($savedFlowData as $status):?>
               <select class="orderStatusSelect" name="paymentsStatusFlow[<?=$gateway->id?>][]">
                   <?php foreach ($availableStatuses as $key => $availableStatus):?>
                       <option value="<?=$key?>"<?=$key === $status ? 'selected' : ''?>><?=$availableStatus?></option>
                   <?php endforeach;?>
               </select>
       <?php endforeach;?>
       <?php else:?>
           <select class="orderStatusSelect" name="paymentsStatusFlow[<?=$gateway->id?>][]">
               <?php foreach ($availableStatuses as $key => $availableStatus):?>
                   <option value="<?=$key?>"><?=$availableStatus?></option>
               <?php endforeach;?>
           </select>
       <?php endif;?>
       <button class="addNextStatusButton">Add status</button>
       <button class="removeLastStatusButton">Remove last status</button>
   </label>
   <?php endforeach;?>
    <button id="submitFlowForm" data-method="saveFlowForm" type="submit"><?='Submit'?></button>
    <span id="loaderContainer"></span>
</form>
