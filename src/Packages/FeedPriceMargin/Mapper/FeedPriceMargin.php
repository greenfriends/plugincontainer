<?php

namespace PluginContainer\Packages\FeedPriceMargin\Mapper;

use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class FeedPriceMargin extends BaseMapper
{

    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfFeedPriceMargin');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `categoryId` int(11) NOT NULL,
		  `supplierId` int(11) NOT NULL,
		  `rules` text NOT NULL,
		  `limiter` text NOT NULL,
		  `margin` text NOT NULL,
		  `defaultMargin` int(3) UNSIGNED DEFAULT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  (id)
		) DEFAULT CHARSET=utf8mb4;";
        $this->handleStatement($sql);
    }

    public function deleteOld($supplierId)
    {
        $sql = "DELETE FROM {$this->tableName} WHERE supplierId = {$supplierId}";
        $this->handleStatement($sql);
    }
}