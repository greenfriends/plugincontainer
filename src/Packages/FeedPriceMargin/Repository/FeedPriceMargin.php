<?php

namespace PluginContainer\Packages\FeedPriceMargin\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\FeedPriceMargin\Mapper\FeedPriceMargin as Mapper;
use PluginContainer\Packages\FeedPriceMargin\Model\FeedPriceMargin as Model;

class FeedPriceMargin extends BaseRepository
{

    public function __construct(Mapper $mapper)
    {
        parent::__construct(Model::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }

    public function deleteOld($supplierId)
    {
        $this->mapper->deleteOld($supplierId);
    }
}