import Tab from "../../../../../assets/admin/js/Tab.js";

export default class FeedImportPage extends Tab {
    constructor() {
        super();
        this.supplierFilter = document.getElementById('supplierFilter');
        this.saveMapping = document.getElementById('saveMapping');
    }

    init() {
        this.addListeners();
    }

    showModalForMarginInput(button) {
        button.parentElement.querySelector('.setMarginsModal').classList.remove('hidden');
        this.addOverlayToBody();
    }

    hideModalForMarginInput(button) {
        button.parentElement.classList.add('hidden');
        this.removeOverlayFromBody();
    }

    addListeners(){
        this.supplierFilter.addEventListener('change', async (e) => {
            this.removeNotice();
            let urlParams = new URLSearchParams({
                action: 'gfAjaxRouter',
                handlerClass: 'PluginContainerPackagesFeedPriceMarginControllerFeedPriceMargin',
                method: 'getMappingPage',
                supplierId: e.target.value
            });
            this.isLoading = true;
            this.startLoader();
            let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`);
            let response = JSON.parse(await data.text());
            if (!response.success) {
                alert('error');
            }
            document.getElementById('feedFormWrapper').innerHTML = response.body.data;
            this.removeLoader();
            jQuery('.accordionFirstLevel').accordion({
                collapsible: true,
                header: ">h2",
                heightStyle: "content",
                active:false,
                icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
            });
            jQuery('.accordionSecondLevel').accordion({
                collapsible: true,
                header: ">h4",
                heightStyle: "content",
                active:false,
                icons: {"header": "ui-icon-plus", "activeHeader": "ui-icon-minus"}
            });
            let removeFeedPriceMarginButtons = document.querySelectorAll('.removeFeedPriceMargin');
            removeFeedPriceMarginButtons.forEach((button) => {
                button.addEventListener('click', (e) => {
                    e.target.parentElement.remove();
                })
            });
            let addFeedMarginRulesButtons = document.querySelectorAll('.setMarginsButton');
            addFeedMarginRulesButtons.forEach((button) => {
                button.addEventListener('click', (e) => {
                    e.stopPropagation();
                    this.showModalForMarginInput(button);
                });
                button.parentElement.querySelector('.setMarginsModal').addEventListener('click', (e) => {
                    e.stopPropagation();
                })
                let addFeedPriceMarginButton = button.parentElement.querySelector('.setMarginsModal .addFeedPriceMargin');
                if(addFeedPriceMarginButton) {
                    addFeedPriceMarginButton.addEventListener('click', (e) => {
                        if ('content' in document.createElement('template')) {
                            let parentContainer = e.target.parentElement;
                            let template = parentContainer.querySelector('template');
                            let clone = template.content.cloneNode(true);
                            parentContainer.appendChild(clone);
                            parentContainer.lastElementChild.querySelector('.removeFeedPriceMargin').addEventListener('click',(e) => {
                                e.target.parentElement.remove();
                            });
                        }
                    });
                }
                let removeFeedPriceMarginButton = button.parentElement.querySelector('.setMarginsModal .removeFeedPriceMargin');
                if(removeFeedPriceMarginButton) {
                    removeFeedPriceMarginButton.addEventListener('click', (e) => {
                        e.target.parentElement.remove();
                    })
                }
                button.parentElement.querySelector('.setMarginsModal .closeFeedModal').addEventListener('click', (e) => {
                    e.stopPropagation();
                    this.hideModalForMarginInput(e.target);
                });
            });
        });

        this.saveMapping.addEventListener('click', async (e) => {
            this.startLoader();
            let feedForm = document.getElementById('marginForm');
            if (feedForm) {
                let supplierId = this.supplierFilter.value;
                let formData = new FormData(feedForm);
                let urlParams = new URLSearchParams({
                    action: 'gfAjaxRouter',
                    handlerClass: 'PluginContainerPackagesFeedPriceMarginControllerFeedPriceMargin',
                    method: 'saveMapping',
                    supplierId: supplierId
                });
                let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`, {body:formData, method:'post'});
                let response = JSON.parse(await data.text());
                this.removeLoader();
                this.printNotice(response);
            }
        });
    };
}