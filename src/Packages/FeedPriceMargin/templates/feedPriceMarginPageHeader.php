<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\Wishlist\Controller\Wishlist;

if(!is_user_logged_in()):?>
<h2><?=__(
        sprintf('You need to be logged in to use this page, you can login <a href="%s">here</a>',
                get_permalink( get_option('woocommerce_myaccount_page_id'))), 'plugin-container')?></h2>
<?php else:
global $gfContainer;

endif; ?>
<div id="priceMarginContainer">
    <div class="filterWrapper">
        <label>
            Podešavanje cena i marža za feed
            <select id="supplierFilter" name="supplierId">
                <option disabled selected value="-1">Izaberite dobavljača</option>
                <?php foreach (\SUPPLIERS as $supplier):?>
                    <option value="<?=$supplier['supplierId']?>" <?=($supplier['supplierId'] == @$_GET['supplierId']) ? 'selected':''?>><?=$supplier['name']?></option>
                <?php endforeach;?>
            </select>
        </label>
    </div>

    <div id="feedFormWrapper">

    </div>
    <input type="button" value="Snimi" id="saveMapping" class="greenButton" />
</div>


