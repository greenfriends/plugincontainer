<?php
/**
 * @var $categoryList
 * @see \PluginContainer\Packages\FeedPriceMargin\Controller\FeedPriceMargin::getMappingPage()
 */

global $gfContainer;
?>
<?php

?>
<form method="post" id="marginForm">
    <?php
    if ($msg): ?>
        <p><strong><?= $msg ?></strong></p>
    <?php
    endif; ?>

    <?php
    $defaultMargin = '';
    if (isset($items[0])) {
        $defaultMargin = $items[0]->getDefaultMargin();
    } ?>
    <label>
        <?= __('Podrazumevana marza', 'plugin-container') ?>
        <input class="defaultMargin" name="defaultMargin" type="number" value="<?= $defaultMargin ?>"/>
        <span>%</span>
    </label>
    <?php
    if ($categoryList): ?>
        <div id="feedMarginPriceList">
            <ul class="topLevel">
                <?php
                foreach ($categoryList as $category): ?>
                    <?php
                    $catObject = get_term_by('id', (int)$category['topLevelCategory']['id'], 'product_cat');
                    $catName = $catObject->name;
                    $catUrl = get_term_link($catObject->term_id, 'product_cat');
                    if (is_wp_error($catUrl)) {
                        continue;
                    }
                    ?>
                    <li class="accordionFirstLevel">
                        <h2><?= $catName ?>
                            <div class="setMarginsButton" title="<?= __('Postavi marže', 'plugin-container') ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"/>
                                </svg>
                            </div>
                            <div class="setMarginsModal hidden">
                                <div class="closeFeedModal" title="<?= __('Zatvori', 'plugin-container') ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                    </svg>
                                </div>
                                <h4><?= __(
                                        'Postavljanje marži za kategoriju: ',
                                        'plugin-container'
                                    ) ?><?= $catName ?></h4>
                                <p class="addFeedPriceMargin greenButton"><?= __(
                                        'Dodaj novo pravilo',
                                        'plugin-container'
                                    ) ?></p>
                                <?php
                                include(CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceInputs.php') ?>
                                <template>
                                    <div>
                                        <label>
                                            <?= __('Uslov', 'plugin-container') ?>
                                            <select name="categories[<?= $catObject->term_id ?>][rules][]">
                                                <option value="-1"><?= __('Izaberi', 'plugin-container') ?></option>
                                                <option value="gt">>=</option>
                                                <option value="lt"><=</option>
                                            </select>
                                        </label>
                                        <label>
                                            <?= __('Iznos', 'plugin-container') ?>
                                            <input name="categories[<?= $catObject->term_id ?>][limiter][]"
                                                   type="number">
                                            <span>RSD</span>
                                        </label>
                                        <label>
                                            <?= __('Marža', 'plugin-container') ?>
                                            <input name="categories[<?= $catObject->term_id ?>][margin][]"
                                                   type="number">
                                            <span>%</span>
                                        </label>
                                        <div class="removeFeedPriceMargin">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                            </svg>
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </h2>
                        <div>
                            <input type="hidden"
                                   name="categories[<?= $category['topLevelCategory']['id'] ?>][topLevelCategory][id]"
                                   value="<?= $category['topLevelCategory']['id'] ?>">
                            <ul class="secondLevel">
                                <?php
                                foreach ($category['secondLevelCategories'] as $secondLevelCategory): ?>
                                    <?php
                                    $catObject = get_term_by('id', (int)$secondLevelCategory['id'], 'product_cat');
                                    $catName = $catObject->name;
                                    $catUrl = get_term_link($catObject->term_id, 'product_cat');
                                    if (is_wp_error($catUrl)) {
                                        continue;
                                    }
                                    ?>
                                    <li class="accordionSecondLevel">
                                        <h4><?= $catName ?>
                                            <div class="setMarginsButton"
                                                 title="<?= __('Postavi marže', 'plugin-container') ?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                     viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2"
                                                          d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"/>
                                                </svg>
                                            </div>
                                            <div class="setMarginsModal hidden">
                                                <div class="closeFeedModal"
                                                     title="<?= __('Zatvori', 'plugin-container') ?>">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                         viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              stroke-width="2"
                                                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                                    </svg>
                                                </div>
                                                <h4><?= __(
                                                        'Postavljanje marži za kategoriju: ',
                                                        'plugin-container'
                                                    ) ?><?= $catName ?></h4>
                                                <p class="addFeedPriceMargin greenButton"><?= __(
                                                        'Dodaj novo pravilo',
                                                        'plugin-container'
                                                    ) ?></p>
                                                <?php
                                                include(CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceInputs.php') ?>
                                                <template>
                                                    <div>
                                                        <label>
                                                            <?= __('Uslov', 'plugin-container') ?>
                                                            <select name="categories[<?= $catObject->term_id ?>][rules][]">
                                                                <option value="-1"><?= __(
                                                                        'Izaberi',
                                                                        'plugin-container'
                                                                    ) ?></option>
                                                                <option value="gt">>=</option>
                                                                <option value="lt"><=</option>
                                                            </select>
                                                        </label>
                                                        <label>
                                                            <?= __('Iznos', 'plugin-container') ?>
                                                            <input name="categories[<?= $catObject->term_id ?>][limiter][]"
                                                                   type="number">
                                                            <span>RSD</span>
                                                        </label>
                                                        <label>
                                                            <?= __('Marža', 'plugin-container') ?>
                                                            <input name="categories[<?= $catObject->term_id ?>][margin][]"
                                                                   type="number">
                                                            <span>%</span>
                                                        </label>
                                                        <div class="removeFeedPriceMargin">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6"
                                                                 fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                                      stroke-width="2"
                                                                      d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </template>
                                            </div>
                                        </h4>
                                        <div>
                                            <input type="hidden"
                                                   name="categories[<?= $category['topLevelCategory']['id'] ?>][secondLevelCategories][<?= $secondLevelCategory['id'] ?>][id]"
                                                   value="<?= $secondLevelCategory['id'] ?>">
                                            <ul class="thirdLevel">
                                                <?php
                                                foreach ($secondLevelCategory['thirdLevelCategories'] as $thirdLevelCategory): ?>
                                                    <?php
                                                    $catObject = get_term_by(
                                                        'id',
                                                        (int)$thirdLevelCategory['id'],
                                                        'product_cat'
                                                    );
                                                    $catName = $catObject->name;
                                                    $catUrl = get_term_link($catObject->term_id, 'product_cat');
                                                    if (is_wp_error($catUrl)) {
                                                        continue;
                                                    }
                                                    ?>
                                                    <li class="thirdLevelItem">
                                                        <h5><?= $catName ?>
                                                            <div class="setMarginsButton"
                                                                 title="<?= __('Postavi marže', 'plugin-container') ?>">
                                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6"
                                                                     fill="none" viewBox="0 0 24 24"
                                                                     stroke="currentColor">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          stroke-width="2"
                                                                          d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="setMarginsModal hidden">
                                                                <div class="closeFeedModal"
                                                                     title="<?= __('Zatvori', 'plugin-container') ?>">
                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                         class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                                                         stroke="currentColor">
                                                                        <path stroke-linecap="round"
                                                                              stroke-linejoin="round" stroke-width="2"
                                                                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                                                    </svg>
                                                                </div>
                                                                <h4><?= __(
                                                                        'Postavljanje marži za kategoriju: ',
                                                                        'plugin-container'
                                                                    ) ?><?= $catName ?></h4>
                                                                <p class="addFeedPriceMargin greenButton"><?= __(
                                                                        'Dodaj novo pravilo',
                                                                        'plugin-container'
                                                                    ) ?></p>
                                                                <?php
                                                                include(CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceInputs.php') ?>
                                                                <template>
                                                                    <div>
                                                                        <label>
                                                                            <?= __('Uslov', 'plugin-container') ?>
                                                                            <select name="categories[<?= $catObject->term_id ?>][rules][]">
                                                                                <option value="-1"><?= __(
                                                                                        'Izaberi',
                                                                                        'plugin-container'
                                                                                    ) ?></option>
                                                                                <option value="gt">>=</option>
                                                                                <option value="lt"><=</option>
                                                                            </select>
                                                                        </label>
                                                                        <label>
                                                                            <?= __('Iznos', 'plugin-container') ?>
                                                                            <input name="categories[<?= $catObject->term_id ?>][limiter][]"
                                                                                   type="number">
                                                                            <span>RSD</span>
                                                                        </label>
                                                                        <label>
                                                                            <?= __('Marža', 'plugin-container') ?>
                                                                            <input name="categories[<?= $catObject->term_id ?>][margin][]"
                                                                                   type="number">
                                                                            <span>%</span>
                                                                        </label>
                                                                        <div class="removeFeedPriceMargin">
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                 class="h-6 w-6" fill="none"
                                                                                 viewBox="0 0 24 24"
                                                                                 stroke="currentColor">
                                                                                <path stroke-linecap="round"
                                                                                      stroke-linejoin="round"
                                                                                      stroke-width="2"
                                                                                      d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </h5>
                                                        <input type="hidden"
                                                               name="categories[<?= $category['topLevelCategory']['id'] ?>][secondLevelCategories][<?= $secondLevelCategory['id'] ?>][thirdLevelCategories][<?= $thirdLevelCategory['id'] ?>][id]"
                                                               value="<?= $thirdLevelCategory['id'] ?>">
                                                    </li>
                                                <?php
                                                endforeach; ?>
                                            </ul>
                                        </div>
                                    </li>
                                <?php
                                endforeach; ?>
                            </ul>
                        </div>
                    </li>
                <?php
                endforeach; ?>
            </ul>
        </div>
    <?php
    endif; ?>
</form>


