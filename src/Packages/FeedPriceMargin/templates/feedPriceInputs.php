<?php
/* @var \PluginContainer\Packages\FeedPriceMargin\Model\FeedPriceMargin[] $items
 * @see \PluginContainer\Packages\FeedPriceMargin\Controller\FeedPriceMargin::getMappingPage()
 *
 * @var WP_Term $catObject
 */
foreach ($items as $item):
    if ($item->getCategoryId() !== $catObject->term_id) {
        continue;
    }
    $limiter = unserialize($item->getLimiter());
    $rules = unserialize($item->getRules());

    $margins = unserialize($item->getMargin());

    ?>
    <?php
    $i = 0;
    foreach ($rules as $rule):
        echo '<div>';
        $limit = $limiter[$i];
        $margin = $margins[$i];

        ?>
        <label>
            <?= __('Uslov', 'plugin-container') ?>
            <select name="categories[<?= $catObject->term_id ?>][rules][]">
                <option value="-1"><?= __('Izaberi', 'plugin-container') ?></option>
                <option <?php
                if ($rule === 'gt') {
                    echo 'selected';
                } ?> value="gt">>=
                </option>
                <option <?php
                if ($rule === 'lt') {
                    echo 'selected';
                } ?> value="lt"><=
                </option>
            </select>
        </label>
        <label>
            <?= __('Iznos', 'plugin-container') ?>
            <input name="categories[<?= $catObject->term_id ?>][limiter][]" type="number" value="<?= $limit ?>">
            <span>RSD</span>
        </label>
        <label>
            <?= __('Marža', 'plugin-container') ?>
            <input name="categories[<?= $catObject->term_id ?>][margin][]" type="number" value="<?= $margin ?>">
            <span>%</span>
        </label>
        <div class="removeFeedPriceMargin">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                 stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
            </svg>
        </div>
        </div>
        <?php
        $i++; endforeach;?>

        <?php endforeach; ?>