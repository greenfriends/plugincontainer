<?php

namespace PluginContainer\Packages\FeedPriceMargin\Model;

class FeedPriceMargin
{
    private $id;
    private $categoryId;
    private $supplierId;
    private $rules;
    private $limiter;
    private $margin;
    private $defaultMargin;
    private $createdAt;
    private $updatedAt;

    /**
     * Redirect constructor.
     *
     * @param string $fromUrl
     * @param string $toUrl
     * @param string $statusCode
     * @param int|null $id
     * @param int $active
     * @param int|null $userId
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(
        int $id = null,
        int $categoryId,
        int $supplierId,
        string $rules,
        string $limiter,
        string $margin,
        int $defaultMargin = null,
        string $createdAt = null,
        string $updatedAt = null
    ) {
        $this->id = $id;
        $this->categoryId = $categoryId;
        $this->supplierId = $supplierId;
        $this->rules = $rules;
        $this->limiter = $limiter;
        $this->margin = $margin;
        $this->defaultMargin = $defaultMargin;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSupplierId(): int
    {
        return $this->supplierId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return string
     */
    public function getRules(): string
    {
        return $this->rules;
    }

    /**
     * @return string
     */
    public function getLimiter(): string
    {
        return $this->limiter;
    }

    /**
     * @return string
     */
    public function getMargin(): string
    {
        return $this->margin;
    }

    /**
     * @return int
     */
    public function getDefaultMargin(): int
    {
        return $this->defaultMargin;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
}