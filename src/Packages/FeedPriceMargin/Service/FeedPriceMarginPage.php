<?php


namespace PluginContainer\Packages\FeedPriceMargin\Service;


class FeedPriceMarginPage
{
    public $pageSlug;

//    private $categoryMapper;

//    private $ignoredVendors = [];

    public function __construct()
    {
        $this->pageSlug = 'feedPriceMargin';
//        $this->categoryMapper = new CategoryMapper();
    }

    public function initShortcode()
    {
        add_shortcode('feedPriceMargin_page', [$this, 'pageView']);

    }

    public function createPage(): void
    {
        $isPageCreated = get_page_by_title('FeedPriceMargin', 'OBJECT', 'page');
        // Check if the page already exists
        if (empty($isPageCreated)) {
            $pageId = wp_insert_post(
                [
                    'comment_status' => 'close',
                    'ping_status' => 'close',
                    'post_author' => 1,
                    'post_title' => 'FeedPriceMargin',
                    'post_name' => $this->pageSlug,
                    'post_status' => 'publish',
                    'post_content' => '[feedPriceMargin_page]',
                    'post_type' => 'page'
                ]
            );
            add_filter('display_post_states', static function ($post_states, $post) use ($pageId) {
                if ($post->ID === $pageId) {
                    $post_states[] = 'Do not delete this page';
                }
                return $post_states;
            }, 10, 2);
        }
    }

    /**
     * Callback for the [wishlist_page] shortcode.
     * @return false|string
     */
    public function pageView() :?string
    {
        $msg = false;
        if (isset($_GET['supplierId'])) {


        }

        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceMarginPage.php';
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getPageUrl() :string
    {
        return get_home_url() . '/' . $this->pageSlug . '/';
    }


    public function deletePage(): void
    {
        /**
         * @var \WP_Post $page
         */
        $page = get_page_by_title('FeedPriceMargin', 'OBJECT', 'page');
        if($page) {
            wp_delete_post($page->ID,true);
        }
    }
}