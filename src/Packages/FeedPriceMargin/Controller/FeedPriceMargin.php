<?php

namespace PluginContainer\Packages\FeedPriceMargin\Controller;

use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\FeedPriceMargin\Repository\FeedPriceMargin as Repo;
use PluginContainer\Packages\FeedImport\Service\CategoryMapper;

use PluginContainer\Packages\FeedPriceMargin\Service\FeedPriceMarginPage;
use PluginContainer\Packages\WcUtilityPack\Controller\WcUtilityPack;

class FeedPriceMargin extends BaseController implements ControllerWithHooksInterface, ControllerWithSetupInterface
{

    /**
     * @var FeedImportPage
     */
    private $page;
    private $categoryMapper;

    /* @var CacheWrapper */
    private $cache;
    private $repo;

//    private $ignoredVendors = [];

    public function __construct(
        Feature $featureRepo, Logger $logger, FeedPriceMarginPage $page, CacheWrapper $cache, Repo $repo, \PDO $pdo
    ) {
        parent::__construct($featureRepo, $logger);
        $this->page = $page;
        $this->cache = $cache;
        $this->repo = $repo;
        $this->categoryMapper = new CategoryMapper($pdo, $logger);
    }

    public function init(): void
    {
        $this->page->initShortcode();
        $this->addOriginalCostColumnToProductList();
    }

    /**
     * @throws \Exception
     */
    public function setup() :void
    {
        $this->repo->createTable();
        $this->page->createPage();
    }

    /**
     * @throws \Exception
     */
    public function cleanup() :void
    {
        $this->repo->deleteTable();
        $this->page->deletePage();
    }

    public function getView(): AjaxResponse
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceMarginPageHeader.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    public function getMappingPage()
    {
        $msg = false;
        $categoryList = WcUtilityPack::getCategoryListFromWp();
        $items = [];
        if (isset($_GET['supplierId']) && $_GET['supplierId']) {
            $items = $this->getData((int) $_GET['supplierId']);
        }

        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedPriceMargin/templates/feedPriceMarginPage.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());

        return $response;
    }

    public function saveMapping()
    {
        $supplierId = $_GET['supplierId'];
        $rules = [];
        $this->repo->deleteOld($supplierId);
        foreach ($_POST['categories'] as $categoryId => $data) {
            // multiple rules allowed
            $margin = [];
            $limiter = [];
            $rules = [];
            if ((int) $data['margin'] > 0) {
                $margin = $data['margin'];
            }
            if ((int) $data['limiter'] > 0) {
                $limiter = $data['limiter'];
                $rules = $data['rules'];
            }
            $this->repo->create([
                'supplierId' => $supplierId,
                'margin' => serialize($margin),
                'limiter' => serialize($limiter),
                'rules' => serialize($rules),
                'categoryId' => $categoryId,
                'defaultMargin' => (int) $_POST['defaultMargin'],
            ]);
        }
        return new AjaxResponse(true,__('Data Saved','plugin-container'));
    }

    private function getData($supplierId)
    {
        return $this->repo->getBy('supplierId', $supplierId, true);
    }

    private function addOriginalCostColumnToProductList(): void
    {
        add_filter('manage_edit-product_columns', function ($columns) {
            $array = [];
            foreach ($columns as $key => $value) {
                if ($key === 'price') {
                    $array['originalPrice'] = __('Originalna cena', 'plugin-container');
                }
                $array[$key] = $value;
            }
            return $array;
        }, 11);
        add_action('manage_product_posts_custom_column', function($column, $productId) {
            if($column === 'originalPrice') {
                echo number_format((int)get_post_meta($productId, 'inputPrice', true), 2) .' '. get_woocommerce_currency_symbol();
            }
        }, 10, 2);
    }

}
