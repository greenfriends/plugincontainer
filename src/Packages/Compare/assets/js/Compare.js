import BaseFetcher from "../../../../../assets/front/js/BaseFetcher.js";
export default class Compare extends BaseFetcher  {
    handlerClass = 'PluginContainerPackagesCompareControllerCompare';
    compareButtons = document.querySelectorAll('.compareProduct');
    compareModal = document.getElementById('compareModal');
    closeModal = document.getElementById('closeCompareModal');
    compareOverlay = document.getElementById('compareOverlay');
    compareContentProducts = document.getElementById('compareContentProducts');
    compareButton = document.getElementById('compareProductsButton');
    deleteAllButton = document.getElementById('deleteAllCompareProducts');
    limitReachedTitle = document.getElementById('limitReachedTitle');
    compareButtonInMenu = document.getElementById('compareMenuItem');
    compareButtonMenuIcon = document.querySelector('#compareMenuItem .icon-wrapper');
    compareProductsWindow = document.getElementById('compareProductsWindow');
    compareProductsWindowContent = document.getElementById('compareProductsContent');
    closeCompareProductsWindow = document.getElementById('closeCompareProductsWindow');
    productInWindowTemplate = document.getElementById('productWindowTemplate');
    keyPrefix = 'gfCpr_';
    maximumItemsToCompare = 3;


    init() {
        let self = this;
        if(this.compareButtons.length > 0) {
            this.compareButtons.forEach((compareButton) => {
                compareButton.addEventListener('click', async function handler() {
                    let productId = compareButton.getAttribute('data-id');

                    if (self.isMaximumItemsThresholdReached()) {
                        self.limitReachedTitle.classList.add('shown');
                    } else {
                        await localStorage.setItem(`${self.keyPrefix}${productId}`, JSON.stringify({
                            productId: productId,
                            timestamp: Date.now()
                        }));
                    }
                    self.startLoader(compareButton);
                    self.printModal().then(() => {
                        self.removeLoader(compareButton);
                        self.toggleClass(self.compareModal, 'showModal');
                        self.toggleClass(self.compareOverlay, 'showOverlay');
                        self.toggleClass(document.body, 'modalIsOpen');
                        self.printCheckmark(compareButton);
                    });
                });
            });
        }
        this.addCloseModalEventListener();
        this.removeExpiredItemsFromLocalStorage();
        this.addDeleteAllListener();
        this.addMenuCompareItemListener();
        this.addCompareProductsListener();
        this.addCloseModalOnBodyClickListener();
    }

    checkIfElementsAreVisible() {
        return this.compareModal && this.closeModal && this.compareContentProducts
            && this.deleteAllButton && this.compareButtonInMenu;
    }

    toggleClass(elem, className) {
        if (elem.classList.contains(className)) {
            elem.classList.remove(className);
        } else {
            elem.classList.add(className);
        }
    }


    printCheckmark(elem)
    {
        elem.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 checkmarkCompare" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
        </svg>`;
    }

    addMenuCompareItemListener() {
        this.compareButtonInMenu.addEventListener('click',(e) => {
           e.preventDefault();
            this.startLoader(this.compareButtonMenuIcon);
            this.printModal().then(() => {
                this.toggleClass(self.compareModal,'showModal');
                this.toggleClass(self.compareOverlay,'showOverlay');
                this.toggleClass(document.body,'modalIsOpen');
                let icon = `<svg class="compareMenuSvg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#444444" d="M18,10a1,1,0,0,0-1-1H5.41l2.3-2.29A1,1,0,0,0,6.29,5.29l-4,4a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,11H17A1,1,0,0,0,18,10Zm3.92,3.62A1,1,0,0,0,21,13H7a1,1,0,0,0,0,2H18.59l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l4-4A1,1,0,0,0,21.92,13.62Z"/></svg>`;
                this.removeLoader(this.compareButtonMenuIcon,icon);
            });
        });
    }

    addCloseModalOnBodyClickListener() {
        document.body.addEventListener('click',(e) => {
            if(e.target !== this.compareModal
                && !this.compareModal.contains(e.target)
                && !e.target.classList.contains('removeProduct')
                && this.compareModal.classList.contains('showModal')) {
                this.closeInitialModal();
            }
        });
    }

    closeInitialModal() {
        this.toggleClass(this.compareModal,'showModal');
        this.toggleClass(self.compareOverlay,'showOverlay');
        this.toggleClass(document.body,'modalIsOpen');
    }
    addCloseModalEventListener() {
        this.closeModal.addEventListener('click',() => {
            this.closeInitialModal();
        })
    }

    addDeleteAllListener() {
        this.deleteAllButton.addEventListener('click',() => {
            let keys = this.getItemKeysFromLocalStorage();
            keys.forEach((key) => {
               localStorage.removeItem(key);
            });
           this.compareContentProducts.innerHTML = '';
           this.limitReachedTitle.classList.remove('shown');
        });
    }

    /**
     * Checks whether the maximum number of compare items have been reached in the localstorage by iterating
     * over the items in the storage by the prefix
     * @return {boolean}
     */
    isMaximumItemsThresholdReached() {
        let items = this.getItemKeysFromLocalStorage();
        return items.length >= this.maximumItemsToCompare;
    }

    /**
     * @return {string[]}
     */
    getItemKeysFromLocalStorage() {
        return Object.keys(localStorage).filter(item => item.startsWith(`${this.keyPrefix}`));
    }

    /**
     * @return {*[]}
     */
    getProductIdsFromLocalStorage() {
        let keys = this.getItemKeysFromLocalStorage();
        let productIds = [];
        keys.forEach((key) => {
            productIds.push(JSON.parse(localStorage.getItem(key)).productId);
        });
        return productIds;
    }

    /**
     * Removes the expired items from the localstorage based on the difference in time
     */
    removeExpiredItemsFromLocalStorage() {
        let time = Date.now();
        let keys = this.getItemKeysFromLocalStorage();
        keys.forEach((key) => {
            let item = JSON.parse(localStorage.getItem(key));
            let oneWeekInSeconds = 604800;
            if(Math.floor((time - item.timestamp) / 1000) >= oneWeekInSeconds) {
                localStorage.removeItem(key);
            }
        });
    }

    async printModal() {
        let productIds = this.getProductIdsFromLocalStorage();
        let params = {
          method: 'getProductsForCompare',
          productIds: productIds
        };
        let response = await this.fetchResponse(params);
        let responseBody = await response.body;
        let products = responseBody.products;
        let jsonProducts = JSON.parse(products);
        this.compareModal.productData = jsonProducts;
        await this.printProductsInModal(jsonProducts);
    }

    /**
     * Appends product nodes to the compare content element inside the modal
     * @param data
     * @return {Promise<unknown>}
     */
    printProductsInModal(data) {
        this.compareContentProducts.innerHTML = '';
        return new Promise((resolve) => {
            data.forEach((productData) => {
                this.compareContentProducts.appendChild(this.generateProduct(productData));
            });
            resolve();
        })
    }

    /**
     * Generate a product based on the data given
     * @param productData
     * @return {HTMLDivElement}
     */
    generateProduct(productData) {
        let productName = productData.productName;
        let productImage = productData.productImg;
        let productPermalink = productData.productPermalink;
        let productId = productData.productId;
        let productContainer = document.createElement('div');
        productContainer.classList.add('compareProductEntity');

        let productTitle = document.createElement('h3');
        productTitle.innerHTML = `<a href="${productPermalink}" title="${productName}">${productName}</a>`
        productContainer.appendChild(productTitle);

        let imageContainer = document.createElement('a');
        imageContainer.setAttribute('href',productPermalink);
        imageContainer.setAttribute('title', productName);
        imageContainer.innerHTML = productImage;
        productContainer.appendChild(imageContainer);

        let removeProductFromStorage = document.createElement('div');
        removeProductFromStorage.setAttribute('title','Ukloni proizvod');
        removeProductFromStorage.classList.add('removeProduct');
        removeProductFromStorage.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>`;
        removeProductFromStorage.addEventListener('click',() => {
           productContainer.remove();
           localStorage.removeItem(`${this.keyPrefix}${productId}`);
           this.compareModal.productData.forEach((productEntity,key) => {
               if(productEntity.productId === productData.productId) {
                   delete this.compareModal.productData[key];
               }
           });
           console.log(this.compareModal.productData);
           this.limitReachedTitle.classList.remove('shown');
        });
        productContainer.appendChild(removeProductFromStorage);
        return productContainer;
    }

    startLoader(elem)
    {
        elem.innerHTML = '<div class="gfLoader blue"></div>';
    }

    removeLoader(elem,icon = null)
    {
        if(icon !== null) {
            elem.innerHTML = icon;
            return;
        }
        elem.innerHTML = `<svg style="color:#1877f2;" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 noHover" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4" />
                         </svg>`;
    }

    /**
     * Prints the compare products window after clicking the compare products button in the modal
     * @param productData
     * @return {Promise<unknown>}
     */
    printCompareProductsWindow(productData) {
        return new Promise((resolve) => {
            if('content' in document.createElement('template')) {
                productData.forEach((product) => {
                   let clone = this.productInWindowTemplate.content.cloneNode(true);
                   let productElement = this.generateProductInCompareWindow(clone,product);
                   this.compareProductsWindowContent.appendChild(productElement);
                });
                this.compareProductsWindow.classList.add('shown');
            }
        })
    }

    /**
     * Generates a product with the data provided
     * @param cloneNode
     * @param productData
     */
    generateProductInCompareWindow(cloneNode,productData) {
        cloneNode.querySelector('.compareProductTopWindow').insertAdjacentHTML('beforeend',productData.productImg);
        cloneNode.querySelector('.compareProductTitle').innerText = productData.productName;
        cloneNode.querySelector('.compareProductPrice').innerHTML = productData.productPrice;
        cloneNode.querySelector('.compareProductDescription').innerHTML = productData.productDescription;
        let addToCart = cloneNode.querySelector('.compareProductAddToCart');
        console.log(productData.productType);
        if(productData.productType !== 'simple') {
            addToCart.remove();
        }
        addToCart.setAttribute('href',`?add-to-cart=${productData.productId}`);
        addToCart.setAttribute('data-product-id',productData.productId);
        addToCart.setAttribute('data-product_sku',productData.productSku);
        return cloneNode;
    }

    /**
     * Adds the click listener to the compare products button in the modal
     */
    addCompareProductsListener() {
        this.compareButton.addEventListener('click',async () => {
            let productData = this.compareModal.productData;
            this.toggleClass(this.compareModal,'showModal');

            await this.printCompareProductsWindow(productData);

        });
        this.closeCompareProductsWindow.addEventListener('click',() => {
           this.closeCompareWindowProducts();
        });
    }

    /**
     * Closes the window that prints the item compare (after clicking the compare button in the modal)
     */
    closeCompareWindowProducts() {
        this.compareProductsWindowContent.querySelectorAll('.productContentContainer').forEach((item) => {
            item.remove();
        });
        this.compareProductsWindow.classList.remove('shown');
        this.compareOverlay.classList.remove('showOverlay');
        document.body.classList.remove('modalIsOpen');
    }
}