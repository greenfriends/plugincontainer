<?php


namespace PluginContainer\Packages\Compare\Controller;


use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;

class Compare extends BaseController implements ControllerWithHooksInterface
{

    public function __construct(Feature $featureRepo, Logger $logger)
    {
        parent::__construct($featureRepo, $logger);
    }

    public function init(): void
    {
        $this->addCompareButtonHtmlInProductInfo();
        $this->addModalToFooter();
        $this->addMenuItem();
    }

    public function addCompareButtonHtmlInProductInfo(): void
    {
        add_action( 'woocommerce_after_shop_loop_item_title',function() {
            global $product;
            echo '<div title="' . __('Compare', 'plugin-container') .'" data-id="' . $product->get_id() .'" class="compareProductLoop compareProduct">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#444444" d="M18,10a1,1,0,0,0-1-1H5.41l2.3-2.29A1,1,0,0,0,6.29,5.29l-4,4a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,11H17A1,1,0,0,0,18,10Zm3.92,3.62A1,1,0,0,0,21,13H7a1,1,0,0,0,0,2H18.59l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l4-4A1,1,0,0,0,21.92,13.62Z"/></svg>
            </div>';
        },11);
        add_action( 'woocommerce_before_add_to_cart_form',function() {
            global $product;
            echo '<div title="' . __('Compare', 'plugin-container') .'" data-id="' . $product->get_id() .'" class="compareProductSinglePage compareProduct">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#444444" d="M18,10a1,1,0,0,0-1-1H5.41l2.3-2.29A1,1,0,0,0,6.29,5.29l-4,4a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,11H17A1,1,0,0,0,18,10Zm3.92,3.62A1,1,0,0,0,21,13H7a1,1,0,0,0,0,2H18.59l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l4-4A1,1,0,0,0,21.92,13.62Z"/></svg>
            <span>' . __('Compare', 'plugin-container') .'</span></div>';
        },11);
    }

    private function addModalToFooter(): void
    {
        add_action('wp_footer', static function() {
            include CONTAINER_PLUGIN_DIR . '/src/Packages/Compare/templates/compareModal.php';
        });
    }

    /**
     * @return AjaxResponse
     */
    public function getProductsForCompare(): AjaxResponse
    {
        $products = [];
        $data = [];
        if(isset($_GET['productIds']) && $_GET['productIds'] !== '') {
            $args = [
                'include' => explode(',',$_GET['productIds'])
            ];
            $products = wc_get_products($args);
        }
        if (count($products) > 0) {
            foreach ($products as $product) {
                $data[] = [
                    'productName' => $product->get_title(),
                    'productImg' => $product->get_image('thumbnail'),
                    'productPermalink' => $product->get_permalink(),
                    'productId' => $product->get_id(),
                    'productDescription' => $product->get_description(),
                    'productPrice' => $product->get_price_html(),
                    'productSku' => $product->get_sku(),
                    'productType' => $product->get_type(),
                ];
            }
        }

        $response =  new AjaxResponse(true);
        $response->addParamToBody('products',json_encode($data));
        return $response;
    }

    public function addMenuItem(): void
    {
        add_filter( 'wp_nav_menu_items', static function($items,$args) {
            if($args->theme_location === 'secondary') {
                $items = explode('</li>',$items);
                $items[] = $items[1];
                $items[1] = '<li class="menu-item menu-item-type-custom menu-item-object-custom">
                                <span id="compareMenuItem" title="' . __('Compare', 'plugin-container') .'">
                                ' . __('Compare', 'plugin-container') .'
                                <div class="icon-wrapper">
                                              <svg class="compareMenuSvg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#444444" d="M18,10a1,1,0,0,0-1-1H5.41l2.3-2.29A1,1,0,0,0,6.29,5.29l-4,4a1,1,0,0,0-.21,1.09A1,1,0,0,0,3,11H17A1,1,0,0,0,18,10Zm3.92,3.62A1,1,0,0,0,21,13H7a1,1,0,0,0,0,2H18.59l-2.3,2.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0l4-4A1,1,0,0,0,21.92,13.62Z"/></svg>
                                </div>
                                </span>
                            </li>';
                $items = implode($items);
            }
            return $items;
        }, 10, 2);
    }

}