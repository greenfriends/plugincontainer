<?php

use PluginContainer\Core\WpBridge\Translations as T;

?>
<div id="compareOverlay">
    <div id="compareModal">
        <div id="compareTop">
            <h2><?= __('Compare', 'plugin-container')?></h2>
            <div title="<?=__('Close', 'plugin-container')?>" id="closeCompareModal">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </div>
        </div>
        <h3 id="limitReachedTitle"><?=__('Maximum limit for compare reached, you need to remove one product and try again.', 'plugin-container')?></h3>
        <div id="compareContentProducts">

        </div>
        <div id="compareBottom">
            <button id="compareProductsButton" class="button"><?=__('Compare', 'plugin-container')?></button>
            <button id="deleteAllCompareProducts" class="button"><?=__('Remove all', 'plugin-container')?></button>
        </div>
    </div>

    <div id="compareProductsWindow">
        <div title="<?=__('Close', 'plugin-container')?>" id="closeCompareProductsWindow">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </div>
        <div id="compareProductsContent">
            <div id="tableHead">
                <p><?=__('Product', 'plugin-container')?></p>
                <p><?=__('Price', 'plugin-container')?></p>
                <p><?=__('Description', 'plugin-container')?></p>
                <p></p>
            </div>
        </div>

        <template id="productWindowTemplate">
            <div class="productContentContainer">
                <div class="compareProductTopWindow">
                    <h2 class="compareProductTitle"></h2>
                </div>
                <span class="compareProductPrice"></span>
                <p class="compareProductDescription"></p>
                <div>
                    <a data-product_id="" data-quanity="1" rel="nofollow" data-product_sku="" class="compareProductAddToCart button product_type_simple add_to_cart_button ajax_add_to_cart" href="" title="<?=__('Add to cart', 'plugin-container')?>">
                        <?=__('Add to cart', 'plugin-container')?>
                    </a>
                </div>
            </div>
        </template>
    </div>
</div>