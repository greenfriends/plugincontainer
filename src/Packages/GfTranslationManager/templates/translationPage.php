<div>
    <button class="greenButton" id="addNewEntry">Add string</button>
    <template id="inputTemplate">
        <tr>
            <td>
                <input size="100" type="text" value="" name="source[]"/>
            </td>
            <td>
                <input type="text" size="100" value="" name="translation[]"/>
            </td>
        </tr>
    </template>
</div>
<form id="form" method="post">
    <table>
        <tr>
            <td>Izvor</td>
            <td>Prevod</td>
        </tr>
        <?php
        /* @var \Translation_Entry $value */
        foreach ($mo->entries as $key => $value) { ?>
            <tr>
                <td><input readonly size="100" type="text" value="<?=esc_html($value->singular)?>" name="source[]"/>
                </td>
                <td><?php
                    foreach ($value->translations as $translation) { ?>
                        <input type="text" size="100" value="<?=esc_html($translation)?>" name="translation[]"/>
                    <?php
                    }
                    ?></td>
            </tr>
        <?php
        } ?>
    </table>
</form>

