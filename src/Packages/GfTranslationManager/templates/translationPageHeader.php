<?php

use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\Wishlist\Controller\Wishlist;

if(!is_user_logged_in()):?>
<h2><?=__(
        sprintf('You need to be logged in to use this page, you can login <a href="%s">here</a>',
                get_permalink( get_option('woocommerce_myaccount_page_id'))), 'plugin-container')?></h2>
<?php else:
global $gfContainer;

endif; ?>
<div id="priceMarginContainer">
    <div class="filterWrapper">
        <label>
            Upravljanje prevodima
            <select id="filter" name="domain">
                <option disabled selected value="-1">Izaberite domen</option>
                <?php foreach ($this->domains as $domain): ?>
                <option value="<?=$domain?>"><?=$domain?></option>
                <?php endforeach;?>
            </select>
        </label>
    </div>

    <div id="formWrapper">

    </div>
    <input type="button" value="Snimi" id="save" class="greenButton" />
</div>


