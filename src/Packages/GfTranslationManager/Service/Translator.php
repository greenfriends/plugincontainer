<?php

namespace PluginContainer\Packages\GfTranslationManager\Service;


class Translator
{
    private $ignoredDomains = [
        'query-monitor', 'a3-lazy-load', 'complianz-gdpr', 'contact-form-7', 'complianz-terms-conditions', 'one-click-demo-import',
        'woo-preview-emails', ''
    ];

    private $domains = [
        'shoptimizer', 'default', 'woocommerce', 'woocommerce-gateway-nestpay', 'plugin-container'
    ];

    private $translations;

    private $newItems;

    private $language;

    public function __construct($translations, $language)
    {
        $this->translations = $translations;
        $this->language = $language;
    }

    private function addTranslation($string, $domain)
    {

    }

    private function translate($string, $domain)
    {
        // new string detected
//        if (!isset($this->translations[$domain][$string])) {
//
//        }

        return $this->translations[$domain][$string];
    }

    public function getText($string, $domain)
    {
        global $l10n;


//        $test = load_child_theme_textdomain($domain);

//        var_dump($l10n);
//        die();

        $translations = get_translations_for_domain($domain);
        if (get_class($translations) === 'NOOP_Translations') {
            if (in_array($domain, $this->ignoredDomains)) {
                return $string;
            }

            if (in_array($domain, $this->domains)) {
                return $string;
//                return $this->translate($string, $domain);
            }

//            var_dump($domain);
            return $string;

//            throw new \Exception('Domain does not exists for translations. ' . $domain);
        } else {
//            if (get_class($translations) === 'MO') {
            if ($translations instanceof \MO) {
                /* @var \MO $translations */
//                var_dump($domain);
                return $string;
//                var_dump($string);
//                var_dump($translations->translate($string));
//                die();
            }
            var_dump(get_class($translations));
            die();
        }


    }
}