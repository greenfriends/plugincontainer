<?php

namespace PluginContainer\Packages\GfTranslationManager\Model;


class Translation
{
    const LANG_EN = 1;
    const LANG_SR = 2;

    private $id;

    private $originalString;

    private $translatedString;

    private $hash;

    private $language;

    private $domain;

    /**
     * Translation constructor.
     * @param $id
     * @param $originalString
     * @param $translatedString
     * @param $hash
     * @param $language
     * @param $domain
     */
    public function __construct($id, $originalString, $translatedString, $hash, $language, $domain)
    {
        $this->id = $id;
        $this->originalString = $originalString;
        $this->translatedString = $translatedString;
        $this->hash = $hash;
        $this->language = $language;
        $this->domain = $domain;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOriginalString()
    {
        return $this->originalString;
    }

    /**
     * @return mixed
     */
    public function getTranslatedString()
    {
        return $this->translatedString;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

}