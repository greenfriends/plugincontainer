import Tab from "../../../../../assets/admin/js/Tab.js";

export default class GfTranslationManager extends Tab {
    constructor() {
        super();
        this.filter = document.getElementById('filter');
        this.save = document.getElementById('save');
    }

    init() {
        this.addListeners();
    }

    addListeners() {
        this.filter.addEventListener('change', async (e) => {
            let urlParams = new URLSearchParams({
                action: 'gfAjaxRouter',
                handlerClass: 'PluginContainerPackagesGfTranslationManagerControllerGfTranslationManager',
                method: 'getPage',
                domain: e.target.value
            });
            this.isLoading = true;
            this.startLoader();
            let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`);
            let response = JSON.parse(await data.text());
            if (!response.success) {
                alert('error');
            }

            document.getElementById('formWrapper').innerHTML = response.body.data;
            document.getElementById('addNewEntry').addEventListener('click', (e) => {
                let form = document.getElementById('form');
                let template = document.getElementById('inputTemplate');
                let clone = template.content.cloneNode(true);
                form.prepend(clone);
            });
            this.removeLoader();
        });

        this.save.addEventListener('click', async (e) => {
            this.startLoader();
            window.scrollTo({
                top: 100,
                left: 100,
                behavior: 'smooth'
            });
            let form = document.getElementById('form');
            if (form) {
                let formData = new FormData(form);
                let urlParams = new URLSearchParams({
                    action: 'gfAjaxRouter',
                    handlerClass: 'PluginContainerPackagesGfTranslationManagerControllerGfTranslationManager',
                    method: 'saveTranslations',
                    domain: this.filter.value
                });
                let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`, {
                    body: formData,
                    method: 'post'
                });
                let response = JSON.parse(await data.text());
                this.removeLoader();
                this.printNotice(response);
            }
        });
    }
}