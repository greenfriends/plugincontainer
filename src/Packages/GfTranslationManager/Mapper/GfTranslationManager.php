<?php


namespace PluginContainer\Packages\GfTranslationManager\Mapper;


use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class GfTranslationManager extends BaseMapper
{

    /**
     * GfTranslationManager constructor.
     */
    public function __construct(\PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfTranslations');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `originalString` varchar(150) NOT NULL,
		  `translatedString` varchar(150) NOT NULL,
		  `hash` varchar(150) NOT NULL,
		  `language` int(3) NOT NULL,
		  `domain` varchar(30) NOT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  (id)
		) DEFAULT CHARSET=utf8mb4;";
        $this->handleStatement($sql);
    }

}