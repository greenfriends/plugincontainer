<?php

namespace PluginContainer\Packages\GfTranslationManager\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\GfTranslationManager\Mapper\GfTranslationManager as Mapper;
use PluginContainer\Packages\GfTranslationManager\Model\Translation;

class GfTranslationManager extends BaseRepository
{

    protected $mapper;

    /**
     * GfTranslationManager constructor.
     */
    public function __construct(Mapper $mapper)
    {
        parent::__construct(Translation::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }
}