<?php

namespace PluginContainer\Packages\GfTranslationManager\Controller;

use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Packages\GfTranslationManager\Service\Translator;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Packages\GfTranslationManager\Repository\GfTranslationManager as Repo;

class GfTranslationManager extends BaseController implements ControllerWithHooksInterface,
    ControllerWithSettingPageInterface, ControllerWithSetupInterface
{
    private $repo;
    private $translator;

    private $ignoredDomains = [
        'query-monitor', 'a3-lazy-load', 'complianz-gdpr', 'contact-form-7', 'complianz-terms-conditions', 'one-click-demo-import',
        'woo-preview-emails', ''
    ];

    private $domains = [
        'shoptimizer', 'default', 'woocommerce', 'woocommerce-gateway-nestpay', 'plugin-container', 'wordfence'
    ];

    /**
     * GfTranslationManager constructor.
     * @param Repo $repo
     * @param Feature $featureRepo
     * @param Logger $logger
     */
    public function __construct(Repo $repo, Feature $featureRepo, Logger $logger)
    {
        parent::__construct($featureRepo, $logger);
        $this->repo = $repo;

        $this->translator = new Translator([], 'sr');
    }

    public function init(): void
    {
        add_filter('gettext', [$this, 'translate'], 1, 3);
        add_filter( 'gettext_with_context',[$this,'translateWithContext'],10,4);
        add_filter('override_load_textdomain', [$this, 'loadTextDomain'], 1, 3);
    }



    public function loadTextDomain($domain, $mofile, $retval = null)
    {
    	global $l10n;

	    if (!is_readable($mofile)) {
            return false;
        }
        $data = get_transient(md5($mofile));
        $mtime = filemtime($mofile);

    	$mo = new \MO();
        if (!$data || !isset($data['mtime']) || $mtime > $data['mtime']) {
            if (!$mo->import_from_file($mofile)) {
                return false;
            }
            $data = array(
                'mtime' => $mtime,
                'entries' => $mo->entries,
                'headers' => $mo->headers
            );
            set_transient(md5($mofile), $data);
        } else {
            $mo->entries = $data['entries'];
            $mo->headers = $data['headers'];
        }

        if (isset($l10n[$domain])) {
            $mo->merge_with($l10n[$domain]);
        }
        $l10n[$domain] = $mo;

       return true;
    }


    //@todo: use context here, example: home breadcrump has the breadcrumb context
    public function translateWithContext($translated_text, $untranslated_text, $context, $domain): string
    {
        if (is_admin()) {
            return $translated_text;
        }

        if (in_array($domain, $this->ignoredDomains)) {
            return $translated_text;
        }
        global $l10n;

        /* @var \MO $t */
        if (!isset($l10n[$domain])) {
            $file = WP_CONTENT_DIR . '/languages/plugins/'.$domain.'-'. determine_locale() .'.mo';
            $this->loadTextDomain($domain, $file);
        }

        if (!isset($l10n[$domain])) {
            return $translated_text;
        }

        $t = $l10n[$domain];

        return $t->translate($untranslated_text);
    }
    public function translate($translated_text, $untranslated_text, $domain)
    {
        if (is_admin()) {
            return $translated_text;
        }

        if (in_array($domain, $this->ignoredDomains)) {
            return $translated_text;
        }
        global $l10n;

        /* @var \MO $t */
        if (!isset($l10n[$domain])) {
            $file = WP_CONTENT_DIR . '/languages/plugins/'.$domain.'-'. determine_locale() .'.mo';
            $this->loadTextDomain($domain, $file);
        }

        if (!isset($l10n[$domain])) {
            return $translated_text;
        }

        $t = $l10n[$domain];

        return $t->translate($untranslated_text);
    }

    private function saveTranslationsToFile()
    {
        $mo = new \MO();
        foreach ($_POST['source'] as $key => $source) {
            $entry = new \Translation_Entry([
                'singular' => stripslashes($source),
                'translations' => [stripslashes($_POST['translation'][$key])],
            ]);
            $mo->add_entry_or_merge($entry);
        }

        $file = WP_CONTENT_DIR . '/languages/plugins/'.$_GET['domain'].'-'. determine_locale() .'.mo';
        $mo->export_to_file($file);
    }

    public function saveTranslations()
    {
        if (isset($_GET['domain']) && $_GET['domain']) {
            try {
                $this->saveTranslationsToFile($_GET['domain']);
                $msg = 'saved data';
            } catch (\Exception $e) {
                $msg = $e->getMessage();
            }
        }

        ob_start();
        echo $msg;
//        include CONTAINER_PLUGIN_DIR . '/src/Packages/FeedImport/templates/feedImportPage.php';
        $response = new AjaxResponse(true, 'Uspešno sačuvano');
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    /**
     * @return AjaxResponse
     */
    public function getView(): AjaxResponse
    {
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Packages/GfTranslationManager/templates/translationPageHeader.php';
        $html = ob_get_clean();

        $response = new AjaxResponse(true, '');
        $response->addParamToBody('data', $html);

        return $response;
    }

    public function getPage()
    {
        $mo = new \MO();

        $domain = 'woocommerce';
        if (isset($_GET['domain']) && $_GET['domain']) {
            $domain = $_GET['domain'];
        }

        $file = WP_CONTENT_DIR . '/languages/plugins/'.$domain.'-'. determine_locale() .'.mo';
        $mo->import_from_file($file);

        ob_start();
        echo $file;
        include CONTAINER_PLUGIN_DIR . '/src/Packages/GfTranslationManager/templates/translationPage.php';
        $html = ob_get_clean();

        $response = new AjaxResponse(true, '');
        $response->addParamToBody('data', $html);

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function setup() :void
    {
        $this->repo->createTable();
        $this->repo->createIndex('gf_translation_language_index','language');
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->repo->deleteTable();
    }
}