<?php

namespace PluginContainer\Packages\WcCustomOrderStatus\Service;

class StatusSlugFormatter
{
    public static function formatStatusSlug(string $string): string
    {
        $prefix = 'wc-';
        if(strpos($string, $prefix) === 0) {
            $prefix = '';
        }
        return iconv('UTF-8','ASCII//TRANSLIT', $prefix.str_replace(" ", "-", strtolower($string)));
    }
}