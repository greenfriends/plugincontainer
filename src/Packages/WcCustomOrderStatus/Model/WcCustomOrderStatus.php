<?php

namespace PluginContainer\Packages\WcCustomOrderStatus\Model;

class WcCustomOrderStatus
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string|null
     */
    private $color;
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string|null
     */
    private $createdAt;
    /**
     * @var string|null
     */
    private $updatedAt;
    /**
     * @var string
     */
    private $slug;

    /**
     * @param string $name
     * @param string $slug
     * @param string|null $color
     * @param int|null $id
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(string $name, string $slug, string $color = null, int $id = null, string $createdAt = null,
        string $updatedAt = null)
    {
        $this->name = $name;
        $this->color = $color;
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

}