<?php

namespace PluginContainer\Packages\WcCustomOrderStatus\Controller;

use Exception;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Components\CrudTable\Column;
use PluginContainer\Core\Components\CrudTable\FormField;
use PluginContainer\Core\Components\CrudTable\Traits\CrudTable;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithCrudTableInterface;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\WcCustomOrderStatus\Model\WcCustomOrderStatus as WcCustomOrderStatusModel;
use PluginContainer\Packages\WcCustomOrderStatus\Repository\WcCustomOrderStatus as WcCustomOrderStatusesRepo;
use PluginContainer\Packages\WcCustomOrderStatus\Service\StatusSlugFormatter;

class WcCustomOrderStatus extends BaseController implements ControllerWithHooksInterface,
                                                            ControllerWithSetupInterface,
                                                            ControllerWithCrudTableInterface,
                                                            ControllerWithSettingPageInterface
{
    use CrudTable;
    /**
     * @var WcCustomOrderStatusesRepo
     */
    private $repo;

    /**
     * @param Feature $featureRepo
     * @param Logger $logger
     * @param CacheWrapper $cache
     * @param WcCustomOrderStatusesRepo $repo
     */
    public function __construct(Feature $featureRepo, Logger $logger, WcCustomOrderStatusesRepo $repo)
    {
        parent::__construct($featureRepo, $logger);
        $this->repo = $repo;
    }

    /**
     * @throws \Exception
     */
    public function create(): AjaxResponse
    {
        if (!isset($_GET['slug']) || $_GET['slug'] === '') {
            $_GET['slug'] = StatusSlugFormatter::formatStatusSlug($_GET['name']);
        } else {
            $_GET['slug'] = StatusSlugFormatter::formatStatusSlug($_GET['slug']);
        }
        $this->repo->create($_GET);

        return new AjaxResponse(true, 'Successfully created status');
    }

    /**
     * @throws \Exception
     */
    public function update(): AjaxResponse
    {
        if (!isset($_GET['slug']) || $_GET['slug'] === '') {
            $_GET['slug'] = StatusSlugFormatter::formatStatusSlug($_GET['name']);
        } else {
            $_GET['slug'] = StatusSlugFormatter::formatStatusSlug($_GET['slug']);
        }
        $this->repo->update($_GET);
        return new AjaxResponse(true, 'Successfully updated status');
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function delete(): AjaxResponse
    {
        if (isset($_GET['id'])) {
            $this->repo->delete((int)$_GET['id']);
            return new AjaxResponse(true, 'Successfully deleted status');
        }
        throw new Exception('Custom status id not found in request');
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function getFormFields(): AjaxResponse
    {
        $method = 'create';
        $formHeader = 'Create Status';
        $color = '#10b2daff';//default color
        if (isset($_GET['id'])){
            /** @var WcCustomOrderStatusModel $customStatus */
            $customStatus = $this->repo->getById((int)$_GET['id']);
            if ($customStatus) {
                $method = 'update';
                $formHeader = 'Update Status';
                $name = $customStatus->getName();
                $color = $customStatus->getColor();
                $slug = $customStatus->getSlug();
            } else {
                throw new Exception('Custom status you are trying to update does not exist');
            }
        }
        $nameField = new FormField(
            'name',
            'text',
            __('Name', 'plugin-container'),
            $name ?? null
        );
        $slugField = new FormField(
            'slug',
            'text',
            __('Slug', 'plugin-container'),
            $slug ?? null
        );
        $colorField = new FormField(
            'color',
            'text',
            __('Color', 'plugin-container'),
            $color
        );
        $response = new AjaxResponse(true);
        $response
            ->addParamToBody('data', [$nameField, $slugField, $colorField])
            ->addParamToBody('formHeader', $formHeader)
            ->addParamToBody('method', $method)
            ->addParamToBody('submitLabel', __('Submit', 'plugin-container'));
        return $response;
    }

    /**
     * @throws \ReflectionException
     */
    public function init():void
    {
        $this->registerStatuses();
        $this->setStatusColorsInAdmin();

    }
    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function getView(): AjaxResponse
    {
        $name = new Column('name', 'Name');
        $slug = new Column('slug', 'Slug');
        $color = new Column('color', 'Color');
        $crudTable = new \PluginContainer\Core\Components\CrudTable\CrudTable([$name, $slug, $color], $this->repo);
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [$crudTable->getList()]);
        return $response;
    }

    /**
     * @throws \Exception
     */
    public function setup():void
    {
        $this->repo->createTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup():void
    {
        $this->repo->deleteTable();
    }

    /**
     * @throws \ReflectionException
     */
    private function registerStatuses(): void
    {
        foreach ($this->getStatuses() as $status) {
            $statusName = $status->getName();
            $statusSlug = $status->getSlug();
            //Registers custom post status in wp
            register_post_status($statusSlug, [
                'label' => _x($status->getName(), 'Order status', 'pluginContainer'),
                'public' => true,
                'exclude_from_search' => false,
                'show_in_admin_all_list' => true,
                'show_in_admin_status_list' => true,
                'label_count' => _n_noop($statusName . ' <span class="count">(%s)</span>',
                    $statusName . '<span class="count">(%s)</span>', 'pluginContainer')
            ]);

            //Sets our registered status inside wc status list
            add_filter('wc_order_statuses', static function ($orderStatuses) use ($statusSlug, $statusName) {
                $orderStatuses[$statusSlug] = _x($statusName, 'Order status', 'pluginContainer');
                return $orderStatuses;
            });
        }
    }

    /**
     * @return WcCustomOrderStatusModel[]
     * @throws \ReflectionException
     */
    public function getStatuses(): array
    {
        return $this->repo->getAll();
    }

    /**
     * @throws \ReflectionException
     */
    private function setStatusColorsInAdmin(): void
    {
        add_action('admin_head-edit.php', function(){
            if (isset ($_GET['post_type'])&& $_GET['post_type'] === 'shop_order'){
                $statuses = $this->getStatuses();
                $style = '<style>';
                foreach ($statuses as $status) {
                    $style .= '.'.str_replace('wc-','status-', $status->getSlug());
                    $style .=  '{';
                    $style .=  "background-color: {$status->getColor()};";
                    $style .=  "color: white;";
                    $style .=  '}';
                    $style .=  ".{$status->getSlug()} *";
                    $style .=  '{';
                    $style .=  "color: {$status->getColor()}";
                    $style .=  '}';
                }
                $style .=  '</style>';
                echo $style;
            }
        });
    }
}