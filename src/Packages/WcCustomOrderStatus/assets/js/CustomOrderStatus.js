export default class CustomOrderStatus {
    init() {
        document.addEventListener('modalFormOpened', (e) => {
            let colorInput = document.getElementsByName("color")[0];
            colorInput.parentElement.insertAdjacentHTML('afterend','<button id="colorPickerButton">Pick color</button>');
            let picker = new Picker(document.getElementById('colorPickerButton'));
            picker.onChange = function(color) {
                colorInput.value = color.hex;
            };
        })
    };
}