<?php

namespace PluginContainer\Packages\WcCustomOrderStatus\Mapper;

use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class WcCustomOrderStatus extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfCustomOrderStatus');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `name` varchar(30) UNIQUE NOT NULL,
          `slug` varchar(30) UNIQUE NOT NULL,
		  `color` varchar(10) NOT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $sql = "DROP TABLE $this->tableName;";
        $this->handleStatement($sql);
    }

}