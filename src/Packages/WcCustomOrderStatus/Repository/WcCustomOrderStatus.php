<?php

namespace PluginContainer\Packages\WcCustomOrderStatus\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\WcCustomOrderStatus\Mapper\WcCustomOrderStatus as Mapper;
use PluginContainer\Packages\WcCustomOrderStatus\Model\WcCustomOrderStatus as Model;

class WcCustomOrderStatus extends BaseRepository
{
    public function __construct(Mapper $mapper)
    {
        parent::__construct(Model::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $this->mapper->deleteTable();
    }
}