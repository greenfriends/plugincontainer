<?php

namespace PluginContainer\Packages\ProductSlider\Controller;

use Automattic\WooCommerce\Blocks\BlockTypes\ProductSearch;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Service\WcProductSearch;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Core\WpBridge\WpEnqueue;
use PluginContainer\Packages\ProductSlider\Model\ProductSlider as ProductSliderModel;
use PluginContainer\Packages\ProductSlider\Model\SliderProduct as SliiderProductModel;
use PluginContainer\Packages\ProductSlider\Repository\SliderProduct;
use PluginContainer\Packages\ProductSlider\Repository\ProductSlider as SliderRepo;
use PluginContainer\Packages\ProductSlider\Widget\ProductSliderWidget;
use PluginContainer\Packages\ProductSlider\Widget\TabbedProductSliderWidget;

class ProductSlider extends BaseController implements ControllerWithSettingPageInterface, ControllerWithSetupInterface,
                                                      ControllerWithHooksInterface
{
    /**
     * @var SliderRepo
     */
    private $sliderRepo;
    /**
     * @var SliderProduct
     */
    private $productSliderRepo;
    /**
     * @var ProductSearch
     */
    private $productSearch;
    /**
     * @var CacheWrapper
     */
    private $cache;

    public function __construct(
        Feature $featureRepo, Logger $logger, SliderRepo $sliderRepo,
        SliderProduct $productSliderRepo, WcProductSearch $productSearch, CacheWrapper $cacheWrapper
    ) {
        parent::__construct($featureRepo, $logger);
        $this->sliderRepo = $sliderRepo;
        $this->productSliderRepo = $productSliderRepo;
        $this->productSearch = $productSearch;
        $this->cache = $cacheWrapper;
    }

    public function init(): void
    {
        add_action('widgets_init', function ()
        {
            register_widget(ProductSliderWidget::class);
            register_widget(TabbedProductSliderWidget::class);
        });
        add_action('woocommerce_update_product', [$this, 'resetSliderCacheOnProductUpdate'], 10);
        $this->filtersForSliderFrontend();
        $this->addLikeNameQueryToWc();
    }

    /**
     * @param $productId
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function resetSliderCacheOnProductUpdate(int $productId)
    {
        /** @var SliiderProductModel $sliderProduct */
        $slidersProduct = $this->productSliderRepo->getBy('productId', $productId);
        foreach ($slidersProduct as $sliderProduct) {
            /** @var ProductSliderModel $slider */
            $slider = $this->sliderRepo->getById($sliderProduct->getSliderId());
            $this->cache->delete("gf_productSlider_{$slider->getId()}");
        }
    }

    /**
     * @param int $sliderId
     */
    private function resetCacheForSlider(int $sliderId): void
    {
        $this->cache->delete("gf_productSlider_{$sliderId}");
    }

    /**
     * @return AjaxResponse
     */
    public function getView(): AjaxResponse
    {
        ob_start();
        include __DIR__ . '/../templates/sliderList.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', ob_get_clean());
        return $response;
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->sliderRepo->createSliderTable();
        $this->productSliderRepo->createSliderProductTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->sliderRepo->deleteTable();
        $this->productSliderRepo->deleteTable();
    }

    /**
     * @return AjaxResponse
     * @throws \Exception
     */
    public function getSliderForm(): AjaxResponse
    {
        $sliderId = $_GET['id'] ?? null;
        $formTitle = __('Create slider', 'plugin-container');
        $categoryId = null;
        $sliderTitle = null;
        $method = 'createSlider';
        $maxNumberOfSlidesDesktop = 12;
        $maxNumberOfSlidesMobile = 6;
        $linkTo = null;
        $cats = get_terms(
            'product_cat',
            [
                'orderby' => 'name',
                'order' => 'ASC',
                'hierarchical' => 1,
                'hide_empty' => 1
            ]);
        if ($sliderId) {
            /** @var ProductSliderModel $slider */
            $slider = $this->sliderRepo->getById($sliderId);
            if ($slider) {
                $formTitle = __('Edit slider', 'plugin-container');
                $categoryId = $slider->getCategoryId();
                $sliderTitle = $slider->getTitle();
                $method = 'updateSlider';
                $maxNumberOfSlidesDesktop = $slider->getMaxNumberOfSlidesDesktop();
                $maxNumberOfSlidesMobile = $slider->getMaxNumberOfSlidesMobile();
                $linkTo = $slider->getLinkTo();
                $autoFillCategory = $slider->getAutoFillCategory();
                $autoFillRandom = $slider->getAutoFillRandom();
            } else {
                throw new \Exception('Slider not found');
            }
        }
        ob_start();
        include(__DIR__ . '/../templates/sliderForm.php');
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [ob_get_clean()]);
        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function createSlider(): AjaxResponse
    {
        //@todo filer and validation component
        $maxNumberOfSlidesDesktop = $_GET['maxNumberOfSlidesDesktop'] ?? null;
        $maxNumberOfSlidesMobile = $_GET['maxNumberOfSlidesMobile'] ?? null;
        if ((int)$maxNumberOfSlidesDesktop < 4) {
            $_GET['maxNumberOfSlidesDesktop'] = 4;
        }
        if ((int)$maxNumberOfSlidesMobile < 2) {
            $_GET['maxNumberOfSlidesMobile'] = 2;
        }
        if (($_GET['linkTo'] === '') && $_GET['categoryId'] !== '-1') {
            $_GET['linkTo'] = get_term_link((int)$_GET['categoryId'], 'product_cat');
        }
        $this->sliderRepo->create($_GET);
        return new AjaxResponse(true, 'Slider created');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function updateSlider(): AjaxResponse
    {
        //@todo filer and validation component
        $maxNumberOfSlidesDesktop = $_GET['maxNumberOfSlidesDesktop'] ?? null;
        $maxNumberOfSlidesMobile = $_GET['maxNumberOfSlidesMobile'] ?? null;
        if ((int)$maxNumberOfSlidesDesktop < 4) {
            $_GET['maxNumberOfSlidesDesktop'] = 4;
        }
        if ((int)$maxNumberOfSlidesMobile < 2) {
            $_GET['maxNumberOfSlidesMobile'] = 2;
        }
        if (($_GET['linkTo'] === '') && $_GET['categoryId'] !== '-1') {
            $_GET['linkTo'] = get_term_link((int)$_GET['categoryId'], 'product_cat');
        }
        $this->sliderRepo->update($_GET);
        $this->resetCacheForSlider((int)$_GET['id']);
        return new AjaxResponse(true, 'Slider updated');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function deleteSlider(): AjaxResponse
    {
        $this->sliderRepo->delete($_GET['id']);
        return new AjaxResponse(true, 'Slider deleted');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function editSliderProductsForm(): AjaxResponse
    {
        $sliderId = $_GET['id'] ?? null;
        $slider = $this->sliderRepo->getById($sliderId);
        if (!$slider) {
            throw new \Exception('Slider not found');
        }
        $sliderProducts = $this->productSliderRepo->getBy('sliderId', $sliderId);
        ob_start();
        include(__DIR__ . '/../templates/editSliderProductsForm.php');
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [ob_get_clean()]);
        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function ajaxProductSearch(): AjaxResponse
    {
        $search = $_GET['search'] ?? null;
        $limit = $_GET['limit'] ?? '10';
        $offset = $_GET['offset'] ?? '0';
        $data = [];
        if($search === '') {
            $msg = __('No more results', 'plugin-container');
            return new AjaxResponse(true, $msg);
        }
        $products = $this->productSearch->productSearch($search, $limit, $offset);
        foreach ($products as $product) {
            $data[] = [
                'id' => $product['ID'],
                'name' => $product['post_title'],
                'imgSrc' => wp_get_attachment_url(get_post_thumbnail_id($product['ID']))
            ];
        }
        if (count($data) === 0) {
            $msg = __('No more results', 'plugin-container');
        }
        $response = new AjaxResponse(true, $msg ?? '');
        $response->addParamToBody('data', $data);
        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function saveSliderProducts(): AjaxResponse
    {
        $sliderId = $_GET['id'] ?? null;
        $slider = $this->sliderRepo->getById($sliderId);
        if (!$slider) {
            throw new \Exception('Slider not found');
        }
        $this->productSliderRepo->deleteAllForSlider($sliderId);
        foreach ($_GET['sliderProducts'] as $productId) {
            $this->productSliderRepo->create([
                'sliderId' => $sliderId,
                'productId' => $productId,
            ]);
        }
        $this->resetCacheForSlider((int)$sliderId);
        return new AjaxResponse(true, 'Slider products saved');
    }

    /**
     *  These are filters in case we need to override some part of slider loop
     */
    private function filtersForSliderFrontend(): void
    {
        add_filter('sliderBeforeLoopItem', function ($product)
        {
            do_action('woocommerce_before_shop_loop_item');
        });
        add_filter('sliderBeforeItemTitle', function ($product)
        {
            if ($product) {
                echo '<a title="' . $product->get_title() . '" href="' . $product->get_permalink() . '">';
                echo woocommerce_get_product_thumbnail();
                echo '</a>';
                echo '<div>';
            }
        });
        add_filter('sliderItemTitle', function ($product)
        {
            //Remove action for product title because we are not in loop and wc can't get correct title
            remove_action('woocommerce_template_loop_product_title', 'woocommerce_template_loop_product_title', 10);
            do_action('woocommerce_template_loop_product_title');
            //Print correct title
            echo "<h3>{$product->get_title()}</h3>";
        });
        add_filter('sliderAfterItemTitle', function ($product)
        {
            do_action('woocommerce_after_shop_loop_item_title');
        });
        add_filter('sliderAfterLoopItem', function ($product)
        {
            do_action('woocommerce_after_shop_loop_item');
        });
    }

    private function addLikeNameQueryToWc(): void
    {
        add_filter('woocommerce_product_data_store_cpt_get_products_query', function ($query, $queryVars)
        {
            if (isset($queryVars['like_name']) && !empty($queryVars['like_name'])) {
                $query['s'] = esc_attr($queryVars['like_name']);
            }
            return $query;
        }, 10, 2);
    }
}