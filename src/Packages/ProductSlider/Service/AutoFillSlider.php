<?php

namespace PluginContainer\Packages\ProductSlider\Service;

use PluginContainer\Packages\ProductSlider\Model\ProductSlider;
use PluginContainer\Packages\ProductSlider\Model\SliderProduct;

class AutoFillSlider
{
    /**
     * @param ProductSlider $productSlider
     * @param array $excludeArray
     * @return array
     */
    public function fillMissingProductsForSlider(ProductSlider $productSlider, array $excludeArray): array
    {
        $productsByCat = [];
        $productsRandom = [];
        $numberDifference = $productSlider->getMaxNumberOfSlidesDesktop() - count($excludeArray);
        if (wp_is_mobile()) {
            $numberDifference = $productSlider->getMaxNumberOfSlidesMobile() - count($excludeArray);
        }
        if ($productSlider->getAutoFillCategory() === 1) {
            $categoryId = $productSlider->getCategoryId();
            if ($categoryId !== -1) {
                $productsByCat = wc_get_products([
                    'category' => get_term($categoryId)->slug,
                    'limit' => $numberDifference,
                    'exclude' => [implode(',', $excludeArray)],
                    'orderby' => 'rand',
                    'status' => 'publish'
                ]);
            }
        }
        if ($productSlider->getAutoFillRandom() === 1) {
            $totalCount = count($excludeArray);
            if ($productsByCat !== []) {
                $totalCount = count($productsByCat);
            }
            if ($totalCount < $numberDifference) {
                $productsRandom = wc_get_products([
                    'limit' => $numberDifference - $totalCount,
                    'exclude' => [implode(',', $excludeArray)],
                    'orderby' => 'rand',
                    'status' => 'publish'
                ]);
            }
        }
        return array_merge($productsByCat, $productsRandom);
    }
}