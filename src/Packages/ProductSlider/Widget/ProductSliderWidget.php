<?php

namespace PluginContainer\Packages\ProductSlider\Widget;

use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\ProductSlider\Repository\ProductSlider;
use PluginContainer\Packages\ProductSlider\Repository\SliderProduct;
use PluginContainer\Packages\ProductSlider\Service\AutoFillSlider;

class ProductSliderWidget extends \WP_Widget
{
    /**
     * @var ProductSlider
     */
    private $slidersRepo;
    /**
     * @var SliderProduct
     */
    private $sliderProductsRepo;
    /**
     * @var mixed|AutoFillSlider
     */
    private $autoFillService;
    /**
     * @var mixed|CacheWrapper
     */
    private $cache;
    /**
     * @var string
     */
    private $cachePrefix;

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'gfProductSliderWidget', // Base ID
            __('Product slider widget', 'plugin-container'), // Name
            ['description' => __('Widget that shows product slider', 'plugin-container')] // Args
        );
        global $gfContainer;
        $this->slidersRepo = $gfContainer->get(ProductSlider::class);
        $this->sliderProductsRepo = $gfContainer->get(SliderProduct::class);
        $this->autoFillService = $gfContainer->get(AutoFillSlider::class);
        $this->cache = $gfContainer->get(CacheWrapper::class);
        $this->cachePrefix = 'gf_productSlider_';
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @throws \ReflectionException
     * @throws \Exception
     * @see WP_Widget::widget()
     *
     */
    public function widget($args, $instance)
    {
        $sliderId = $instance['selectedSliderId'];
        if ($sliderId === -1 || $sliderId === null) {
            return;
        }
        $html = $this->cache->get($this->cachePrefix.$sliderId);
        if (!$html) {
            /** @var \PluginContainer\Packages\ProductSlider\Model\ProductSlider $slider */
            $slider = $this->slidersRepo->getById($sliderId);
            if (!$slider) {
                return;
            }
            ob_start();
            include __DIR__ . '/templates/sliderViewSingleSlider.php';
            $html = ob_get_clean();
            $ttl = null;
            $categoryId = $slider->getCategoryId();
            if ($categoryId !== -1 || $slider->getAutoFillRandom() === 1 || $slider->getAutoFillCategory() === 1) {
                $ttl = 600;
            }
            $this->cache->set($this->cachePrefix.$slider->getId(), $html, $ttl);
        }
        echo $html;
    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @throws \ReflectionException
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $selectedSlider = !empty($instance['selectedSliderId']) ? $instance['selectedSliderId'] : '';
        $sliders = $this->slidersRepo->getAll();
        ?>
        <label for="<?=$this->get_field_id('selectedSliderId')?>">
            <?=__('Select slider', 'plugin-container')?>
        </label><select id="<?=$this->get_field_id('selectedSliderId')?>"
                        name="<?=$this->get_field_name('selectedSliderId')?>">
        <option value="-1">-----</option>
        <?php
        foreach ($sliders as $slider): ?>
            <option value="<?php
            echo $slider->getId(); ?>" <?=(int)$selectedSlider === $slider->getId() ? 'selected' : ''?>><?php
                echo $slider->getTitle(); ?></option>
        <?php
        endforeach; ?>
    </select>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance): array
    {
        $instance = [];
        $instance['selectedSliderId'] = (!empty($new_instance['selectedSliderId'])) ? sanitize_text_field($new_instance['selectedSliderId']) : '';
        return $instance;
    }
}