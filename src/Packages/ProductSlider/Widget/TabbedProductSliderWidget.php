<?php

namespace PluginContainer\Packages\ProductSlider\Widget;

use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\WpBridge\Translations as T;
use PluginContainer\Packages\ProductSlider\Repository\ProductSlider;
use PluginContainer\Packages\ProductSlider\Repository\SliderProduct;
use PluginContainer\Packages\ProductSlider\Service\AutoFillSlider;

class TabbedProductSliderWidget extends \WP_Widget
{
    /**
     * @var ProductSlider
     */
    private $slidersRepo;
    /**
     * @var SliderProduct
     */
    private $sliderProductsRepo;
    /**
     * @var int
     */
    private $maxTabs = 6;
    /**
     * @var mixed|CacheWrapper
     */
    private $cache;
    /**
     * @var string
     */
    private $cachePrefix;

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'gfProductSliderTabbedWidget', // Base ID
            __('Product slider tabbed widget', 'plugin-container'), // Name
            ['description' => __('Widget that shows product slider with tabs', 'plugin-container')] // Args
        );
        global $gfContainer;
        $this->slidersRepo = $gfContainer->get(ProductSlider::class);
        $this->sliderProductsRepo = $gfContainer->get(SliderProduct::class);
        $this->autoFillService = $gfContainer->get(AutoFillSlider::class);
        $this->cache = $gfContainer->get(CacheWrapper::class);
        $this->cachePrefix = 'gf_productSlider_';
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @throws \ReflectionException
     * @throws \Exception
     * @see WP_Widget::widget()
     *
     */
    public function widget($args, $instance)
    {
        $html = $this->cache->get($this->cachePrefix . serialize($instance));
        if(!$html) {
            ob_start();
            include (__DIR__ . '/templates/sliderViewTabbedSlider.php');
            $html =  ob_get_clean();
            $this->cache->set($this->cachePrefix . serialize($instance), $html, 600);
        }
        echo $html;
    }
    private function parseTitleToHtml($title, $sliderId)
    {
        return '<h3 data-target="' . $sliderId .'">' . $title .'</h3>';
    }
    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @throws \ReflectionException
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        for($i = 0; $i < $this->maxTabs; $i++) {
            $selectedSlider = !empty($instance['selectedSliderId' . $i]) ? $instance['selectedSliderId' . $i] : '';
            $sliders = $this->slidersRepo->getAll();
            ?>
            <label for="<?=$this->get_field_id('selectedSliderId' . $i)?>">
                <?=__('Select slider', 'plugin-container')?>
            </label>
            <select id="<?=$this->get_field_id('selectedSliderId' . $i)?>" name="<?=$this->get_field_name('selectedSliderId' . $i)?>">
                <option value="-1">-----</option>
                <?php
                foreach ($sliders as $slider): ?>
                    <option value="<?php
                    echo $slider->getId(); ?>" <?=(int)$selectedSlider === $slider->getId() ? 'selected' : ''?>><?php
                        echo $slider->getTitle(); ?></option>
                <?php
                endforeach; ?>
            </select>
            <?php
        }
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance): array
    {
        $instance = [];
        for($i = 0; $i < $this->maxTabs; $i++) {
            $instance['selectedSliderId' . $i] = (!empty($new_instance['selectedSliderId' . $i])) ?
                sanitize_text_field($new_instance['selectedSliderId' . $i]) : '';
        }
        $this->cache->delete($this->cachePrefix . serialize($old_instance));
        return $instance;
    }
}