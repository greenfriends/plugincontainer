<?php
$sliderIds = [];
for($i = 0; $i < $this->maxTabs; $i++) {
    $sliderIds[] = (int)$instance['selectedSliderId'.$i];
}
$slidersContent = [
    'headers' => '',
    'content' => '',
];
foreach($sliderIds as $sliderId) {
    if ($sliderId === -1 || $sliderId === null) {
        continue;
    }
    $slider = $this->slidersRepo->getById($sliderId);
    if (!$slider) {
        continue;
    }
    ob_start();
    include __DIR__ . '/sliderContent.php';
    $slidersContent['headers'] .= $this->parseTitleToHtml($slider->getTitle(), $slider->getId());
    $slidersContent['content'] .= ob_get_clean();
}
echo <<<HTML
        <div class="tabbedSliderContainer">    
           <div class="sliderHeading">
                {$slidersContent['headers']}
           </div>
            <div class="tabbedSliders">
                {$slidersContent['content']}
            </div>
        </div>
        HTML;