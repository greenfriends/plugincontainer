<div class="productSlider invisible" data-id="<?=$slider->getId()?>">
    <div class="swiper">
        <div class="swiper-wrapper">
            <?php
            if (!empty($instance['title'])) {
                echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
            }
            $products = $this->sliderProductsRepo->getBy('sliderId', $sliderId);
            $excludeArray = [];
            foreach ($products as $sliderProduct) {
                global $product;
                $productId = $sliderProduct->getProductId();
                $product = wc_get_product($productId);
                $productStatus = false;
                if($product) {
                    $productStatus = $product->get_status();
                }
                if (!$product || $productStatus === 'trash' || $productStatus === 'draft') {
                    continue;
                }
                $excludeArray[] = $productId;
                ob_start();
                include __DIR__ . '/slide.php';
                echo ob_get_clean();
            }
            if (count($products) < $slider->getMaxNumberOfSlidesDesktop()) {
                global $product;
                foreach ($this->autoFillService->fillMissingProductsForSlider($slider, $excludeArray) as $product) {
                    $excludeArray[]= $product->get_id();
                    ob_start();
                    include __DIR__ . '/slide.php';
                    echo ob_get_clean();
                }
            }
            ?>
        </div>
    </div>
    <?php if (count($excludeArray) > 4) :?>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
   <?php endif;?>
</div>
