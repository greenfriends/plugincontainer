<?php
/**
 * @var $product \WC_Product
 */

?>
<div class="swiper-slide">
    <?php
    apply_filters('sliderBeforeLoopItem', $product);
    apply_filters('sliderBeforeItemTitle', $product);
    apply_filters('sliderItemTitle', $product);
    apply_filters('sliderAfterItemTitle', $product);
    apply_filters('sliderAfterLoopItem', $product);
    ?>
</div>