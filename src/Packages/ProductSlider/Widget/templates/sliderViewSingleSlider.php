<?php
echo $args['before_widget'];
echo '<h3><a href="'.$slider->getLinkTo().'" title="link to category '.$slider->getTitle().'">' . $slider->getTitle() . '</a></h3>';
echo '<div class="productSlider">';
echo '<div class="swiper">
            <div class="swiper-wrapper">';
if (!empty($instance['title'])) {
    echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
}
$products = $this->sliderProductsRepo->getBy('sliderId', $sliderId);
$maxProductsDesktop = $slider->getMaxNumberOfSlidesDesktop();
$maxProductsMobile = $slider->getMaxNumberOfSlidesMobile();
$products = array_splice($products, 0, $maxProductsDesktop);
if (wp_is_mobile()) {
    $products = array_splice($products, 0, $maxProductsMobile);
}
$excludeArray = [];
foreach ($products as $sliderProduct) {
    global $product;
    $productId = $sliderProduct->getProductId();
    $product = wc_get_product($productId);
    if (!$product || $product->get_status() === 'trash') {
        continue;
    }
    $excludeArray[] = $productId;
    ob_start();
    include __DIR__ . '/slide.php';
    echo ob_get_clean();
}
if (count($products) < $slider->getMaxNumberOfSlidesDesktop()) {
    global $product;
    foreach ($this->autoFillService->fillMissingProductsForSlider($slider, $excludeArray) as $product) {
        $excludeArray[]= $product->get_id();
        ob_start();
        include __DIR__ . '/slide.php';
        echo ob_get_clean();
    }
}
echo '</div></div>';
if (count($excludeArray) > 4) {
    echo '<div class="swiper-button-prev"></div> <div class="swiper-button-next"></div>';
}
echo '</div>';
if (isset($args['after_widget'])) {
    echo $args['after_widget'];
}