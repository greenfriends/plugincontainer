<?php
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\ProductSlider\Model\ProductSlider;

/** @var ProductSlider[] $sliders */
$sliders = $this->sliderRepo->getAll();
?>
<button id="addNewSlider" data-method="getSliderForm"><?=__('Add new slider', 'plugin-container')?></button>

<?php foreach ($sliders as $slider):?>
<div>
    <h2><?=$slider->getTitle()?></h2>
    <button class="editSliderButton" data-method="getSliderForm" data-id="<?=$slider->getId()?>"><?=__('Edit slider', 'plugin-container')?></button>
    <button class="deleteSliderButton" data-method="deleteSlider" data-id="<?=$slider->getId()?>"><?=__('Delete slider', 'plugin-container')?></button>
    <button class="editSliderProductsButton" data-method="editSliderProductsForm" data-id="<?=$slider->getId()?>"><?=__('Edit products', 'plugin-container')?></button>
</div>
<?php endforeach;?>
