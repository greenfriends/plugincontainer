<?php

use PluginContainer\Core\WpBridge\Translations as T;

/**
 * @var \PluginContainer\Packages\ProductSlider\Model\ProductSlider $slider
 * @var \PluginContainer\Packages\ProductSlider\Model\SliderProduct[] $sliderProducts ;
 * @see \PluginContainer\Packages\ProductSlider\Controller\ProductSlider::editSliderProductsForm()
 */
?>
<h2>Editing slider:<?=$slider->getTitle()?></h2>
<div id="addProduct" class="searchContainer">
    <button id="productName" class="greenButton"><?=__('Add product', 'plugin-container')?></button>
    <input type="hidden" name="productId" id="productId">
</div>
<form data-id="<?=$slider->getId()?>" data-method="saveSliderProducts" id="sliderProducts">
    <div id="productsPreviewContainer">
        <?php
        if (count($sliderProducts) > 0): ?><?php
            foreach ($sliderProducts as $sliderProduct): ?><?php
                $product = wc_get_product($sliderProduct->getProductId());
                if (!$product) {
                    continue;
                }
                ?>
                <div class="product">
                    <input type="hidden" name="sliderProducts[]" value="<?=$sliderProduct->getProductId()?>">
                    <img src="<?=wp_get_attachment_url($product->get_image_id())?>" alt="product image">
                    <h3><?=$product->get_title()?></h3>
                    <div title="<?=__('Delete product', 'plugin-container')?>>" class="deleteProductButton">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                    </div>
                </div>
            <?php
            endforeach; ?><?php
        endif; ?>
    </div>
    <input id="submitButton" class="greenButton" type="submit" value="<?=__('Save', 'plugin-container')?>">
</form>
