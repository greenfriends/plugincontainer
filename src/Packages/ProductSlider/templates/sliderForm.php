<?php
/**
 * @see \PluginContainer\Packages\ProductSlider\Controller\ProductSlider::getSliderForm()
 * @var int | null $sliderId
 * @var string | null $formTitle
 * @var string | null $sliderTitle
 * @var int | null $categoryId
 * @var WC_Product[] | null $products
 * @var string $method
 * @var WP_Term[] $cats
 * @var int $maxNumberOfSlidesDesktop
 * @var int $maxNumberOfSlidesMobile
 *  @var int $autoFillCategory
 * @var int $autoFillRandom
 * @var string $linkTo
 */

use PluginContainer\Core\WpBridge\Translations as T;

?>
<form data-method ="<?=$method?>" data-id="<?=$sliderId?>" id="sliderForm">
    <h2><?=$formTitle?></h2>
    <label>
        <?=__('Slider title', 'plugin-container')?>
        <input type="text" name="title" value="<?=$sliderTitle?>"/>
    </label>
    <label>
        <?=__('Select category', 'plugin-container')?>
        <select name="categoryId">
            <option value="-1">----</option>
            <?php foreach ($cats as $cat):?>
            <option <?=$categoryId === $cat->term_id ? 'selected' : ''?> value="<?=$cat->term_id?>"><?=$cat->name?></option>
            <?php endforeach;?>
        </select>
    </label>
    <label>
        <?=__('Link to', 'plugin-container')?>
        <input type="url" name="linkTo" value="<?=$linkTo?>">
    </label>
    <label>
        <?=__('Number of slides on desktop (min 4)', 'plugin-container')?>
        <input type="number" name="maxNumberOfSlidesDesktop" value="<?=$maxNumberOfSlidesDesktop?>"/>
    </label>
    <label>
        <?=__('Number of slides on mobile (min 2)', 'plugin-container')?>
        <input type="number" name="maxNumberOfSlidesMobile" value="<?=$maxNumberOfSlidesMobile?>"/>
    </label>
    <label class="noFlex">
        <?=__('Autofill with category items', 'plugin-container')?>
        <input <?=$autoFillCategory === 1 ? 'checked' : ''?> type="checkbox" name="autoFillCategory" value="1"/>
    </label>
    <label class="noFlex">
        <?=__('Autofill with random items', 'plugin-container')?>
        <input <?=$autoFillRandom === 1 ? 'checked' : ''?> type="checkbox" name="autoFillRandom" value="1"/>
    </label>
    <button type="submit"><?=__('Submit', 'plugin-container')?></button>
</form>