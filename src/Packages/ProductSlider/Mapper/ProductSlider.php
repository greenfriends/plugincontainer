<?php

namespace PluginContainer\Packages\ProductSlider\Mapper;

use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class ProductSlider extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfProductSliders', 'id');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `title` varchar(50) NOT NULL,
		  `categoryId` int(5) DEFAULT NULL,
		  `maxNumberOfSlidesDesktop` int(2) DEFAULT NULL,
		  `maxNumberOfSlidesMobile` int(2) DEFAULT NULL,
		  `linkTo` varchar(150) DEFAULT NULL,
          `autoFillCategory` int(1) DEFAULT 0,
          `autoFillRandom` int(1) DEFAULT 0,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function dropTable(): void
    {
        $sql = "DROP TABLE $this->tableName;";
        $this->handleStatement($sql);
    }

}