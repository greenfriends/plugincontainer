<?php

namespace PluginContainer\Packages\ProductSlider\Mapper;

use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class SliderProduct extends BaseMapper
{

    public function __construct(\PDO $driver, Logger $logger, $tableName = 'gfSliderProducts')
    {
        parent::__construct($driver, $logger, $tableName);
    }

    /**
     * @throws \Exception
     */
    public function createSliderProductTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} ( 
            `id` int(5) NOT NULL AUTO_INCREMENT,
            `sliderId` int(5) NOT NULL,
            `productId` int(10) NOT NULL,
            `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		    `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
        ) DEFAULT CHARSET=utf8mb4;";
        $this->handleStatement($sql);
    }

    /**
     * @param $sliderId
     * @throws \Exception
     */
    public function deleteAllForSlider($sliderId): void
    {
        $sql = "DELETE FROM {$this->tableName} WHERE sliderId = {$sliderId}";
        $this->handleStatement($sql);
    }
}