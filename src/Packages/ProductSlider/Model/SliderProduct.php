<?php

namespace PluginContainer\Packages\ProductSlider\Model;

class SliderProduct
{
    /**
     * @var int|null
     */
    private $sliderId;
    /**
     * @var int
     */
    private $productId;
    /**
     * @var int|null
     */
    private $id;

    /**
     * @return int
     */
    public function getSliderId(): int
    {
        return $this->sliderId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct(int $sliderId, int $productId, int $id = null)
    {
        $this->sliderId = $sliderId;
        $this->productId = $productId;
        $this->id = $id;
    }
}