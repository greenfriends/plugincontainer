<?php

namespace PluginContainer\Packages\ProductSlider\Model;

class ProductSlider
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $categoryId;
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string|null
     */
    private $createdAt;
    /**
     * @var string|null
     */
    private $updatedAt;
    /**
     * @var int|null
     */
    private $maxNumberOfSlidesDesktop;
    /**
     * @var int|null
     */
    private $maxNumberOfSlidesMobile;
    /**
     * @var string|null
     */
    private $linkTo;
    /**
     * @var int|null
     */
    private $autoFillCategory;
    /**
     * @var int
     */
    private $autoFillRandom;

    /**
     * @param string $title
     * @param int $categoryId
     * @param int|null $id
     * @param int|null $maxNumberOfSlidesDesktop
     * @param int|null $maxNumberOfSlidesMobile
     * @param string|null $linkTo
     * @param int|null $autoFillCategory
     * @param int|null $autoFillRandom
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(string $title, int $categoryId, int $id = null,
        int $maxNumberOfSlidesDesktop = null, int $maxNumberOfSlidesMobile = null, string $linkTo = null,
        ?int $autoFillCategory = null, ?int $autoFillRandom = null, string $createdAt = null, string $updatedAt = null)
    {
        $this->title = $title;
        $this->categoryId = $categoryId;
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->maxNumberOfSlidesDesktop = $maxNumberOfSlidesDesktop;
        $this->maxNumberOfSlidesMobile = $maxNumberOfSlidesMobile;
        $this->linkTo = $linkTo;
        $this->autoFillCategory = $autoFillCategory;
        $this->autoFillRandom = $autoFillRandom;
    }

    /**
     * @return int|null
     */
    public function getAutoFillCategory(): ?int
    {
        return $this->autoFillCategory;
    }

    /**
     * @return int|null
     */
    public function getAutoFillRandom(): ?int
    {
        return $this->autoFillRandom;
    }

    /**
     * @return string|null
     */
    public function getLinkTo(): ?string
    {
        return $this->linkTo;
    }

    /**
     * @return int|null
     */
    public function getMaxNumberOfSlidesDesktop(): ?int
    {
        return $this->maxNumberOfSlidesDesktop;
    }

    /**
     * @return int|null
     */
    public function getMaxNumberOfSlidesMobile(): ?int
    {
        return $this->maxNumberOfSlidesMobile;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }
}