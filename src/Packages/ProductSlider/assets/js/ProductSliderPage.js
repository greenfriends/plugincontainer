import Tab from "../../../../../assets/admin/js/Tab.js";
import InputAjaxSearch from "../../../../../assets/components/InputAjaxSearch/InputAjaxSearch.js";

export default class ProductSliderPage extends Tab  {
    init() {
        this.addSliderButton = document.getElementById('addNewSlider');
        this.deleteSliderButtons = document.querySelectorAll('.deleteSliderButton');
        this.editSliderButtons = document.querySelectorAll('.editSliderButton');
        this.editSliderProductsButton = document.querySelectorAll('.editSliderProductsButton');
        this.submitButton = document.getElementById('submitButton');
        this.addDeleteButtonListeners();
        this.addEditButtonListeners();
        this.addAddSliderButtonListener();
        this.addEditSliderProductsButtonListeners();
        this.addSortableToForm();
    };

    addAddSliderButtonListener() {
        this.addSliderButton.addEventListener('click',async () => {
            this.printFormModal('500','500',this.addSliderButton.getAttribute('data-method'));
        });
    }

    addEditButtonListeners() {
        this.editSliderButtons.forEach((button) => {
            button.addEventListener('click',async () => {
                this.printFormModal('500','500',
                    button.getAttribute('data-method'), button.getAttribute('data-id'));
            });
        });
    }

    addDeleteButtonListeners() {
        this.deleteSliderButtons.forEach((button) => {
            button.addEventListener('click',async () => {
                let sliderId = button.getAttribute('data-id');
                let method = button.getAttribute('data-method');
                let response = await this.fetchResponse({id:sliderId, method:method});
                this.printNotice(response);
                this.reloadTab();
            });
        });
    }
    addEditSliderProductsButtonListeners() {
        this.editSliderProductsButton.forEach((button) => {
            button.addEventListener('click',async () => {
                await this.printFormModal('60','70', button.getAttribute('data-method'), button.getAttribute('data-id'), '%');
                this.form = document.getElementById('sliderProducts');
                this.form.querySelectorAll('.deleteProductButton').forEach((elem) => {
                    elem.addEventListener('click', (e) => {
                        e.target.parentElement.remove();
                    })
                });

                let productSearchInput = new InputAjaxSearch({
                    containerId: 'addProduct',
                    nameInputId: 'productName',
                    optionClickCallback: this.addProduct,
                    fetchUrl: `/wp-admin/admin-ajax.php?method=ajaxProductSearch&action=${this.action}&handlerClass=${this.handlerClass}`,
                    showOptionsBeforeInput: false
                });
            });
        });
    }

    addProduct(data = null) {
        // Append hidden input with the product ID as the value
        let input = document.createElement('input');
        input.setAttribute('type','hidden');
        input.setAttribute('name','sliderProducts[]');
        input.value = data.id;

        // Append the product preview
        let product = document.createElement('div');
        product.appendChild(input);
        product.classList.add('product');

        let productImg = document.createElement('img');
        productImg.setAttribute('src',data.imgSrc);

        let productName = document.createElement('h3');
        productName.innerText = data.name;

        let deleteProductButton = document.createElement('div');
        deleteProductButton.title = 'Delete product';
        deleteProductButton.classList.add('deleteProductButton');
        deleteProductButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>`;
        deleteProductButton.addEventListener('click', (e) => {
            e.target.parentElement.remove();
        })


        product.appendChild(productImg);
        product.appendChild(productName);
        product.appendChild(deleteProductButton);
        document.getElementById('productsPreviewContainer').appendChild(product);
        document.dispatchEvent(new CustomEvent('productAddedToSlider'));
    }
    addSortableToForm(){
        jQuery(document).on('modalFormOpened', () =>{
            jQuery('#productsPreviewContainer').sortable({cursor:'move'})
        });
    }
}