export function productSlider() {
    const swiper = new Swiper('.swiper', {
        slidesPerView: 4,
        slidesPerGroup: 2,
        spaceBetween: 0,
        loop: true,
        preloadImages: false,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 15,
                slidesPerGroup: 1,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            1023: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            1024: {
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 15
            },
            1376: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 15
            }
        }
    });
    let prevButtons = document.getElementsByClassName("swiper-button-prev");
    let nextButtons = document.getElementsByClassName("swiper-button-next");
    if (null != prevButtons)
        for (let e of prevButtons) {
            const t = e.parentElement.firstElementChild.swiper;
            e.addEventListener("click", e => {
                e.preventDefault(), t.slidePrev()
            })
        }
    if (null != nextButtons)
        for (let e of nextButtons) {
            const t = e.parentElement.firstElementChild.swiper;
            e.addEventListener("click", e => {
                e.preventDefault(), t.slideNext()
            })
        }
    let sliderHeadings = document.querySelectorAll('.sliderHeading');
    sliderHeadings.forEach((heading, index) => {
        heading.querySelectorAll('h3').forEach((title, index) => {
            if(index === 0) {
                title.classList.add('active');
                heading.parentElement.querySelector(`[data-id="${title.getAttribute('data-target')}"]`).classList.remove('invisible');
            }
            title.addEventListener('click', (e) => {
                let target = e.target.getAttribute('data-target');
                let targetElement = heading.parentElement.querySelector(`[data-id="${target}"]`);
                let oldActiveTab = heading.querySelector('.active');
                oldActiveTab.classList.remove('active');
                oldActiveTab.parentElement.parentElement.querySelector(`[data-id="${oldActiveTab.getAttribute('data-target')}"]`).classList.add('invisible');
                targetElement.classList.remove('invisible');
                title.classList.add('active');
            });
        });
    });
}