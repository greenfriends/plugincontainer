<?php

namespace PluginContainer\Packages\ProductSlider\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\ProductSlider\Mapper\ProductSlider as ProductSliderMapper;
use PluginContainer\Packages\ProductSlider\Model\ProductSlider as ProductSliderModel;

class ProductSlider extends BaseRepository
{
    public function __construct(ProductSliderMapper $mapper)
    {
        parent::__construct(ProductSliderModel::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createSliderTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteSliderTable(): void
    {
        $this->mapper->dropTable();
    }

}