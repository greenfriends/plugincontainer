<?php

namespace PluginContainer\Packages\ProductSlider\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\ProductSlider\Model\SliderProduct as Model;

class SliderProduct extends BaseRepository
{

    public function __construct(\PluginContainer\Packages\ProductSlider\Mapper\SliderProduct $mapper)
    {
        parent::__construct(Model::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createSliderProductTable(): void
    {
        $this->mapper->createSliderProductTable();
    }

    /**
     * @param $sliderId
     * @throws \Exception
     */
    public function deleteAllForSlider($sliderId): void
    {
        $this->mapper->deleteAllForSlider($sliderId);
    }
}