<?php

namespace PluginContainer\Packages\Vendor\Controller;

use Exception;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Components\CrudTable\Column;
use PluginContainer\Core\Components\CrudTable\CrudTable;
use PluginContainer\Core\Components\CrudTable\FormField;
use PluginContainer\Core\Components\CrudTable\FormFieldSelect;
use PluginContainer\Core\Components\CrudTable\FormFieldTextArea;
use PluginContainer\Core\Controller\BaseController;
use PluginContainer\Core\Controller\ControllerWithCrudTableInterface;
use PluginContainer\Core\Controller\ControllerWithHooksInterface;
use PluginContainer\Core\Controller\ControllerWithSettingPageInterface;
use PluginContainer\Core\Controller\ControllerWithSetupInterface;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\Translations;
use PluginContainer\Packages\Vendor\Repository\Vendor as repo;
use WP_Roles;

class Vendor extends BaseController implements
    ControllerWithSettingPageInterface, ControllerWithSetupInterface, ControllerWithHooksInterface,
    ControllerWithCrudTableInterface
{
    use \PluginContainer\Core\Components\CrudTable\Traits\CrudTable;

    /**
     * @var repo
     */
    private $repo;

    public function __construct(Feature $featureRepo, Logger $logger, repo $repo)
    {
        parent::__construct($featureRepo, $logger);
        $this->repo = $repo;
    }

    /**
     * @throws Exception
     */
    public function init(): void
    {
        $this->addVendorCustomField();
        $this->addVendorFilterToProductList();
        $this->addVendorColumnToProductRow();
        $this->vendorCustomFieldHandler();
        $this->enqueueJs();
        $this->enqueueCss();
    }

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->createSupplierRole();
        $this->repo->createTable();
    }

    /**
     * @throws \Exception
     */
    public function cleanup(): void
    {
        $this->removeSupplierRole();
        $this->repo->deleteTable();
    }

    /**
     * @throws Exception
     */
    private function createSupplierRole(): void
    {
        global $wp_roles;
        if (!isset($wp_roles)) {
            $wp_roles = new WP_Roles();
        }
        $role = $wp_roles->get_role('subscriber');
        if ($role) {
            $wp_roles->add_role('supplier', 'supplier', $role->capabilities);
            return;
        }
        throw new Exception('User role you are trying to copy does not exist');
    }

    private function removeSupplierRole(): void
    {
        remove_role('vendor');
    }

    /**
     * @throws \ReflectionException
     */
    private function addVendorFilterToProductList(): void
    {
        add_filter('woocommerce_product_filters', function($output){
                $suppliers = $this->repo->getAll();
                $html = '<select name="productSupplierFilter">';
                $html .= '<option value="">Filtriraj po dobavljaču </option>';
                foreach ($suppliers as $supplier) {
                    $selected = isset($_GET['productSupplierFilter']) && (int)$_GET['productSupplierFilter'] === $supplier->getId() ? 'selected' : '';
                    $html .= '<option ' . $selected  . ' value="' . $supplier->getId() . '">' . $supplier->getName(). '</option>';
                }
                $html .= '</select>';
            return $html . $output;
        }, 10, 1);

        add_filter('parse_query', function($query){
            global $typenow;
            if ($typenow == 'product' && !empty($_GET['productSupplierFilter'])) {
                $query->query_vars['meta_key'] = 'supplier';
                $query->query_vars['meta_value'] = $_GET['productSupplierFilter'];
            }
        });
    }

    /**
     * @throws Exception
     */
    private function addVendorColumnToProductRow(): void
    {
        add_filter('manage_edit-product_columns', function ($columns) {
            unset($columns['seopress_title'], $columns['seopress_desc'], $columns['seopress_noindex'],
                $columns['seopress_nofollow'], $columns['seopress_score'], $columns['product_tag']);
            $columns['supplier'] = __('Dobavljač', 'woocommerce'); // title
                return $columns;
        }, 11);
        add_action('manage_product_posts_custom_column', function($column, $productId) {
            if($column === 'supplier') {
                $supplierId = get_post_meta($productId,'supplier',true);
                if($supplierId) {
                    $supplier = $this->repo->getById($supplierId);
                    if($supplier) {
                        echo $supplier->getName();
                    }
                }
            }
        }, 10, 2);
    }

    /**
     * @throws Exception
     */
    private function addVendorCustomField(): void
    {
        $postId = $_GET['post'] ?? '';
        $supplierName = '';
        if ($postId !== '') {
            /** @var \PluginContainer\Packages\Vendor\Model\Vendor $supplier */
            $supplier = $this->repo->getById((int)get_post_meta($postId, 'supplier', true));
            if ($supplier) {
                $supplierName = $supplier->getName();
            }
        }
        add_action('woocommerce_product_options_general_product_data', static function () use ($supplierName, $postId)
        {
            echo <<<html
                <p id="supplierContainer" class="form-field vendor_field">
                    <span class="chevron bottom"></span>
                    <label for="supplierName">Supplier</label>
                    <input value="{$supplierName}" name="supplierName" id="supplierName" type="text" readonly placeholder="Select">
                </p>
            html;
            woocommerce_wp_text_input(
                [
                    'id' => 'supplier',
                    'placeholder' => 'Select',
                    'label' => '',
                    'type' => 'hidden',
                    'class' => 'gfSupplierField',
                    'required' => true,
                    'value' => get_post_meta($postId,'supplier',true) ?? ''
                ]
            );
        });
    }

    private function vendorCustomFieldHandler(): void
    {
        add_action('woocommerce_process_product_meta', static function ($postId)
        {
            $vendor = $_POST['supplier'];
            if (!empty($vendor)) {
                update_post_meta($postId, 'supplier', esc_attr($vendor));
            }
        });
    }

    private function enqueueJs(): void
    {
        add_action('admin_enqueue_scripts', function ($hook)
        {
            global $post;
            if ($hook === 'post-new.php' || $hook === 'post.php') {
                if ($post->post_type === 'product') {
                    wp_enqueue_script('vendorJs', CONTAINER_PLUGIN_DIR_URI . 'src/Packages/Vendor/assets/js/vendor.js');
                }
            }
        }, 10, 1);
    }

    private function enqueueCss(): void
    {
        add_action('admin_enqueue_scripts', function ($hook)
        {
            global $post;
            if ($hook === 'post-new.php' || $hook === 'post.php') {
                if ($post->post_type === 'product') {
                    wp_enqueue_style('vendorCss',
                        CONTAINER_PLUGIN_DIR_URI . 'src/Packages/Vendor/assets/css/vendor.min.css');
                }
            }
        }, 10, 1);
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @throws \ReflectionException
     * @see AjaxRouter::handleRoute()
     */
    public function getVendors(): AjaxResponse
    {
        $offset = null;
        $limit = null;
        $search = '';
        if (isset($_GET['search']) && $_GET['search'] !== '') {
            $search = $_GET['search'];
        }
        if (isset($_GET['offset'])) {
            $offset = (int)$_GET['offset'];
        }
        if (isset($_GET['limit'])) {
            $limit = (int)$_GET['limit'];
        }

        $data = [];
        if ($search !== '') {
            $results = $this->repo->searchByField('name', $search, $offset, $limit);
        } else {
            $results = $this->repo->getAll($offset, $limit);
        }
        foreach ($results as $result) {
            $data[] = ['id' => $result->getId(), 'name' => $result->getName()];
        }
        if (count($data) === 0) {
            $msg = 'No more results found';
            if ($search !== '') {
                $msg = "No results found for \"$search\"";
            }
            throw new \Exception($msg);
        }
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', $data);
        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function getView(): AjaxResponse
    {
        $name = new Column('name', 'Name');
        $city = new Column('city', 'City');
        $address = new Column('address', 'Address');
        $zipCode = new Column('zipCode', 'Zip code');
        $wpUserId = new Column('wpUserId', 'User id');
        $notes = new Column('notes', 'Notes');
        $notes
            ->addJsDataParam('orderable', false)
            ->addJsDataParam('searchable', false);
        $columns = [$name, $city, $address, $zipCode, $wpUserId, $notes];
        $crudTable = new CrudTable($columns, $this->repo);
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [$crudTable->getList()]);
        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws Exception
     * @see AjaxRouter::handleRoute()
     */
    public function getFormFields(): AjaxResponse
    {
        $method = 'create';
        $formHeader = 'Create vendor';
        if (isset($_GET['id'])) {
            /** @var \PluginContainer\Packages\Vendor\Model\Vendor $vendor */
            $vendor = $this->repo->getById((int)$_GET['id']);
            if ($vendor) {
                $method = 'update';
                $formHeader = 'Update Vendor';
                $name = $vendor->getName();
                $wpUserId = $vendor->getWpUserId();
                $city = $vendor->getCity();
                $address = $vendor->getAddress();
                $zipCode = $vendor->getZipCode();
                $notes = $vendor->getNotes();
            } else {
                throw new Exception('Vendor you are trying to update does not exist');
            }
        }
        $suppliers = get_users(['role' => 'supplier', 'fields' => ['ID', 'display_name']]);
        $userSelectOptions = [];
        foreach ($suppliers as $key => $supplier) {
            $userSelectOptions[$key]['displayValue'] = $supplier->display_name;
            $userSelectOptions[$key]['value'] = $supplier->ID;
        }
        $nameField = new FormField(
            'name',
            'text',
            __('Name', 'plugin-container'),
            $name ?? null
        );
        $wpUserIdField = new FormFieldSelect(
            'wpUserId',
            'select',
            __('User id', 'plugin-container'),
            $userSelectOptions,
            $wpUserId ?? null,
            __('Select user associated with this vendor', 'plugin-container'));
        $cityField = new FormField(
            'city',
            'text',
            __('City', 'plugin-container'),
            $city ?? null
        );
        $addressField = new FormField(
            'address',
            'text',
            __('Address', 'plugin-container'),
            $address ?? null
        );
        $zipCodeField = new FormField(
            'zipCode',
            'number',
            __('Zip code', 'plugin-container'),
            $zipCode ?? null);
        $notesField = new FormFieldTextArea(
            'notes',
            'textarea',
            __('Notes', 'plugin-container'),
            10,
            20,
            $notes ?? null,
            __('Notes about user, like contact phones, names etc...', 'plugin-container'));
        $response = new AjaxResponse(true);
        $response
            ->addParamToBody('data',
                [$nameField, $addressField, $zipCodeField, $notesField, $cityField, $wpUserIdField])
            ->addParamToBody('formHeader', $formHeader)
            ->addParamToBody('method', $method)
            ->addParamToBody('submitLabel', __('Submit', 'plugin-container'));

        return $response;
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function create(): AjaxResponse
    {
        $this->repo->create($_GET);
        return new AjaxResponse(true, __('Successfully created vendor', 'plugin-container'));
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function update(): AjaxResponse
    {
        $this->repo->update($_GET);
        return new AjaxResponse(true, __('Successfully updated vendor', 'plugin-container'));
    }

    /**
     * @return AjaxResponse
     * @throws Exception
     */
    public function delete(): AjaxResponse
    {
        if (isset($_GET['id'])) {
            $this->repo->delete((int)$_GET['id']);
            return new AjaxResponse(true, __('Successfully deleted', 'plugin-container'));
        }
        throw new Exception('Id not found in request');
    }
}