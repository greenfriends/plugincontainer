<?php

namespace PluginContainer\Packages\Vendor\Model;

class Vendor
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $address;
    /**
     * @var string
     */
    private $zipCode;
    /**
     * @var string|null
     */
    private $notes;
    /**
     * @var int|null
     */
    private $wpUserId;
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string|null
     */
    private $createdAt;
    /**
     * @var string|null
     */
    private $updatedAt;

    /**
     * @param string $name
     * @param string $city
     * @param string $address
     * @param string $zipCode
     * @param string|null $notes
     * @param int|null $wpUserId
     * @param int|null $id
     * @param string|null $createdAt
     * @param string|null $updatedAt
     */
    public function __construct(
        string $name,
        string $city,
        string $address,
        string $zipCode,
        string $notes = null,
        int $wpUserId = null,
        int $id = null,
        string $createdAt = null,
        string $updatedAt = null
    ) {
        $this->name = $name;
        $this->city = $city;
        $this->address = $address;
        $this->zipCode = $zipCode;
        $this->notes = $notes;
        $this->wpUserId = $wpUserId;
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @return int|null
     */
    public function getWpUserId(): ?int
    {
        return $this->wpUserId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}