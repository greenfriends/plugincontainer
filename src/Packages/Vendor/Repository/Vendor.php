<?php

namespace PluginContainer\Packages\Vendor\Repository;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Packages\Vendor\Mapper\Vendor as Mapper;
use PluginContainer\Packages\Vendor\Model\Vendor as Model;

class Vendor extends BaseRepository
{
    public function __construct(Mapper $mapper)
    {
        parent::__construct(Model::class);
        $this->mapper = $mapper;
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $this->mapper->createTable();
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $this->mapper->deleteTable();
    }
}