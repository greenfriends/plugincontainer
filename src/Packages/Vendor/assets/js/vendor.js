class SupplierSelect
{
    /**
     * @class SupplierSelect
     * @param props
     * @property {HTMLElement} container - The container which holds all the elements
     * @property {HTMLElement} mainInput - The main input which contains the selected value
     */
    constructor(props) {
        this.container = document.getElementById(props.containerId);
        this.mainInput = document.getElementById(props.supplierNameInputId);
        this.supplierHiddenInputId = document.getElementById(props.supplierIdInputId);
        this.isLoading = false;
        this.offsetSearch = 0;
        this.suppliersPerFetch = 10;
        this.limit = 10;
        this.allowFetching = true;
    }

    /**
     * Bundle all event listeners to be called in the class constructor
     */
    addEventListeners()
    {
        this.addMainInputListener();
        this.closeOptionsOnDocumentClickListener();
    }

    /**
     * Listen to a click event on the main input element and generate the list of suppliers with their listeners
     * If we are closing the option list return the offset to 0
     */
    addMainInputListener()
    {
        this.mainInput.addEventListener('click',() => {
            this.handleOptionContainer().then((append) => {
                if(append) {
                    this.allowFetching = true;
                    this.appendSuppliers('',0, this.limit);
                } else {
                    this.offsetSearch = 0;
                    this.allowFetching = false;
                }
            });
        });
    }

    addSearchInputListener(){
        let timeout;
        this.searchInput.addEventListener('input',() => {
            clearTimeout(timeout)
            timeout = setTimeout(() =>{
                this.allowFetching = true;
                this.offsetSearch = 0;
                this.supplierOptions.innerHTML = '';
                let searchInputValue = this.searchInput.value;
                this.appendSuppliers(searchInputValue,this.offsetSearch, this.limit);
            },650)
        })
    }

    /**
     * Removes the option container if the user clicks elsewhere in the document
     */
    closeOptionsOnDocumentClickListener()
    {
        document.addEventListener('click',(event) => {
            if(event.target !== this.container && !this.container.contains(event.target)) {
                this.removeOptionContainer();
            }
        });
    }

    /**
     *  Append new suppliers when the user passes a certain threshold when scrolling
     */
    addLazyLoadOnScrollListener()
    {
        if(this.supplierOptions) {
            this.supplierOptions.addEventListener('scroll', () => {
                if(this.allowFetching === true) {
                    let offset = this.supplierOptions.offsetHeight + this.supplierOptions.scrollTop;
                    let chunk = this.supplierOptions.scrollHeight;
                    if (this.isLoading === false) {
                        if (offset >= chunk - 40) {
                            this.offsetSearch += this.suppliersPerFetch;
                            this.appendSuppliers(this.searchInput.value, this.offsetSearch, this.limit);
                        }
                    }
                }
            })
        }
    }
    /**
     * Appends / Removes a Node to the main container which will be used for the options.
     * Handles the classes and visuals depending on whether the options are open or closed
     */
    async handleOptionContainer() {
        if(!this.optionContainerExists()) {
            /** append the option container */
            this.container.appendChild(this.generateOptionContainer());
            /** get the container when append is finished */
            this.supplierOptions = document.getElementById('supplierOptions');
            /** add scroll listener after getting the container */
            this.addLazyLoadOnScrollListener();
            /** get the search input and add it's listener */
            this.searchInput = document.getElementById('supplierSearchInput');
            this.addSearchInputListener();
            /** set the option container as the class property because it is ready in the dom now */
            this.optionContainer = document.getElementById('supplierOptionsContainer');
            this.mainInput.classList.add('focused')
            return true;
        } else {
            this.removeOptionContainer();
            return false;
        }

    }

    /**
     * Generates a HTML structure for supplier options
     * @returns {HTMLDivElement}
     */
    generateOptionContainer()
    {
        const supplierOptionsContainer = document.createElement('div');
        supplierOptionsContainer.id = 'supplierOptionsContainer';

        const supplierSearchInput = document.createElement('input');
        supplierSearchInput.type = 'text';
        supplierSearchInput.id = 'supplierSearchInput'
        const supplierOptions = document.createElement('div');

        supplierOptions.id = 'supplierOptions';
        supplierOptionsContainer.appendChild(supplierSearchInput);
        supplierOptionsContainer.appendChild(supplierOptions);
        return supplierOptionsContainer;
    }

    /**
     * Check if the option container exists in the DOM
     * @returns {boolean}
     */
    optionContainerExists()
    {
        return !!document.getElementById('supplierOptions');
    }

    /**
     * Fetch the suppliers and return a promise
     * @returns {Promise<*>}
     */
    async fetchSuppliers(searchInput, offset, limit)
    {
        let urlParams = new URLSearchParams({
            action: 'gfAjaxRouter',
            handlerClass: 'PluginContainerPackagesVendorControllerVendor',
            method: 'getVendors',
            offset: offset,
            limit: limit,
            search: searchInput
        })
        this.isLoading = true;
        let data = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`);
        let suppliers = await data.text();
        return await JSON.parse(suppliers);
    }

    /**
     * Appends supplier options based on the fetched suppliers to the supplierOptions element,
     * and calls the method to add their listeners
     * @param searchInput The string that the user typed in
     * @param offset The offset - number of times we already fetched
     * @param limit
     */
    appendSuppliers(searchInput, offset, limit)
    {
        if(searchInput.length <3 && searchInput !== '') {
            return;
        }
        this.supplierOptions.innerHTML += `<div id="loader"></div>`;
        this.fetchSuppliers(searchInput,offset, limit).then((response) => {
            if(document.getElementById('loader')) {
                document.getElementById('loader').remove();
            }
            if (response.body.data) {
                response.body.data.forEach((supplier) => {
                    this.supplierOptions.innerHTML += `<div class="supplierOption" data-id="${supplier.id}">${supplier.name}</div>`;
                })
            } else {
                this.allowFetching = false;
                if(!document.getElementById('noMoreVendorsMessage')) {
                    this.supplierOptions.innerHTML += `<span id="noMoreVendorsMessage">${response.message}</span>`;
                    this.supplierOptions.scrollTop = this.supplierOptions.scrollHeight;
                }
            }
        }).then(() => {
            this.addSupplierOptionElementListeners();
            this.isLoading = false;
        })
    }

    /**
     * Add event listeners to option elements
     */
    addSupplierOptionElementListeners()
    {
        let supplierOptionElements = document.querySelectorAll('.supplierOption');
        supplierOptionElements.forEach((elem) => {
            elem.addEventListener('click',() => {
                this.mainInput.value = elem.innerText;
                this.supplierHiddenInputId.value = elem.getAttribute('data-id');
                this.removeOptionContainer();
            })
        })
    }

    /**
     * Remove the option container from the main container
     */
    removeOptionContainer()
    {
        if(this.optionContainer) {
            this.optionContainer.remove();
            this.mainInput.classList.remove('focused')
            delete this.optionContainer;
            delete this.supplierOptions;
            delete this.searchInput;
            this.offsetSearch = 0;
        }
    }

}

document.addEventListener('DOMContentLoaded', function (){
    let supplier = new SupplierSelect({
        containerId: 'supplierContainer',
        supplierNameInputId: 'supplierName',
        supplierIdInputId: 'supplier'
    });
    supplier.addEventListeners();
})
