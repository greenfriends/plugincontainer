<?php

namespace PluginContainer\Packages\Vendor\Mapper;

use PDO;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class Vendor extends BaseMapper
{
    public function __construct(PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfVendor');
    }

    /**
     * @throws \Exception
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->tableName (
		  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) UNIQUE NOT NULL,
		  `city` varchar(20) NOT NULL,
		  `address` varchar(20) NOT NULL,
		  `zipCode` varchar(20) NOT NULL,
		  `notes` varchar(150) DEFAULT NULL,
		  `wpUserId` int(10) DEFAULT NULL,
		  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY  ($this->primaryKey)
		) DEFAULT CHARSET=utf8mb4";
        $this->handleStatement($sql);
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $sql = "DROP TABLE $this->tableName;";
        $this->handleStatement($sql);
    }
}