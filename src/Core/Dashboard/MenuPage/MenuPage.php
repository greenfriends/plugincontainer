<?php

namespace PluginContainer\Core\Dashboard\MenuPage;

use PluginContainer\Core\Dashboard\Model\Feature as FeatureModel;
use PluginContainer\Core\Dashboard\Repository\Feature;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\WpBridge\WpEnqueue;

class MenuPage
{
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Feature
     */
    private $featureRepo;

    public function __construct(Logger $logger, Feature $featureRepo)
    {
        $this->logger = $logger;
        $this->featureRepo = $featureRepo;
    }

    /**
     * Creates menu page inside wordpress admin menu
     */
    public function setupMainPage(): void
    {
        $menuSlug = 'green-friends-settings';
        WpEnqueue::addAdminStyle('admin-main-css', $menuSlug, CONTAINER_PLUGIN_DIR_URI . '/assets/admin/css/style.min.css', [],
            '0.0.9');
        WpEnqueue::addAdminScript('admin-main-js', $menuSlug, CONTAINER_PLUGIN_DIR_URI . 'assets/admin/js/admin.js', [], '0.0.5',
            true);
        WpEnqueue::addAdminScript('color-picker-js', $menuSlug, 'https://unpkg.com/vanilla-picker@2', [], '2.11.2',
            true);
        add_action('admin_enqueue_scripts', function (){
            wp_enqueue_style('jquery-ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
            wp_enqueue_script('jquery-ui-sortable');
        });
        //Convert script type to module
        add_filter('script_loader_tag', function ($tag, $handle, $src) {
            if ('admin-main-js' === $handle) {
                $tag = sprintf('<script type="module" id="%s" src="%s"></script>',$handle, esc_url($src));
            }
            return $tag;
        }, 99, 3);
        add_action('admin_menu', function () use ($menuSlug) {
            add_menu_page('Green Friends Settings', 'Green Friends Settings', 'manage_options', $menuSlug, function () {
                $data = $this->getPageData();
                require_once CONTAINER_PLUGIN_DIR . '/src/Core/Dashboard/templates/mainPage.php';
            });
        });
    }

    /**
     * @return array
     */
    private function getPageData(): array
    {
        $data = [];
        try {
            /** @var FeatureModel[] $features */
            $features = $this->featureRepo->getAll(null, null, true, ['by' => 'displayName', 'order' => 'ASC']);
            $data['featureList'] = $features;
            foreach ($features as $feature) {
                if ($feature->getIsActive() && $feature->getOptionsPage()) {
                    $data['activeWithPage'][] = $feature;
                }
            }
        } catch (\Exception $e) {
            $msg = 'Error getting feature list';
            $this->logger->error($msg, ['exceptionMessage' => $e->getMessage()]);
            $data['error'] = $msg;
        }
        return $data;
    }
}