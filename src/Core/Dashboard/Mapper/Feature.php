<?php

namespace PluginContainer\Core\Dashboard\Mapper;

use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Mapper\BaseMapper;

class Feature extends BaseMapper
{

    public function __construct(\PDO $driver, Logger $logger)
    {
        parent::__construct($driver, $logger, 'gfFeatureList','featureId');
    }
}