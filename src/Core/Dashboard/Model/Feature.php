<?php

namespace PluginContainer\Core\Dashboard\Model;

class Feature
{
    /**
     * @var int|null
     */
    private $featureId;
    /**
     * @var string
     */
    private $displayName;
    /**
     * @var string
     */
    private $className;
    /**
     * @var int
     */
    private $isActive;
    /**
     * @var int
     */
    private $optionsPage;
    /**
     * @var int
     */
    private $isSetupDone;
    /**
     * @var int
     */
    private $configPage;

    /**
     * @param string $displayName
     * @param string $className
     * @param int $optionsPage
     * @param int $configPage
     * @param int $isSetupDone
     * @param int $isActive
     * @param int|null $featureId
     */
    public function __construct(string $displayName, string $className, int $optionsPage, int $configPage, int $isSetupDone = 0, int $isActive = 0, int $featureId = null)
    {
        $this->featureId = $featureId;
        $this->displayName = $displayName;
        $this->className = $className;
        $this->isActive = $isActive;
        $this->optionsPage = $optionsPage;
        $this->isSetupDone = $isSetupDone;
        $this->configPage = $configPage;
    }

    /**
     * @return int
     */
    public function getConfigPage(): int
    {
        return $this->configPage;
    }

    /**
     * @return int
     */
    public function getFeatureId(): ?int
    {
        return $this->featureId;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->isActive;
    }

    /**
     * @return int
     */
    public function getOptionsPage(): int
    {
        return $this->optionsPage;
    }
    
    public function getIsSetupDone():int
    {
        return $this->isSetupDone;
    }
}