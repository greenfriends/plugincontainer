<?php

namespace PluginContainer\Core\Dashboard\Controller;

use DI\Container;
use Exception;
use Laminas\Config\Config;
use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\AjaxRouter\AjaxRouter;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Dashboard\Model\Feature as FeatureModel;
use PluginContainer\Core\Dashboard\Repository\Feature as FeatureRepo;
use PluginContainer\Core\WpBridge\Translations;

class Feature
{
    /**
     * @var FeatureRepo
     */
    private $featureRepo;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var Config
     */
    private $config;

    /**
     * @param FeatureRepo $featureRepo
     * @param Logger $logger
     * @param Container $container
     * @param Config $config
     */
    public function __invoke(FeatureRepo $featureRepo, Logger $logger, Container $container, Config $config)
    {
        $this->featureRepo = $featureRepo;
        $this->logger = $logger;
        $this->container = $container;
        $this->config = $config;
        $this->initActivatedFeatures();
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws \ReflectionException
     * @see AjaxRouter::handleRoute()
     */
    public function getView(): AjaxResponse
    {
        if ((!current_user_can('developer'))) {
            return new AjaxResponse(true);
        }
        $features = $this->featureRepo->getAll(null, null, true, ['by' => 'displayName', 'order' => 'ASC']);
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Core/Dashboard/templates/featureList.php';
        $response = new AjaxResponse(true);
        $response->addParamToBody('data', [ob_get_clean()]);
        return $response;
    }

    public function initActivatedFeatures(): void
    {
        try {
            $activatedFeatures = $this->featureRepo->getAllWithFilters(['isActive' => ['value' => 1]]);
            /** @var FeatureModel $feature */
            foreach ($activatedFeatures as $feature) {
                $this->container->call($feature->getClassName(), [$feature->getFeatureId(), true]);
            }
        } catch (Exception $e) {
            $msg = 'Error in activating feature';
            $this->logger->error($msg, ['exceptionError' => $e->getMessage()]);
            echo $msg;
            die();
        }
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws Exception
     * @see AjaxRouter::handleRoute()
     */
    public function activateFeature(): AjaxResponse
    {
        if (isset($_GET['featureId'])) {
            $feature = $this->featureRepo->getById($_GET['featureId'], false);
            if ($feature) {
                $this->container->call($feature['className'],
                    [$feature['featureId'], $feature['isSetupDone']]);
                $feature['isActive'] = 1;
                $feature['isSetupDone'] = 1;
                $this->featureRepo->update($feature);
                return new AjaxResponse(true, 'Feature successfully activated');
            }
        }
        $this->logger->debug('Feature id was not present inside of GET');
        throw new Exception('Error in activating feature');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws Exception
     * @see AjaxRouter::handleRoute()
     */
    public function deactivateFeature(): AjaxResponse
    {
        if (isset($_GET['featureId'])) {
            $feature = $this->featureRepo->getById($_GET['featureId'], false);
            if ($feature) {
                $feature['isActive'] = 0;
                $this->featureRepo->update($feature);
                return new AjaxResponse(true,'Deactivation of feature successful');
            }
        }
        $this->logger->debug('Feature id was not present inside of GET', ['ajaxAction' => 'activate']);
        throw new Exception('Error in deactivating feature!');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws Exception
     * @see AjaxRouter::handleRoute()
     */
    public function cleanUpAndDeactivateFeature(): AjaxResponse
    {
        if (isset($_GET['featureId'])) {
            $feature = $this->featureRepo->getById($_GET['featureId'], false);
            if ($feature) {
                $this->container->call($feature['className'], [$feature['featureId'], true, true]);
                $feature['isActive'] = 0;
                $feature['isSetupDone'] = 0;
                $this->featureRepo->update($feature);
                return new AjaxResponse(true, 'Feature deactivated successfully and data was deleted');
            }
        }
        $this->logger->debug('Feature id was not present inside of GET');
        throw new Exception('Error in deactivating feature or cleanup of the feature data!');
    }

    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @return AjaxResponse
     * @throws Exception
     * @see AjaxRouter::handleRoute()
     */
    public function syncWithConfig(): AjaxResponse
    {
        $configFeatures = $this->config->get('featureList')->toArray();
        $featureNames = [];
        $features = $this->featureRepo->getAll();
        foreach ($features as $feature) {
            $featureNames[] = $feature->getDisplayName();
        }
        $diffCreate = array_diff(array_keys($configFeatures), $featureNames);
        $diffDelete = array_diff($featureNames, array_keys($configFeatures));
        foreach ($diffCreate as $featureName) {
            $this->featureRepo->create([
                'displayName' => $featureName,
                'className' => $configFeatures[$featureName]['handlerClass'],
                'optionsPage' => $configFeatures[$featureName]['optionsPage'],
                'configPage' => $configFeatures[$featureName]['configPage'],
                'isSetupDone' => false,
                'isActive' => 0,
                'featureId' => null
            ]);
        }
        foreach ($diffDelete as $featureName) {
            $feature = $this->featureRepo->getAllWithFilters(['displayName' => ['value' => $featureName]])[0];
            if ($feature->getIsSetupDone()) {
                $msg = 'Trying to delete feature without doing cleanup first.';
                $this->logger->debug($msg,
                    ['featureId' => $feature->getFeatureId(), 'featureName' => $feature->getDisplayName()]);
                throw new Exception (sprintf("%s feature name is %s", $msg, $featureName));
            } else {
                $this->featureRepo->delete($feature->getFeatureId());
            }
        }
        return new AjaxResponse(true, 'Sync completed');
    }
}