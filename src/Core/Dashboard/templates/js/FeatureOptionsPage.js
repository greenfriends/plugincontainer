import Tab from "../../../../../assets/admin/js/Tab.js";

export default class FeatureOptionsPage extends Tab
{
    constructor(props) {
        super();
        this.activateButtons = props.activateButtons;
        this.deactivateButtons = props.deactivateButtons;
        this.cleanUpButtons = props.cleanUpButtons;
        this.configButtons = props.configButtons;
        this.syncButton = props.syncButton;
        this.container = props.container;

    }
    init() {
        this.addEventListenersToButtons();
    }
    addEventListenersToButtons() {
        this.activateButtonsListeners();
        this.deactivateButtonsListeners();
        this.cleanUpButtonsListeners();
        this.configButtonsListeners();
    }

    activateButtonsListeners() {
        let self = this;
        self.handlerClass = document.getElementsByClassName('tab active')[0].getAttribute('data-handler');
        this.activateButtons.forEach(function(elem){
            elem.addEventListener('click',function(){
                let params = {
                    method: 'activateFeature',
                    featureId: this.getAttribute('data-featureId')
                };
                self.fetchResponse(params).then(function(response){
                    localStorage.setItem('message', JSON.stringify({message: response.message, success: response.success}))
                    location.reload();
                })
            });
        })
        self.syncButton.addEventListener('click', function (){
            self.removeNotice();
            self.fetchResponse({method: 'syncWithConfig'}).then(function (response) {
                    document.getElementsByClassName('tab active')[0].click();
                    self.printNotice(response);
            });
        })
    }
    deactivateButtonsListeners() {
        let self = this;
        this.deactivateButtons.forEach(function(elem){
            elem.addEventListener('click',function(){
                let confirmation = confirm('Da li ste sigurni da želite da deaktivirate dodatak?');
                if(confirmation === false) {
                    return;
                }
                let params = {
                    method: 'deactivateFeature',
                    featureId: this.getAttribute('data-featureId')
                };
                self.fetchResponse(params).then(function(response){
                    localStorage.setItem('message', JSON.stringify({message: response.message, success: response.success}))
                    location.reload();
                })
            });
        })
    }
    cleanUpButtonsListeners() {
        let self = this;
        this.cleanUpButtons.forEach(function(elem){
            elem.addEventListener('click',function(){
                let button = this;
                let confirmation = confirm('UPOZORENJE, OVA AKCIJA JE NEPOVRATNA, DA LI STE SIGURNI DA ŽELITE DA NASTAVITE?');
                if(confirmation === true) {
                    setTimeout(function(){
                        let confirmation2 = confirm('DA LI I DALJE ŽELITE DA OBRIŠETE SVE PODATKE?');
                        if(confirmation2 === true) {
                            let params = {
                                method: 'cleanUpAndDeactivateFeature',
                                featureId: button.getAttribute('data-featureId')
                            };
                            self.fetchResponse(params).then(function(response){
                                localStorage.setItem('message', JSON.stringify({message: response.message, success: response.success}))
                                location.reload();
                            })
                        }
                    },700)
                }
            });
        })
    }

    configButtonsListeners() {
        let self = this;
        self.handlerClass = document.getElementsByClassName('tab active')[0].getAttribute('data-handler');
        this.configButtons.forEach(function(elem){
            elem.addEventListener('click',function(){
                alert('Coming soon :P')
                // let params = {
                //     method: 'getConfigForm',
                //     featureId: this.getAttribute('data-featureId')
                // };
                // self.fetchResponse(params).then(function(response){
                //     localStorage.setItem('message', JSON.stringify({message: response.message, success: response.success}))
                //     location.reload();
                // })
            });
        })
    }
    insertContent(content) {
        return new Promise((resolve) => {
            this.container.innerHTML = content;
            resolve();
        });

    }
}