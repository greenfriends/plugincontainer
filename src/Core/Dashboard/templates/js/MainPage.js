import Tab from "../../../../../assets/admin/js/Tab.js";

export default class MainPage extends Tab
{
    constructor(props) {
        super();
        this.tabs = props.tabs;
        this.container = props.container;
        this.message = props.message;
        this.historyPush = true;
    }
    init(){
        this.addEventListeners();
        this.clickActiveTab()
        window.addEventListener('popstate', (event) =>  {
            this.historyPush = false;
            this.clickActiveTab();
            this.historyPush = true;
        });
        //JS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (this.message !== null && this.message !== '' && this.message !== 'undefined' && this.message !== 'null')  {
            this.printNotice(JSON.parse(this.message))
            localStorage.setItem('message', null);
        }
    }

    clickActiveTab() {
        let url = new URL(window.location);
        let previousTabIndex = url.searchParams.get('tab') ?? 0;
        if (previousTabIndex !== 0 ) {
            this.tabs[previousTabIndex].click();
        }
    }
    addEventListeners() {
        let self = this;
        this.tabs.forEach(function (elem,index) {
            elem.addEventListener('click', function () {
                if (self.historyPush === true) {
                    let url = new URL(window.location);
                    url.searchParams.set('tab', index);
                    window.history.pushState({}, '', url.toString());
                }
                document.getElementsByClassName('tab active')[0].classList.remove('active');
                elem.classList.add('active');
                self.handlerClass = document.getElementsByClassName('tab active')[0].getAttribute('data-handler');
                self.removeNotice();
                self.fetchResponse({method: 'getView'}).then(function (response) {
                    self.insertContent(response.body.data).then(function () {
                        self.container.dispatchEvent(new CustomEvent(elem.getAttribute('data-name')))
                    });
                });
            })
        })

    }
    async insertContent(content) {
        this.container.innerHTML = content;
    }
}