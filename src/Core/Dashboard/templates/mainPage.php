<?php
/**
 * @var array $data
 * @see \PluginContainer\Core\Dashboard\MenuPage\MenuPage::getPageData()
 */
use PluginContainer\Core\Dashboard\Controller\Feature as FeatureController;
use PluginContainer\Core\Dashboard\Model\Feature;
use PluginContainer\Core\WpBridge\Translations as T;
$developer = current_user_can('developer');
$activeTabClass = '';
if (!$developer) {
    $activeTabClass = ' active';
}
?>
<h1><?=__('Green Friends Settings', 'plugin-container')?></h1>
<div id="noticeContainer"></div>
<div id="mainWrapper">
    <nav>
        <ul class="tabs">
            <?php if($developer):?>
            <li data-name="Feature list" data-handler="<?=stripslashes(FeatureController::class)?>" class="tab active">
                <span><?=__('Feature list', 'plugin-container')?></span>
            </li>
            <?php endif;?>
            <?php
            /** @var Feature $feature */
            foreach ($data['activeWithPage'] ?? [] as $key => $feature): //@TODO: fix this with config ?
                if (!$developer && $feature->getDisplayName() === 'Role Manager'){
                    continue;
                }
                ?>
                <li data-name="<?=$feature->getDisplayName()?>" data-handler="<?=stripslashes($feature->getClassName())?>"
                    class="tab <?=$key === 0 && !$developer ? $activeTabClass : ''?>">
                    <span><?=$feature->getDisplayName()?></span>
                </li>
            <?php endforeach;?>
        </ul>
    </nav>
    <div id="content"></div>
</div>


