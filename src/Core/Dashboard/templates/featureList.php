<?php

use PluginContainer\Core\Dashboard\Controller\Feature as FeatureController;
use PluginContainer\Core\Dashboard\Model\Feature;
use PluginContainer\Core\WpBridge\Translations;

/**
 * @var $features Feature[]
 * @see FeatureController::getView()
 */
 ?>
<table id="featureList">
    <button id="syncWithConfig"><?=__('Sync with config', 'plugin-container')?></button>
    <thead>
    <tr>
        <th><?=__('Feature name', 'plugin-container')?></th>
        <th><?=__('Actions', 'plugin-container')?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($features as $feature): ?>
        <tr class="<?=$feature->getIsActive() ? 'active' : ''?>">
            <td><?=$feature->getDisplayName()?></td>
            <td>
                <div>
                    <?php if ($feature->getConfigPage()):?>
                        <button class="featureConfig" data-featureId="<?=$feature->getFeatureId()?>">
                            <?=__('Config', 'plugin-container')?></button>
                    <?php endif;?>
                    <button data-featureId="<?=$feature->getFeatureId()?>"
                        <?=$feature->getIsActive() ? 'disabled class="activateFeature disabled"' : 'class="activateFeature"'?>>
                        <?=__('Activate', 'plugin-container')?></button>
                    <button data-featureId="<?=$feature->getFeatureId()?>"
                        <?=$feature->getIsActive() ? 'class="deactivateFeature"' : 'disabled class="deactivateFeature disabled"'?>>
                        <?=__('Deactivate', 'plugin-container')?></button>
                    <button data-featureId="<?=$feature->getFeatureId()?>" class="cleanUpFeature">
                        <?=__('Deactivate and delete data', 'plugin-container')?></button>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


