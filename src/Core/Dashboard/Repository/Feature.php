<?php

namespace PluginContainer\Core\Dashboard\Repository;

use PluginContainer\Core\Dashboard\Model\Feature as Model;
use PluginContainer\Core\Repository\BaseRepository;

class Feature extends BaseRepository
{
    public function __construct(\PluginContainer\Core\Dashboard\Mapper\Feature $mapper)
    {
        parent::__construct(Model::class);
        $this->mapper = $mapper;
    }

    /**
     * @param array $modelData
     * @return Model
     * @throws \ReflectionException
     */
    protected function make(array $modelData): Model
    {
        return parent::make($modelData);
    }


}