<?php

namespace PluginContainer\Core\Activator;

use Exception;
use Laminas\Config\Config;
use PluginContainer\Core\Logger\Logger;
use PDO;
use PDOStatement;
use PluginContainer\Core\Dashboard\Repository\Feature;
use WP_Roles;

class Activator
{
    /**
     * @var PDO
     */
    private $driver;
    /**
     * @var Config
     */
    private $config;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var Feature
     */
    private $featureRepo;

    /**
     * @param PDO $driver
     * @param Config $config
     * @param Logger $logger
     * @param Feature $featureRepo
     */
    public function __construct(PDO $driver, Config $config, Logger $logger, Feature $featureRepo)
    {
        $this->driver = $driver;
        $this->config = $config;
        $this->logger = $logger;
        $this->featureRepo = $featureRepo;

    }

    public function activate(): void
    {
        $this->logger->debug('---Activating pluginContainer---');
        try {
            $this->createDeveloperRole();
            $this->createDeveloperUsers();
            $this->createFeatureListTable();
            $this->createIndexesForFeatureList();
            $this->creatFeatureConfigFieldsTable();
            $this->populateFeatureListTable();
        } catch (Exception $e) {
            if (!strpos($e->getMessage(), 'Duplicate entry')) {
                $this->logger->error($e->getMessage(), ['class' => __CLASS__, 'function' => 'activate','trace' => $e->getTrace()]);
                die();
            }
        }
        $this->logger->debug('---Activated pluginContainer successfully---');
    }

    /**
     * @throws Exception
     */
    private function createFeatureListTable(): void
    {
        $this->logger->debug('Creating feature list table');
        $sql = "CREATE TABLE IF NOT EXISTS `gfFeatureList` (
          `featureId` int(3) unsigned NOT NULL AUTO_INCREMENT,
          `displayName` varchar(128) NOT NULL,
          `className` varchar(128)  UNIQUE NOT NULL,
          `optionsPage` int(1) unsigned NOT NULL,
          `configPage` int(1) unsigned NOT NULL,
          `isSetupDone` int(1) unsigned NOT NULL,
          `isActive` int(1) unsigned DEFAULT 0,
          PRIMARY KEY (`featureId`)
        ) DEFAULT CHARSET=utf8mb4;";
        $statement = $this->driver->prepare($sql);
        if (!$statement->execute()) {
            $this->handleSqlError($statement, $sql);
        }
        $this->logger->debug('Finished creating feature list table');
    }
    /**
     * @throws Exception
     */
    private function creatFeatureConfigFieldsTable(): void
    {
        $this->logger->debug('Creating feature config table');
        $sql = "CREATE TABLE IF NOT EXISTS `gfFeatureConfig` (
          `featureId` int(3) unsigned NOT NULL,
          `fieldsData` LONGTEXT NOT NULL,
          PRIMARY KEY (`featureId`)
        ) DEFAULT CHARSET=utf8mb4;";
        $statement = $this->driver->prepare($sql);
        if (!$statement->execute()) {
            $this->handleSqlError($statement, $sql);
        }
        $this->logger->debug('Finished creating feature config table');
    }
    /**
     * @throws Exception
     */
    private function populateFeatureListTable(): void
    {
        $this->logger->debug('Populating feature list table');
        foreach ($this->config->get('featureList') as $name => $data) {
            $this->featureRepo->create([
                'displayName' => $name,
                'className' => $data['handlerClass'],
                'optionsPage' => $data['optionsPage'],
                'configPage' => $data['configPage'],
                'isSetupDone' => 0,
                'isActive' => 0
            ]);
        }
        $this->logger->debug('Finished populating feature list table');
    }

    /**
     * @throws Exception
     */
    private function createIndexesForFeatureList(): void
    {
        $this->logger->debug('Creating isActive index');
        $sql = "SHOW INDEX FROM gfFeatureList WHERE Column_name='isActive';";
        $result = $this->driver->query($sql)->fetchAll();
        if (count($result) === 0) {
            $sql = "CREATE INDEX gfFeatureList_activeIndex ON gfFeatureList(isActive)";
            $statement = $this->driver->prepare($sql);
            if (!$statement->execute()) {
                $this->handleSqlError($statement, $sql);
            }
            $this->logger->debug('Finished creating isActive index in gfFeatureList table');
        }
        $this->logger->debug('isActive index already exists');
    }

    private function createDeveloperRole(): void
    {
        $this->logger->debug('Creating developer role');
        global $wp_roles;
        if (!isset($wp_roles)) {
            $wp_roles = new WP_Roles();
        }
        $adm = $wp_roles->get_role('administrator');
        $wp_roles->add_role('developer', 'Developer', $adm->capabilities);
        $this->logger->debug('Finished creating developer role');
    }

    private function createDeveloperUsers(): void
    {
        $this->logger->debug('Creating developer users');
        $users = $this->config->get('developerAccounts');
        if ($users) {
            foreach ($users as $username => $params) {
                $this->logger->debug('Creating developer', ['username' => $username, 'email' => $params['email']]);
                if (!username_exists($username) && !email_exists($params['email'])) {
                    $userId = wp_create_user($username, $params['password'], $params['email']);
                    $user = get_user_by('id', $userId);
                    $user->remove_role('subscriber');
                    $user->add_role('developer');
                    $user->add_role('administrator');
                    $this->logger->debug('Finished creating developer', ['username' => $username, 'email' => $params['email']]);
                }
                $this->logger->debug('User with this email or username already exists', ['username' => $username, 'email' => $params['email']]);
            }
            return;
        }
        $this->logger->debug('Users not found in config');
    }

    /**
     * @param PDOStatement $statement
     * @param string $sql
     * @throws Exception
     */
    private function handleSqlError(PDOStatement $statement, string $sql): void
    {
        throw new Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
    }
}