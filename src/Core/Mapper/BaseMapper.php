<?php

namespace PluginContainer\Core\Mapper;

use Exception;
use PluginContainer\Core\Logger\Logger;
use PDO;
use PDOStatement;
use PluginContainer\Core\Factory\BaseFactory;

class BaseMapper
{
    /**
     * @var PDO
     */
    protected $driver;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var string
     */
    protected $tableName;
    /**
     * @var string
     */
    protected $primaryKey;

    /**
     * @param PDO $driver
     * @param Logger $logger
     * @param string $tableName
     * @param string $primaryKey
     */
    public function __construct(PDO $driver, Logger $logger, string $tableName, string $primaryKey = 'id')
    {
        $this->driver = $driver;
        $this->logger = $logger;
        $this->tableName = $tableName;
        $this->primaryKey = $primaryKey;
    }
    
    private function getPrimaryKeyFromModel($model)
    {
        $primaryKeyGetter = "get".ucfirst($this->primaryKey);
        return $model->$primaryKeyGetter();
    }

    /**
     * @param $model
     * @return array
     * @throws \ReflectionException
     */
    private function getColumnsDefinition($model): array
    {
        return BaseFactory::getModelColumnDefinitions(get_class($model));
    }
    
    /**
     * @param string $sql
     * @param array $statementParams
     * @return PDOStatement
     * @throws Exception
     */
    protected function handleStatement(string $sql, array $statementParams = []):PDOStatement
    {
        $statement = $this->driver->prepare($sql);
        if (!$statement->execute($statementParams)) {
            throw new Exception('Sql: ' . $sql . ' generated error:' . print_r($statement->errorInfo(), true));
        }
        return $statement;
    }

    /**
     * @param array $modelColumns
     * @param $model
     * @return array
     * @throws Exception
     */
    private function prepareArrayForExecute(array $modelColumns, $model):array
    {
        $id = $this->getPrimaryKeyFromModel($model);
        if ($id) {
            $oldData = $this->getById($id);
        }
        $array = [];
        foreach ($modelColumns as $modelColumn) {
            $getter = "get" . ucfirst($modelColumn);
            $newValue = $model->$getter();
            if ($id && $newValue === $oldData[$modelColumn]) {
                continue;
            }
            if (isset($oldData)) {
                $this->logger->debug('Updating field',
                    [
                        'tableName' => $this->tableName,
                        'modelId' => $id,
                        'columnName' => $modelColumn,
                        'oldValue' => $oldData[$modelColumn],
                        'newValue' => $newValue
                    ]);
            } else {
                $this->logger->debug('Inserting into field',
                    [
                        'tableName' => $this->tableName,
                        'columnName' => $modelColumn,
                        'value' => $newValue
                    ]);
            }

            $array[":$modelColumn"] = $newValue;
        }
        return $array;
    }

    /**
     * @param $model
     * @return int
     * @throws Exception
     */
    public function create($model): int
    {
        $this->logger->debug(sprintf('Trying to insert into %s', $this->tableName));
        $modelColumns = array_keys($this->getColumnsDefinition($model));
        $columnsString = implode(', ',$modelColumns);
        $preparedArray = $this->prepareArrayForExecute($modelColumns, $model);
        $valuesString = implode(', ', array_keys($preparedArray));
        $sql = "INSERT INTO {$this->tableName} ($columnsString) VALUES ($valuesString)";
        $this->handleStatement($sql, $preparedArray);
        $lastInsertId = $this->driver->lastInsertId();
        $this->logger->debug(sprintf('Inserting into %s complete', $this->tableName),['lastInsertId' => $lastInsertId]);
        return $lastInsertId;
    }

    /**
     * @param $model
     * @throws Exception
     */
    public function update($model):void
    {
        $this->logger->debug(sprintf('Trying to update %s', $this->tableName), ['modelId' => $this->getPrimaryKeyFromModel($model)]);
        $modelColumns = array_keys($this->getColumnsDefinition($model));
        $modelColumns = $this->prepareArrayForExecute($modelColumns, $model);
        if(count($modelColumns) === 0) {
            return;
        }
        //Deletes the primary key from column definition array we don't need it here
        if (($key = array_search($this->primaryKey, $modelColumns,true)) !== false) {
            unset($modelColumns[$key]);
        }
        $sql = "UPDATE {$this->tableName} SET ";
        foreach ($modelColumns as $key => $columnData) {
            $comma = ', ';
            if (array_key_last($modelColumns) === $key){
                $comma = '';
            }
            $sql.= str_replace(':','',$key)." = $key$comma";
        }
        $sql .= " WHERE {$this->primaryKey} = {$this->getPrimaryKeyFromModel($model)}";
        $this->handleStatement($sql, $modelColumns);
        $this->logger->debug(sprintf('Updating %s complete', $this->tableName), ['modelId' => $this->getPrimaryKeyFromModel($model)]);
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function delete(int $id):void
    {
        $this->logger->debug(sprintf('Trying to delete from %s', $this->tableName), ['modelId' => $id]);
        $sql = "DELETE FROM {$this->tableName} WHERE $this->primaryKey = :$this->primaryKey";
        $this->handleStatement($sql, [":$this->primaryKey" => $id] );
        $this->logger->debug(sprintf('Deleting from %s complete', $this->tableName), ['modelId' => $id]);
    }

    /**
     * @param int $id
     * @return array|null
     * @throws Exception
     */
    public function getById(int $id):?array
    {
        $sql = "SELECT * FROM $this->tableName WHERE $this->primaryKey = :$this->primaryKey";
        $statement = $this->handleStatement($sql, [":$this->primaryKey" => $id]);
        if ($statement->rowCount() === 0) {
            return null;
        }
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @return array
     * @throws Exception
     */
    public function getALl(?int $offset, ?int $limit, $order = null): array
    {
        $sql = "SELECT * FROM $this->tableName";
        $orderBy = " ORDER BY $this->primaryKey DESC";
        if (isset($order['by'], $order['order'])) {
            $orderBy = ' ORDER BY '. $order['by']. ' ' . $order['order'];
        }
        $sql .= $orderBy;
        if (isset($limit)) {
            $sql.= " LIMIT $limit";
        }
        if (isset($offset)) {
            $sql .= " OFFSET $offset";
        }
        return $this->handleStatement($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $filters
     * @param int|null $offset
     * @param int|null $limit
     * @return array
     * @throws Exception
     */
    public function getWithFilters(array $filters, ?int $offset, ?int $limit): array
    {
       $sql = "SELECT * FROM $this->tableName WHERE";
       foreach ($filters as $key => $value) {
           if (array_key_first($filters) === $key) {
               $sql .= " $key = '{$value['value']}'";
               continue;
           }
           $sql .= " {$value['operator']} $key = '{$value['value']}'";
           if (isset($limit)) {
               $sql.= " LIMIT $limit";
           }
           if (isset($offset)) {
               $sql .= " OFFSET $offset";
           }
       }
        return $this->handleStatement($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $fieldName
     * @param string $value
     * @param int|null $offset
     * @param int|null $limit
     * @return array|false
     * @throws Exception
     */
    public function searchByField(string $fieldName, string $value, ?int $offset, ?int $limit)
    {
        $sql = "SELECT * FROM $this->tableName WHERE $fieldName LIKE '%{$value}%'";
        if (isset($limit)) {
            $sql.= " LIMIT $limit";
        }
        if (isset($offset)) {
            $sql .= " OFFSET $offset";
        }
        return $this->handleStatement($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $columnName
     * @return array|false
     * @throws Exception
     */
    public function getPossibleFiltersForColumn(string $columnName)
    {
        if ($columnName === 'createdAt' || $columnName === 'updatedAt') {
            $sql = "SELECT DISTINCT DATE($columnName) FROM $this->tableName";
        } else {
            $sql = "SELECT DISTINCT $columnName FROM $this->tableName";
        }
        return $this->handleStatement($sql)->fetchAll(PDO::FETCH_NUM);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getCount()
    {
        $sql = "SELECT COUNT(*) FROM $this->tableName";
        return $this->handleStatement($sql)->fetch(PDO::FETCH_NUM);
    }

    /**
     * @throws \Exception
     */
    public function deleteTable(): void
    {
        $sql = "DROP TABLE $this->tableName;";
        $this->handleStatement($sql);
    }

    /**
     * @param string $indexName
     * @param string $columnName
     * @throws \Exception
     */
    public function createIndex(string $indexName, string $columnName): void
    {
        $indexName = "{$this->tableName}_$indexName";
        $sql = "CREATE index $indexName ON $this->tableName (`$columnName`);";
        $this->handleStatement($sql);
    }

    /**
     * @param string $fieldName
     * @param string $fieldValue
     * @return array|false
     * @throws Exception
     */
    public function getByField(string $fieldName, string $fieldValue)
    {
        $sql = "SELECT * FROM $this->tableName WHERE `{$fieldName}` = '{$fieldValue}'";
        return $this->handleStatement($sql)->fetchAll();
    }
}