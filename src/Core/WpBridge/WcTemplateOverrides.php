<?php


namespace PluginContainer\Core\WpBridge;
class WcTemplateOverrides
{

    /**
     * Helper function to load a WooCommerce template or template part file from the active theme or a plugin folder
     **/
    private static function loadTemplateFile($templateName, $templateDir): ?string
    {
        // Check theme folder first
        $file = get_stylesheet_directory() . '/woocommerce/' . $templateName;
        if (@file_exists( $file )) {
            return $file;
        }

        // Now check plugin folder
        $file = CONTAINER_PLUGIN_DIR . $templateDir . '/' . $templateName;
        if ( @file_exists( $file ) ) {
            return $file;
        }
        return null;
    }

    /**
     * @param string $templateName
     * @param string $templateDir
     */
    public static function overrideWcTemplate(string $templateName, string $templateDir): void
    {
        add_filter('woocommerce_template_loader_files', function($templates, $templateName) use($templateDir){
            // Capture/cache the $template_name which is a file name like single-product.php
            wp_cache_set($templateName.$templateDir, $templateName); // cache the template name
            return $templates;
        }, 10, 2 );
        add_filter('template_include', function($template) use ($templateName, $templateDir){
            if ($templateName = wp_cache_get($templateName.$templateDir)) {
                wp_cache_delete($templateName.$templateDir); // delete the cache
                if ($file = self::loadTemplateFile($templateName, $templateDir)) {
                    return $file;
                }
            }
            return $template;
        }, 11 );
        add_filter('wc_get_template_part', function($template, $slug, $name) use ($templateName, $templateDir) {
            $file = self::loadTemplateFile($templateName, $templateDir);
            return $file ?: $template;
        }, 10, 3 );

        add_filter('woocommerce_locate_template', function($template, $slug, $name) use ($templateName, $templateDir) {
            $file = self::loadTemplateFile($templateName, $templateDir);
            return $file ?: $template;
        }, 10, 3 );
    }
}