<?php

namespace PluginContainer\Core\WpBridge;

use Laminas\Config\Config;

class Translations
{
    public static function getTranslatedString(string $string)
    {
        global $gfContainer;
        $config = $gfContainer->get(Config::class);
        return __($string, $config->get("translationDomain"));
    }
}