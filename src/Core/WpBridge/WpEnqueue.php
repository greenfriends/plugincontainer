<?php

namespace PluginContainer\Core\WpBridge;

/**
 * Class WpEnqueue.
 *
 * Proxy class for more convenient asset setup.
 *
 */
class WpEnqueue
{

    public static function actionEnqueueScripts($function): void
    {
        add_action('wp_enqueue_scripts', static function ($hook) use ($function) {
            $function($hook);
        });
    }

    public static function actionBlocksEnqueueScripts($function): void
    {
        add_action('enqueue_block_assets', static function ($hook) use ($function) {
            $function($hook);
        });
    }

    public static function actionAdminEnqueueScripts($function): void
    {
        add_action('admin_enqueue_scripts', static function ($hook) use ($function) {
            $function($hook);
        });
    }

    /**
     * @param $handle
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param string $media
     */
    public static function addFrontendStyle($handle, $src = '', $deps = [], $ver = false, $media = 'all'): void
    {
        self::actionEnqueueScripts(static function () use ($handle, $src, $deps, $ver, $media) {
            wp_enqueue_style($handle, $src, $deps, $ver, $media);;
        });
    }

    /**
     * @param $handle
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param bool $inFooter
     */
    public static function addFrontendScript($handle, $src = '', $deps = [], $ver = false, $inFooter = true): void
    {
        self::actionEnqueueScripts(static function () use ($handle, $src, $deps, $ver, $inFooter) {
            wp_enqueue_script($handle, $src, $deps, $ver, $inFooter);
        });
    }

    /**
     * @param $handle
     * @param $menuSlug
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param string $media
     */
    public static function addAdminStyle($handle, $menuSlug, $src = '', $deps = [], $ver = false, $media = 'all'): void
    {
        self::actionAdminEnqueueScripts(static function ($hook) use ($handle, $menuSlug, $src, $deps, $ver, $media) {
            if (strpos($hook, $menuSlug) === false) {
                return;
            }
            wp_enqueue_style($handle, $src, $deps, $ver, $media);
        });
    }

    /**
     * @param $handle
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param string $media
     */
    public static function addGlobalAdminStyle($handle, $src = '', $deps = [], $ver = false, $media = 'all'): void
    {
        self::actionAdminEnqueueScripts(static function () use ($handle, $src, $deps, $ver, $media) {
            wp_enqueue_style($handle, $src, $deps, $ver, $media);
        });
    }


    /**
     * @param $handle
     * @param $menuSlug
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param false $in_footer
     */
    public static function addAdminScript(
        $handle,
        $menuSlug,
        $src = '',
        $deps = [],
        $ver = false,
        $in_footer = false
    ): void {
        self::actionAdminEnqueueScripts(static function ($hook) use (
            $handle,
            $menuSlug,
            $src,
            $deps,
            $ver,
            $in_footer
        ) {
            if (strpos($hook, $menuSlug) === false) {
                return;
            }
            wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
        });
    }

    /**
     * @param $handle
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param false $in_footer
     */
    public static function addGlobalAdminScript($handle, $src = '', $deps = [], $ver = false, $in_footer = false): void
    {
        self::actionAdminEnqueueScripts(static function ($hook) use ($handle, $src, $deps, $ver, $in_footer) {
            wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
        });
    }

    /**
     * @param $handle
     */
    public static function removeFrontendStyle($handle): void
    {
        self::actionEnqueueScripts(static function () use ($handle) {
            wp_dequeue_style($handle);
            wp_deregister_style($handle);
        });
    }

    /**
     * @param $handle
     */
    public static function removeFrontendScript($handle): void
    {
        self::actionEnqueueScripts(static function () use ($handle) {
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        });
    }

    /**
     * @param $handle
     * @param string $src
     * @param array $deps
     * @param false $ver
     * @param false $in_footer
     */
    public static function addGutenbergBlockScript(
        $handle,
        $src = '',
        $deps = [],
        $ver = false,
        $in_footer = false
    ): void {
        self::actionBlocksEnqueueScripts(static function () use ($handle, $src, $deps, $ver, $in_footer) {
            wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
        });
    }

    public static function list_enqueued_scripts(): void
    {
        add_action('wp_print_scripts', static function () {
            global $wp_scripts;
            foreach ($wp_scripts->queue as $handle) :
                echo $handle . ', ';
            endforeach;
        });
    }
}
