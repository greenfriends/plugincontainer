<?php

namespace PluginContainer\Core\Factory;

use Exception;
use PluginContainer\Packages\Vendor\Model\Vendor;

class BaseFactory
{
    /**
     * @var string
     */
    private $modelClassName;

    /**
     * @param string $modelClassName
     */
    public function __construct(string $modelClassName)
    {
        $this->modelClassName = $modelClassName;
    }

    /**
     * @throws \ReflectionException
     */
    public function createModel(array $data)
    {
        $modelData = $this->reorderModelDataForConstructor($data);
        return new $this->modelClassName(... array_values($modelData));
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     * @throws \ReflectionException
     */
    private function reorderModelDataForConstructor(array $data): array
    {
        $filteredData = [];
        foreach ($this::getModelColumnDefinitions($this->modelClassName) as $key => $value) {
            $filteredValue = null;
            if (isset($data[$key]) && $value['type'] !== 'bool') {
                settype($data[$key], $value['type']);
                $filteredValue = $data[$key];
            }
//            $filteredData[$key] = $data[$key] ?? null;
            $filteredData[$key] = $filteredValue;
        }
        return $filteredData;
    }

    /**
     * @param string $modelClassName
     * @return array
     * @throws \ReflectionException
     */
    public static function getModelColumnDefinitions(string $modelClassName): array
    {
        $modelColumnDef = [];
        $reflectionClass = new \ReflectionClass($modelClassName);
        $constructor = $reflectionClass->getConstructor();
        if (!$constructor) {
            throw new \ReflectionException("Constructor is not defined in: $modelClassName");
        }
        foreach ($constructor->getParameters() as $param){
            if (!$param->getType()){
                throw new \ReflectionException("Return type for {$param->getName()} is not defined class: $modelClassName");
            }
            $modelColumnDef[$param->getName()] = ['type' => $param->getType()->getName(), 'required' => !$param->isOptional()];
        }
        return $modelColumnDef;
    }
}