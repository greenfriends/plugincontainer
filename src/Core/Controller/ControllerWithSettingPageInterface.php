<?php

namespace PluginContainer\Core\Controller;

Interface ControllerWithSettingPageInterface
{
    public function getView();
}