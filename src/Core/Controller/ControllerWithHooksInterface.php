<?php

namespace PluginContainer\Core\Controller;

interface ControllerWithHooksInterface
{
    public function init(): void;
}