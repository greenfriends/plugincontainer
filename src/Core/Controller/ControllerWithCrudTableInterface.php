<?php

namespace PluginContainer\Core\Controller;

use PluginContainer\Core\AjaxRouter\AjaxResponse;
use PluginContainer\Core\Components\CrudTable\Traits\CrudTable;

interface ControllerWithCrudTableInterface
{
    public function create(): AjaxResponse;

    public function update(): AjaxResponse;

    public function delete(): AjaxResponse;

    /**
     * @return AjaxResponse
     * @see CrudTable::getTableData()
     */
    public function getTableData(): AjaxResponse;

    public function getFormFields(): AjaxResponse;

    public function getView(): AjaxResponse;

}