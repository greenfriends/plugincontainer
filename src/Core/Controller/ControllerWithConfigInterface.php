<?php

namespace PluginContainer\Core\Controller;

interface ControllerWithConfigInterface
{
    public function getConfigForm();
    public function saveConfig();
}