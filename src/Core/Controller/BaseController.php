<?php

namespace PluginContainer\Core\Controller;

use PluginContainer\Core\Cache\CacheWrapper;
use PluginContainer\Core\Logger\Logger;
use PluginContainer\Core\Dashboard\Repository\Feature;

abstract class BaseController
{
    /**
     * @var Feature
     */
    protected $featureRepo;
    /**
     * @var int
     */
    protected $featureId;
    /**
     * @var Logger
     */
    protected $logger;


    /**
     * @param Feature $featureRepo
     * @param Logger $logger
     */
    public function __construct(Feature $featureRepo, Logger $logger)
    {
        $this->featureRepo = $featureRepo;
        $this->logger = $logger;
    }

    /**
     * @param int $featureId
     * @param int $setup
     * @param int $cleanup
     */
    public function __invoke(int $featureId, int $setup = 0, int $cleanup = 0)
    {
        $this->featureId = $featureId;
        if (!$setup && method_exists($this, 'setup')) {
            $this->setup();
        }
        if ($cleanup) {
            if (method_exists($this, 'cleanup')) {
                $this->cleanup();
            }
        } elseif (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * @throws \Exception
     */
    protected function setSetupToTrue(): void
    {
        $data = $this->featureRepo->getById($this->featureId, false);
        $data['isSetupDone'] = 1;
        $this->featureRepo->update($data);
    }

    /**
     * @throws \Exception
     */
    protected function setSetupToFalse(): void
    {
        $data = $this->featureRepo->getById($this->featureId, false);
        $data['isSetupDone'] = 0;
        $this->featureRepo->update($data);
    }
}