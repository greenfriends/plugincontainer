<?php

namespace PluginContainer\Core\Controller;

interface ControllerWithSetupInterface
{
    public function setup();

    public function cleanup();
}