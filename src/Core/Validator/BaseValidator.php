<?php

namespace PluginContainer\Core\Validator;
class BaseValidator
{

    /**
     * @param array $data
     * @param string $modelClassName
     * @param string $for
     * @return bool
     * @throws \Exception
     */
    public function isValid(array $data, string $modelClassName, string $for): bool
    {
        $exception = '';
        $modelColumnDef = $modelClassName::getColumnDefinitions($for);
        foreach ($modelColumnDef as $key => $value) {
            if (($value['required'] === true)) {
                $arr = preg_split('/(?=[A-Z])/', $key);
                $string = '';
                foreach ($arr as $index => $stringPart) {
                    if (array_key_first($arr) === $index) {
                        $string .= ucfirst($stringPart) . ' ';
                        continue;
                    }
                   $string .= lcfirst($stringPart);
                }
                if (!isset($data[$key])) {
                    $exception .= sprintf('%s is required field!', $string) . PHP_EOL;
                    continue;
                }
                if ($data[$key] === '') {
                    $exception .= sprintf("%s can't be empty!", $string) . PHP_EOL;
                }
            }
        }
        if ($exception !== '') {
            throw new \Exception($exception);
        }
       return true;
    }
}