<?php

namespace PluginContainer\Core\Logger;

class Logger extends \Monolog\Logger
{
    public function debug($message, array $context = []): void
    {
        if (DEBUG_LOG === true) {
            parent::debug($message, $context);
        }
    }
}