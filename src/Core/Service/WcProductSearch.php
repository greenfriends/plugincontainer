<?php

namespace PluginContainer\Core\Service;

class WcProductSearch
{
    private $driver;
    public function __construct(\PDO $driver)
    {
        $this->driver = $driver;
    }

    public function productSearch(string $searchValue, int $limit, int $offset)
    {
        $sql = "SELECT * FROM wp_posts WHERE (ID LIKE '%$searchValue%' OR 
                ID IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_sku' AND meta_value LIKE '%$searchValue%' 
                OR post_title LIKE '%$searchValue%' AND post_status = 'publish' AND post_type = 'product')) 
                LIMIT $limit OFFSET $offset;";
        $stmt = $this->driver->query($sql);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}