<?php

namespace PluginContainer\Core\AjaxRouter;

class AjaxResponse
{
    /**
     * @var array
     */
    private $responseBody = [];
    /**
     * @var string
     */
    private $responseMessage;

    /**
     * @var bool
     */
    private $responseSuccess;

    /**
     * @param bool $responseSuccess
     * @param string $responseMessage
     */
    public function __construct(bool $responseSuccess, string $responseMessage = '')
    {
        $this->responseMessage = $responseMessage;
        $this->responseSuccess = $responseSuccess;
    }

    /**
     * @param string $paramName
     * @param $paramValue
     * @return AjaxResponse
     */
    public function addParamToBody(string $paramName, $paramValue): AjaxResponse
    {
        $this->responseBody[$paramName] = $paramValue;
        return $this;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->responseBody;
    }

    /**
     * @return string
     */
    public function getResponseMessage(): string
    {
        return $this->responseMessage;
    }

    /**
     * @return string
     */
    public function isSuccessful(): string
    {
        return $this->responseSuccess;
    }

}