<?php

namespace PluginContainer\Core\AjaxRouter;
//@todo remove translated strings from exception messages and move it here only when that exception is shown to user
use Laminas\Config\Config;
use PluginContainer\Core\Dashboard\Controller\Feature;
use PluginContainer\Core\Logger\Logger;

class AjaxRouter
{
    /**
     * @var Config
     */
    private $config;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param Config $config
     * @param Logger $logger
     */
    public function __construct(Config $config, Logger $logger)
    {
        add_action('wp_ajax_gfAjaxRouter', [$this, 'handleRouteAdmin']);
        add_action('wp_ajax_gfAjaxRouterFront', [$this, 'handleRouteFront']);
        add_action('wp_ajax_nopriv_gfAjaxRouterFront', [$this, 'handleRouteFront']);
        $this->config = $config;
        $this->logger = $logger;
    }

    public function handleRoute(): void
    {
        if (!isset($_GET['handlerClass'], $_GET['method'])){
            $this->logger->error('Handler class or method not set');
            wp_send_json([
                'success' => false,
                'message' => 'Handler class or method not set'
            ]);
        }
        global $gfContainer;
        $featureList = $this->config->get('featureList')->toArray();
        //We add base feature handler to array manual outside of config because it is not part of the feature list
        $featureList['feature']['handlerClass'] = Feature::class;
        $handler = $_GET['handlerClass'];
        $method = $_GET['method'];
        $this->logger->debug('Searching for handler', ['handler' => $handler , 'haystack' => $featureList]);
        foreach ($featureList as $featureData) {
            if (strpos(stripslashes($featureData['handlerClass']), $handler) !== false)  {
                $this->logger->debug('Handler found');
                try {
                    $this->logger->debug('Trying to get handler from container');
                    $handlerClass = $gfContainer->get($featureData['handlerClass']);
                    $this->logger->debug('Handler successfully constructed');
                    $this->logger->debug('Trying to call handler method', ['method' => $method]);
                    /** @var AjaxResponse $response */
                    $response = $handlerClass->$method();
                    $this->logger->debug('Method successfully called', ['handler' => $handler, 'function' => $method]);
                    wp_send_json([
                        'success' => (bool)$response->isSuccessful(),
                        'message' => $response->getResponseMessage(),
                        'body' => $response->getBody()
                    ]);
                } catch (\Exception $e) {
                    $this->logger->error('Error in ajax', ['error' => $e->getMessage() , 'trace' => $e->getTrace()]);
                    wp_send_json([
                        'success' => false,
                        'message' => $e->getMessage(),
                        'body' => []
                    ]);
                }
            }
        }
        wp_send_json([
            'success' => false,
            'message' => "Handler $handler not found"
        ]);
        $this->logger->error('Handler not found', ['handler' => $handler]);
    }
    public function handleRouteAdmin(): void
    {
        if (!is_admin()) {
            exit();
        }
        $this->handleRoute();
    }
    public function handleRouteFront(): void
    {
        $this->handleRoute();
    }
}