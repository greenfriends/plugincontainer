<?php

namespace PluginContainer\Core\Repository;

use Exception;
use PluginContainer\Core\Factory\BaseFactory;
use PluginContainer\Core\Mapper\BaseMapper;

class BaseRepository
{
    /**
     * @var BaseMapper
     */
    protected $mapper;
    /**
     * @var string
     */
    protected $modelClassName;

    /**
     * @param string $modelClassName
     */
    public function __construct(string $modelClassName)
    {
        $this->modelClassName = $modelClassName;
    }

    /**
     * @param array $modelData
     * @return int
     * @throws Exception
     */
    public function create(array $modelData): int
    {
        return $this->mapper->create($this->make($modelData));
    }

    /**
     * @param array $modelData
     * @throws Exception
     */
    public function update(array $modelData): void
    {
        $this->mapper->update($this->make($modelData));
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function delete(int $id): void
    {
        $this->mapper->delete($id);
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @param bool $models
     * @return array|null
     * @throws \ReflectionException
     * @throws Exception
     */
    public function getAll(int $offset = null, int $limit = null, bool $models = true, $order = null): ?array
    {
        $result = $this->mapper->getALl($offset, $limit, $order);
        $data = [];
        if ($models) {
            foreach ($result as $modelData) {
                $data[] = $this->make($modelData);
            }
            return $data;
        }
        return $result;
    }

    /**
     * @param array $filters
     * @param bool $models
     * @param int|null $offset
     * @param int|null $limit
     * @return array|null
     * @throws \ReflectionException
     * @throws Exception
     */
    public function getAllWithFilters(array $filters, bool $models = true, int $offset = null, int $limit = null): ?array
    {
        $result = $this->mapper->getWithFilters($filters, $offset, $limit);
        $data = [];
        if ($models) {
            foreach ($result as $modelData) {
                $data[] = $this->make($modelData);
            }
            return $data;
        }
        return $result;
    }

    /**
     * @param int $id
     * @param bool $model
     * @return mixed|null
     * @throws Exception
     */
    public function getById(int $id, bool $model = true)
    {
        $modelData = $this->mapper->getById($id);
        if ($modelData !== null) {
            if ($model) {
                return $this->make($modelData);
            }
            return $modelData;
        }
        return null;
    }

    /**
     * @param string $fieldName
     * @param string $value
     * @param int|null $offset
     * @param int|null $limit
     * @return array
     * @throws \ReflectionException
     * @throws Exception
     */
    public function searchByField(string $fieldName, string $value, ?int $offset, ?int $limit): array
    {
        $result = $this->mapper->searchByField($fieldName, $value, $offset, $limit);
        $data = [];
        if (count($result) > 0) {
            foreach ($result as $modelData) {
                $data[] = $this->make($modelData);
            }
        }
        return $data;
    }

    /**
     * @param array $modelData
     * @return mixed
     * @throws \ReflectionException
     */
    protected function make(array $modelData)
    {
        $factory = new BaseFactory($this->modelClassName);
        return $factory->createModel($modelData);
    }

    /**
     * @param string $columnName
     * @return array|false
     * @throws Exception
     */
    public function getPossibleFiltersForColumn(string $columnName)
    {
        return $this->mapper->getPossibleFiltersForColumn($columnName);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getCount()
    {
        return $this->mapper->getCount();
    }

    /**
     * @throws Exception
     */
    public function deleteTable(): void
    {
        $this->mapper->deleteTable();
    }

    /**
     * @param string $indexName
     * @param string $columnName
     * @throws \Exception
     */
    public function createIndex(string $indexName, string $columnName): void
    {
        $this->mapper->createIndex($indexName, $columnName);
    }

    /**
     * @param string $field
     * @param string $value
     * @param bool $models
     * @return array
     * @throws \ReflectionException
     * @throws Exception
     */
    public function getBy(string $field, string $value, bool $models = true): ?array
    {
        $result = $this->mapper->getByField($field, $value);
        $data = [];
        if ($models) {
            foreach ($result as $modelData) {
                $data[] = $this->make($modelData);
            }
            return $data;
        }
        return $result;
    }
}