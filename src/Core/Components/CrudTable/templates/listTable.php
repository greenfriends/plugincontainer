<?php

/**
 * @var string $filters
 * @see \PluginContainer\Core\Components\CrudTable\CrudTable::generateFiltersHtml()
 *
 * @var string $columnHeaders
 * @see \PluginContainer\Core\Components\CrudTable\CrudTable::generateColumnHeadersForView()
 */
echo $filters;
?>
<div id="controlButtonsWrapper">
    <button id="createRow">Create</button>
    <div id="controlButtonsHidden" class="hidden">
        <button id="editRow">Edit</button>
        <button id="deleteRow">Delete</button>
    </div>
</div>
<table id="crudTable">
    <thead>
    <tr>
        <td>
            <select aria-label="Per page" id="perPageSelector" name="perPage">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </td>
        <td>
            <div class="tablePagination"></div>
        </td>
        <td>
           <input aria-label="search" id="search" type="text" name="search"
                  placeholder="<?=__('Search', 'plugin-container')?>">
        </td>
    </tr>
    <tr><?=$columnHeaders?></tr>
    </thead>
    <tbody id="tableData"></tbody>
    <tfoot>
    <tr>
        <td colspan="100%">
            <div class="tablePagination"></div>
        </td>
    </tr>
    </tfoot>
</table>
<div id="modalOverlay" class="hidden"></div>
<div id="crudModal" class="hidden">

</div>