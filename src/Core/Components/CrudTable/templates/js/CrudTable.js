export default class CrudTable {
    crudButtonsHaveListeners = false;

    constructor(props)
    {
        this.crudTable = props.crudTable;
        this.crudTableBody = props.crudTableBody;
        this.handlerClass =
            document.getElementsByClassName('tab active')[0].getAttribute('data-handler');
        this.action = 'gfAjaxRouter';
        this.createButton = props.crudControlButtonWrapper.querySelector('#createRow');
        this.editButton = props.crudControlButtonWrapper.querySelector('#editRow');
        this.deleteButton = props.crudControlButtonWrapper.querySelector('#deleteRow');
        this.hiddenControlContainer = props.crudControlButtonWrapper.querySelector('#controlButtonsHidden');
        this.isControlOpen = false;
        this.formContainer = props.formContainer;
        if(props.modalWidth && props.modalHeight) {
            this.formContainer.style.width = props.modalWidth;
            this.formContainer.style.height = props.modalHeight;
        }
        this.selectPerPageListener();
        this.setPerPageValue();
    }

    getData(page)
    {
        this.perPage = localStorage.getItem('limitSelect') ?? document.getElementById('perPageSelector').value;
        let offset = 0;
        if (page !== 1) {
            offset = (page - 1) * this.perPage;
        }
        return this.fetchResponse({
            action:'gfAjaxRouter',
            handlerClass: this.handlerClass,
            method: 'getTableData',
            limit: this.perPage,
            offset: offset
        },this.crudTableBody)
    }

    async populateTable(page)
    {
        let response = JSON.parse(await this.getData(page));
        this.totalCount = response.body.totalCount;
        let html = '';
        if (response.success === true) {
            let activeTab = document.getElementsByClassName('tab active')[0];
            let activeTabName = activeTab.getAttribute('data-name').toLowerCase();
            let data = response.body.data;
            let tableHeaderNames = this.getTableHeaderNames();
            for (let i = 0; i < data.length; i++) {
                html += `<tr data-id="${data[i].id}">`;
                tableHeaderNames.forEach((name) => {
                    let dataVal = data[i][name];
                    //@todo think about this :D, shortcode needs to be printed here but its hardcoded now
                    if(name === 'shortcode') {
                        dataVal = `[gf_${activeTabName} id="${dataVal}"]`;
                    }
                    html += `<td> ${dataVal}</td>`
                });
                html += '</tr>';
            }
            this.crudTableBody.innerHTML = html;
            return new Promise((resolve) => {
                resolve(response);
            });
        } else {
            this.crudTableBody.innerHTML = `<p class="tableMessage">${response.message}</p>`;
        }

    }

    getTableHeaderNames()
    {
        let tableHeaders = document.querySelectorAll('.tableHeader');
        let data = [];
        tableHeaders.forEach((elem) => {
            data.push(elem.getAttribute('data-columnname'));
        })
        return data;
    }

    async fetchResponse(params, loaderContainer)
    {
        let urlParams = new URLSearchParams(params)
        this.startLoader(loaderContainer);
        const req = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}`);
        return await req.text();
    }

    startLoader(loaderContainer)
    {
        if (!document.getElementById('loader')) {
            loaderContainer.innerHTML = '<div id="loader"></div>'
        }
    }

    async generatePagination()
    {
        let maxNumberOfPages = Math.ceil(this.totalCount / this.perPage);
        let html = '';
        if (maxNumberOfPages !== 1) {
            html += `<button data-page="1" class="paginationButton">First</button>`;
        }
        html += '<div class="paginationPagesWrapper">';
        html += '<div class="buttonContainer">'
        if (maxNumberOfPages !== 1) {
            html += `<button class="prevButton"><</button>`;
        }
        html += `<button data-page="1" class="activePagination">1</button>`;
        if (maxNumberOfPages !== 1) {
            html += `<button class="nextButton">></button>`;
        }
        html += '</div>';
        html += '</div>';
        if (maxNumberOfPages !== 1) {
            html += `<button data-page="${maxNumberOfPages}" class="paginationButton">${maxNumberOfPages}</button>`;
        }
        this.crudTable.querySelectorAll('.tablePagination').forEach((pagination) => {
            pagination.innerHTML = html;
        })
    }


    addPaginationEventListener()
    {
        let previousButtons = document.querySelectorAll('.prevButton');
        let nextButtons = document.querySelectorAll('.nextButton');
        let currentPage = 1;
        if (previousButtons.length > 0) {
            let newCurrentPage = 1;
            previousButtons.forEach((button) => {
                button.addEventListener('click', () => {
                    let activePageElem = document.querySelectorAll('.activePagination');
                    activePageElem.forEach((activeElem) => {
                        currentPage = parseInt(activeElem.getAttribute('data-page'));
                        if (currentPage === 1) {
                            return;
                        }
                        newCurrentPage = `${currentPage - 1}`;
                        activeElem.setAttribute('data-page', newCurrentPage);
                        activeElem.innerText = newCurrentPage;
                    })
                    if (currentPage !== 1) {
                        this.populateTable(newCurrentPage).then(() => {
                            this.addRowListeners();
                        });
                    }
                })
            })
        }

        if (nextButtons.length > 0) {
            let newCurrentPage = 1;
            let currentPage = 1;
            let maxPages = Math.ceil(this.totalCount / this.perPage);
            nextButtons.forEach((button) => {
                button.addEventListener('click', () => {
                    let activePageElem = document.querySelectorAll('.activePagination');
                    activePageElem.forEach((activeElem) => {
                        currentPage = parseInt(activeElem.getAttribute('data-page'));
                        if (currentPage >= maxPages) {
                            return;
                        }
                        newCurrentPage = `${currentPage + 1}`;
                        activeElem.setAttribute('data-page', newCurrentPage);
                        activeElem.innerText = newCurrentPage;
                    });
                    if (currentPage < maxPages) {
                        this.populateTable(newCurrentPage).then(() => {
                            this.addRowListeners();
                        });
                    }
                })
            })
        }
        if (document.querySelectorAll('.paginationButton').length > 0) {
            document.querySelectorAll('.paginationButton').forEach((elem) => {
                let newCurrentPage = 1;
                elem.addEventListener('click', () => {
                    let activePageElem = document.querySelectorAll('.activePagination');
                    newCurrentPage = elem.getAttribute('data-page');
                    activePageElem.forEach((activeElem) => {
                        activeElem.setAttribute('data-page', newCurrentPage);
                        activeElem.innerText = newCurrentPage;
                    });
                    this.populateTable(newCurrentPage).then(() => {
                        this.addRowListeners();
                    });
                })
            })
        }
    }

    selectPerPageListener()
    {
        let self = this;
        document.getElementById('perPageSelector').addEventListener('change', function () {
            localStorage.setItem('limitSelect', this.value);
            self.setupTable();
        })
    }

    addRowListeners()
    {
        let tableRows = document.querySelectorAll('#tableData tr');
        tableRows.forEach((row) => {
            row.addEventListener('mousedown', () => {
                let rowId = row.getAttribute('data-id');
                let activeRow = document.querySelector('.activeRow');
                if(activeRow) {
                    activeRow.classList.remove('activeRow');
                }
                row.classList.add('activeRow');
                if(this.hiddenControlContainer.classList.contains('hidden')) {
                    this.hiddenControlContainer.classList.remove('hidden');
                }
                this.editButton.setAttribute('data-id', rowId);
                this.deleteButton.setAttribute('data-id', rowId);
            });
        });
    }

    crudButtonListeners()
    {
        if(!this.crudButtonsHaveListeners) {
            this.createButton.addEventListener('click', async () => {
                document.getElementById('modalOverlay').classList.remove('hidden');
                document.body.classList.add('preventScroll');
                this.formContainer.classList.remove('hidden');
                await this.fetchResponse({
                    'action': this.action,
                    'handlerClass': this.handlerClass,
                    'method': 'getFormFields'
                }, this.formContainer).then((response) => {
                    response = JSON.parse(response);
                    this.printFormHtml(response.body);
                });
            })

            this.deleteButton.addEventListener('click', async () => {
                await this.fetchResponse({
                    'action': this.action,
                    'handlerClass': this.handlerClass,
                    'method': 'delete',
                    'id': this.deleteButton.getAttribute('data-id')
                }, this.crudTableBody).then((response) => {
                    this.printNotice(JSON.parse(response));
                    this.setupTable();
                })
            })

            this.editButton.addEventListener('click', async () => {
                document.getElementById('modalOverlay').classList.remove('hidden');
                document.body.classList.add('preventScroll');
                this.formContainer.classList.remove('hidden');
                let id = this.editButton.getAttribute('data-id');
                await this.fetchResponse({
                    'action': this.action,
                    'handlerClass': this.handlerClass,
                    'method': 'getFormFields',
                    'id': id
                }, this.formContainer).then((response) => {
                    response = JSON.parse(response);
                    this.printFormHtml(response.body, id);
                });
            });
            this.crudButtonsHaveListeners = true;
        }
    }

    printFormHtml(response, id = null)
    {
        let html = `<h2>${response.formHeader}</h2>`;
        html += `<form data-id="${id}" data-method="${response.method}" id="modalForm">`
        response.data.forEach((formField) => {
            html += this.parseFormField(formField);
        })
        html += `<button type="submit" class="greenButton" id="submitForm">${response.submitLabel}</button>`;
        html += `</form>`;
        this.formContainer.innerHTML = html;
        let thumbnailInput = document.querySelector('[name="imageThumbnail"]');
        let thumbnailMobileInput = document.querySelector('[name="imageMobileThumbnail"]');
        if(thumbnailInput && thumbnailInput.value !== '') {
            let image = document.createElement('img');
            let imagePreview = document.querySelector('.imagePreview');
            image.src = thumbnailInput.value;
            if(imagePreview) {
                imagePreview.appendChild(image);
            }
        }
        if(thumbnailMobileInput && thumbnailMobileInput.value !== '') {
            let imageMobile = document.createElement('img');
            let imagePreviewMobile = document.querySelector('.imagePreviewMobile');
            imageMobile.src = thumbnailMobileInput.value;
            if(imagePreviewMobile) {
                imagePreviewMobile.appendChild(imageMobile);
            }
        }

        let imagesInput = document.querySelector('[name="images"]');
        if(imagesInput) {
            let imageInstances = [];
            let imageInstancesString = imagesInput.value.split(';');
            imageInstancesString.forEach(imageInstanceString => {
               let params =  imageInstanceString.split(',');
               if(params[0] && params[1]) {
                   imageInstances.push({src: params[0], url: params[1]});
               }
            });
            let imagesPreview = document.querySelector('.imagesPreview');
            if(imagesPreview) {
                imageInstances.forEach((imageInstance) => {
                    let imageInstanceFromData = this.generateImageInstance(imageInstance);
                    imagesPreview.appendChild(imageInstanceFromData.container);
                    this.addImageInstanceEventListener(imageInstanceFromData);
                });
            }
        }
        document.dispatchEvent(new CustomEvent('modalFormOpened', {detail: this.formContainer}))
        this.addModalCloseListener();
        this.addSubmitButtonListener();
        this.addImageUploadButtonsListener();
        this.multipleImageUploadListener();
    }


    addImageUploadButtonsListener()
    {
        let imageUploadButtons = document.querySelectorAll('.imageUpload');
        let imageUploadButtonSecondary = document.querySelectorAll('.imageUploadSecondary');

        if (imageUploadButtons.length > 0) {
            jQuery('.imageUpload').click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var $button = jQuery(this);
                // Create the media frame.
                var file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select or upload image',
                    library: { // remove these to show all
                        type: 'image' // specific mime
                    },
                    button: {
                        text: 'Select'
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                });
                // When an image is selected, run a callback.
                file_frame.on('select', function () {
                    // We set multiple to false so only get one image from the uploader
                    var attachment = file_frame.state().get('selection').first().toJSON();
                    console.log(attachment.sizes);
                    jQuery('[name="imageThumbnail"]').val(attachment.sizes.thumbnail.url).change();
                    jQuery('[name="imageMedium"]').val(attachment.sizes.large.url).change();
                    jQuery('[name="imageFull"]').val(attachment.sizes.full.url).change();
                    jQuery('.imagePreview').empty();
                    jQuery('.imagePreview').append(`<img src="${attachment.sizes.thumbnail.url}">`);
                    let data = {
                        src: attachment.sizes.thumbnail.url,
                        elem: $button[0]
                    };
                    document.dispatchEvent(new CustomEvent('imageSelected', {'detail': data}));
                });

                // Finally, open the modal
                file_frame.open();
            });
        }
        if (imageUploadButtonSecondary.length > 0) {
            jQuery('.imageUploadSecondary').click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var $button = jQuery(this);
                // Create the media frame.
                var file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select or upload image',
                    library: { // remove these to show all
                        type: 'image' // specific mime
                    },
                    button: {
                        text: 'Select'
                    },
                    multiple: false  // Set to true to allow multiple files to be selected
                });
                // When an image is selected, run a callback.
                file_frame.on('select', function () {
                    // We set multiple to false so only get one image from the uploader
                    var attachment = file_frame.state().get('selection').first().toJSON();
                    console.log(attachment.sizes);
                    jQuery('[name="imageMobileThumbnail"]').val(attachment.sizes.thumbnail.url).change();
                    jQuery('[name="imageMobile"]').val(attachment.sizes.large.url).change();
                    jQuery('.imagePreviewMobile').empty();
                    jQuery('.imagePreviewMobile').append(`<img src="${attachment.sizes.thumbnail.url}">`);
                    let data = {
                        src: attachment.sizes.thumbnail.url,
                        elem: $button[0]
                    };
                    document.dispatchEvent(new CustomEvent('imageSelected', {'detail': data}));
                });

                // Finally, open the modal
                file_frame.open();
            });
        }
    }
    addSubmitButtonListener()
    {
        document.getElementById('modalForm').addEventListener('submit',(e) => {
            e.preventDefault();
            if(document.querySelectorAll('.imageInstanceContainer').length > 0) {
                this.setFinalImagesValue();
            }
            let formData = new FormData(e.target);
            formData.append('action', this.action);
            formData.append('method', e.target.getAttribute('data-method'));
            formData.append('handlerClass', this.handlerClass);
            if (e.target.getAttribute('data-id') !== null) {
                formData.append('id', e.target.getAttribute('data-id'));
            }
            let params = [...formData.entries()];
            this.fetchResponse(params, document.getElementById('crudModal')).then((response) => {
                this.closeModal();
                response = JSON.parse(response);
                this.printNotice(response);
                this.setupTable();
            })
        })
    }

    printNotice(noticeResponse) {
        let className = 'error';
        if(noticeResponse.success === true) {
            className = 'success';
        }
        document.getElementById('noticeContainer').innerHTML =
            `<span id="notice" class="${className}">${noticeResponse.message}</span>`;
    }

    removeNotice() {
        let notice = document.getElementById('notice');
        if(notice) {
            notice.remove();
        }
    }

    parseFormField(fieldData)
    {
        let fieldName = fieldData.name;
        let fieldLabel = fieldData.label;
        let fieldValue = fieldData.value ?? '';
        let fieldType = fieldData.type;
        let readonly = fieldData.readonly;
        switch(fieldType) {
            case 'text':
            case 'number':
            case 'email':
                return `<label>
                            ${fieldLabel}
                            <input ${readonly ? 'readonly' : ''} type="${fieldType}" name="${fieldName}" value="${fieldValue}">
                        </label>`;
            case 'select':
                let options = fieldData.options;
                let html = `<label>${fieldLabel}
                            <select name="${fieldName}">
                            <option value="-1">---</option>`;
                options.forEach(option => {
                    if (option.value == fieldValue) {
                        html += `<option selected value="${option.value}">${option.displayValue}</option>`;
                    } else {
                        html += `<option value="${option.value}">${fieldValue}${option.displayValue}</option>`;
                    }
                })
                html += `</select></label>`;
                return html;
            case 'checkbox':
                return `<label style="display:block;">
                            ${fieldLabel}
                            <input type="${fieldType}" name="${fieldName}" ${fieldValue ? 'checked' : ''}>
                        </label>`;
            case 'textarea':
                return `<label>
                        ${fieldLabel}
                <textarea cols="${fieldData.cols}" rows="${fieldData.rows}" name="${fieldName}">${fieldValue}</textarea>
                </label>`
            case 'hidden':
                return  `<input type="${fieldType}" name="${fieldName}" value="${fieldValue}">`
            case 'wpImageUpload':
                return `<div class="imageUploadWrapper">
                            <div class="imagePreview">
                                
                            </div>
                            <button class="imageUpload greenButton">Upload/select image</button>
                            <div class="imagePreviewMobile"></div>
                            <div class="imagePreviewSecondary">
                            
                            </div>
                            <button class="imageUploadSecondary greenButton">Upload/select secondary image</button>
                        </div>`
            case 'wpImageUploadMultiple':
                return `<div class="imagesUploadWrapper">
                    <div class="imagesPreview">
                     
                    </div>
                    <button style="margin-bottom:1rem;margin-top:1rem;" class="greenButton addImage">Add Image</button>
                </div>`;

        }
    }

    multipleImageUploadListener() {
        let addImage = document.querySelector('.addImage');
        if(addImage) {
            addImage.addEventListener('click', (e) => {
                e.preventDefault();
                e.stopPropagation();
                let imagesPreview = document.querySelector('.imagesPreview');
                if(imagesPreview) {
                    let imageInstance = this.generateImageInstance();
                    imagesPreview.appendChild(imageInstance.container);
                    this.addImageInstanceEventListener(imageInstance);
                }
            });
        }
    }

    generateImageInstance(instance = null) {
        let container = document.createElement('div');
        container.classList.add('imageInstanceContainer');
        container.style.display = 'flex';
        container.style.flexDirection = 'column';
        container.style.gap = '1rem';
        container.style.padding = '1rem 0.5rem';
        container.style.borderRadius = '10px';
        container.style.border = '1px solid green';

        let imageLinkInput = document.createElement('input');
        imageLinkInput.classList.add('imageLink');
        imageLinkInput.type = 'text';
        imageLinkInput.placeholder = 'Permalink';

        let imageButton = document.createElement('button');
        imageButton.classList.add('greenButton');
        imageButton.style.marginTop = '1rem';
        imageButton.innerText = 'Choose Image';

        let imagePreviewElement = document.createElement('img');
        imagePreviewElement.style.maxHeight = '150px';
        imagePreviewElement.style.maxWidth = '150px';

        let imageUrlInput = document.createElement('input');
        imageUrlInput.type = 'hidden';
        imageUrlInput.classList.add('imageUrl');

        let deleteImage = document.createElement('button');
        deleteImage.classList.add('greenButton');
        deleteImage.style.background = 'red';
        deleteImage.innerText = 'Delete Image';

        container.appendChild(imageButton);
        container.appendChild(imagePreviewElement);
        container.appendChild(imageLinkInput);
        container.appendChild(imageUrlInput);
        container.appendChild(deleteImage);
        if(instance) {
            imageLinkInput.value = instance.url;
            imageUrlInput.value = instance.src;
            imagePreviewElement.src = instance.src;
        }
        return {
            container: container,
            imageButton: imageButton,
            imageLinkInput: imageLinkInput,
            imageUrlInput: imageUrlInput,
            imagePreviewElement: imagePreviewElement,
            deleteImageElement: deleteImage
        };
    }

    addImageInstanceEventListener(imageInstance) {
        let chooseImage = imageInstance.imageButton;
        let container = imageInstance.container;
        let imageLinkInput = imageInstance.imageLinkInput;
        let imageUrlInput = imageInstance.imageUrlInput;
        let imagePreviewElement = imageInstance.imagePreviewElement;
        let deleteImageElement = imageInstance.deleteImageElement;

        chooseImage.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            // Create the media frame.
            var file_frame = wp.media.frames.file_frame = wp.media({
                title: 'Select or upload image',
                library: { // remove these to show all
                    type: 'image' // specific mime
                },
                button: {
                    text: 'Select'
                },
                multiple: false  // Set to true to allow multiple files to be selected
            });
            // When an image is selected, run a callback.
            file_frame.on('select', function () {
                // We set multiple to false so only get one image from the uploader
                var attachment = file_frame.state().get('selection').first().toJSON();
                imageUrlInput.value = attachment.sizes.full.url;
                imagePreviewElement.src = attachment.sizes.thumbnail.url;
            });
            // Finally, open the modal
            file_frame.open();
        });

        deleteImageElement.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            deleteImageElement.parentElement.remove();
        })
    }

    setFinalImagesValue() {
        let images = document.querySelectorAll('.imageInstanceContainer');
        let imagesInput = document.querySelector('[name="images"]');
        imagesInput.value = '';
        images.forEach((image,index) => {
            let url = image.querySelector('.imageUrl').value;
            let link = image.querySelector('.imageLink').value;
                imagesInput.value = imagesInput.value + url + ',' + link  + ';'
        });
    }

    setupTable() {
        this.populateTable().then(() => {
            this.generatePagination().then(() => {
                this.addPaginationEventListener();
                this.addRowListeners();
                this.crudButtonListeners();
            });
        })
    }

    setPerPageValue() {
        document.getElementById('perPageSelector').value = localStorage.getItem('limitSelect') ?? '10';
    }

    addModalCloseListener()
    {
        let modalOverlay = document.getElementById('modalOverlay');
        let crudModal = document.getElementById('crudModal');
        document.addEventListener('mousedown', (e) => {
            if(e.target.id === 'modalOverlay') {
                this.closeModal()
            }
        });
    }

    closeModal()
    {
        document.getElementById('modalOverlay').classList.add('hidden');
        this.formContainer.classList.add('hidden');
        document.body.classList.remove('preventScroll');
    }
}