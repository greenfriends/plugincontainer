<?php

namespace PluginContainer\Core\Components\CrudTable\Traits;

use PluginContainer\Core\AjaxRouter\AjaxResponse;

trait CrudTable
{
    /**
     * @route("wp-admin/admin-ajax.php?action=wp_ajax_gfAjaxRouter")
     * @throws \Exception
     * @see AjaxRouter::handleRoute()
     */
    public function getTableData(): AjaxResponse
    {
        $offset = null;
        $limit = 10;
        if (isset($_GET['search']) && $_GET['search'] !== '') {
            $search = $_GET['search'];
        }
        if (isset($_GET['offset'])) {
            $offset = (int)$_GET['offset'];
        }
        if (isset($_GET['limit'])) {
            $limit = (int)$_GET['limit'];
        }
        $result = $this->repo->getAll($offset, $limit, false);
        $totalCount = $this->repo->getCount();
        if (count($totalCount) > 0) {
            $totalCount = $totalCount[0];
        }
        if (count($result) > 0) {
            $response = new AjaxResponse(true);
            $response
                ->addParamToBody('data', $result)
                ->addParamToBody('totalCount', $totalCount)
                ->addParamToBody('resultsFound', count($result));
            return $response;
        }
        throw new \Exception('No results found');
    }
}