<?php

namespace PluginContainer\Core\Components\CrudTable;

class FormField
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $label;
    /**
     * @var string
     */
    public $tooltip;
    /**
     * @var null|string
     */
    public $value;

    public $readonly;

    /**
     * @param string $name
     * @param string $type
     * @param string $label
     * @param null|mixed $value
     * @param string $tooltip
     * @param bool $readonly
     */
    public function __construct(string $name, string $type, string $label, $value = null, string $tooltip = '', bool $readonly = false)
    {
        $this->name = $name;
        $this->type = $type;
        $this->label = $label;
        $this->tooltip = $tooltip;
        $this->value = $value;
        $this->readonly = $readonly;
    }
}