<?php
namespace PluginContainer\Core\Components\CrudTable;

use PluginContainer\Core\Repository\BaseRepository;
use PluginContainer\Core\WpBridge\Translations;

class CrudTable
{
    private $filters = [];

    private $viewDefinitions = [];
    private $createDefinitions = [];
    private $editDefinitions = [];
    private $repo;
    private $jsData;

    /**
     * @param Column[] $columnDefinition
     */
    public function __construct(array $columnDefinition, BaseRepository $repo)
    {
        foreach ($columnDefinition as $column) {
            $columnName = $column->getName();
            $columnDefinitions = $column->getColumnDefinitions();
            $this->viewDefinitions[$columnName] = $columnDefinitions['view'];
            $this->createDefinitions[$columnName] = $columnDefinitions['create'];
            $this->editDefinitions[$columnName] = $columnDefinitions['edit'];
            $this->jsData[$columnName] = $columnDefinitions['jsDataParams'];
        }
        $this->repo = $repo;
    }

    /**
     * @return false|string
     * @throws \Exception
     */
    public function getList()
    {
        $filters = $this->generateFiltersHtml();
        $columnHeaders = $this->generateColumnHeadersForView();
        ob_start();
        include CONTAINER_PLUGIN_DIR . '/src/Core/Components/CrudTable/templates/listTable.php';
        return ob_get_clean();
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function generateFiltersHtml(): string
    {
        $html = '<div id="filterContainer">';
        foreach ($this->viewDefinitions as $columnName => $columnDef) {
            if ($columnDef['filterable'] === true) {
                $filters = $this->repo->getPossibleFiltersForColumn($columnName);
                if (count($filters) > 0) {
                    $html .= '<div class="filterWrapper">';
                    $html .= sprintf('<label>%s</label>',
                        __('Filter by', 'plugin-container') . " $columnName");
                    $html .= '<select name="' . $columnName . 'Filter">';
                    $html .= '<option value="-1">---</option>';
                    foreach ($filters as $filter) {
                        $filter = $filter[0];
                        $filterValue = $filter;
                        if ($filter === null) {
                            $filterValue = '-1';
                            $filter = 'N/A';
                        }
                        $html .= '<option value="' . $filterValue . '">' . $filter . '</option>';
                    }
                    $html .= '</select>';
                    $html .= '</div>';
                }
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * @param $columnDefArray
     * @return array
     */
    private function sortColumnsByPriority($columnDefArray): array
    {
        $order = [];
        $sortedColumns = [];
        foreach ($columnDefArray as $columnName => $columnDef) {
            $priority = $columnDef['priority'];
            $order[$priority][$columnName] = $columnDef;
        }
        foreach ($order as $value) {
            $sortedColumns = array_merge($sortedColumns, $value);
        }
        return $sortedColumns;
    }

    /**
     * @return string
     */
    private function generateColumnHeadersForView(): string
    {
        $sortedColumns = $this->sortColumnsByPriority($this->viewDefinitions);
        $sortedJsParams = $this->sortColumnsByPriority($this->jsData);
        $html = '';
        foreach ($sortedJsParams as $columnName => $jsParamValue) {
            $html .= '<th class="tableHeader"';
            $viewName = $sortedColumns[$columnName]['name'];
            $html .= sprintf('data-%s="%s"', 'columnname', $columnName);
            foreach ($jsParamValue as $pName => $value) {
                if ($value === false) {
                    $value = 'false';
                }
                if ($value === true) {
                    $value = 'true';
                }
                $html .= sprintf('data-%s="%s"', $pName, $value);
            }


            $html .= '>';
            $html .= $viewName;
            $html .= '</th>';
        }
        return $html;
    }
}