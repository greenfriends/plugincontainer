<?php

namespace PluginContainer\Core\Cache;

use Memcached;
use Redis;

class CacheWrapper
{
    /**
     * @var Redis | Memcached | NULL
     */
    private $cache;

    private $prefix;


    /**
     * @param CacheConnection $cacheConnection
     * @throws \Exception
     */
    public function __construct(CacheConnection $cacheConnection)
    {
        $this->cache = $cacheConnection();
        if (!defined('ENVIRONMENT')){
            throw new \Exception('ENVIRONMENT is not defined');
        }
        if (!defined('PROJECT_PREFIX')){
            throw new \Exception('PROJECT_PREFIX is not defined');
        }
        $this->prefix = ENVIRONMENT . '_' . PROJECT_PREFIX . '_';
    }

    public function set($key, $value, $ttl = null): bool
    {
        $key = $this->prefix . $key;
        if ($this->cache instanceof Redis) {
            return $this->cache->set($key, $value, $ttl);
        }

        if ($this->cache instanceof Memcached) {
            return $this->cache->set($key, $value, $ttl);
        }
        return false;
    }

    public function get($key)
    {
        $key = $this->prefix . $key;
        if ($this->cache instanceof Redis) {
            return $this->cache->get($key);
        }

        if ($this->cache instanceof Memcached) {
            return $this->cache->get($key);
        }
        return false;
    }

    public function delete($key): bool
    {
        $key = $this->prefix . $key;
        if ($this->cache instanceof Redis) {
            return $this->cache->del($key);
        }

        if ($this->cache instanceof Memcached) {
            return $this->cache->delete($key);
        }
        return false;
    }

    /**
     * @param $prefix
     * @return bool
     */
    public function deleteKeyByPrefix($prefix): bool
    {
        if ($this->cache instanceof Redis) {
            $iterator = null;
            while(false !== ($keys = $this->cache->scan($iterator, $prefix . '*'))) {
                foreach($keys as $key) {
                    $this->cache->del($this->prefix.$key);
                }
            }
            return true;
        }
        if ($this->cache instanceof Memcached) {
            $len = strlen($prefix);
            $proc = popen('/usr/local/bin/memdump --servers=localhost', 'r');
            while (($key = fgets($proc)) !== false) {
                if (substr_compare($key, $prefix, 0, $len) === 0) {
                    $this->cache->delete(substr($this->prefix.$key, 0, -1));
                }
            }
            return true;
        }
        return false;
    }

    public function clear(): bool
    {
        if ($this->cache instanceof Redis) {
            return $this->cache->flushAll();
        }
        if ($this->cache instanceof Memcached) {
            return $this->cache->flush();
        }
        return false;
    }
}