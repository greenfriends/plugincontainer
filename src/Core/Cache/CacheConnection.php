<?php

namespace PluginContainer\Core\Cache;

use Memcached;
use Redis;

class CacheConnection
{
    /**
     * @var Redis | Memcached | Null
     */
    public $engine = null;

    public function __invoke()
    {
        if (class_exists('Redis')) {
            $engine = new Redis();
           if ($engine->connect(CACHE_HOST, REDIS_PORT)){
               $this->engine = $engine;
           }
        } elseif (class_exists('Memcached')) {
            $engine = new Memcached();
            if($engine->addServer(CACHE_HOST, MEMCACHED_PORT)){
                $this->engine = $engine;
            }
        }

        return $this->engine;
    }
}