export default class InputAjaxSearch
{
    entities = [];
    indexCounter = 0;
    /**
     * @class InputAjaxSearch
     * @param props
     * @property {HTMLElement} container - The container which holds all the elements
     * @property {HTMLElement} mainInput - The main input which contains the selected value
     */
    constructor(props) {
        this.container = document.getElementById(props.containerId);
        this.mainInput = document.getElementById(props.nameInputId);
        this.isLoading = false;
        this.offsetSearch = 0;
        this.entitiesPerFetch = 10;
        this.limit = 10;
        this.optionClickCallback = props.optionClickCallback;
        this.fetchUrl = props.fetchUrl;
        this.allowFetching = true;
        this.showOptionsBeforeInput = props.showOptionsBeforeInput ?? true;

        // Calls
        this.addEventListeners();
    }

    /**
     * Bundle all event listeners to be called in the class constructor
     */
    addEventListeners()
    {
        this.addMainInputListener();
        this.closeOptionsOnDocumentClickListener();
    }

    /**
     * Listen to a click event on the main input element and generate the list of suppliers with their listeners
     * If we are closing the option list return the offset to 0
     */
    addMainInputListener()
    {
        this.mainInput.addEventListener('click',() => {
            this.handleOptionContainer().then(() => {});
        });
    }

    addSearchInputListener(){
        let timeout;
        this.searchInput.addEventListener('input',() => {
            this.entityOptions.classList.remove('hidden');
            clearTimeout(timeout)
            timeout = setTimeout(() =>{
                this.allowFetching = true;
                this.offsetSearch = 0;
                this.entityOptions.innerHTML = '';
                let searchInputValue = this.searchInput.value;
                this.appendEntities(searchInputValue,this.offsetSearch, this.limit);
            },650)
        })
    }

    /**
     * Removes the option container if the user clicks elsewhere in the document
     */
    closeOptionsOnDocumentClickListener()
    {
        document.addEventListener('click',(event) => {
            if(event.target !== this.container && !this.container.contains(event.target)) {
                this.removeOptionContainer();
            }
        });
    }

    /**
     *  Append new suppliers when the user passes a certain threshold when scrolling
     */
    addLazyLoadOnScrollListener()
    {
        if(this.entityOptions) {
            this.entityOptions.addEventListener('scroll', () => {
                if(this.allowFetching === true) {
                    let offset = this.entityOptions.offsetHeight + this.entityOptions.scrollTop;
                    let chunk = this.entityOptions.scrollHeight;
                    if (this.isLoading === false) {
                        if (offset >= chunk - 40) {
                            this.offsetSearch += this.entitiesPerFetch;
                            this.appendEntities(this.searchInput.value, this.offsetSearch);
                        }
                    }
                }
            })
        }
    }
    /**
     * Appends / Removes a Node to the main container which will be used for the options.
     * Handles the classes and visuals depending on whether the options are open or closed
     */
    async handleOptionContainer() {
        if(!this.optionContainerExists()) {
            /** append the option container */
            this.container.appendChild(this.generateOptionContainer());
            /** get the container when append is finished */
            this.entityOptions = document.getElementById('entityOptions');
            /** add scroll listener after getting the container */
            this.addLazyLoadOnScrollListener();
            /** get the search input and add it's listener */
            this.searchInput = document.getElementById('entitySearchInput');
            this.addSearchInputListener();
            /** set the option container as the class property because it is ready in the dom now */
            this.optionContainer = document.getElementById('entityOptionsContainer');
            this.mainInput.classList.add('focused')
            return true;
        } else {
            this.removeOptionContainer();
            return false;
        }

    }

    /**
     * Generates a HTML structure for entity options
     * @returns {HTMLDivElement}
     */
    generateOptionContainer()
    {
        const entityOptionsContainer = document.createElement('div');
        entityOptionsContainer.id = 'entityOptionsContainer';

        const entitySearchInput = document.createElement('input');
        entitySearchInput.type = 'text';
        entitySearchInput.id = 'entitySearchInput'
        const entityOptions = document.createElement('div');
        this.entityOptions = entityOptions;

        entityOptions.id = 'entityOptions';
        if(this.showOptionsBeforeInput === false) {
            entityOptions.classList.add('hidden');
        }
        entityOptionsContainer.appendChild(entitySearchInput);
        entityOptionsContainer.appendChild(entityOptions);
        return entityOptionsContainer;
    }

    /**
     * Check if the option container exists in the DOM
     * @returns {boolean}
     */
    optionContainerExists()
    {
        return !!document.getElementById('entityOptions');
    }

    /**
     * Fetch the entities and return a promise
     * @returns {Promise<*>}
     */
    async fetchEntities(searchInput, offset)
    {
        this.isLoading = true;
        let data = await fetch(`${this.fetchUrl}&search=${searchInput}&offset=${offset}`);
        let entities = await data.text();
        return await JSON.parse(entities);
    }

    /**
     * Appends entity options based on the fetched entities to the entityOptions element,
     * and calls the method to add their listeners
     * @param searchInput The string that the user typed in
     * @param offset The offset - number of times we already fetched
     */
    appendEntities(searchInput, offset)
    {
        if (searchInput.length < 2 && searchInput !== '') {
            return;
        }
        let loader = document.createElement('div');
        loader.id = 'loader';
        this.optionContainer.appendChild(loader);
        this.fetchEntities(searchInput, offset).then((response) => {
            if(document.getElementById('loader')) {
                document.getElementById('loader').remove();
            }
            let entities = response.body.data;
            if(entities.length > 0) {
                entities.forEach((entity) => {
                    this.entities.push(entity)
                });
                entities.forEach((entity) => {
                    let entityOption = document.createElement('div');
                    entityOption.classList.add('entityOption');
                    entityOption.setAttribute('data-index', this.indexCounter.toString());
                    entityOption.addEventListener('click',() => {
                        this.optionClickCallback(this.entities[entityOption.getAttribute('data-index')]);
                        this.removeOptionContainer();
                    })
                    entityOption.innerText = entity.name;
                    this.entityOptions.appendChild(entityOption);
                    this.indexCounter++;
                })
            } else {
                this.allowFetching = false;
                if(!document.getElementById('noMoreMessage')) {
                    this.entityOptions.innerHTML += `<span id="noMoreMessage">${response.message}</span>`;
                    this.entityOptions.scrollTop = this.entityOptions.scrollHeight;
                }
            }

        }).then(() => {
            this.isLoading = false;
        })
    }


    /**
     * Remove the option container from the main container
     */
    removeOptionContainer()
    {
        if(this.optionContainer) {
            this.optionContainer.remove();
            this.mainInput.classList.remove('focused')
            delete this.optionContainer;
            delete this.entityOptions;
            delete this.searchInput;
            this.offsetSearch = 0;
        }
    }

}