export default class BaseFetcher {
    action = 'gfAjaxRouterFront';
    handlerClass;

    async fetchResponse(params) {
        let urlParams = new URLSearchParams(params)
        const req = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}&action=${this.action}&handlerClass=${this.handlerClass}`);
        let reqText = await req.text();
        return JSON.parse(reqText);
    }

    startLoader(container) {
        if (!document.getElementById('loader')) {
            if (container !== null) {
                container.innerHTML += '<div id="loader"></div>';
                return true;
            }
            document.getElementById('content').innerHTML += '<div id="loader"></div>';
        }
    }

    removeLoader() {
        let loader = document.getElementById('loader');
        if (loader) {
            loader.remove();
        }
    }

    printNotice(noticeResponse) {
        let className = 'error';
        if (noticeResponse.success === true) {
            className = 'success';
        }
        document.getElementById('noticeContainer').innerHTML =
            `<span id="notice" class="${className}">${noticeResponse.message}</span>`;
    }

    removeNotice() {
        let notice = document.getElementById('notice');
        if (notice) {
            notice.remove();
        }
    }

    async fetchResponsePost(params, method) {
        const req = await fetch(`/wp-admin/admin-ajax.php?method=${method}&action=${this.action}&handlerClass=${this.handlerClass}`, {
            method: 'POST',
            body: new URLSearchParams(params)
        });
        let reqText = await req.text();
        return await JSON.parse(reqText);
    }
}