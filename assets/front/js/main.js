import Wishlist from "../../../src/Packages/Wishlist/assets/js/Wishlist.js";
import {productSlider} from "../../../src/Packages/ProductSlider/assets/js/Slider.js";
import MegaMenu from "../../../src/Packages/MegaMenu/assets/js/front/MegaMenu.js";
import Compare from "../../../src/Packages/Compare/assets/js/Compare.js";
import ProductFilters from "../../../src/Packages/WcUtilityPack/js/ProductFilters.js";
import PurgeCssAndScripts from "../../../src/Packages/AssetsCompiler/js/PurgeCssAndScripts.js";
import BannerSlider from "../../../src/Packages/BannerSlider/assets/js/BannerSlider.js";

document.addEventListener('DOMContentLoaded', () => {
    let megaMenu = new MegaMenu();
    if (megaMenu.checkIfElementsAreVisible()) {
        megaMenu.init();
    }
    productSlider();
    let wishlist = new Wishlist();
    if (wishlist.checkIfElementsAreVisible()) {
        wishlist.init();
    }

    let compare = new Compare();
    if (compare.checkIfElementsAreVisible()) {
        compare.init();
    }

    let productFilters = new ProductFilters();
    if (productFilters.checkIfElementsAreVisible()) {
        productFilters.init();
    }

    let purgeCssAndScripts = new PurgeCssAndScripts();
    if (purgeCssAndScripts.checkIfElementsAreVisible()) {
        purgeCssAndScripts.init();
    }


    let sliders = document.querySelectorAll('.bannerSliderContainer');
    if(sliders && sliders.length > 0) {
        sliders.forEach((slider) => {
            let bannerSlider = new BannerSlider(slider);
            bannerSlider.initProps(slider);
            bannerSlider.init();
        });
    }
})