export default class Tab {
    constructor() {
        this.activeTab = document.querySelector('.tab.active');
        if (this.activeTab) {
            this.handlerClass = this.activeTab.getAttribute('data-handler');
        } else {
            console.info('Active tab not found');
        }
        this.action = 'gfAjaxRouter';
    }

    fetchResponse(params, loaderContainer = null) {
        return new Promise(async (resolve) => {
            let urlParams = new URLSearchParams(params)
            this.startLoader(loaderContainer);
            const req = await fetch(`/wp-admin/admin-ajax.php?${urlParams.toString()}&action=${this.action}&handlerClass=${this.handlerClass}`);
            let reqText = await req.text();
            this.removeLoader();
            resolve(JSON.parse(reqText));
        });
    }

    fetchResponsePost(params,method,loaderContainer = null) {
        return new Promise(async (resolve) => {
            this.startLoader(loaderContainer);
            const req = await fetch(`/wp-admin/admin-ajax.php?method=${method}&action=${this.action}&handlerClass=${this.handlerClass}`,{
                method: 'POST',
                body: new URLSearchParams(params)
            });
            let reqText = await req.text();
            this.removeLoader();
            resolve(JSON.parse(reqText));
        });
    }

    startLoader(container = null) {
        if (!document.getElementById('loader')) {
            if (container !== null) {
                container.innerHTML += '<div id="loader"></div>';
                return true;
            }
            document.getElementById('content').insertAdjacentHTML('beforeend','<div id="loader"></div>')
        }
    }

    removeLoader() {
        let loader = document.getElementById('loader');
        if (loader) {
            loader.remove();
        }
    }

    printNotice(noticeResponse) {
        let className = 'error';
        if (noticeResponse.success === true) {
            className = 'success';
        }
        document.getElementById('noticeContainer').innerHTML =
            `<span id="notice" class="${className}">${noticeResponse.message}</span>`;
    }

    removeNotice() {
        let notice = document.getElementById('notice');
        if (notice) {
            notice.remove();
        }
    }

    async printFormModal(height, width, method, id = null, measurement = 'px') {
        let modal = await this.openModal(height, width, measurement)
        let params = {method: method}
        if (id) {
            params.id = id;
        }
        let form = await this.fetchResponse(params, modal);
        modal.innerHTML = await form.body.data;
        this.appendCloseModalButton(modal);
        this.addFormSubmitListener(modal);
        document.dispatchEvent(new CustomEvent('modalFormOpened'));
    }

    /**
     * Prints a modal with the content provided inside it
     * @param height {String} Height of the modal
     * @param width {String} Width of the modal
     * @param content{ Node|String} Content with which to fill the modal with
     * @param node {boolean} If set to true, the content should be a Node, if false it should be a string containing HTML
     * @param measurement {String} Unit of measurement to be used for the height and width
     * @param className {String} Class name for the modal
     * @return {Promise<void>}
     */
    async printContentModal(height, width, content, node = false, measurement = 'px', className = '') {
        let modal = await this.openModal(height, width, measurement);
        if(className !== '') {
            modal.classList.add(className);
        }
        this.appendCloseModalButton(modal);
        if(node) {
            modal.appendChild(content);
        } else {
            modal.innerHTML = content;
        }
        document.dispatchEvent(new CustomEvent('modalContent'));
    }

    openModal(height, width, measurement) {
        let modal = document.createElement('div');
        modal.id = 'tabModal';
        modal.style.width = `${width}${measurement}`;
        modal.style.height = `${height}${measurement}`;
        this.addOverlayToBody();
        document.body.appendChild(modal);
        return modal;
    }

    addOverlayToBody() {
        let overlay = document.createElement('div');
        overlay.id = 'tabOverlay';
        document.body.classList.add('preventScroll');
        document.body.appendChild(overlay);
    }

    removeOverlayFromBody() {
        let overlay = document.getElementById('tabOverlay');
        if (overlay) {
            overlay.remove();
        }
        document.body.classList.remove('preventScroll');
    }

    appendCloseModalButton(modal) {
        let closeModal = document.createElement('span');
        closeModal.id = 'closeModal';
        closeModal.title = 'Close';
        closeModal.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>';
        modal.appendChild(closeModal);
        closeModal.addEventListener('click', () => {
            this.removeModal();
        });
    }

    addFormSubmitListener(modal) {
        let form = modal.querySelector('form')
        form.addEventListener('submit', (e) => {
            e.preventDefault();
            let formData = new FormData(e.target);
            formData.append('method', e.target.getAttribute('data-method'));
            if (e.target.getAttribute('data-id') !== null) {
                formData.append('id', e.target.getAttribute('data-id'));
            }
            let params = [...formData.entries()];
            this.fetchResponse(params, document.getElementById('crudModal')).then((response) => {
                this.removeModal();
                this.activeTab.click();
                this.printNotice(response);
            })
        })
    }

    removeModal() {
        let modal = document.getElementById('tabModal');
        if (modal) {
            modal.remove();
        }
        this.removeOverlayFromBody();
    }

    reloadTab() {
        this.activeTab.click();
    }

    elementPrintCheckmark(element) {
        element.style.display = 'none';
        let parent = element.parentElement;
        if(parent.querySelector('.checkmark')) {
            return;
        }
        let checkmark = `<svg class="checkmark" id="checkmarkGreen" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmarkCircle" cx="26" cy="26" r="25" fill="none"/>
                                  <path class="checkmarkCheck" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                                </svg>`;
        parent.insertAdjacentHTML('beforeend',checkmark);
        setTimeout(() => {
        element.style.display = 'block';
        parent.querySelector('.checkmark').remove();
        },2000);

    }
}