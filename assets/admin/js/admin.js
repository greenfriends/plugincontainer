/*
 * TODO: Don't forget to bump js version when you make changes to this file
 * it is located inside the MenuPage.php file in the function setupMainPage. Thank you <3
 **/
import MainPage from "../../../src/Core/Dashboard/templates/js/MainPage.js";
import FeatureOptionsPage from "../../../src/Core/Dashboard/templates/js/FeatureOptionsPage.js";
import CrudTable from "../../../src/Core/Components/CrudTable/templates/js/CrudTable.js";
import ProductSliderPage from "../../../src/Packages/ProductSlider/assets/js/ProductSliderPage.js";
import BankReportPage from "../../../src/Packages/BankReportParser/assets/js/BankReportPage.js";
import CustomOrderStatus from "../../../src/Packages/WcCustomOrderStatus/assets/js/CustomOrderStatus.js";
import WcOrderStatusFlowPage from "../../../src/Packages/WcOrderStatusFlow/assets/js/WcOrderStatusFlowPage.js";
import FeedImportPage from "../../../src/Packages/FeedImport/assets/js/FeedImportPage.js";
import FeedPriceMargin from "../../../src/Packages/FeedPriceMargin/assets/js/FeedPriceMargin.js";
import SortableCategories from "../../../src/Packages/MegaMenu/assets/js/sortableCategories.js";
import GfTranslationManager from "../../../src/Packages/GfTranslationManager/assets/js/GfTranslationManager.js";
import RoleManager from "../../../src/Packages/RoleManager/assets/js/RoleManager.js";

document.addEventListener('DOMContentLoaded', () => {
    let container = document.getElementById('content');
    let mainPage = new MainPage({
        tabs: document.querySelectorAll('.tab'),
        container: container,
        message: localStorage.getItem('message')
    });
    mainPage.init();
    container.addEventListener('Feature list', () => {
        let featureOptionsPage = new FeatureOptionsPage({
            activateButtons: document.querySelectorAll('.activateFeature'),
            deactivateButtons: document.querySelectorAll('.deactivateFeature'),
            cleanUpButtons: document.querySelectorAll('.cleanUpFeature'),
            configButtons: document.querySelectorAll('.featureConfig'),
            syncButton: document.getElementById('syncWithConfig'),
            container: container
        })
        featureOptionsPage.init();
    })
    container.addEventListener('Vendor', () => {
        initCrudeTable()
    })
    container.addEventListener('Wc custom order statuses', () => {
        initCrudeTable()
        let customOrderStatusJs = new CustomOrderStatus();
        customOrderStatusJs.init()
    })

    container.addEventListener('Redirects', () => {
        initCrudeTable()
    })

    container.addEventListener('Wc order status flow', () => {
        let wcOrderStatusFlow = new WcOrderStatusFlowPage();
        wcOrderStatusFlow.init()
    })
    container.addEventListener('ProductSlider', () => {
        let productSliderPage = new ProductSliderPage()
        productSliderPage.init();
    });

    container.addEventListener('Bank report parser', () => {
        let bankReportPage = new BankReportPage();
        bankReportPage.init();
    });

    container.addEventListener('Feed import', () => {
        let feedImportPage = new FeedImportPage();
        feedImportPage.init();
    });

    container.addEventListener('Feed Margin Price', () => {
        let feedMarginPage = new FeedPriceMargin();
        feedMarginPage.init();
    });

    container.addEventListener('Mega Menu', () => {
        let sortableCategories = new SortableCategories();
        sortableCategories.init();
    })

    container.addEventListener('Translation Manager', () => {
        let translationManager = new GfTranslationManager();
        translationManager.init();
    })

    container.addEventListener('Role Manager',() => {
        let roleManager = new RoleManager();
        roleManager.init();
    })

    container.addEventListener('BannerSlider', () => {
       initCrudeTable();
    });

})

function initCrudeTable(modalWidth = null, modalHeight = null) {
    let crudTableElement = document.getElementById('crudTable');
    if (crudTableElement) {
        let crudTable = new CrudTable({
            crudTable: crudTableElement,
            crudTableBody: document.getElementById('tableData'),
            crudControlButtonWrapper: document.getElementById('controlButtonsWrapper'),
            formContainer: document.getElementById('crudModal'),
            modalWidth: modalWidth,
            modalHeight: modalHeight
        });
        crudTable.setupTable()
    }
}

